//
// lvppCompleteTest.h
//
// header file with all definitions needed for LVPP test
//

// current limits
// Digital 2.5V
//average	    min	        max
//1.81425	    1.71425	    1.91425
//1.702333333	1.602333333	1.802333333
//1.589916667	1.489916667	1.689916667
//1.477483333	1.377483333	1.577483333
//1.364066667	1.264066667	1.464066667
//1.252216667	1.152216667	1.352216667

// Analog Positive 3.0V
//average	    min     	max
//2.048716667	1.948716667	2.148716667
//1.987666667	1.887666667	2.087666667
//1.928166667	1.828166667	2.028166667
//1.867633333	1.767633333	1.967633333
//1.8071	    1.7071	    1.9071
//1.74655	    1.64655	    1.84655

//average	     min	        max
//-1.6815	    -1.7815     	-1.5815
//-1.740466667	-1.840466667	-1.640466667
//-1.809133333	-1.909133333	-1.709133333
//-1.880866667	-1.980866667	-1.780866667
//-1.9531	    -2.0531	        -1.8531
//-2.027866667	-2.127866667	-1.927866667



//      WB/Barrel           5 Ohm   2 Ohm
/*
#define CURRENT_PLUS_MAX	0.737 //1.847 //3.410/2.0 + 0.1
#define CURRENT_PLUS_MIN	0.637 //1.647 //3.410/2.0 - 0.1
#define CURRENT_MINUS_MAX	0.860 //2.128 //4.094/2.0 + 0.1
#define CURRENT_MINUS_MIN	0.760 //1.928 //4.094/2.0 - 0.1
#define CURRENT_DIG_MAX		0.562 //1.350 //2.606/2.0 + 0.1
#define CURRENT_DIG_MIN		0.462 //1.150 //2.606/2.0 - 0.1
*/
//      WA                  2 Ohm
#define CURRENT_PLUS_MAX	1.8
#define CURRENT_PLUS_MIN	1.6
#define CURRENT_MINUS_MAX	2.1
#define CURRENT_MINUS_MIN	1.9
#define CURRENT_DIG_MAX		1.3
#define CURRENT_DIG_MIN		1.1





// voltage limits

#define VOLTAGE_PLUS_MAX	3.410 + 0.1
#define VOLTAGE_PLUS_MIN	3.410 - 0.1
#define VOLTAGE_MINUS_MAX	-(4.094 - 0.1)
#define VOLTAGE_MINUS_MIN	-(4.094 + 0.1)
#define VOLTAGE_DIG_MAX		2.606 + 0.1
#define VOLTAGE_DIG_MIN		2.606 - 0.1

// factors for current and voltage
#define CURRENT_FACTOR	 0.002/20.0
//#define CURRENT_FACTOR	 0.002/22.0   
#define VOLTAGE_FACTOR	11.0/1000000.0    //100k/10k
#define VOLTAGE_FACTOR_CALIB 11.0
//#define VOLTAGE_FACTOR   101.0/1000000.0     //100k/1k

// voltage and current limits for COARSE checks
// wheel A CALIB 5 ohm
//#define CURRENT_COARSE_WHEELA_MIN_5_CALIB 0.6
//#define CURRENT_COARSE_WHEELA_MAX_5_CALIB 1.2

#define CURRENT_COARSE_WHEELA_MIN_5_CALIB 0.52
#define CURRENT_COARSE_WHEELA_MAX_5_CALIB 0.82

// wheel A NO CALIB 5 ohm
#define CURRENT_COARSE_WHEELA_MIN_5_NOCAL 0.6
#define CURRENT_COARSE_WHEELA_MAX_5_NOCAL  1.2

//#define CURRENT_COARSE_WHEELA_OFF 0.05
#define CURRENT_COARSE_WHEELA_OFF 0.1
//#define CURRENT_COARSE_WHEELA_MIN_5 0.67   // 103
//#define CURRENT_COARSE_WHEELA_MAX_5 0.83  // 103

#define CURRENT_COARSE_WHEELA_MIN_5 0.60   // 103
#define CURRENT_COARSE_WHEELA_MAX_5 0.83  // 103


//---------------------------------WB/Barrel------------------------------------
// voltage and current limits for wheel B 5 ohm


#define CURRENT_COARSE_WHEELB_MIN_5 0.60
#define CURRENT_COARSE_WHEELB_MAX_5 0.81

#define CURRENT_WHEELA_ANPOS_MIN_5 0.65
#define CURRENT_WHEELA_ANPOS_MAX_5 0.85
#define CURRENT_WHEELA_ANNEG_MIN_5 0.65 
#define CURRENT_WHEELA_ANNEG_MAX_5 0.85 
#define CURRENT_WHEELA_DIGI_MIN_5  0.55
#define CURRENT_WHEELA_DIGI_MAX_5  0.75

#define VOLTAGE_WHEELA_ANPOS_MIN_5 3.65
#define VOLTAGE_WHEELA_ANPOS_MAX_5 3.85
#define VOLTAGE_WHEELA_ANNEG_MIN_5 3.50 
#define VOLTAGE_WHEELA_ANNEG_MAX_5 3.70 
#define VOLTAGE_WHEELA_DIGI_MIN_5  3.15
#define VOLTAGE_WHEELA_DIGI_MAX_5  3.35

/*
#define CURRENT_WHEELA_ANPOS_MIN_5 0.7
#define CURRENT_WHEELA_ANPOS_MAX_5 0.8
#define CURRENT_WHEELA_ANNEG_MIN_5 0.7
#define CURRENT_WHEELA_ANNEG_MAX_5 0.8
#define CURRENT_WHEELA_DIGI_MIN_5  0.6
#define CURRENT_WHEELA_DIGI_MAX_5  0.7

#define VOLTAGE_WHEELA_ANPOS_MIN_5 3.7
#define VOLTAGE_WHEELA_ANPOS_MAX_5 3.8
#define VOLTAGE_WHEELA_ANNEG_MIN_5 3.5 
#define VOLTAGE_WHEELA_ANNEG_MAX_5 3.6 
#define VOLTAGE_WHEELA_DIGI_MIN_5  3.2
#define VOLTAGE_WHEELA_DIGI_MAX_5  3.3
*/

#define CURRENT_FINE_WHEELB_MIN_5_AnPos 0.68
#define CURRENT_FINE_WHEELB_MAX_5_AnPos 0.80

#define VOLTAGE_FINE_WHEELB_MIN_5_AnPos 3.0
#define VOLTAGE_FINE_WHEELB_MAX_5_AnPos 3.92

#define CURRENT_COARSE_WHEELB_OFF 0.05
#define CURRENT_COARSE	0.5

#define VOLTAGE_COARSE_ON	2.0
#define VOLTAGE_COARSE_OFF	0.5

#define TEMPERATURE_COARSE_MIN 15.10
#define TEMPERATURE_COARSE_MAX 60.0
//  #define TEMPERATURE_LIMIT 50.1
#define TEMPERATURE_LIMIT 70.1

// limits taken from lvppCompleteTest case 90 and case 92
// WHEEL A  (CASE 90, CASE 92)
			#define AnalPosTotMax  4.7
            #define AnalPosTotMin  4.5

			#define AnalNegTotMax  4.4
			#define AnalNegTotMin  4.2

			#define DigTotMax  3.9
			#define DigTotMin  3.7

// WHEEL B (CASE 91)
			#define AnalPosTotMax_B  5.8 
            #define AnalPosTotMin_B  5.4
			#define AnalNegTotMax_B  6.6
			#define AnalNegTotMin_B  6.3
			#define DigTotMax_B  4.4
			#define DigTotMin_B  4.0
