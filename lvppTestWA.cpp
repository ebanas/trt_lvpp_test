/*
  103
*/

#include "lvppSharedLib.h"
#include "lvppTest.h"
#include "lvppTestLib.h"

// global wide declarations
int portCGlobal; // keeps content of port C - global wide 

void printUsage(char *programName)
{

  printf ("Program %s\n", programName);
  printf ("Usage: %s -c 'CAN port' -e ELMB ID -b lvpp card number -l number of loops -v \n");
  
}


main (int argc, char **argv)
{
int handle;
int cardNb;
int item;
int status;
int channel;
lvppSharedLib zasilacz;
int elmbTimeOut = 10000;
int elmbNb;
int delay=1;  // period between 2 loops, by default 1 second
int portCInternal;  // to be supressed in next versions of the library
int dtmroc;
int value;
int dac;
int dacRead;

// loop control
int loopNb;
int currentLoop;
bool continueLoop = true; 
bool continueTest;

char textBuf[64];

FILE *logFile;
FILE *calibFile;

// time
time_t actualTime;
struct tm *brokenTime;
unsigned int seconds;

// for calibration
double calibCoeff[36], calibCurrentOffset[36];

int opt;
bool verbose = false;


  printf ("============================================\n");
  printf ("Check basic functions of a card TYPE A\n");
  printf ("============================================\n");
  channel = 4; elmbNb = 3; cardNb = 202; loopNb = 5;

 while ((opt = getopt(argc, argv, "hvc:e:b:l:d:")) != -1) { 
   switch (opt) {
     case 'c':
       channel = atoi(optarg);
       if ((channel < 0 ) ||  (channel > 5)) {
	 printf ("Bad CAN port: %d \n", channel);
	 exit(1);
       }
       break;
     case 'e':
       elmbNb = atoi(optarg);
       if ((elmbNb < 0) && (elmbNb > 64)) {
	 printf ("Bad ELMB ID: %d \n", elmbNb);
	 exit(1);
       }
       break;
     case 'b':
       cardNb = atoi(optarg);
       if ((cardNb < 1) || (cardNb > 564)) {
	 printf ("Bad LVPP card number: %d \n", cardNb);
	 exit(1);
       }
       break;
      case 'l':
       loopNb = atoi(optarg);
       if (loopNb < 0) {
	 printf ("Loop number should be a positive value: %d \n", loopNb);
	 exit(1);
       }
       break;
      case 'd':
       delay = atoi(optarg);
       if ((delay < 1) || (delay > 100)) {
	 printf ("Loop number should be a positive value: %d \n", loopNb);
	 exit(1);
       }
       break;
    case 'v':
       verbose = true;
       break;
     default:
       printUsage(basename(argv[0]));
       exit(1);
       break;
   }
 }

  printf ("CAN device: can%d  ELMB ID %d   Card number %d   loops %d   delay %d\n", channel, elmbNb, cardNb, loopNb, delay);

						
  printf ("Continue test? (y/n)  ");
  scanf ("%c", &continueTest);
  if (continueTest != 'y') {
     printf ("Test is stopped on operator request\n");
     exit (1);
  }


  printf ("Starting test: channel %d, elmbNb %d, cardNb %d  - %d petli \n\n", channel, elmbNb, cardNb, loopNb);

//------------------------------------------------------------------------------------------------


// create directory

  sprintf (textBuf, "lvppTestData/card_%03d", cardNb);

  if ((mkdir (textBuf, S_IXGRP|S_IWGRP|S_IRGRP|S_IXUSR|S_IWUSR|S_IRUSR))) { 
    if (errno != 17) { 
      printf ("directory not created : %s\n", strerror (errno )); 
    }
    else
      printf ("Directory %s created.\n", textBuf); 
  } 
  chmod (textBuf, S_IXGRP|S_IWGRP|S_IRGRP|S_IXUSR|S_IWUSR|S_IRUSR);

//  chmod(textBuf, 
// open log file
  //_ftime( &timebuffer ); newtime  = localtime( &(timebuffer.time) );
  actualTime = time(NULL);
  brokenTime = localtime(&actualTime); 
  sprintf (textBuf, "lvppTestData/card_%03d/LVPP_COARSE_%02d%02dH%02d%02d.txt",
	   cardNb, brokenTime->tm_mday, brokenTime->tm_mon+1,
	   brokenTime->tm_hour, brokenTime->tm_min);

  if (!(logFile =fopen(textBuf,"w"))) {
    printf ("File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
    return 0;
  }

  fprintf (logFile, "============================================\n");
  fprintf (logFile, "Check basic functions of a card TYPE A\n");
  fprintf (logFile, "============================================\n");
  fprintf (logFile, "Card Serial Number: %d\n", cardNb);
/*
			printf (" with calibration: 0   without calibration: 1  -> "); 
			scanf ("%d", &value);
*/
  value=0;
  if (!value) {
    sprintf (textBuf, "/winccoa/Development/ATLAS_DCS_TRT/source/trtPP2/lvppCalib/LVPP_CALIB_%03d.csv", cardNb, cardNb);
	
    if ((calibFile = fopen (textBuf, "r")) == NULL) {
      printf ("%s\n", textBuf);
      printf ("Calibration file does not exist for card %d - please make calibration\n", cardNb); 
      return 0;
    }

    readCalibration(calibFile, calibCoeff, calibCurrentOffset, 36);
			
    fclose (calibFile);
    printf ("CALIBRATION FILE: %s\n", textBuf);
    fprintf (logFile, "CALIBRATION FILE: %s\n", textBuf);
    printf ("Load: 5 ohms, valid current range sum: <%2.3f A - %2.3f A> \n",
	    CURRENT_COARSE_WHEELA_MIN_5_CALIB, CURRENT_COARSE_WHEELA_MAX_5_CALIB);
    fprintf (logFile, "Load: 5 ohms, valid current range: <%2.3f A - %2.3f A> \n",					
	     CURRENT_COARSE_WHEELA_MIN_5_CALIB, CURRENT_COARSE_WHEELA_MAX_5_CALIB);
  }
  else {
    for (item=0; item<36; item++) calibCoeff[item] = 0;
    printf ("NO CALIBRATION \n");
    fprintf (logFile, "NO CALIBRATION \n");
    printf ("Load: 5 ohms, valid current range summed: <%2.3f A - %2.3f A> \n",
	    CURRENT_COARSE_WHEELA_MAX_5_NOCAL, CURRENT_COARSE_WHEELA_MIN_5_NOCAL);
    fprintf (logFile, "Load: 5 ohms, valid current range: <%2.3f A - %2.3f A> \n",
	     CURRENT_COARSE_WHEELA_MAX_5_NOCAL, CURRENT_COARSE_WHEELA_MIN_5_NOCAL);
  }

  printf ("                temperature range: <%2.2f C - %2.2f C> \n\n",
	  TEMPERATURE_COARSE_MIN, TEMPERATURE_COARSE_MAX);
  fprintf (logFile, "                temperature range: <%2.2f C - %2.2f C> \n\n",
	   TEMPERATURE_COARSE_MIN, TEMPERATURE_COARSE_MAX);


  //  _ftime( &timebuffer ); seconds = (short) timebuffer.time; newtime  = localtime( &(timebuffer.time) );
  actualTime = time(NULL);
  brokenTime = localtime(&actualTime); 
  seconds = (unsigned int) actualTime;
  srand (seconds );
  fprintf (logFile, "TEST is started: %02d.%02d.%4d %02d:%02d:%02d\n\n",
	   brokenTime->tm_mday, brokenTime->tm_mon+1, brokenTime->tm_year+1900, 
	   brokenTime->tm_hour, brokenTime->tm_min,
	   brokenTime->tm_sec); 
  printf ("TEST is started: %02d.%02d.%4d %02d:%02d:%02d\n\n",
	  brokenTime->tm_mday, brokenTime->tm_mon+1, brokenTime->tm_year+1900, 
	  brokenTime->tm_hour, brokenTime->tm_min,
	  brokenTime->tm_sec); 

  if ((status = initLVPP (channel, elmbNb, logFile, &handle)) < 0) {
    printf ("initLVPP exit with error: %d\n", status);
    fprintf (logFile, "initLVPP exit with error: %d\n", status);
    fclose (logFile);
    return status;
  }  

  checkInhibitsBroadcast (handle, elmbNb, portCGlobal,  3, elmbTimeOut, logFile);

  printf ("Single Write/Read DACs 01 23 in all DTMROCS\n\n");
  fprintf (logFile, "Single Write/Read DACs 01 23 in all DTMROCS\n\n");

  for (dtmroc = 0; dtmroc <12; dtmroc++) {
    for (item = 0; item < 3; item ++) {
      dac = ((rand() & 0xFF) << 8) | (rand() & 0xFF);
      checkWrite_SPI (handle, elmbNb, dtmroc, 2, dac, elmbTimeOut, logFile); 
    }
    for (item = 0; item < 3; item ++) {
      dac = ((rand() & 0xFF) << 8) | (rand() & 0xFF);
      checkWrite_SPI (handle, elmbNb, dtmroc, 3, dac, elmbTimeOut, logFile); 
    }
  }

  printf ("Write to all DACs - broadcast \n");
  fprintf (logFile, "Write to all DACs  - broadcast\n");

  for (item = 0; item < 3; item ++) {
    dac = ((rand() & 0xFF) << 8) | (rand() & 0xFF);

    checkWriteBroadcast (handle, elmbNb, 2, dac, elmbTimeOut, logFile);

    dac = ((rand() & 0xFF) << 8) | (rand() & 0xFF);

    checkWriteBroadcast (handle, elmbNb, 3, dac, elmbTimeOut, logFile);
				
  }
			
  printf ("\nCheck set INHIBITS\n");
  fprintf (logFile, "Check set INHIBITS");

  printf ("Set all regulators ON one by one\n");
  fprintf (logFile, "\nSet all regulators ON one by one\n"); 
  for (dtmroc = 0; dtmroc <12; dtmroc++) {
    checkInhibitsSingle (handle, elmbNb, dtmroc, portCGlobal, 1, 1, elmbTimeOut, logFile); 
    checkInhibitsSingle (handle, elmbNb, dtmroc, portCGlobal, 2, 1, elmbTimeOut, logFile); 
			}
// read and check all once more
  if ((status = zasilacz.GetAllInhibitsWheelA(handle, elmbNb, portCInternal, elmbTimeOut, &dacRead)) < 0) {
    printf ("GetAllInhibitsWheelA exit with error: %d\n", status);
    printf ("%s\n", zasilacz.GetErrorText ());
    fprintf (logFile, "GetAllInhibitsWheelA exit with error: %d \n", status);
    fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
  }
  if (dacRead != 0xFFFFFF) {
    printf ("ERROR: not all regulators are ON: 0x%X\n", dacRead);
    fprintf (logFile, "ERROR: not all regulators are ON: 0x%X\n", dacRead);
  }
//-----Set all regulators OFF one by one
  printf ("Set all regulators OFF one by one\n");
  fprintf (logFile, "Set all regulators OFF one by one\n"); 
  for (dtmroc = 0; dtmroc <12; dtmroc++) {
    checkInhibitsSingle (handle, elmbNb, dtmroc, portCGlobal, 1, 0, elmbTimeOut, logFile);
    checkInhibitsSingle (handle, elmbNb, dtmroc, portCGlobal, 2, 0, elmbTimeOut, logFile);
  }
// read and check all once more
  if ((status = zasilacz.GetAllInhibitsWheelA(handle, elmbNb, portCInternal, elmbTimeOut, &dacRead)) < 0) {
    printf ("GetAllInhibitsWheelA exit with error: %d\n", status);
    printf ("%s\n", zasilacz.GetErrorText ());
    fprintf (logFile, "GetAllInhibitsWheelA exit with error: %d \n", status);
    fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
  }
  if (dacRead != 0) {
    printf ("ERROR: not all regulators are OFF: 0x%X\n", dacRead);
    fprintf (logFile, "ERROR: not all regulators are OFF: 0x%X\n", dacRead);
  }
//-------Set all regulators ON - BROADCAST
  printf ("Set all regulators ON - BROADCAST\n");
  fprintf (logFile, "Set all regulators ON - BROADCAST\n"); 
  checkInhibitsBroadcast (handle, elmbNb, portCGlobal,  3, elmbTimeOut, logFile);

//-------Set all regulators ON - BROADCAST
  printf ("Set all regulators OFF - BROADCAST\n");
  fprintf (logFile, "Set all regulators OFF - BROADCAST\n"); 
  checkInhibitsBroadcast (handle, elmbNb, portCGlobal, 0, elmbTimeOut, logFile);

//-------Check ANALOG readout
  printf ("\nCheck ANALOG readout\n");
  fprintf (logFile, "\nCheck ANALOG readout\n");
			
  printf ("\nHard reset: all regulators are off, all DACs are set to 0xFF\n");
  fprintf (logFile, "\nHard reset: all regulators are off, all DACs are set to 0xFF.\n");
  if ((status = zasilacz.Hardware_Reset_DTMROC(handle, elmbNb, portCInternal, elmbTimeOut)) <0) {
    printf ("Hardware_Reset_DTMROC exit with status: %d\n", status);
    printf ("%s\n", zasilacz.GetErrorText ());
    fprintf (logFile, "Hardware_Reset_DTMROC exit with status: %d\n", status);
    fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
  }
  
  checkAnalogReadWheelA (handle, elmbNb, 0, elmbTimeOut, logFile, calibCoeff);

//*********************************************************************
int DACtableout[18];
int DACtable[18];  

// Ana
 DACtable[0]  = 0x8080;  //DAC1 DAC0     CH44 CH36         1
 DACtable[1]  = 0x8080;  //DAC3 DAC2
 DACtable[2]  = 0x8080;  //DAC1 DAC0     CH45 CH37         2
 DACtable[3]  = 0x8080;  //DAC3 DAC2
 DACtable[4]  = 0x8080;  //DAC1 DAC0     CH46 CH38         3
 DACtable[5]  = 0x8080;  //DAC3 DAC2
 DACtable[6]  = 0x8080;  //DAC1 DAC0     CH47 CH39         4
 DACtable[7]  = 0x8080;  //DAC3 DAC2

 DACtable[8]  = 0x8080;  //DAC1 DAC0     CH48 CH40         5
 DACtable[9]  = 0x8080;  //DAC3 DAC2     CH49 CH41
 DACtable[10] = 0x8080; //DAC1 DAC0     CH50 CH42         6 
 DACtable[11] = 0x8080; //DAC3 DAC2     CH51 CH43
//  Digi
 DACtable[12] = 0x8080; //DAC1 DAC0          CH52         9
 DACtable[13] = 0x8080; //DAC1 DAC0          CH53        10
 DACtable[14] = 0x8080; //DAC1 DAC0          CH54        11
 DACtable[15] = 0x8080; //DAC1 DAC0          CH55        12
 DACtable[16] = 0x8080; //DAC1 DAC0    CH57  CH56        13
 DACtable[17] = 0x8080; //DAC1 DAC0    CH59  CH58        14

//*************************************************************************************

 if ((status = zasilacz.WriteAllDacsWheelA (handle, elmbNb, elmbTimeOut, portCInternal, DACtable)) < 0) {
   printf ("\nWriteAllDacs exit with error: %d\n", status);
   printf ("%s\n", zasilacz.GetErrorText ());
 }
        
//**************************************************************************************
//*************************************Regulator 1 ON ***********************
//------- Write 0x80 (128) to all DACs
/*
			checkWriteBroadcast (handle, elmbNb, 2, 0x8080, portC, elmbTimeOut, logFile);
			checkWriteBroadcast (handle, elmbNb, 3, 0x8080, portC, elmbTimeOut, logFile);
*/

//*************************************Regulator 1 ON ***********************
 printf ("\nSet regulator number 1 ON\n");
 fprintf (logFile, "\nSet regulator number 1 ON\n");
 checkInhibitsBroadcast (handle, elmbNb, portCGlobal,  1, elmbTimeOut, logFile);

 checkAnalogReadWheelA (handle, elmbNb, 1, elmbTimeOut, logFile, calibCoeff);			
//********************************* Regulator 2 ON **************************
 printf ("\nSet regulator number 2 ON\n");
 fprintf (logFile, "\nSet regulator number 2 ON\n");
 checkInhibitsBroadcast (handle, elmbNb, portCGlobal,  2, elmbTimeOut, logFile);

 checkAnalogReadWheelA (handle, elmbNb, 2, elmbTimeOut, logFile, calibCoeff);			
//************************************ Both ON **************************
 printf ("\nSet both regulators ON\n");
 fprintf (logFile, "\nSet both regulators ON\n");
 checkInhibitsBroadcast (handle, elmbNb, portCGlobal,  3, elmbTimeOut, logFile);


 
//************************************portC**********************************
/*
            printf ("PORT C: 0x%02X\n", portC);
			printf ("4.Set port C\n");
			printf ("PORT C - ");
			scanf ("%x", &pc);
			portC = pc;

			if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Write_C exit OK - status: %d\n", status);
*/
 portCGlobal=0xF0;
 if ((status = zasilacz.Write_C(handle, elmbNb,portCGlobal)) < 0) {
   printf ("Write_C exit with error: %d\n", status);
   printf ("%s\n", zasilacz.GetErrorText ());
 }
 else 
   printf ("Write_C exit OK - status: %d\n", status);
//**********************************end portC********************************

 printf ("Starting test loop: all regulators are on\n");
//     checkInhibitsBroadcast (handle, elmbNb, portC,  3, elmbTimeOut, logFile);

//***********************loop***********************************************
 currentLoop = 1;
 while (continueLoop && (currentLoop < loopNb+1 )) {

   printf ("\n Loop: %d\n", currentLoop); 

   printf ("PORT C: 0x%02X\n", portCGlobal);
   printf ("Element DACtable %d 0x%X\n", 1, DACtable[1]);

   printf ("\n Write DACtable \n");
//-----------------------------------------------------------------------------
   printf ("\n BEEP start - %d seconds wait\n", delay);
   printf ("Starting with tune is time to shortened load resistors by means of a cable\n");
   printf ("Shortened channels should appear as 'Errors' and testprogram is interrupted\n");
 
   usleep (1000*1000*delay);

   printf ("\n BEEP end\n");

//--------------------------------------------------------------------
//*********************************************************************
/*
                  //WB/Barrel    ANeg APos    DTMROC
                  //WB/Barrel    ANeg APos    DTMROC
			DACtable[0] = 0x90A0;  //DAC1 DAC0     CH44 CH36         1
			DACtable[1] = 0x90A0;  //DAC3 DAC2
			DACtable[2] = 0x90A5;  //DAC1 DAC0     CH45 CH37         2
			DACtable[3] = 0x80A0;  //DAC3 DAC2
			DACtable[4] = 0x90A0;  //DAC1 DAC0     CH46 CH38         3
			DACtable[5] = 0x90A5;  //DAC3 DAC2
			DACtable[6] = 0x90A5;  //DAC1 DAC0     CH47 CH39         4
			DACtable[7] = 0x80A0;  //DAC3 DAC2

			DACtable[8] = 0x90A5;  //DAC1 DAC0     CH48 CH40         5
			DACtable[9] = 0x8090;  //DAC3 DAC2     CH49 CH41
		    DACtable[10] = 0x80A5; //DAC1 DAC0     CH50 CH42         6 
		    DACtable[11] = 0x60A0; //DAC3 DAC2     CH51 CH43
//                                                Dig
			DACtable[12] = 0x0a01; //DAC1 DAC0          CH52         9
			DACtable[13] = 0x010a; //DAC1 DAC0          CH53        10
			DACtable[14] = 0x0101; //DAC1 DAC0          CH54        11
			DACtable[15] = 0x0101; //DAC1 DAC0          CH55        12
			DACtable[16] = 0x0101; //DAC1 DAC0    CH57  CH56        13
			DACtable[17] = 0x0105; //DAC1 DAC0    CH59  CH58        14
*/

//*************************************************************************************
//*********************************************************************
                  //WB/Barrel    ANeg APos    DTMROC
                  //WB/Barrel    ANeg APos    DTMROC
/*			DACtable[0]  = 0x8080;  //DAC1 DAC0     CH44 CH36         1
			DACtable[1]  = 0x8080;  //DAC3 DAC2
			DACtable[2]  = 0x8080;  //DAC1 DAC0     CH45 CH37         2
			DACtable[3]  = 0x8080;  //DAC3 DAC2
			DACtable[4]  = 0x8080;  //DAC1 DAC0     CH46 CH38         3
			DACtable[5]  = 0x8080;  //DAC3 DAC2
			DACtable[6]  = 0x8080;  //DAC1 DAC0     CH47 CH39         4
			DACtable[7]  = 0x8080;  //DAC3 DAC2

			DACtable[8]  = 0x8080;  //DAC1 DAC0     CH48 CH40         5
			DACtable[9]  = 0x8080;  //DAC3 DAC2     CH49 CH41
		    DACtable[10] = 0x8080; //DAC1 DAC0     CH50 CH42         6 
		    DACtable[11] = 0x8080; //DAC3 DAC2     CH51 CH43
//                                                Dig
			DACtable[12] = 0x8080; //DAC1 DAC0          CH52         9
			DACtable[13] = 0x8080; //DAC1 DAC0          CH53        10
			DACtable[14] = 0x8080; //DAC1 DAC0          CH54        11
			DACtable[15] = 0x8080; //DAC1 DAC0          CH55        12
			DACtable[16] = 0x8080; //DAC1 DAC0    CH57  CH56        13
			DACtable[17] = 0x8080; //DAC1 DAC0    CH59  CH58        14
*/
//*************************************************************************************

/*			if ((status = zasilacz.WriteAllDacsWheelA (handle, elmbNb, portC, elmbTimeOut, DACtable)) < 0) {
				printf ("\nWriteAllDacs exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				iPetlaA = 0;
			} */
//**************************************************************************************
   printf ("Start current division\n");
   currentDivision (handle, elmbNb, elmbTimeOut, calibCoeff, cardNb);

   printf ("\nDACs are set OK   and   ");
   usleep (1000*10);
   if ((status = zasilacz.ReadAllDacsWheelA (handle, elmbNb, elmbTimeOut, portCInternal, DACtableout)) < 0) {
     printf ("ReadAllDacsWheelA exit with error: %d\n", status);
     printf ("%s\n", zasilacz.GetErrorText ());
     continueLoop = false;
   }
   printf ("DAC read back OK\n");
   usleep (1000*5); 
   printf ("\n Read out voltages, currents and temperatures\n");
		// EB	    printf ("6.checkAnalogReadWheelA\n");
         //EB       checkInhibitsBroadcast (handle, elmbNb, portC,  1, elmbTimeOut, logFile);
         //EB       if (checkAnalogReadWheelA (handle, elmbNb, 1, portC, elmbTimeOut, logFile, calibCoeff) == false) iPetlaA = 0;
        //  EB      checkInhibitsBroadcast (handle, elmbNb, portC,  2, elmbTimeOut, logFile);
         //  EB     if (checkAnalogReadWheelA (handle, elmbNb, 2, portC, elmbTimeOut, logFile, calibCoeff) == false) iPetlaA = 0;
        // EB        checkInhibitsBroadcast (handle, elmbNb, portC,  3, elmbTimeOut, logFile);
   if (checkAnalogReadWheelA (handle, elmbNb, 3, elmbTimeOut, logFile, calibCoeff) == false) continueLoop = false;
   currentLoop++;
 }  // glowna petla while
/*
			if ((status = zasilacz.Hardware_Reset_DTMROC(handle, elmbNb, portC)) <0) {
				printf ("Hardware_Reset_DTMROC exit with status: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "Hardware_Reset_DTMROC exit with status: %d\n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			}

*/
 portCGlobal=0x00;
 if ((status = zasilacz.Write_C(handle, elmbNb,portCGlobal)) < 0) {
   printf ("Write_C exit with error: %d\n", status);
   printf ("%s\n", zasilacz.GetErrorText ());
 }
 else 
   printf ("Write_C exit OK - status: %d\n", status);

 printf ("PORT C: 0x%02X\n", portCGlobal);

  if ((status = zasilacz.ReadAllDacsWheelA (handle, elmbNb, elmbTimeOut, portCInternal, DACtableout)) < 0) {
     printf ("ReadAllDacsWheelA exit with error: %d\n", status);
     printf ("%s\n", zasilacz.GetErrorText ());
   }
  for(int i=0;i<12;++i)
    {
      if(i%2==0){
	if(i==0)  printf("Dac from DTMROC %d: DAC01: 0%X, DAC23: 0%X \n",i+1,DACtableout[i],DACtableout[i+1]);
        if(i>0)  printf("Dac from DTMROC %d: DAC01: 0%X, DAC23: 0%X \n",(i/2)+1,DACtableout[i],DACtableout[i+1]);
      }
    }
  int tabdtmrocdignumber[6]={9,10,11,12,13,14};
 for(int j=12;j<18;++j)
    {
      
      printf("Dac from DIGITAL DTMROC %d: DAC01: 0%X \n",tabdtmrocdignumber[j-12],DACtableout[j]);
           
    }


  if ((status = zasilacz.Hardware_Reset_DTMROC(handle, elmbNb, portCInternal, elmbTimeOut)) <0) {
    printf ("Hardware_Reset_DTMROC exit with status: %d\n", status);
    printf ("%s\n", zasilacz.GetErrorText ());
    fprintf (logFile, "Hardware_Reset_DTMROC exit with status: %d\n", status);
    fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
  }
  printf ("Hardware Reset sent - all voltages are switched off\n");
  fprintf (logFile, "Hardware Reset sent - all voltages are switched off\n");

 if ((status = zasilacz.CloseCan (handle)) < 0) {
   printf ("CloseCan exit with error: %d\n", status);
   printf ("%s\n", zasilacz.GetErrorText ());
   fprintf (logFile, "CloseCan exit with error: %d\n", status);
   fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
 }
	
 // _ftime( &timebuffer ); newtime  = localtime( &(timebuffer.time) );
  actualTime = time(NULL);
  brokenTime = localtime(&actualTime); 
 
  if (continueLoop) {
    fprintf (logFile, "\n NO ERRORS in %d loops (with %d seconds interval between loops)\n TEST finished at %02d.%02d.%4d %02d:%02d:%02d\n",
	  currentLoop-1, delay,
	  brokenTime->tm_mday, brokenTime->tm_mon+1, brokenTime->tm_year+1900, 
	  brokenTime->tm_hour, brokenTime->tm_min,
	  brokenTime->tm_sec); 
    printf ("\n NO ERRORS in %d loops (with %d seconds interval between loops)\n TEST finished at %02d.%02d.%4d %02d:%02d:%02d\n", 
	 currentLoop-1, delay,
	 brokenTime->tm_mday, brokenTime->tm_mon+1, brokenTime->tm_year+1900, 
	 brokenTime->tm_hour, brokenTime->tm_min,
	 brokenTime->tm_sec); 
  } 
  else {
    fprintf (logFile, "\n TEST interrupted at %02d.%02d.%4d %02d:%02d:%02d because of errros\n after %d loops with %d seconds interval between loops \n", 
	  brokenTime->tm_mday, brokenTime->tm_mon+1, brokenTime->tm_year+1900, 
	  brokenTime->tm_hour, brokenTime->tm_min,
	  brokenTime->tm_sec, currentLoop-1, delay); 
    printf ("\n TEST interrupted at %02d.%02d.%4d %02d:%02d:%02d because of errros\n after %d loops with %d seconds interval between loops\n", 
	 brokenTime->tm_mday, brokenTime->tm_mon+1, brokenTime->tm_year+1900, 
	 brokenTime->tm_hour, brokenTime->tm_min,
	 brokenTime->tm_sec, currentLoop-1, delay); 
  } 


 fclose (logFile);
 
 return 0;

//     END OF COARSE test Wheel A
// =======================================================================================================


}
