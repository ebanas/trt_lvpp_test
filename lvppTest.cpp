//

#include "lvppSharedLib.h"
#include "lvppTestLib.h"
#include "lvppTest.h"

// global wide declarations
int portCGlobal; // keeps content of port C - global wide 

void help ()
{

 printf ("\nSimple program for LVPP tests\n\n");
 printf (" 10 - CAN Bus INIT: open socket\n");
 printf (" 11 - Initialization of ELMB\n");
 printf (" 12 - Initialization of all DTMROC's\n\n");

 printf (" 20 - DTMROC Hard Reset\n");
 printf (" 21 - DTMROC's Software Reset\n");
 printf (" 22 - Set ELMB state \n");
 printf (" 23 - Read ELMB state \n");
 printf (" 24 - Send only clocks to DTMROC\n");
 printf (" 25 - Set ELMB PORT C\n");
 printf (" 26 - Read ELMB PORT C\n");
 printf (" 27 - Read Status of single DTMROC \n\n");

 printf (" 31  - Get Overrun Error \n");
 printf (" 32 - Get Emergency Code \n");
 printf (" 33 - Read SPI period \n");
 printf (" 34 - Set SPI period\n");
 printf (" 38 - ReadELMB Serial Number \n\n");

 printf (" 41 - Write to 1 DAC in any DTMROC\n");
 printf (" 42 - Write to all DAC's the same value using single write \n");
 printf (" 43 - Write to all DAC's the a value incremented by 1, staring from a base\n\n");
 //printf (" 44 - Broadcast writing loop \n");
 //printf (" 45 - Write a table to all DACs in a board\n\n");

 printf (" 51 - Read from any DTMROC any dacs \n");
 printf (" 52 - Read from all DTMROC's one by one \n\n");
 //printf (" 53 - Read all dacs in a board to an array\n\n");

 printf (" 61 - Set ON a single regulator\n");
 printf (" 62 - Set OFF a single regulator \n");
 printf (" 63 - Set all regulators ON one by one \n");
 printf (" 64 - Set all regulators OFF one by one \n");
 printf (" 65 - Set ON / OFF with broadcast\n");
 printf (" 66 - Read all inhibits one by one - once\n");
 printf (" 67 - Read all inhibits one by one in a loop\n");
 printf (" 68 - Read all inhibits with one function\n\n");

 printf (" 70 - Read ADC - 64 channels in ADC counts\n");
 printf (" 71 - Read ADC - 64 channels in microvolts\n\n");

 printf (" 81 - Read voltages, currents and temperatures for WHEEL A type board \n\n");
 
 printf ("124 - Send only clocks in a loop ?\n\n");
  printf ("400 - Check OCM ?\n\n");

 printf (" 99 - Close CAN socket\n");
 printf ("  0  - quit program\n\n");

}

int main(int argc, char* argv[])
{
int handle;	// handle - important - keeps handle number - global wide
int cardNb;	// serial card number -IMPORTANT
int portCInternal;  // to be suppressed later, because this parameter is not needed any more
	
int adcValues[64];	// array for ADC values (counts or microVolts)
bool adcConvStat[64];	//  array for ADC convertion status (OK (0) or error (1))
double CurrentFactor = CURRENT_FACTOR;		// 
double VoltageFactor = VOLTAGE_FACTOR;  // 


// needed for time read
// struct _timeb timebuffer; 
struct tm *newtime; 
char timeline [100];
char timestart[100];
char timestop [100];

int valueRead;			// integer variable for data form LVPP
int dac;
int dacValue;//bk
char dtmrocs[64];	//?
int channel;

int dacRead;
int dtmrocStat;
int configReg;
int end17;
int clockNb;
int pc;
int i;

double current1;
double current2;
double CurrMin;
double CurrMax;

double VoltMin;
double VoltMax;

int end86;
FILE *burnFile;
FILE *calibFile;
FILE *fp80;

bool end;
bool bYes;
int status;
unsigned long queue;
canStatus stat;
unsigned long canStatFlags;
char errorMsg[100];

int item;

unsigned char buffer[8];
unsigned int dlen, flags;
unsigned long time;
unsigned char elmbSerialNumber[10];
unsigned long uData;

long sdoID = 0x5Bf;

int dtmroc;
int regulator;
int dacNb;
int value;

int elmbTimeOut = 10000;
int elmbNb;

unsigned char emErr[8];
char textBuf[64];

int ocmValues[3];
int licznik;

// for GPIB
char gpibBuffer[128];
double keithleyBuffer[24];
double keithleyMeasur;

// for calibration
double calibCoeff[36], calibCurrentOffset[36];

lvppSharedLib zasilacz;

// test program for lvppSharedLib class

//Menu (5 March 2008)
/*
int Value_60;
float fValue;

printf ("ADC uvolts:");
scanf (" %d", &Value_60);

  fValue=-0.1357*log(100000*Value_60/(1000000*(2.5-Value_60/1000000)))*log(100000*Value_60/(1000000*(2.5-Value_60/1000000)))
         *log(100000*Value_60/(1000000*(2.5-Value_60/1000000)))+
         5.4856*log(100000*Value_60/(1000000*(2.5-Value_60/1000000)))
         *log(100000*Value_60/(1000000*(2.5-Value_60/1000000)))
         -89.55*log(100000*Value_60/(1000000*(2.5-Value_60/1000000)))+490.21+0;

printf ("Wynik: %f\n", fValue);

return 1;
*/
end =  true;
//-----------------------------------------------------------------------------
int CanElmb;
printf (" Default CANbus=0  and ELMB ID=1  = 0  \n"); 
printf (" Set by User                      = 1  \n");
printf (" 0 or 1:");
scanf (" %d", &CanElmb);

switch (CanElmb)  
					{
						case 1:
							if (CanElmb == 1) 
//								printf ("Inhibit bad set: dtmroc %d R %d W %d\n", dtmroc, dacRead, onOff);
                                printf ("Set CAN Port =  "); scanf ("%d", &channel);
                               printf ("Set ELMB Number =  "); scanf ("%d", &elmbNb); 
							break;
						case 0:
							if (CanElmb  == 0) 
								channel=0;
							    elmbNb=1;
                                printf ("CAN Port = 1; ELMB = 1");



							break;
						
						default:
							printf ("Bad:0 or 1 %d\n", CanElmb);
					}








//--------------------------------------------------------------------------------

//printf ("CAN channel (2 - Cracow, 0 - CERN - "); scanf ("%d", &channel);
//printf ("ELMB Nb - "); scanf ("%d", &elmbNb);
 portCGlobal = 0;

 help ();

 while (end) {
//	printf ("\n > Option - "); scanf ("%d", &item);
   printf ("\n > Chose Command (-1 - menu, 0 - quit):  ");

   scanf ("%d", &item);

   switch (item) {
     case 0:
       end = false;
       break;
     case -1:
       help ();
       break;
     
	case 10:
			printf ("CAN Bus init - open socket\n");
			if ((status = zasilacz.Init_CANBus(channel, &handle)) < 0) {
				printf ("Init_CANBus exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Initializing of CANBUS OK\n");
			break;

	case 9:      
			printf ("Send one can packet - write port C\n");
			if ((status = zasilacz.Write_C(handle, elmbNb, portCGlobal)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
			  printf ("PortC set OK to: %X\n", portCGlobal);
			break;
	case 11:
			printf ("ELMB Init\n");
			if ((status = zasilacz.Init_ELMB(handle, elmbNb)) < 0) {
				printf ("Init_ELMB exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Initializing of ELMB: Init_ELMB OK\n");
			break;

		case 12:
			printf ("DTMROC Init\n");
			if ((status = zasilacz.Init_DTMROC(handle, elmbNb, portCInternal, elmbTimeOut, dtmrocs)) < 0) {
				printf ("Init_DTMROC exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				printf ("Initializing of DTMROCS OK\n");
				printf ("DTMROC-i: %s\n", dtmrocs);
			}
			break;
		case 20:
			printf ("Hard Reset DTMROC\n");
			status = zasilacz.Hardware_Reset_DTMROC(handle, elmbNb, portCInternal, elmbTimeOut);
			if (status < 0) {
				printf ("Hardware_Reset_DTMROC exit with status: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Hardware_Reset_DTMROC OK\n");

			
			break;

		case 21:
			printf ("Software Reset DTMROC\n");
			status = zasilacz.Software_Reset_DTMROC(handle, elmbNb, portCInternal, 10000);
				if (status < 0) {
					printf ("Software_Reset_DTMROC exit with status: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else 
					printf ("Software_Reset_DTMROC OK\n");

			break;
		case 22:
			printf ("Send a command to ELMB\n");
			printf ("ELMB command (1-Start, 2-Stop, 128-Preop, 129-ResetNode, 130-ResetComm) - ");
			scanf ("%d", &value);

			if ((status = zasilacz.Set_ELMB_State (handle, elmbNb, value)) < 0) {
				printf ("Set_ELMB_State exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("ELMB set to: %d\n", value);
			break;

		case 23:
			printf ("Read ELMB state\n");
			if ((status = zasilacz.Read_ELMB_State (handle, elmbNb, &value)) < 0) {
				printf ("Read_ELMB_State exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("ELMB state is: %d\n", value);
			break;

		case 24:
			printf ("Send only clocks\n");
			printf ("Clocks - ");
			scanf ("%d", &clockNb);
			for (i=0; i<clockNb/32; i++) {
				if ((status = zasilacz.SPI_write (handle, elmbNb, 0, 32, elmbTimeOut)) < 0) {
					printf ("BC exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					break;
				}
			}
			if (clockNb%32) 
				if ((status = zasilacz.SPI_write (handle, elmbNb, 0, clockNb%32, elmbTimeOut)) < 0) {
					printf ("BC exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}

			break;

		case 25:
			printf ("Set port C\n");
			printf ("PORT C - 0x");
			scanf ("%x", &portCGlobal);
			if ((status = zasilacz.Write_C(handle, elmbNb, portCGlobal)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Write_C exit OK - status: %d\n", status);
			break;

		case 27:
			printf (" Read DTMROC Status \n");
		   	printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
			if (((dtmroc > 6) && (dtmroc < 9)) || (dtmroc > 14) || (dtmroc < 1)) {
				printf ("Bad dtmroc: %d. Try again. \n", dtmroc);
				break;
			}
                        int nloop;
                        printf("Loop: ");
                        scanf("%d", &nloop);
			for(int i1=0;i1<nloop;++i1)
                         {
		        	if ((status = zasilacz.GetDtmrocState(handle, elmbNb, dtmroc, portCInternal, elmbTimeOut, &dtmrocStat)) < 0) { // status
			        	printf ("Read exit with error: %d\n", status);
			        	printf ("%s\n", zasilacz.GetErrorText ());
		        	}
			}
			printf ("DTMROC %d Status: 0x%X\n",dtmroc, dtmrocStat);
			break;
		case 29:
			printf (" Read status of all DTMROCs \n");
			for (dtmroc = 0; dtmroc < 12; dtmroc++) {
			  if ((status = zasilacz.GetDtmrocState (handle, elmbNb, zasilacz.dtmrocAddress(dtmroc), portCInternal, elmbTimeOut, &dtmrocStat)) < 0) { // status
					printf ("Read exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					end17 = 0;
				}
				if (zasilacz.dtmrocAddress(dtmroc) != (dtmrocStat & 0xF))
					printf ("DTMROC %d BAD Status: %d\n",dtmroc, dtmrocStat & 0xF);
				else
					printf ("DTMROC %d OK Status: %d\n",dtmroc, dtmrocStat & 0xF);
			}
			break;

		case 30:
			printf (" Read DTMROC Config Register \n");
		        printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
			if (((dtmroc > 6) && (dtmroc < 9)) || (dtmroc > 14) || (dtmroc < 1)) {
				printf ("Bad dtmroc: %d. Try again. \n", dtmroc);
				break;
			}
			if ((status = zasilacz.Read (handle, elmbNb, dtmroc, ConfigRegister, portCInternal, elmbTimeOut, &configReg)) < 0) { // status
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				end17 = 0;
			}
			printf ("DTMROC %d Config Reg: 0x%X\n",dtmroc, configReg);
			break;
	case 31:   
			printf ("Get Overrun Error \n");
			bool overRun;
			if ((status = zasilacz.Get_ELMB_CANBufferOverrun (handle, elmbNb, &overRun)) < 0) {
			  printf ("Get_ELMB_CANBufferOverrun exit with error %d\n", status);
			}
			else {
			  if (overRun) printf ("OVERRUN \n");
			  else printf ("No Overrun \n"); 
			}
			break;

	case 32:  //nie dziala
			printf (" 32 - Get Emergency Code \n");
			if ((status = zasilacz.Get_ELMB_EmergencyError (handle, elmbNb, &emErr[0])) < 0) {
			  printf ("Get_ELMB_EmergencyError exit with error %d\n", status);
			}
			else {
			  printf ("Emergency Error:\n");
			  for (i=0; i<8; i++) printf (" 0x%X ", emErr[i]);
			  printf ("\n");
			}
		
			break;
			
	case 33:
	  printf ("Read SPI period\n");
  
	  if ((status = zasilacz.readSPI_period (handle, elmbNb, elmbTimeOut, &uData)) < 0) {
				printf ("readSPI_period exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			printf ("SPI period: %d mikroseconds\n", uData);
			break;

	case 34:
	  printf ("Set SPI period\n");
  
	  if ((status = zasilacz.readSPI_period (handle, elmbNb, elmbTimeOut, &uData)) < 0) {
				printf ("readSPI_period exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
          printf ("SPI period is %d microseconds - please give a new value: ",uData);
         scanf ("%d", &uData);
	  if ((status = zasilacz.setSPI_period (handle, elmbNb, uData, elmbTimeOut)) < 0) {
				printf ("readSPI_period exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}

 
	  if ((status = zasilacz.readSPI_period (handle, elmbNb, elmbTimeOut, &uData)) < 0) {
				printf ("readSPI_period exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			printf ("SPI period set to %d mikroseconds\n", uData);
			break;

                   
		case 38:

			if ((status = zasilacz.Get_ELMB_SerialNumber (handle, elmbNb, elmbSerialNumber)) < 0) {
				printf ("Get_ELMB_SerialNumber exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			printf ("Serial number: %s\n", elmbSerialNumber);
			break;
		case 41:
			printf ("Write to any DAC in any dtmroc (SPI)\n");
			printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
			if (((dtmroc > 6) && (dtmroc < 9)) || (dtmroc > 14) || (dtmroc < 1)) {
				printf ("Bad dtmroc: %d. Try again. \n", dtmroc);
				break;
			}
			printf ("DAC nb (0-3)  - ");
			scanf ("%d", &dacNb);
			if ((dacNb < 0) || (dacNb > 3)) {
				printf ("Bad dacs: %d. Try again. \n", dacNb);
				break;
			} 
			printf ("DAC value  (hex, <= 0xFF) - ");
			scanf ("%x", &dac); 
                        if (dac > 0xFF) { printf ("value too high: 0x%X, should be less then 0xFF, changed to: 0x%X\n", dac, 0xFF&dac); dac &= 0xFF;}
                        
			if ((status = zasilacz.WriteDac(handle, elmbNb, dtmroc, dacNb, dac, portCInternal, elmbTimeOut)) < 0) {
				printf ("Write exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			break;

		case 42:
			printf ("Write to all DAC's the same value using single write (SPI)\n");
			printf ("DAC  (hex) - 0x");
			scanf ("%x", &dac);
                        if (dac > 0xFF) { printf ("value too high: 0x%X, should be less then 0xFF, changed to: 0x%X\n", dac, 0xFF&dac); dac &= 0xFF;}
                        for (dtmroc=0; dtmroc<12; dtmroc++) {
                          for (dacNb = 0; dacNb <4; dacNb++) {
			    if ((status = zasilacz.WriteDac(handle, elmbNb, zasilacz.dtmrocAddress(dtmroc), dacNb, dac&0xFF, portCInternal, elmbTimeOut)) < 0) {
				printf ("Write exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			    }
                            //printf ("DTMROC %d DAC %d value 0x%X\n",zasilacz.dtmrocAddress(dtmroc), dacNb, dac&0xFF );
			  }
                        }                        
			break;

		case 43:
			printf ("Write to all DAC's a value incremented by 1 starting form the base, using single write (SPI)\n");
			printf ("DAC base  (hex) - 0x");
			scanf ("%x", &dac);
                        if (dac > 0xFF) { printf ("value too high: 0x%X, should be less then 0xFF, changed to: 0x%X\n", dac, 0xFF&dac); dac &= 0xFF;}
                        for (dtmroc=0; dtmroc<12; dtmroc++) {
                          for (dacNb = 0; dacNb <4; dacNb++) {
			    if ((status = zasilacz.WriteDac(handle, elmbNb, zasilacz.dtmrocAddress(dtmroc), dacNb, (dac++)&0xFF, portCInternal, elmbTimeOut)) < 0) {
				printf ("Write exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			    }
                            //printf ("DTMROC %d DAC %d value 0x%X\n",zasilacz.dtmrocAddress(dtmroc), dacNb, dac&0xFF );
			  }
                        }                        
			break;

		case 51:
			printf ("Read from any DTMROC dacs 0 1 \n");
			printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
			if (((dtmroc > 6) && (dtmroc < 9)) || (dtmroc > 14) || (dtmroc < 1)) {
				printf ("Bad dtmroc: %d. Try again. \n", dtmroc);
				break;
			}
			printf ("Dacs 2 - dacs 0,1; 3 dacs - 2,3)  - ");
			scanf ("%d", &dacNb);
			if ((dacNb != 2) && (dacNb != 3)) {
				printf ("Bad dacs: %d. Try again. \n", dacNb);
				break;
			}
			if ((status = zasilacz.Read (handle, elmbNb, dtmroc, dacNb, portCInternal, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				if (dacNb == 2) printf ("DAC 0 1: 0x%X\n", dacRead);
				else printf ("DAC 2 3: 0x%X\n", dacRead);
			}

			break;

		case 52:
			printf ("Read from all DTMROCS one by one\n");
                        
                        for (dtmroc=0; dtmroc<12; dtmroc++) {
                          printf ("DTMROC %d  ",  zasilacz.dtmrocAddress(dtmroc));
				  if ((status = zasilacz.Read (handle, elmbNb,  zasilacz.dtmrocAddress(dtmroc), 2, portCInternal, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			  }
			  else {
				printf ("\tDAC 1 0: 0x%04X", dacRead); 
			  }
			  if ((status = zasilacz.Read (handle, elmbNb,  zasilacz.dtmrocAddress(dtmroc), 3, portCInternal, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			  }
			  else {
				printf ("  DAC 3 2: 0x%04X", dacRead);
			  }
				  printf ("\n");
			}
			break;

		case 53:
			printf ("Read from all DTMROCS one by one and compare\n");
  			printf ("DAC base  (hex) - 0x");
			scanf ("%x", &i); 

	while (1) {
dac = i;
dacValue=dac-1; dac-=2;

             








           
                        for (dtmroc=0; dtmroc<12; dtmroc++) {
 //                         printf ("DTMROC %d  ",  zasilacz.dtmrocAddress(dtmroc));
				  if ((status = zasilacz.Read (handle, elmbNb,  zasilacz.dtmrocAddress(dtmroc), 2, portCInternal, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			  }
			  else {
				
				if (dacRead != (((dacValue+=2)<<8) | (dac+=2))) printf ("\tError in dtmroc %d DAC 1 0: is 0x%04X  should be: 0x%04X\n", zasilacz.dtmrocAddress(dtmroc), dacRead, (dacValue<<8) | (dac)); 
			  }
			  if ((status = zasilacz.Read (handle, elmbNb,  zasilacz.dtmrocAddress(dtmroc), 3, portCInternal, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			  }
			  else {//
				//dac++;
				//dacValue = (dac<<8)|(++dac);
				if (dacRead != (((dacValue+=2)<<8) | (dac+=2))) printf ("\tError in dtmroc %d DAC 2 3: is 0x%04X  should be: 0x%04X\n", zasilacz.dtmrocAddress(dtmroc), dacRead, (dacValue<<8) | (dac));
			  }
				//printf ("#");





			}
	//usleep(5000000);
}
			break;


		case 61:
			printf ("Set ON a single regulator\n");
			printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);

			printf ("Regulator (1 first, 2 - second, 3 - both) - ");
			scanf ("%d", &regulator);

			// set port C to "0" -> disable OCM circuitry
			if ((status = zasilacz.Write_C(handle, elmbNb, 0)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Write_C exit OK - status: %d\n", status);

			if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, portCInternal, elmbTimeOut, &dacRead)) < 0) {
				printf ("Get_INHIBIT exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else
				printf ("DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);

			if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, dtmroc, portCInternal, regulator, 1, elmbTimeOut)) < 0) {
				printf ("Set_INHIBIT exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
			  if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, portCInternal, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
				printf ("Setting INHIBIT OK: DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);
			}
                        //set port C back to previous value
			if ((status = zasilacz.Write_C(handle, elmbNb, portCGlobal)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Write_C exit OK - status: %d\n", status);

			break;

		case 62:
			printf ("Set OFF 1 regulator\n");
			printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
//			int KlasaI::Get_INHIBIT(int handle, int elmbAddress, int address, int Level, int *inhibits)
			if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, portCInternal, elmbTimeOut, &dacRead)) < 0) {
				printf ("Get_INHIBIT exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else
				printf ("DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);

			printf ("Regulator (1 first, 2 - second, 3 - both) - ");
			scanf ("%d", &regulator);

		//	int KlasaI::Set_INHIBIT(int handle, int elmbAddress, int address, int Level, int inhibitNb, int inhibitValue)
			if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, dtmroc, portCInternal, regulator, 0, elmbTimeOut)) < 0) {
				printf ("Set_INHIBIT exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
			  if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, portCInternal, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
				printf ("Setting INHIBIT OK: DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);
			}
			break;
		case 63:
			printf ("Set all regulators ON one by one\n");
			regulator = 3;

			// set portC to 0 -> disable OCM circuitry
			if ((status = zasilacz.Write_C(handle, elmbNb, 0)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Write_C exit OK - status: %d\n", status);

			for (dtmroc=1; dtmroc<=14; dtmroc++) {
				if ((dtmroc==7) || (dtmroc==8)) continue;
				if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, portCInternal, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
					printf ("DTMROC %d, inhibit state before: %d\n", dtmroc, dacRead);

				if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, dtmroc, portCInternal, regulator, 1, elmbTimeOut)) < 0) {
					printf ("Set_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else {
				  if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, portCInternal, elmbTimeOut, &dacRead)) < 0) {
						printf ("Get_INHIBIT exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					}
					else
					printf ("Setting INHIBIT OK: DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);
				}
			}
			// set port C back to previous value
			if ((status = zasilacz.Write_C(handle, elmbNb, portCGlobal)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Write_C exit OK - status: %d\n", status);
			break;


		case 64:
			printf ("Set all regulators OFF one by one\n");
			regulator = 3;
			for (dtmroc=1; dtmroc<=14; dtmroc++) {
				if ((dtmroc==7) || (dtmroc==8)) continue;
				if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, portCInternal, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
					printf ("DTMROC %d, inhibit state before: %d\n", dtmroc, dacRead);

				if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, dtmroc, portCInternal, regulator, 0, elmbTimeOut)) < 0) {
					printf ("Set_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else {
				  if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, portCInternal, elmbTimeOut, &dacRead)) < 0) {
						printf ("Get_INHIBIT exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					}
					else
					printf ("Setting INHIBIT OK: DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);
				}
			}
			break;

		case 65:
		  // set port C to 0 -> disable OCM circuitry
			if ((status = zasilacz.Write_C(handle, elmbNb, 0)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Write_C exit OK - status: %d\n", status);
			printf ("Set ON / OFF with broadcast\n");
//			printf ("ON - 1, OFF - 0: - "); scanf ("%d", &dacNb);
            printf (" 0 - OFF\n 1 - ON even\n 2 - ON odd\n 3 - ON all\n Inhibit ?"); scanf ("%d", &dacNb);
	    if ((status = zasilacz.SetInhBrdc (handle, elmbNb, portCInternal, dacNb, elmbTimeOut)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
                if (dacNb==0) {printf ("All regulators should be switched OFF.\n");}
				if (dacNb==1) {printf ("Even regulators should be switched ON.\n");}
				if (dacNb==2) {printf ("Odd regulators should be switched ON.\n");}
				if (dacNb==3) {printf ("All regulators should be switched ON.\n");}
                    
			    }
		  // set port C back to previous value
			if ((status = zasilacz.Write_C(handle, elmbNb, portCGlobal)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Write_C exit OK - status: %d\n", status);


			break;


		case 66:
			printf ("Read all inhibits one by one - once\n");
			for (dtmroc=1; dtmroc<=14; dtmroc++) {
				if ((dtmroc==7) || (dtmroc==8)) continue;
				if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, portCInternal, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
					printf ("DTMROC %d \t inhibit state: %d\n", dtmroc, dacRead);
			}
			break;
		case 70:
			printf ("Read ADC - 64 channels in ADC counts\n");

			if ((status = zasilacz.ReadADC_Counts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				for (i=0; i<64; i++) {
				
						
					printf ("%d. Conversion Status: %d \t %d\n", i, adcConvStat[i], adcValues[i]);
					}
				
				}
			break;

		case 71:
			printf ("Read ADC - 64 channels in microvolts\n");

			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				for (i=0; i<64; i++) {
					printf ("%d Conversion Status: %d \t %d\n", i, adcConvStat[i], adcValues[i]);
				}
			}
			break;
    case 81:
      printf ("Read voltages currents and temperatures for WHEEL A type\n");
      printf ("Board nb - "); scanf ("%d", &cardNb);
      // read calibration file
      sprintf (textBuf, "/det/dcs/lvtemp/lvppCalib/card_%03d/LVPP_CALIB_%03d.csv", cardNb, cardNb);
      if ((calibFile = fopen (textBuf, "r")) == NULL) {
   printf ("%s\n", textBuf);
      printf ("Proceeding without calibration\n");
			    }
			    else {

			      readCalibration(calibFile, calibCoeff, calibCurrentOffset, 36);
			
			      fclose (calibFile);
			    }

			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
			  printf ("Currents WITHOUT calibration:\n");
				for (i=0; i<36; i+=2) {
                                  if (i==0) printf ("Analog Positive \n");
                                  if (i==12) printf ("Analog Negative \n");
				  if (i==24) printf ("Digital \n");
				  if (!(adcConvStat[i]||adcConvStat[i+1])) 
				    printf ("%d:\t %f \t %f \t SUM: %f\n", i, adcValues[i]*CurrentFactor, adcValues[i+1]*CurrentFactor,
					    adcValues[i+1]*CurrentFactor+ adcValues[i+1]*CurrentFactor);
				  else 
				    printf ("%d: **** bad conversion status: %d\t%d\n", adcConvStat[i], adcConvStat[i+1]);
				}
				if (calibFile) {
			  printf ("Currents WITH calibration:\n");
				for (i=0; i<36; i+=2) {
                                  if (i==0) printf ("Analog Positive \n");
                                  if (i==12) printf ("Analog Negative \n");
				  if (i==24) printf ("Digital \n");
				  if (!(adcConvStat[i]||adcConvStat[i+1])) {
				    current1 = adcValues[i]*CurrentFactor - (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000;
				    current2 = adcValues[i+1]*CurrentFactor - (calibCoeff[i+1]*adcValues[returnVoltageChannelWheelA(i+1)]*11)/10000;
				    printf ("%d:\t %f \t %f \t SUM: %f\n", i, current1, current2, current1+current2);
				  }
				  else 
				    printf ("%d: **** bad conversion status: %d\t%d\n", adcConvStat[i], adcConvStat[i+1]);
				}
				}
			
				printf ("Voltages:\n");
				for (i=36; i<60; i++) {
                                  if (i==36) printf ("Analog Positive \n");
                                  if (i==44) printf ("Analog Negative \n");
				  if (i==52) printf ("Digital \n");
				  if (!(adcConvStat[i]||adcConvStat[i+1])) 

				  if (!(adcConvStat[i])) 
				    printf ("%d:\t %f \n", i, adcValues[i]*VoltageFactor);
				  else 
				    printf ("%d: **** bad conversion status: %d\t%d\n", adcConvStat[i]);
				}
				printf ("Temperatures:\n");
				for (i=60; i<64; i++) {
				  if (!(adcConvStat[i])) 
				    printf ("%d:\t %f \n", i, temperature (adcValues[i]));
				  else 
				    printf ("%d: **** bad conversion status: %d\t%d\n", adcConvStat[i]);
				}

				}
			break;

		case 124:
			printf ("Send only clocks in a loop - 1 clock\n");
			printf ("Clocks - ");
			scanf ("%d", &clockNb);
			for (i=0; i<clockNb; i++) {
				if ((status = zasilacz.SPI_write (handle, elmbNb, 0, 32, elmbTimeOut)) < 0) {
					printf ("BC exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					break;
				}
			}

			break;


		case 99:
		  printf ("Closing socket %d\n", handle);
			if ((status = zasilacz.CloseCan (handle)) < 0) {
				printf ("CloseCan exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else
				printf ("Closing CANBus OK\n");
			break;
               
   case 400:
     
     int z;   
              int portC =0xF0 ;
     //  int portC =0xA0 ;
     int section;
     // Clear OCM
     
     printf ("Board type WA = 1, WB=2 nb - "); scanf ("%d", &cardNb);
     // F-F CLR=0,Q=0; i AN_OCM=1, OCM disable
     if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
       //              if ((status = zasilacz.Write_C(handle, elmbNb, OCMclear)) < 0) {
       printf ("Write_C exit with error: %d\n", status);
       printf ("%s\n", zasilacz.GetErrorText ());
     }
     
     printf("Card type %d\n",cardNb);
     
     if(cardNb==1) licznik=36;
     if(cardNb==2) licznik=24;
     
     //             portC = 0xA0;   // CLEAR - poziom wysoki, FLIP=FLOP aktywny
     // AN_OCM_D - 0 -> bramka na inhibit zablokowana
     if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
       printf ("Write_C exit with error: %d\n", status);
       printf ("%s\n", zasilacz.GetErrorText ());
     }
     printf("Test of Over Current Monitor \n");
     printf("4 = Digital, 2 = Analog Negative, 1 = Analog Positive  \n");
     printf("Regulator\t OCM_MUX\t Response\n");
     
     //  fprintf(fp400,"Test of Over Current Monitor \n");
     //fprintf(fp400,"4 = Digital, 2 = Analog Negative, 1 = Analog Positive  \n");
     //fprintf(fp400,"Regulator\t OCM_MUX\t Response\n");
     //			licznik = 36; // WA
     //			licznik = 24; // WB/B
     printf ("Licznik przed wejsciem do while %d\n", licznik);
     
     while(licznik>0){
       printf("AAAAAA licznik %d\n",licznik);
       sleep (30);
       for  (z=0; z<12; z++){
	 if (z==0){ (section=0);}
	 if (z==1){ (section=8);}
	 if (z==2){ (section=4);}
	 if (z==3){ (section=0xC);}
	 if (z==4){ (section=2);}
	 if (z==5){ (section=0xA);}
	 if (z==6){ (section=6);}
	 if (z==7){ (section=0xE);}
	 if (z==8) { (section=1);}
	 if (z==9){ (section=9);}
	 if (z==10){ (section=5);}
	 if (z==11){ (section=0xD);}
       //   cout<<" before readOCM 1 "<<endl;
       printf ("Read exit without error \n ocmValue 0x%X, 0x%X, 0x%X",ocmValues[0],ocmValues[1],ocmValues[2]);
	 if ((status = zasilacz.Read_OCM(handle,elmbNb,section,portC, ocmValues))<0){
	   printf ("Read exit with error: %d\n", status);
	 }
	 
	 //  printf ("Read exit without error \n ocmValue 0x%X, 0x%X, 0x%X",ocmValues[0],ocmValues[1],ocmValues[2]);
	  
	  if (ocmValues[2] == 2){
	    licznik--;
	    if (z==0){ printf("CH12 \t");}//fprintf(fp400,"CH12 \t");}
	    if (z==1){ printf("CH13 \t");}//fprintf(fp400,"CH13 \t");}
	    if (z==2){ printf("CH14 \t");}//fprintf(fp400,"CH14 \t");}
	    if (z==3){ printf("CH15 \t");}//fprintf(fp400,"CH15 \t");}
	    if (z==4){ printf("CH16 \t");}//fprintf(fp400,"CH16 \t");}
	    if (z==5){ printf("CH17 \t");}//fprintf(fp400,"CH17 \t");}
	    if (z==6){ printf("CH18 \t");}//fprintf(fp400,"CH18 \t");}
	    if (z==7){ printf("CH19 \t");}//fprintf(fp400,"CH19 \t");}
	    if (z==8){ printf("CH20 \t");}//fprintf(fp400,"CH20 \t");}
	    if (z==9){ printf("CH21 \t");}//fprintf(fp400,"CH21 \t");}
	    if (z==10){ printf("CH22 \t");}//fprintf(fp400,"CH22 \t");}
	    if (z==11){printf("CH23 \t");}//fprintf(fp400,"CH23 \t");}
	    
            printf ("%10d\t  %10d\n",z,ocmValues[2]);

	    //    fprintf (fp400,"%10d\t %10d\n",z,ocmValues[2]);
	    
            sleep(100);
	    
	  }
	} // for 1
       
       for  (z=0; z<12; z++){
	 if (z==0){ (section=0);}
	 if (z==1){ (section=8);}
	 if (z==2){ (section=4);}
	 if (z==3){ (section=0xC);}
	 if (z==4){ (section=2);}
	 if (z==5){ (section=0xA);}
	 if (z==6){ (section=6);}
	 if (z==7){ (section=0xE);}
	 if (z==8) { (section=1);}
	 if (z==9){ (section=9);}
	 if (z==10){ (section=5);}
	 if (z==11){ (section=0xD);}
	 if ((status = zasilacz.Read_OCM(handle,elmbNb,section,portC, ocmValues))<0){
	   printf ("Read exit with error: %d\n", status);
	 }
	 
	 if (ocmValues[1] == 1){
	   
	   licznik--;
	   if (z==0){ printf("CH0  \t");}//fprintf(fp400,"CH0 \t");}
	   if (z==1){ printf("CH1  \t");}//fprintf(fp400,"CH1 \t");}
	   if (z==2){ printf("CH2  \t");}//fprintf(fp400,"CH2 \t");}
	   if (z==3){ printf("CH3  \t");}//fprintf(fp400,"CH3 \t");}
	   if (z==4){ printf("CH4  \t");}//fprintf(fp400,"CH4 \t");}
	   if (z==5){ printf("CH5  \t");}//fprintf(fp400,"CH5 \t");}
	   if (z==6){ printf("CH6  \t");}//fprintf(fp400,"CH6 \t");}
	   if (z==7){ printf("CH7  \t");}//fprintf(fp400,"CH7 \t");}
	   if (z==8){ printf("CH8  \t");}//fprintf(fp400,"CH8 \t");}
	   if (z==9){ printf("CH9  \t");}//fprintf(fp400,"CH9 \t");}
	   if (z==10){ printf("CH10 \t");}//fprintf(fp400,"CH10 \t");}
	   if (z==11){printf("CH11 \t");}//fprintf(fp400,"CH11 \t");}
	   
	   printf ("%10d\t  %10d\n",z,ocmValues[1]);
	   //    fprintf (fp400,"%10d\t  %10d\n",z,ocmValues[1]);
	   
	   //sleep(500);
	   //  QThread::sleep(500);
	   
	 }   // if  ocmValues[1]
       } // for 2
       
       

       for  (section=0; section<12; section++){

	 if ((status = zasilacz.Read_OCM(handle,elmbNb,section,portC, ocmValues))<0){
	   printf ("Read exit with error: %d\n", status);
	 }
	 if (ocmValues[0] == 4){
	   licznik--;
	   if (section==0){ printf("CH24  \t");}//fprintf(fp400,"CH24 \t");}
	   if (section==1){ printf("CH25  \t");}//fprintf(fp400,"CH25 \t");}
	   if (section==2){ printf("CH26  \t");}//fprintf(fp400,"CH26 \t");}
	   if (section==3){ printf("CH27  \t");}//fprintf(fp400,"CH27 \t");}
	   if (section==4){ printf("CH28  \t");}//fprintf(fp400,"CH28 \t");}
	   if (section==5){ printf("CH29  \t");}//fprintf(fp400,"CH29 \t");}
	   if (section==6){ printf("CH30  \t");}//fprintf(fp400,"CH30 \t");}
	   if (section==7){ printf("CH31  \t");}//fprintf(fp400,"CH31 \t");}
	   if (section==8){ printf("CH32  \t");}//fprintf(fp400,"CH32 \t");}
	   if (section==9){ printf("CH33  \t");}//fprintf(fp400,"CH33 \t");}
	   if (section==10){ printf("CH34 \t");}//fprintf(fp400,"CH34 \t");}
	   if (section==11){printf("CH35 \t");}//fprintf(fp400,"CH35 \t");}
	   printf ("%10d\t  %10d\n",section,ocmValues[0]);
	   //   fprintf (fp400,"%10d\t  %10d\n",section,ocmValues[0]);
	   sleep(10);
	   
	 }
	 
       } // for (3)
       //---------------------------------------------------------------------------------------------------------------------
       
       //              if ((status = zasilacz.Write_C(handle, elmbNb, 0xF0)) < 0) { // // F-F nie jes zerowany, INH odblokowany
       if ((status = zasilacz.Write_C(handle, elmbNb, 0x00)) < 0) {  // OCMclear and OCM enable
	 
	 //            if ((status = zasilacz.Write_C(handle, elmbNb, 0x00)) < 0) {  // zerujemy FLIP-FLOP
	 printf ("Write_C exit with error: %d\n", status);
	 printf ("%s\n", zasilacz.GetErrorText ());
       }
       
	
	
       //				Sleep (500);
       //-------------------------------------------------------------------------------------------------------------------------
       //				portC = 0xF0;
       
       if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
	 //	            if ((status = zasilacz.Write_C(handle, elmbNb, 0xA0)) < 0) {
	 printf ("Write_C exit with error: %d\n", status);
	 printf ("%s\n", zasilacz.GetErrorText ());
       }
       
       
     }  // koniec While  !!!
     
    
     printf ("oto licznik: %d\n", licznik);
     
    //   fclose (fp400);
     if ((status = zasilacz.SetInhBrdc(handle, elmbNb, portC, 0, elmbTimeOut)) < 0)
      {
        printf ("%s\n", zasilacz.GetErrorText ());
	
	
      }
     break;
   }
   
   
 }
 
}


     
