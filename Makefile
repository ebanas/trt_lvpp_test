#
#  Makefile for lvpp shared library and testfile


MAKEFLAGS = -k

CC	=g++
CFLAGS    = -O2 -Wall -Wno-parentheses -Iinclude \
	    -fno-strict-aliasing \
	    -DSO_RXQ_OVFL=40 \
	    -DPF_CAN=29 \
	    -DAF_CAN=PF_CAN

PROGRAMS = lvppTest lvppTestWA  lvppTestWB lvppCalibration  lvppTest80

all: $(PROGRAMS)

clean:
	rm -f $(PROGRAMS) *.o


distclean:
	rm -f $(PROGRAMS) *.o *~

lvppSharedLib.o: lvppSharedLib.h

lvppTestLib.o: lvppSharedLib.h lvppTest.h lvppTestLib.h

lvppTest.o:	lvppSharedLib.h lvppTest.h
lvppTest:	lvppTest.o  lvppSharedLib.o lvppTestLib.o

lvppTestWA.o: lvppSharedLib.h lvppTest.h lvppTestLib.h
lvppTestWA:	lvppTestWA.o  lvppSharedLib.o lvppTestLib.o

lvppTestWB.o: lvppSharedLib.h lvppTest.h lvppTestLib.h
lvppTestWB:	lvppTestWB.o  lvppSharedLib.o lvppTestLib.o

lvppCalibration.o: lvppSharedLib.h lvppTest.h lvppTestLib.h
lvppCalibration:   lvppCalibration.o  lvppSharedLib.o lvppTestLib.o

lvppTest80.o: lvppSharedLib.h lvppTest.h lvppTestLib.h
lvppTest80:   lvppTest80.o  lvppSharedLib.o lvppTestLib.o

