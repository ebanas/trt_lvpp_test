#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "lib.h"
  

// some CAN definitions



// COB-IDs


#define cobID_ELMB_BootUp		0x700
#define cobID_ELMB_ModuleCtrl	0x000
#define	cobID_TPDO2				0x280
#define cobID_TPDO3				0x380
#define cobID_TPDO4				0x480
#define	cobID_PDO_Type1			0x080
#define cobID_PDO_Type255		0x280
#define cobID_TPDO1				0x180
#define	cobID_RPDO1				0x200

#define	cobID_RPDO2				0x300
#define	cobID_SDORX				0x600
#define cobID_SD0TX				0x580
#define cobID_ELMBtoHost		0x580
#define cobID_Emergency			0x080





// NMT Services

#define	nmtCS_StartNode				1
#define nmtCS_StopNode				2
#define nmtCS_PreOpState			128
#define nmtCS_ResetNode				129
#define nmtCS_ResetCommunication	130


// internal class constants
#define ConfigRegister	1
#define DAC_01			2
#define DAC_23			3
#define StatusRegister	4

#define FIELD_12345_WRITE_CFG	0x5724000
#define FIELD_12345_WRITE_DAC	0x571C000

#define FIELD_12345_READ	0x570C020

#define	CONF_REG	0x0
#define DAC01_REG	0xA
#define DAC23_REG	0xC
#define COMM_STAT_REG	0x3

#define DTMROC_READ_EXTRA_CLOCKS 4

// klasa I Internal errors
#define kl_OK							0
#define kl_canReadZeroLength			-2005
#define kl_ELMB_BadNMTCommandSpecifier	-2010
#define kl_DTMROC_DAC_BadDacNumber		-2020
#define kl_DTMROC_INH_BadInhNumber		-2030
#define kl_DTMROC_INH_BadInhValue		-2031
#define kl_DTMROC_INH_BadSet			-2032
#define kl_DTMROC_INH_Brdc_BadSet		-2033

#define kl_DTMROC_INH_Read_Failed		-2034
#define kl_DTMROC_Write_NotCorrect		-2040
#define kl_DTMROC_Write_Brdc_BadSet		-2041
#define kl_DTMROC_OCM_BadSection		-2050
#define kl_CANBUS_Read_BadIdentifier	-2060
#define kl_CANBUS_Read_BadLength		-2061


// Klasa I



class KlasaI

{

private:
	int olo;
	char errorText[120];
	void SetErrorText (char* info, canStatus stat);
	void SetInternalErrorText(char* info, int status);
	void AddToErrorText (char *text);
	int recoverOverrun (int handle, int elmbAddress, int portC);
	int recoverOverrunHard (int handle, int elmbAddress, int portC);
	HANDLE hSemaphore;
	inline int dtmrocAddr (int dac) {
		if (dac < 6) 
			return (dac+1);
		else 
			return (dac+3);
	}
	int ocmAnalogSectionNb (int section); 

	canStatus privCanWrite (int handle, long id, void *msg, unsigned int dlc, unsigned int flag);


public:

	KlasaI(void);
	~KlasaI(void);

	int Init_CANBus(int channel, int *handle);
	int Init_ELMB (int handle, int elmbAddress);
	int Init_DTMROC(int handle, int elmbAddress, int portC, int elmbTimeout, char *dtmrocRetAddr);
	int InitCanLibrary();
	int initELMB_SPI (int handle, int elmbAddress, int elmbTimeOut);
	int HardSoftResetDtmrocs(int handle, int elmbAddress, int portC, int elmbTimeOut);
	int GetDtmrocState (int handle, int elmbAddress, int dtmrocAddress, int portC, int elmbTimeOut, int *state);
	int Set_ELMB_State (int handle, int elmbAddress, int nmtCS);
	int Read_ELMB_State (int handle, int elmbAddress, int *state);
	bool Get_ELMB_CANBufferOverrun (int handle, int elmbAddress);
	unsigned short Get_ELMB_EmergencyError (int handle, int elmbAddress);
	int Software_Reset_DTMROC(int handle, int elmbAddress, int portC, int elmbTimeOut);
	int BC(int handle, int clockNb, int elmbAddress, int portC, int elmbTimeOut=2000);
	int Init(int handle, int channel, int elmbAddress, int portC, int elmbTimeOut);
	int CloseCan(int handle);
    int WriteDac(int handle, int elmbAddress, int dtmrocAddress, int dacNb, int Field6, int portC, int elmbTimeOut);
	int WriteAllDacsWheelA(int handle, int elmbAddress, int elmbTimeOut, int portC, int *values);
	int WriteAllDacsWheelB(int handle, int elmbAddress, int elmbTimeOut, int portC, int *values);
	int WriteDacBrdc (int handle, int elmbAddress, int dacs, int data, int portC, int elmbTimeOut);
	int WriteDacBrdcCheck (int handle, int elmbAddress, int dacs, int Field6, int portC, int elmbTimeOut);
	int DacBrdcCheck (int handle, int elmbAddress, int dacs, int Field6, int portC, int elmbTimeOut);
	int Hardware_Reset_DTMROC(int handle, int elmbAddress, int portC, int elmbTimeOut);
	int ReadAllDacsWheelA(int handle, int elmbAddress, int elmbTimeOut, int portC, int *values);
	int ReadAllDacsWheelB(int handle, int elmbAddress, int elmbTimeOut, int portC, int *values);
	int ReadADC_Counts (int handle, int elmbAddress, int *values, bool *convStat);
	int ReadADC_uVolts (int handle, int elmbAddress, int *values, bool *convStat);
	void can_Get_Error_Text(char* id, canStatus stat);
	char *GetErrorText(void);
	int Data_BC(int handle, int clocks, int elmbAddress, int portC);
	int Get_INHIBIT(int handle, int elmbAddress, int address, int portC, int elmbTimeOut, int *inhibits);
	int GetAllInhibitsWheelA(int handle, int elmbAddress, int portC, int elmbTimeOut, int *values);
	int GetAllInhibitsWheelB(int handle, int elmbAddress, int portC, int elmbTimeOut, int *values);
	int Set_INHIBIT(int handle, int elmbAddress, int address, int portC, int inhibitNb, int inhibitValue, int elmbTimeOut);
	int SetInhBrdc (int handle, int elmbAddress, int portC, int inhibitValue, int elmbTimeOut);
	int SetInhibitsAllOn(int handle, int elmbAddress, int portC, int boardType, int channelSkip, 
					     int elmbTimeOut, int *inhibits);
	int SetInhBrdcCheck (int handle, int elmbAddress, int portC, int inhibitValue, int elmbTimeOut);
	int InhBrdcCheck (int handle, int elmbAddress, int portC, int inhibitValue, int elmbTimeOut);
	int Read_OCM(int handle, int elmbAddress, int address, int portC, int *ocmValues);
	int Read_OCM_All_WheelA (int handle, int elmbAddress, int portC, int *ocmValues);
	int Read_OCM_All_WheelB (int handle, int elmbAddress, int portC, int *ocmValues);
	int Clear(int handle, int elmbAddress, int i);
	int Write_C(int handle, int elmbAddress, int Port_C);
	int D_Level(int handle, int elmbAddress, int portC);
	int Write_CMD(int handle, unsigned long Field12345, unsigned long Data, int elmbAddress, int portC);
	int Read_CMD(int handle, unsigned long Field12345, int portC, int elmbAddress, int *value);
	int Get_ELMB_SerialNumber (int handle, int elmbAddress, unsigned char *elmbSerialNumber);
	int SPI_write (int handle, int elmbAddress, unsigned long data, int length, int elmbTimeOut);
	int Write(int handle, int elmbAddress, int dtmrocAddress, int object, int Field6, int portC, int elmbTimeOut,  bool bCheck=true);
    int SPI_read (int handle, int elmbAddress, int *data, int clocks, int elmbTimeOut);
	int Read(int handle, int elmbAddress, int dtmrocAddress, int object, int portC, int elmbTimeOut, int *value);
	int sendWriteSDO(int handle, int elmbAddress, int index, int subIndex, unsigned long data,
						 int nbBytes, int ctrlWord, int elmbTimeOut);
	int sendReadSDO(int handle, int elmbAddress, int index, int subIndex,  unsigned long *data,
						int nbBytes, int ctrlWord, int elmbTimeOut);
     int readSPI_period (int hande, int elmbAddress, int elmbTimeOut, unsigned long *period);	
	int setSPI_period (int handle, int elmbAddress, unsigned long period, int elmbTimeOut);

#ifdef LV_SEMAPHORE
	int SemaphoreCreate();
	int SemaphoreClose();
	int SemaphoreWait ();
	int SemaphoreSignal ();
#endif

};

