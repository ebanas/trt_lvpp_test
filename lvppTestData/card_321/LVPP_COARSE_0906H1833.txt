============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 321
CALIBRATION FILE: lvppCalib/card_321/LVPP_CALIB_321.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
                temperature range: <15.10 C - 60.00 C> 

TEST is started: 09.06.2015 18:33:10

Initializing of CANBUS OK
ELMB ID 11 init OK
ELMB ID 11 TPDO3 Transmission Type set to RTR 
ELMB ID 11  ADCResetAndCalibrate Bit is set. 
ELMB ID 11 serial number: F823
ELMB ID 11 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.

Current channels
******** 0. 0.002986    1. 0.003016  || 0.006003 
******** 2. 0.002992    3. 0.003011  || 0.006003 
******** 4. 0.000003    5. 0.000019  || 0.000022 
******** 6. 0.000025    7. 0.000008  || 0.000033 
******** 8. 0.000004    9. 0.000027  || 0.000031 
******** 10. 0.000004    11. 0.000010  || 0.000014 
******** 12. 0.000005    13. 0.000016  || 0.000020 
******** 14. 0.000015    15. 0.000029  || 0.000044 
******** 16. 0.009109    17. 0.003004  || 0.012113 
******** 18. 0.000006    19. 0.002993  || 0.002999 
******** 20. 0.003004    21. 0.015202  || 0.018206 
******** 22. 0.002999    23. 0.000002  || 0.003001 
******** 24. 0.000002    25. 0.003006  || 0.003008 
******** 26. 0.000005    27. 0.000008  || 0.000013 
******** 28. 0.000004    29. 0.000002  || 0.000006 
******** 30. 0.003008    31. 0.000005  || 0.003013 
******** 32. 0.003005    33. 0.003001  || 0.006007 
******** 34. 0.002996    35. 0.003000  || 0.005996 

Voltage channels:
ELMB Channel 36:   0.005 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.005 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.005 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.005 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.005 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.004 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.001 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.003 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.003 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.001 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.002 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 57.49
Channel 61, temperature 58.13
Channel 62, temperature 57.58
Channel 63, temperature 56.98

Set regulator number 1 ON

Current channels
******** 0. 0.768133    1. 0.000268  || 0.768401 
******** 2. 0.766164    3. 0.000567  || 0.766732 
******** 4. 0.768592    5. 0.003486  || 0.772079 
******** 6. 0.776592    7. 0.000622  || 0.777214 
******** 8. 0.775458    9. 0.004540  || 0.779999 
******** 10. 0.766006    11. 0.001612  || 0.767618 
******** 12. 0.759629    13. 0.001440  || 0.761069 
******** 14. 0.737029    15. 0.001976  || 0.739004 
******** 16. 0.736023    17. 0.000184  || 0.736207 
******** 18. 0.746171    19. 0.000352  || 0.746522 
******** 20. 0.739124    21. 0.000522  || 0.739646 
******** 22. 0.750531    23. 0.001073  || 0.751604 
******** 24. 0.633937    25. 0.019729  || 0.653666 
******** 26. 0.637471    27. 0.014782  || 0.652253 
******** 28. 0.636776    29. 0.008638  || 0.645415 
******** 30. 0.660351    31. 0.005299  || 0.665650 
******** 32. 0.649400    33. 0.003245  || 0.652645 
******** 34. 0.645031    35. 0.003000  || 0.648031 

Voltage channels:
ELMB Channel 36:   3.729 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.710 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.703 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.734 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.731 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.725 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.742 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.744 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.635 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.557 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.538 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.593 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.571 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.564 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.633 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.633 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.196 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.206 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.173 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.137 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.185 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.172 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.188 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.174 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 57.44
Channel 61, temperature 58.01
Channel 62, temperature 57.44
Channel 63, temperature 56.85

Set regulator number 2 ON

Current channels
******** 0. 0.003913    1. 0.765667  || 0.769580 
******** 2. 0.002998    3. 0.774621  || 0.777619 
******** 4. 0.000414    5. 0.762475  || 0.762889 
******** 6. 0.004432    7. 0.769567  || 0.773999 
******** 8. 0.003344    9. 0.764565  || 0.767909 
******** 10. 0.002962    11. 0.764228  || 0.767190 
******** 12. 0.003227    13. 0.758098  || 0.761325 
******** 14. 0.004322    15. 0.748332  || 0.752654 
******** 16. 0.003855    17. 0.747847  || 0.751702 
******** 18. 0.001756    19. 0.760147  || 0.761903 
******** 20. 0.005284    21. 0.750546  || 0.755831 
******** 22. 0.005843    23. 0.746636  || 0.752479 
******** 24. 0.012985    25. 0.620993  || 0.633978 
******** 26. 0.009461    27. 0.625965  || 0.635426 
******** 28. 0.007091    29. 0.638245  || 0.645336 
******** 30. 0.001803    31. 0.635531  || 0.637334 
******** 32. 0.003644    33. 0.646753  || 0.650397 
******** 34. 0.001193    35. 0.640800  || 0.641993 

Voltage channels:
ELMB Channel 36:   3.709 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.748 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.694 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.704 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.716 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.724 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.702 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.714 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.586 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.626 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.608 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.658 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.607 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.619 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.619 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.630 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.164 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.188 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.193 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.151 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.169 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.170 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.163 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.162 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 57.39
Channel 61, temperature 57.94
Channel 62, temperature 57.39
Channel 63, temperature 56.83

Set both regulators ON

Current channels
******** 0. 0.630727    1. 0.137038  || 0.767765 
******** 2. 0.002995    3. 0.774618  || 0.777612 
******** 4. 0.390097    5. 0.374939  || 0.765036 
******** 6. 0.733896    7. 0.043323  || 0.777219 
******** 8. 0.479362    9. 0.291404  || 0.770767 
******** 10. 0.766007    11. 0.001612  || 0.767619 
******** 12. 0.759628    13. 0.001442  || 0.761071 
******** 14. 0.004331    15. 0.748349  || 0.752681 
******** 16. 0.003848    17. 0.747844  || 0.751692 
******** 18. 0.001752    19. 0.760152  || 0.761903 
******** 20. 0.005302    21. 0.750524  || 0.755826 
******** 22. 0.478826    23. 0.270501  || 0.749327 
******** 24. 0.633937    25. 0.019729  || 0.653666 
******** 26. 0.472676    27. 0.149927  || 0.622603 
******** 28. 0.007098    29. 0.638249  || 0.645347 
******** 30. 0.043888    31. 0.595925  || 0.639813 
******** 32. 0.393113    33. 0.253064  || 0.646176 
******** 34. 0.581029    35. 0.064000  || 0.645029 

Voltage channels:
ELMB Channel 36:   3.731 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.747 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.710 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.735 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.736 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.735 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.741 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.744 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.635 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.623 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.604 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.655 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.603 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.616 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.638 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.642 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.196 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.209 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.189 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.148 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.190 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.182 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.189 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.177 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 57.36
Channel 61, temperature 57.89
Channel 62, temperature 57.45
Channel 63, temperature 56.84

 NO ERRORS in  1 loops, TEST finished at 09.06.2015 18:33:55
