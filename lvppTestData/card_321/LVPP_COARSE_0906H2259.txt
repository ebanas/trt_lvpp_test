============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 321
CALIBRATION FILE: lvppCalib/card_321/LVPP_CALIB_321.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
                temperature range: <15.10 C - 60.00 C> 

TEST is started: 09.06.2015 22:59:37

Initializing of CANBUS OK
ELMB ID 11 init OK
ELMB ID 11 TPDO3 Transmission Type set to RTR 
ELMB ID 11  ADCResetAndCalibrate Bit is set. 
ELMB ID 11 serial number: F823
ELMB ID 11 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.

Current channels
******** 0. 0.002987    1. 0.003015  || 0.006002 
******** 2. 0.002993    3. 0.003010  || 0.006003 
******** 4. 0.000003    5. 0.000017  || 0.000020 
******** 6. 0.000023    7. 0.000008  || 0.000031 
******** 8. 0.000004    9. 0.000023  || 0.000027 
******** 10. 0.000004    11. 0.000009  || 0.000013 
******** 12. 0.000005    13. 0.000015  || 0.000019 
******** 14. 0.000014    15. 0.000027  || 0.000041 
******** 16. 0.009108    17. 0.003004  || 0.012112 
******** 18. 0.000005    19. 0.002994  || 0.002999 
******** 20. 0.000002    21. 0.012204  || 0.012206 
******** 22. 0.000001    23. 0.000002  || 0.000002 
******** 24. 0.000002    25. 0.003006  || 0.003007 
******** 26. 0.000004    27. 0.000007  || 0.000011 
******** 28. 0.000004    29. 0.000002  || 0.000006 
******** 30. 0.003007    31. 0.000004  || 0.003011 
******** 32. 0.003005    33. 0.003001  || 0.006007 
******** 34. 0.002995    35. 0.003000  || 0.005995 

Voltage channels:
ELMB Channel 36:   0.005 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.004 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.004 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.004 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.004 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.004 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.001 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.001 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.002 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 35.30
Channel 61, temperature 33.30
Channel 62, temperature 34.62
Channel 63, temperature 32.83

Set regulator number 1 ON

Current channels
******** 0. 0.762043    1. 0.000280  || 0.762323 
******** 2. 0.760075    3. 0.000583  || 0.760658 
******** 4. 0.756390    5. 0.000400  || 0.756790 
******** 6. 0.758262    7. 0.002388  || 0.760650 
******** 8. 0.760154    9. 0.001414  || 0.761568 
******** 10. 0.747713    11. 0.001372  || 0.749085 
******** 12. 0.747464    13. 0.001328  || 0.748791 
******** 14. 0.715727    15. 0.001788  || 0.717515 
******** 16. 0.717655    17. 0.002948  || 0.720602 
******** 18. 0.730922    19. 0.002689  || 0.733611 
******** 20. 0.723623    21. 0.000777  || 0.724399 
******** 22. 0.729164    23. 0.000969  || 0.730133 
******** 24. 0.627832    25. 0.016748  || 0.644580 
******** 26. 0.625254    27. 0.014809  || 0.640064 
******** 28. 0.627563    29. 0.008632  || 0.636195 
******** 30. 0.648126    31. 0.005316  || 0.653442 
******** 32. 0.637179    33. 0.003251  || 0.640430 
******** 34. 0.632842    35. 0.003000  || 0.635842 

Voltage channels:
ELMB Channel 36:   3.725 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.703 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.700 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.729 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.727 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.720 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.733 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.736 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.596 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.524 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.502 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.555 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.533 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.526 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.596 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.596 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.189 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.197 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.165 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.129 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.177 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.165 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.183 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.172 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 35.34
Channel 61, temperature 33.37
Channel 62, temperature 34.67
Channel 63, temperature 32.89

Set regulator number 2 ON

Current channels
******** 0. 0.000899    1. 0.759551  || 0.760451 
******** 2. 0.000116    3. 0.762402  || 0.762518 
******** 4. 0.000418    5. 0.756255  || 0.756673 
******** 6. 0.004797    7. 0.760458  || 0.765254 
******** 8. 0.002757    9. 0.755377  || 0.758134 
******** 10. 0.000043    11. 0.745917  || 0.745960 
******** 12. 0.000196    13. 0.736597  || 0.736793 
******** 14. 0.001443    15. 0.730262  || 0.731705 
******** 16. 0.003780    17. 0.729511  || 0.733291 
******** 18. 0.001693    19. 0.738824  || 0.740517 
******** 20. 0.000562    21. 0.728814  || 0.729375 
******** 22. 0.003217    23. 0.731466  || 0.734682 
******** 24. 0.009992    25. 0.614872  || 0.624863 
******** 26. 0.012574    27. 0.619843  || 0.632417 
******** 28. 0.007102    29. 0.629151  || 0.636252 
******** 30. 0.001826    31. 0.629416  || 0.631242 
******** 32. 0.003664    33. 0.634546  || 0.638211 
******** 34. 0.001787    35. 0.628600  || 0.630387 

Voltage channels:
ELMB Channel 36:   3.703 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.740 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.689 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.699 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.715 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.721 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.697 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.709 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.551 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.586 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.568 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.610 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.559 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.569 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.574 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.584 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.155 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.181 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.186 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.144 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.161 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.163 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.155 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.155 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 35.36
Channel 61, temperature 33.51
Channel 62, temperature 34.78
Channel 63, temperature 33.02

Set both regulators ON

 NO ERRORS in  0 loops, TEST finished at 09.06.2015 23:00:08
