===================================================
Check basic functions of a card TYPE Wheel B/Barrel
====================================================
Card Serial Number: 101
Starting test: CAN device can4  ELMB ID 1   Card number 101  - 100 loops 

CANBus,ELMB,DTMROC initialization 
Initializing of CANBUS OK
Initializing of Init_ELMB OK
Initializing of DTMROCS OK
DTMROC-i: 1 2 3 4 5 6 9 10 11 12 13 14 
CALIBRATION FILE: lvppCalib/card_101/LVPP_CALIB_101.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
                temperature range: <15.10 C - 60.00 C> 

TEST is started: 08.06.2015 20:30:12

ELMB <1> init OK
ELMB NB: 1 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.
Current channels
******** 0. 0.002380  
******** 2. 0.003645  
******** 4. 0.000866  
******** 6. 0.000020  
******** 8. 0.000151  
******** 9. 0.003016  
******** 10. 0.002656  
******** 11. 0.000156  
******** 12. 0.000111  
******** 14. 0.000329  
******** 16. 0.009189  
******** 18. 0.000233  
******** 20. 0.000013  
******** 21. 0.009069  
******** 22. 0.000003  
******** 23. 0.000028  
******** 24. 0.003105  
******** 26. 0.000149  
******** 28. 0.000032  
******** 30. 0.000258  
******** 32. 0.003017  
******** 33. 0.002881  
******** 34. 0.003289  
******** 35. 0.003085  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   0.006 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.005 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.005 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.005 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.006 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.005 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.005 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.000 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.003 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.002 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]
Channel 60, temperature 49.95
Channel 61, temperature 50.51
Channel 62, temperature 50.08
Channel 63, temperature 50.89
Temperature channels

Set all regulators ON
Current channels
******** 0. 0.795046  
******** 2. 0.775907  
******** 4. 0.762790  
******** 6. 0.766401  
******** 8. 0.753030  
******** 9. 0.761997  
******** 10. 0.769964  
******** 11. 0.728489  
******** 12. 0.761363  
******** 14. 0.772336  
******** 16. 0.758877  
******** 18. 0.749547  
******** 20. 0.738954  
******** 21. 0.758171  
******** 22. 0.795249  
******** 23. 0.759348  
******** 24. 0.729112  
******** 26. 0.727270  
******** 28. 0.745530  
******** 30. 0.711401  
******** 32. 0.754614  
******** 33. 0.739016  
******** 34. 0.722504  
******** 35. 0.774980  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.640 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.667 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.651 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.698 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.637 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.641 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.643 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.642 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.649 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.656 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.661 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.721 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.686 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.669 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 49.90
Channel 61, temperature 50.34
Channel 62, temperature 49.95
Channel 63, temperature 50.63
Temperature channels
Current channels
******** 0. 0.795046  
******** 2. 0.775907  
******** 4. 0.762790  
******** 6. 0.766403  
******** 8. 0.750030  
******** 9. 0.761998  
******** 10. 0.769964  
******** 11. 0.731599  
******** 12. 0.761357  
******** 14. 0.772336  
******** 16. 0.758877  
******** 18. 0.746463  
******** 20. 0.738968  
******** 21. 0.758202  
******** 22. 0.792247  
******** 23. 0.759348  
******** 24. 0.729112  
******** 26. 0.727270  
******** 28. 0.745534  
******** 30. 0.711401  
******** 32. 0.754617  
******** 33. 0.738993  
******** 34. 0.725563  
******** 35. 0.778080  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.640 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.667 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.698 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.637 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.641 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.643 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.642 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.648 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.656 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.661 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.721 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.686 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.670 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 49.84
Channel 61, temperature 50.34
Channel 62, temperature 49.89
Channel 63, temperature 50.63
Temperature channels
Current channels
******** 0. 0.795046  
******** 2. 0.775948  
******** 4. 0.765843  
******** 6. 0.766403  
******** 8. 0.753030  
******** 9. 0.761997  
******** 10. 0.769964  
******** 11. 0.728499  
******** 12. 0.761350  
******** 14. 0.772315  
******** 16. 0.758883  
******** 18. 0.746447  
******** 20. 0.738954  
******** 21. 0.758171  
******** 22. 0.795247  
******** 23. 0.756348  
******** 24. 0.729128  
******** 26. 0.727251  
******** 28. 0.745534  
******** 30. 0.708364  
******** 32. 0.754617  
******** 33. 0.739016  
******** 34. 0.725504  
******** 35. 0.774980  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.668 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.698 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.637 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.641 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.643 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.642 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.649 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.656 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.661 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.687 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.669 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 49.80
Channel 61, temperature 50.40
Channel 62, temperature 49.89
Channel 63, temperature 50.70
Temperature channels
Current channels
******** 0. 0.795046  
******** 2. 0.775948  
******** 4. 0.765843  
******** 6. 0.769503  
******** 8. 0.753030  
******** 9. 0.765097  
******** 10. 0.769964  
******** 11. 0.728499  
******** 12. 0.761350  
******** 14. 0.772336  
******** 16. 0.758883  
******** 18. 0.749532  
******** 20. 0.738954  
******** 21. 0.758171  
******** 22. 0.795246  
******** 23. 0.759320  
******** 24. 0.729112  
******** 26. 0.730351  
******** 28. 0.745539  
******** 30. 0.708401  
******** 32. 0.754617  
******** 33. 0.739016  
******** 34. 0.722504  
******** 35. 0.774980  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.668 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.698 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.637 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.641 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.644 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.642 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.649 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.656 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.661 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.686 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.669 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 49.79
Channel 61, temperature 50.50
Channel 62, temperature 49.91
Channel 63, temperature 50.80
Temperature channels
Current channels
******** 0. 0.795046  
******** 2. 0.775907  
******** 4. 0.765790  
******** 6. 0.766403  
******** 8. 0.753030  
******** 9. 0.761998  
******** 10. 0.769964  
******** 11. 0.728499  
******** 12. 0.761344  
******** 14. 0.772294  
******** 16. 0.755888  
******** 18. 0.749547  
******** 20. 0.738941  
******** 21. 0.758139  
******** 22. 0.795247  
******** 23. 0.759348  
******** 24. 0.729112  
******** 26. 0.730370  
******** 28. 0.745534  
******** 30. 0.708401  
******** 32. 0.757717  
******** 33. 0.738993  
******** 34. 0.725563  
******** 35. 0.778100  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.640 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.667 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.699 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.637 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.642 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.643 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.642 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.649 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.656 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.661 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.721 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.686 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.670 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 49.80
Channel 61, temperature 50.61
Channel 62, temperature 49.96
Channel 63, temperature 50.91
Temperature channels
Current channels
******** 0. 0.795046  
******** 2. 0.779048  
******** 4. 0.762843  
******** 6. 0.766403  
******** 8. 0.753030  
******** 9. 0.761998  
******** 10. 0.766964  
******** 11. 0.728499  
******** 12. 0.761344  
******** 14. 0.772294  
******** 16. 0.758888  
******** 18. 0.746432  
******** 20. 0.738941  
******** 21. 0.758109  
******** 22. 0.795246  
******** 23. 0.759320  
******** 24. 0.726028  
******** 26. 0.727251  
******** 28. 0.745534  
******** 30. 0.711401  
******** 32. 0.757717  
******** 33. 0.739016  
******** 34. 0.725563  
******** 35. 0.774980  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.668 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.699 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.637 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.642 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.644 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.642 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.649 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.656 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.661 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.686 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.669 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 49.83
Channel 61, temperature 50.72
Channel 62, temperature 50.01
Channel 63, temperature 51.01
Temperature channels
Current channels
******** 0. 0.795010  
******** 2. 0.779048  
******** 4. 0.765790  
******** 6. 0.769503  
******** 8. 0.753019  
******** 9. 0.765098  
******** 10. 0.769964  
******** 11. 0.728499  
******** 12. 0.761337  
******** 14. 0.772294  
******** 16. 0.758888  
******** 18. 0.746432  
******** 20. 0.738941  
******** 21. 0.758109  
******** 22. 0.795244  
******** 23. 0.759320  
******** 24. 0.726028  
******** 26. 0.727251  
******** 28. 0.745534  
******** 30. 0.711364  
******** 32. 0.757717  
******** 33. 0.742016  
******** 34. 0.722563  
******** 35. 0.778080  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.667 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.699 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.637 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.642 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.644 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.642 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.649 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.657 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.661 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.687 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.669 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 49.86
Channel 61, temperature 50.82
Channel 62, temperature 50.08
Channel 63, temperature 51.11
Temperature channels
Current channels
******** 0. 0.795010  
******** 2. 0.775948  
******** 4. 0.765790  
******** 6. 0.766403  
******** 8. 0.753019  
******** 9. 0.761998  
******** 10. 0.769943  
******** 11. 0.731599  
******** 12. 0.764437  
******** 14. 0.775274  
******** 16. 0.758899  
******** 18. 0.749516  
******** 20. 0.738900  
******** 21. 0.761177  
******** 22. 0.795243  
******** 23. 0.759263  
******** 24. 0.729128  
******** 26. 0.727251  
******** 28. 0.745539  
******** 30. 0.708401  
******** 32. 0.754617  
******** 33. 0.735893  
******** 34. 0.722563  
******** 35. 0.775000  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.667 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.699 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.638 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.642 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.644 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.643 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.650 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.657 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.662 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.686 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.670 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 49.90
Channel 61, temperature 50.95
Channel 62, temperature 50.15
Channel 63, temperature 51.25
Temperature channels
Current channels
******** 0. 0.795046  
******** 2. 0.775948  
******** 4. 0.762790  
******** 6. 0.769503  
******** 8. 0.753019  
******** 9. 0.765098  
******** 10. 0.769943  
******** 11. 0.728499  
******** 12. 0.761331  
******** 14. 0.775274  
******** 16. 0.758894  
******** 18. 0.749532  
******** 20. 0.741900  
******** 21. 0.758046  
******** 22. 0.795243  
******** 23. 0.759263  
******** 24. 0.726012  
******** 26. 0.730351  
******** 28. 0.745534  
******** 30. 0.708401  
******** 32. 0.754620  
******** 33. 0.739016  
******** 34. 0.725563  
******** 35. 0.775000  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.667 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.699 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.638 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.642 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.644 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.643 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.650 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.657 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.662 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.686 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.669 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 49.94
Channel 61, temperature 51.06
Channel 62, temperature 50.21
Channel 63, temperature 51.39
Temperature channels
Current channels
******** 0. 0.795010  
******** 2. 0.775948  
******** 4. 0.765843  
******** 6. 0.769503  
******** 8. 0.753019  
******** 9. 0.765098  
******** 10. 0.769943  
******** 11. 0.731599  
******** 12. 0.761337  
******** 14. 0.772274  
******** 16. 0.758899  
******** 18. 0.749532  
******** 20. 0.738887  
******** 21. 0.758014  
******** 22. 0.795244  
******** 23. 0.759263  
******** 24. 0.729128  
******** 26. 0.727251  
******** 28. 0.745534  
******** 30. 0.711401  
******** 32. 0.757720  
******** 33. 0.739016  
******** 34. 0.722563  
******** 35. 0.774980  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.668 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.699 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.638 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.642 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.644 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.644 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.650 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.657 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.662 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.686 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.669 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 49.98
Channel 61, temperature 51.15
Channel 62, temperature 50.28
Channel 63, temperature 51.55
Temperature channels
Current channels
******** 0. 0.795010  
******** 2. 0.775948  
******** 4. 0.765843  
******** 6. 0.766403  
******** 8. 0.750019  
******** 9. 0.761997  
******** 10. 0.769964  
******** 11. 0.728499  
******** 12. 0.761331  
******** 14. 0.772253  
******** 16. 0.758899  
******** 18. 0.749532  
******** 20. 0.738900  
******** 21. 0.758014  
******** 22. 0.795241  
******** 23. 0.759236  
******** 24. 0.729128  
******** 26. 0.727251  
******** 28. 0.745539  
******** 30. 0.711364  
******** 32. 0.754617  
******** 33. 0.738993  
******** 34. 0.722504  
******** 35. 0.778100  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.668 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.699 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.638 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.642 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.644 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.643 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.650 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.657 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.662 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.687 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.670 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 50.05
Channel 61, temperature 51.26
Channel 62, temperature 50.36
Channel 63, temperature 51.72
Temperature channels
Current channels
******** 0. 0.795010  
******** 2. 0.775948  
******** 4. 0.765843  
******** 6. 0.769503  
******** 8. 0.753019  
******** 9. 0.765098  
******** 10. 0.769943  
******** 11. 0.728509  
******** 12. 0.761331  
******** 14. 0.775212  
******** 16. 0.762005  
******** 18. 0.752500  
******** 20. 0.738860  
******** 21. 0.757952  
******** 22. 0.798338  
******** 23. 0.759207  
******** 24. 0.729128  
******** 26. 0.727251  
******** 28. 0.742439  
******** 30. 0.708401  
******** 32. 0.754620  
******** 33. 0.739016  
******** 34. 0.719463  
******** 35. 0.775000  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.668 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.699 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.639 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.643 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.644 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.644 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.651 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.658 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.662 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.686 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.669 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 50.10
Channel 61, temperature 51.35
Channel 62, temperature 50.44
Channel 63, temperature 51.88
Temperature channels
Current channels
******** 0. 0.798110  
******** 2. 0.775948  
******** 4. 0.765790  
******** 6. 0.766403  
******** 8. 0.753019  
******** 9. 0.761998  
******** 10. 0.769943  
******** 11. 0.728509  
******** 12. 0.761324  
******** 14. 0.775253  
******** 16. 0.758905  
******** 18. 0.749500  
******** 20. 0.738873  
******** 21. 0.757983  
******** 22. 0.795240  
******** 23. 0.762336  
******** 24. 0.726028  
******** 26. 0.727232  
******** 28. 0.745534  
******** 30. 0.711364  
******** 32. 0.757720  
******** 33. 0.739016  
******** 34. 0.722504  
******** 35. 0.778100  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.667 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.700 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.638 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.643 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.644 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.644 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.651 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.658 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.662 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.687 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.669 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 50.16
Channel 61, temperature 51.47
Channel 62, temperature 50.53
Channel 63, temperature 52.01
Temperature channels
Current channels
******** 0. 0.795046  
******** 2. 0.775948  
******** 4. 0.765843  
******** 6. 0.769503  
******** 8. 0.753019  
******** 9. 0.765098  
******** 10. 0.773043  
******** 11. 0.731589  
******** 12. 0.761331  
******** 14. 0.772253  
******** 16. 0.758905  
******** 18. 0.749500  
******** 20. 0.738860  
******** 21. 0.757921  
******** 22. 0.798340  
******** 23. 0.759180  
******** 24. 0.729128  
******** 26. 0.727232  
******** 28. 0.748539  
******** 30. 0.711364  
******** 32. 0.754620  
******** 33. 0.739016  
******** 34. 0.722563  
******** 35. 0.778100  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.668 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.699 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.638 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.643 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.644 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.644 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.651 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.658 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.663 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.687 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.669 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 50.23
Channel 61, temperature 51.60
Channel 62, temperature 50.61
Channel 63, temperature 52.16
Temperature channels
Current channels
******** 0. 0.795046  
******** 2. 0.775948  
******** 4. 0.765843  
******** 6. 0.769504  
******** 8. 0.753009  
******** 9. 0.765098  
******** 10. 0.769943  
******** 11. 0.731609  
******** 12. 0.764418  
******** 14. 0.775212  
******** 16. 0.762016  
******** 18. 0.749485  
******** 20. 0.738846  
******** 21. 0.757921  
******** 22. 0.798338  
******** 23. 0.762251  
******** 24. 0.729128  
******** 26. 0.727232  
******** 28. 0.745539  
******** 30. 0.708364  
******** 32. 0.757722  
******** 33. 0.735893  
******** 34. 0.722563  
******** 35. 0.775000  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.668 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.700 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.639 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.643 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.645 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.645 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.651 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.658 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.663 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.687 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.697 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.670 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 50.28
Channel 61, temperature 51.72
Channel 62, temperature 50.68
Channel 63, temperature 52.29
Temperature channels
Current channels
******** 0. 0.798110  
******** 2. 0.775948  
******** 4. 0.765843  
******** 6. 0.766403  
******** 8. 0.753009  
******** 9. 0.765098  
******** 10. 0.769943  
******** 11. 0.728509  
******** 12. 0.761324  
******** 14. 0.775253  
******** 16. 0.758910  
******** 18. 0.749485  
******** 20. 0.741846  
******** 21. 0.761021  
******** 22. 0.798338  
******** 23. 0.759151  
******** 24. 0.729128  
******** 26. 0.730332  
******** 28. 0.745539  
******** 30. 0.711364  
******** 32. 0.757720  
******** 33. 0.738993  
******** 34. 0.722563  
******** 35. 0.778100  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.668 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.700 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.638 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.643 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.645 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.645 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.651 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.658 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.663 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.687 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.696 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.670 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 50.36
Channel 61, temperature 51.84
Channel 62, temperature 50.76
Channel 63, temperature 52.44
Temperature channels
Current channels
******** 0. 0.795010  
******** 2. 0.775948  
******** 4. 0.762843  
******** 6. 0.769503  
******** 8. 0.756109  
******** 9. 0.765099  
******** 10. 0.773021  
******** 11. 0.728509  
******** 12. 0.761318  
******** 14. 0.775212  
******** 16. 0.762010  
******** 18. 0.749469  
******** 20. 0.738819  
******** 21. 0.760989  
******** 22. 0.798337  
******** 23. 0.759151  
******** 24. 0.729128  
******** 26. 0.730351  
******** 28. 0.745539  
******** 30. 0.708401  
******** 32. 0.757722  
******** 33. 0.738993  
******** 34. 0.722563  
******** 35. 0.778100  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.668 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.645 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.700 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.639 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.643 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.645 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.645 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.652 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.658 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.663 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.686 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.697 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.670 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 50.44
Channel 61, temperature 51.96
Channel 62, temperature 50.83
Channel 63, temperature 52.55
Temperature channels
Current channels
******** 0. 0.795010  
******** 2. 0.775948  
******** 4. 0.762843  
******** 6. 0.766403  
******** 8. 0.756109  
******** 9. 0.761998  
******** 10. 0.773021  
******** 11. 0.728509  
******** 12. 0.761318  
******** 14. 0.775212  
******** 16. 0.762016  
******** 18. 0.749469  
******** 20. 0.741819  
******** 21. 0.760958  
******** 22. 0.801337  
checkAnalogReadWheelB: ERROR: current not in range (0.801 A): ELMB channel 22 [I AN_NEG dtmroc 6]
******** 23. 0.762224  
******** 24. 0.726012  
******** 26. 0.727251  
******** 28. 0.745534  
******** 30. 0.711364  
******** 32. 0.757722  
******** 33. 0.738993  
******** 34. 0.722563  
******** 35. 0.778100  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   3.660 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.641 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.668 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.652 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.686 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.657 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.655 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.700 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.639 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.643 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.645 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.645 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.652 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.658 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.663 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.733 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.722 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.687 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.697 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.670 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.667 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.693 V   [V DIGI dtmroc 14]
Channel 60, temperature 50.49
Channel 61, temperature 52.07
Channel 62, temperature 50.88
Channel 63, temperature 52.65
Temperature channels
Voltages switched off via Hardware Reset

 TEST interrupted at 08.06.2015 20:32:55 because of errros, after 18 loops
