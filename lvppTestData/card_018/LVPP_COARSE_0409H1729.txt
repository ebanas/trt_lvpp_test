============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 18
CALIBRATION FILE: /det/dcs/lvtemp/lvppCalib/card_018/LVPP_CALIB_018.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
                temperature range: <15.10 C - 60.00 C> 

TEST is started: 04.09.2012 17:29:22

ELMB <1> init OK
ELMB NB: 1 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.

Current channels
******** 0. 0.003312    1. 0.003166  || 0.006477 
******** 2. 0.002709    3. 0.002113  || 0.004822 
******** 4. 0.003289    5. 0.000267  || 0.003556 
******** 6. 0.000252    7. 0.001107  || 0.001359 
******** 8. 0.000510    9. 0.000763  || 0.001273 
******** 10. 0.001495    11. 0.000151  || 0.001646 
******** 12. 0.003004    13. 0.001938  || 0.004942 
******** 14. 0.000321    15. 0.000336  || 0.000657 
******** 16. 0.008489    17. 0.002476  || 0.010965 
******** 18. 0.000153    19. 0.005844  || 0.005997 
******** 20. 0.002851    21. 0.009060  || 0.011911 
******** 22. 0.002953    23. 0.003003  || 0.005956 
******** 24. 0.000267    25. 0.002913  || 0.003180 
******** 26. 0.000203    27. 0.000465  || 0.000668 
******** 28. 0.002943    29. 0.000847  || 0.003790 
******** 30. 0.002743    31. 0.002086  || 0.004829 
******** 32. 0.005995    33. 0.005375  || 0.011369 
******** 34. 0.002959    35. 0.005765  || 0.008723 

Voltage channels:
ELMB Channel 36:   0.006 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.005 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.004 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.005 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.004 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.006 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.005 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.004 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.001 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.001 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.000 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.000 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.003 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.004 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.001 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.002 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 48.06
Channel 61, temperature 50.01
Channel 62, temperature 47.78
Channel 63, temperature 48.36

Set all regulators ON

Current channels
******** 0. 0.372620    1. 0.364037  || 0.736657 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.373 A): ELMB channel 0 [I AN_POS dtmroc 1]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.364 A): ELMB channel 1 [I AN_POS dtmroc 1]
******** 2. 0.527001    3. 0.522645  || 1.049646 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.527 A): ELMB channel 2 [I AN_POS dtmroc 2]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.523 A): ELMB channel 3 [I AN_POS dtmroc 2]
******** 4. 0.731243    5. 0.711732  || 1.442975 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.712 A): ELMB channel 5 [I AN_POS dtmroc 3]
******** 6. 0.566888    7. 0.562918  || 1.129806 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.567 A): ELMB channel 6 [I AN_POS dtmroc 4]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.563 A): ELMB channel 7 [I AN_POS dtmroc 4]
******** 8. 0.648463    9. 0.654536  || 1.302999 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.648 A): ELMB channel 8 [I AN_POS dtmroc 5]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.655 A): ELMB channel 9 [I AN_POS dtmroc 5]
******** 10. 0.002294    11. 0.014544  || 0.016839 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.002 A): ELMB channel 10 [I AN_POS dtmroc 6]
******** 12. 0.722340    13. 0.722749  || 1.445089 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.723 A): ELMB channel 13 [I AN_NEG dtmroc 1]
******** 14. 0.354872    15. 0.354277  || 0.709149 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.355 A): ELMB channel 14 [I AN_NEG dtmroc 2]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.354 A): ELMB channel 15 [I AN_NEG dtmroc 2]
******** 16. 0.266204    17. 0.276059  || 0.542263 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.266 A): ELMB channel 16 [I AN_NEG dtmroc 3]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.276 A): ELMB channel 17 [I AN_NEG dtmroc 3]
******** 18. 0.370473    19. 0.377691  || 0.748165 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.370 A): ELMB channel 18 [I AN_NEG dtmroc 4]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.378 A): ELMB channel 19 [I AN_NEG dtmroc 4]
******** 20. 0.518624    21. 0.523052  || 1.041676 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.519 A): ELMB channel 20 [I AN_NEG dtmroc 5]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.523 A): ELMB channel 21 [I AN_NEG dtmroc 5]
******** 22. 0.002183    23. 0.008242  || 0.010426 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.002 A): ELMB channel 22 [I AN_NEG dtmroc 6]
******** 24. 0.566737    25. 0.557169  || 1.123906 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.557 A): ELMB channel 25 [I DIGI dtmroc 9]
******** 26. 0.652181    27. 0.664138  || 1.316319 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.664 A): ELMB channel 27 [I DIGI dtmroc 10]
******** 28. 0.662959    29. 0.661315  || 1.324274 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.661 A): ELMB channel 29 [I DIGI dtmroc 11]
******** 30. 0.647543    31. 0.649312  || 1.296855 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.649 A): ELMB channel 31 [I DIGI dtmroc 12]
******** 32. 0.577760    33. 0.590763  || 1.168523 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.591 A): ELMB channel 33 [I DIGI dtmroc 13]
******** 34. 0.583307    35. 0.586594  || 1.169901 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.587 A): ELMB channel 35 [I DIGI dtmroc 14]

Voltage channels:
ELMB Channel 36:   3.744 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.758 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.752 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.751 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.755 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.742 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.770 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.759 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.567 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.569 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.586 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.582 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.580 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.573 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.596 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.589 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.275 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.252 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.245 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.232 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.249 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.233 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.238 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.235 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 48.02
Channel 61, temperature 49.87
Channel 62, temperature 47.65
Channel 63, temperature 48.23

Set all regulators ON

Current channels
******** 0. 0.393610    1. 0.404787  || 0.798397 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.394 A): ELMB channel 0 [I AN_POS dtmroc 1]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.405 A): ELMB channel 1 [I AN_POS dtmroc 1]
******** 2. 0.245116    3. 0.252199  || 0.497315 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.245 A): ELMB channel 2 [I AN_POS dtmroc 2]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.252 A): ELMB channel 3 [I AN_POS dtmroc 2]
******** 4. 0.049121    5. 0.062590  || 0.111712 
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.063 A): ELMB channel 5 [I AN_POS dtmroc 3]
******** 6. 0.202049    7. 0.212006  || 0.414054 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.202 A): ELMB channel 6 [I AN_POS dtmroc 4]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.212 A): ELMB channel 7 [I AN_POS dtmroc 4]
******** 8. 0.121401    9. 0.116529  || 0.237930 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.121 A): ELMB channel 8 [I AN_POS dtmroc 5]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.117 A): ELMB channel 9 [I AN_POS dtmroc 5]
******** 10. 0.760535    11. 0.775912  || 1.536447 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.761 A): ELMB channel 10 [I AN_POS dtmroc 6]
******** 12. 0.003923    13. 0.008679  || 0.012602 
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.009 A): ELMB channel 13 [I AN_NEG dtmroc 1]
******** 14. 0.377528    15. 0.378023  || 0.755551 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.378 A): ELMB channel 14 [I AN_NEG dtmroc 2]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.378 A): ELMB channel 15 [I AN_NEG dtmroc 2]
******** 16. 0.468177    17. 0.460316  || 0.928492 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.468 A): ELMB channel 16 [I AN_NEG dtmroc 3]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.460 A): ELMB channel 17 [I AN_NEG dtmroc 3]
******** 18. 0.364659    19. 0.358157  || 0.722816 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.365 A): ELMB channel 18 [I AN_NEG dtmroc 4]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.358 A): ELMB channel 19 [I AN_NEG dtmroc 4]
******** 20. 0.216576    21. 0.214412  || 0.430988 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.217 A): ELMB channel 20 [I AN_NEG dtmroc 5]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.214 A): ELMB channel 21 [I AN_NEG dtmroc 5]
******** 22. 0.727336    23. 0.727077  || 1.454413 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.727 A): ELMB channel 22 [I AN_NEG dtmroc 6]
******** 24. 0.107059    25. 0.112266  || 0.219325 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.107 A): ELMB channel 24 [I DIGI dtmroc 9]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.112 A): ELMB channel 25 [I DIGI dtmroc 9]
******** 26. 0.005460    27. 0.000628  || 0.006088 
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.001 A): ELMB channel 27 [I DIGI dtmroc 10]
******** 28. 0.000314    29. 0.001103  || 0.001417 
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.001 A): ELMB channel 29 [I DIGI dtmroc 11]
******** 30. 0.004827    31. 0.001810  || 0.006636 
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.002 A): ELMB channel 31 [I DIGI dtmroc 12]
******** 32. 0.089347    33. 0.078846  || 0.168193 
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.079 A): ELMB channel 33 [I DIGI dtmroc 13]
******** 34. 0.072468    35. 0.070295  || 0.142763 
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.070 A): ELMB channel 35 [I DIGI dtmroc 14]

Voltage channels:
ELMB Channel 36:   3.738 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.760 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.763 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.752 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.762 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.764 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.718 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.718 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.628 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.569 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.578 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.575 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.578 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.588 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.577 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.581 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.252 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.270 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.261 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.264 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.232 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.229 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.222 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.232 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 47.99
Channel 61, temperature 49.83
Channel 62, temperature 47.58
Channel 63, temperature 48.21

Set all regulators ON

Current channels
******** 0. 0.041839    1. 0.051297  || 0.093136 
checkAnalogReadWheelA: ERROR: not in range ON  (0.093 A): ELMB channels 0 1 [I AN_POS dtmroc 1]
******** 2. 0.041353    3. 0.038708  || 0.080061 
checkAnalogReadWheelA: ERROR: not in range ON  (0.080 A): ELMB channels 2 3 [I AN_POS dtmroc 2]
******** 4. 0.033743    5. 0.053326  || 0.087069 
checkAnalogReadWheelA: ERROR: not in range ON  (0.087 A): ELMB channels 4 5 [I AN_POS dtmroc 3]
******** 6. 0.045514    7. 0.036787  || 0.082301 
checkAnalogReadWheelA: ERROR: not in range ON  (0.082 A): ELMB channels 6 7 [I AN_POS dtmroc 4]
******** 8. 0.039998    9. 0.042301  || 0.082299 
checkAnalogReadWheelA: ERROR: not in range ON  (0.082 A): ELMB channels 8 9 [I AN_POS dtmroc 5]
******** 10. 0.002095    11. 0.014556  || 0.016651 
checkAnalogReadWheelA: ERROR: not in range ON  (0.017 A): ELMB channels 10 11 [I AN_POS dtmroc 6]
******** 12. 0.003926    13. 0.006193  || 0.010119 
checkAnalogReadWheelA: ERROR: not in range ON  (0.010 A): ELMB channels 12 13 [I AN_NEG dtmroc 1]
******** 14. 0.051544    15. 0.053984  || 0.105528 
checkAnalogReadWheelA: ERROR: not in range ON  (0.106 A): ELMB channels 14 15 [I AN_NEG dtmroc 2]
******** 16. 0.000050    17. 0.011119  || 0.011170 
checkAnalogReadWheelA: ERROR: not in range ON  (0.011 A): ELMB channels 16 17 [I AN_NEG dtmroc 3]
******** 18. 0.049743    19. 0.054677  || 0.104420 
checkAnalogReadWheelA: ERROR: not in range ON  (0.104 A): ELMB channels 18 19 [I AN_NEG dtmroc 4]
******** 20. 0.007829    21. 0.003354  || 0.011183 
checkAnalogReadWheelA: ERROR: not in range ON  (0.011 A): ELMB channels 20 21 [I AN_NEG dtmroc 5]
******** 22. 0.062012    23. 0.058867  || 0.120880 
checkAnalogReadWheelA: ERROR: not in range ON  (0.121 A): ELMB channels 22 23 [I AN_NEG dtmroc 6]
******** 24. 0.490277    25. 0.480921  || 0.971198 
checkAnalogReadWheelA: ERROR: not in range ON  (0.971 A): ELMB channels 24 25 [I DIGI dtmroc 9]
******** 26. 0.005112    27. 0.001427  || 0.006539 
checkAnalogReadWheelA: ERROR: not in range ON  (0.007 A): ELMB channels 26 27 [I DIGI dtmroc 10]
******** 28. 0.000185    29. 0.000087  || 0.000272 
checkAnalogReadWheelA: ERROR: not in range ON  (0.000 A): ELMB channels 28 29 [I DIGI dtmroc 11]
******** 30. 0.004523    31. 0.002890  || 0.007413 
checkAnalogReadWheelA: ERROR: not in range ON  (0.007 A): ELMB channels 30 31 [I DIGI dtmroc 12]
******** 32. 0.327164    33. 0.340605  || 0.667769 
******** 34. 0.339017    35. 0.342656  || 0.681673 

Voltage channels:
ELMB Channel 36:   3.752 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.766 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.766 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.760 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.766 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.764 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.770 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.760 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.624 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.577 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.591 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.588 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.591 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.596 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.597 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.590 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.276 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.266 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.267 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.260 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.254 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.242 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.242 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.245 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 47.97
Channel 61, temperature 49.80
Channel 62, temperature 47.69
Channel 63, temperature 48.24

 TEST interrupted at 01.04.   9 2012:17:30 because of errros, after 12 loops
