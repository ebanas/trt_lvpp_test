============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 18
CALIBRATION FILE: /det/dcs/lvtemp/lvppCalib/card_018/LVPP_CALIB_018.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
                temperature range: <15.10 C - 60.00 C> 

TEST is started: 03.09.2012 12:56:15

ELMB <1> init OK
ELMB NB: 1 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.

Current channels
******** 0. 0.003821    1. 0.003640  || 0.007462 
******** 2. 0.003010    3. 0.002840  || 0.005851 
******** 4. 0.000362    5. 0.000553  || 0.000915 
******** 6. 0.000461    7. 0.000269  || 0.000730 
******** 8. 0.000368    9. 0.000076  || 0.000444 
******** 10. 0.000482    11. 0.000147  || 0.000629 
******** 12. 0.003011    13. 0.003251  || 0.006262 
******** 14. 0.003209    15. 0.003157  || 0.006365 
******** 16. 0.006132    17. 0.002846  || 0.008978 
******** 18. 0.000297    19. 0.003218  || 0.003514 
******** 20. 0.000091    21. 0.006072  || 0.006163 
******** 22. 0.000000    23. 0.000003  || 0.000003 
******** 24. 0.002685    25. 0.000285  || 0.002970 
******** 26. 0.000263    27. 0.000005  || 0.000268 
******** 28. 0.002949    29. 0.002687  || 0.005636 
******** 30. 0.002842    31. 0.002883  || 0.005724 
******** 32. 0.002887    33. 0.002561  || 0.005448 
******** 34. 0.006108    35. 0.005936  || 0.012044 

Voltage channels:
ELMB Channel 36:   0.005 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.004 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.004 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.004 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.004 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.006 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.005 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.005 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.001 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.000 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.000 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.003 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.003 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.002 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 42.50
Channel 61, temperature 42.87
Channel 62, temperature 41.57
Channel 63, temperature 41.19

Set all regulators ON

Current channels
******** 0. 0.778613    1. 0.004313  || 0.782926 
******** 2. 0.747635    3. 0.005487  || 0.753122 
******** 4. 0.777113    5. 0.011846  || 0.788960 
******** 6. 0.773434    7. 0.020711  || 0.794145 
******** 8. 0.731885    9. 0.014421  || 0.746306 
******** 10. 0.748237    11. 0.008021  || 0.756258 
******** 12. 0.002851    13. 0.003441  || 0.006293 
ERROR: curr not in range ON (1)  (0.003 A): ELMB channel 12 [I AN_NEG dtmroc 1]
******** 14. 0.000339    15. 0.002512  || 0.002850 
ERROR: curr not in range ON (1)  (0.000 A): ELMB channel 14 [I AN_NEG dtmroc 2]
******** 16. 0.003516    17. 0.005481  || 0.008997 
ERROR: curr not in range ON (1)  (0.004 A): ELMB channel 16 [I AN_NEG dtmroc 3]
******** 18. 0.011179    19. 0.003149  || 0.014329 
ERROR: curr not in range ON (1)  (0.011 A): ELMB channel 18 [I AN_NEG dtmroc 4]
******** 20. 0.001737    21. 0.007511  || 0.009248 
ERROR: curr not in range ON (1)  (0.002 A): ELMB channel 20 [I AN_NEG dtmroc 5]
******** 22. 0.004635    23. 0.005909  || 0.010544 
ERROR: curr not in range ON (1)  (0.005 A): ELMB channel 22 [I AN_NEG dtmroc 6]
******** 24. 0.002729    25. 0.003245  || 0.005974 
ERROR: curr not in range ON (1)  (0.003 A): ELMB channel 24 [I DIGI dtmroc 9]
******** 26. 0.002796    27. 0.003004  || 0.005800 
ERROR: curr not in range ON (1)  (0.003 A): ELMB channel 26 [I DIGI dtmroc 10]
******** 28. 0.002949    29. 0.000313  || 0.003262 
ERROR: curr not in range ON (1)  (0.003 A): ELMB channel 28 [I DIGI dtmroc 11]
******** 30. 0.000136    31. 0.000101  || 0.000237 
ERROR: curr not in range ON (1)  (0.000 A): ELMB channel 30 [I DIGI dtmroc 12]
******** 32. 0.002925    33. 0.002687  || 0.005612 
ERROR: curr not in range ON (1)  (0.003 A): ELMB channel 32 [I DIGI dtmroc 13]
******** 34. 0.003006    35. 0.000110  || 0.003116 
ERROR: curr not in range ON (1)  (0.003 A): ELMB channel 34 [I DIGI dtmroc 14]

Voltage channels:
ELMB Channel 36:   3.794 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.806 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.802 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.791 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.798 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.784 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.817 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.804 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.078 V   [V AN_NEG dtmroc 1]
ERROR: voltage too LOW (0.078 V) in ELMB channel 44 [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.075 V   [V AN_NEG dtmroc 2]
ERROR: voltage too LOW (0.075 V) in ELMB channel 45 [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.076 V   [V AN_NEG dtmroc 3]
ERROR: voltage too LOW (0.076 V) in ELMB channel 46 [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.068 V   [V AN_NEG dtmroc 4]
ERROR: voltage too LOW (0.068 V) in ELMB channel 47 [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.019 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (0.019 V) in ELMB channel 48 [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.017 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (0.017 V) in ELMB channel 49 [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.027 V   [V AN_NEG dtmroc 6]
ERROR: voltage too LOW (0.027 V) in ELMB channel 50 [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.022 V   [V AN_NEG dtmroc 6]
ERROR: voltage too LOW (0.022 V) in ELMB channel 51 [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ERROR: voltage too LOW (0.002 V) in ELMB channel 52 [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ERROR: voltage too LOW (0.002 V) in ELMB channel 53 [V DIGI dtmroc 10]
ELMB Channel 54:   0.003 V   [V DIGI dtmroc 11]
ERROR: voltage too LOW (0.003 V) in ELMB channel 54 [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ERROR: voltage too LOW (0.002 V) in ELMB channel 55 [V DIGI dtmroc 12]
ELMB Channel 56:   0.001 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (0.001 V) in ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   0.002 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (0.002 V) in ELMB channel 57 [V DIGI dtmroc 13]
ELMB Channel 58:   0.002 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (0.002 V) in ELMB channel 58 [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (0.001 V) in ELMB channel 59 [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 42.44
Channel 61, temperature 42.83
Channel 62, temperature 41.50
Channel 63, temperature 41.13

Set all regulators ON

Current channels
******** 0. 0.013784    1. 0.768631  || 0.782415 
******** 2. 0.018279    3. 0.760722  || 0.779001 
******** 4. 0.008392    5. 0.760579  || 0.768972 
******** 6. 0.016136    7. 0.748580  || 0.764716 
******** 8. 0.028071    9. 0.754415  || 0.782486 
******** 10. 0.015075    11. 0.756903  || 0.771977 
******** 12. 0.000124    13. 0.002880  || 0.003005 
ERROR: curr not in range ON (2) (0.003 A): ELMB channel 13 [I AN_NEG dtmroc 1]
******** 14. 0.006796    15. 0.740074  || 0.746871 
******** 16. 0.003438    17. 0.005107  || 0.008546 
ERROR: curr not in range ON (2) (0.005 A): ELMB channel 17 [I AN_NEG dtmroc 3]
******** 18. 0.008793    19. 0.003600  || 0.012393 
ERROR: curr not in range ON (2) (0.004 A): ELMB channel 19 [I AN_NEG dtmroc 4]
******** 20. 0.001432    21. 0.010229  || 0.011661 
ERROR: curr not in range ON (2) (0.010 A): ELMB channel 21 [I AN_NEG dtmroc 5]
******** 22. 0.001788    23. 0.002835  || 0.004623 
ERROR: curr not in range ON (2) (0.003 A): ELMB channel 23 [I AN_NEG dtmroc 6]
******** 24. 0.000361    25. 0.003326  || 0.003687 
ERROR: curr not in range ON (2) (0.003 A): ELMB channel 25 [I DIGI dtmroc 9]
******** 26. 0.000204    27. 0.000004  || 0.000208 
ERROR: curr not in range ON (2) (0.000 A): ELMB channel 27 [I DIGI dtmroc 10]
******** 28. 0.000032    29. 0.000195  || 0.000227 
ERROR: curr not in range ON (2) (0.000 A): ELMB channel 29 [I DIGI dtmroc 11]
******** 30. 0.000181    31. 0.000134  || 0.000316 
ERROR: curr not in range ON (2) (0.000 A): ELMB channel 31 [I DIGI dtmroc 12]
******** 32. 0.000113    33. 0.002938  || 0.003051 
ERROR: curr not in range ON (2) (0.003 A): ELMB channel 33 [I DIGI dtmroc 13]
******** 34. 0.003000    35. 0.002836  || 0.005836 
ERROR: curr not in range ON (2) (0.003 A): ELMB channel 35 [I DIGI dtmroc 14]

Voltage channels:
ELMB Channel 36:   3.782 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.800 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.805 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.787 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.798 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.799 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.757 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.754 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.065 V   [V AN_NEG dtmroc 1]
ERROR: voltage too LOW (0.065 V) in ELMB channel 44 [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.520 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.064 V   [V AN_NEG dtmroc 3]
ERROR: voltage too LOW (0.064 V) in ELMB channel 46 [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.058 V   [V AN_NEG dtmroc 4]
ERROR: voltage too LOW (0.058 V) in ELMB channel 47 [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.016 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (0.016 V) in ELMB channel 48 [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.013 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (0.013 V) in ELMB channel 49 [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.022 V   [V AN_NEG dtmroc 6]
ERROR: voltage too LOW (0.022 V) in ELMB channel 50 [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.019 V   [V AN_NEG dtmroc 6]
ERROR: voltage too LOW (0.019 V) in ELMB channel 51 [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.003 V   [V DIGI dtmroc 9]
ERROR: voltage too LOW (0.003 V) in ELMB channel 52 [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ERROR: voltage too LOW (0.002 V) in ELMB channel 53 [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ERROR: voltage too LOW (0.002 V) in ELMB channel 54 [V DIGI dtmroc 11]
ELMB Channel 55:   0.003 V   [V DIGI dtmroc 12]
ERROR: voltage too LOW (0.003 V) in ELMB channel 55 [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (0.002 V) in ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   0.000 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (0.000 V) in ELMB channel 57 [V DIGI dtmroc 13]
ELMB Channel 58:   0.000 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (0.000 V) in ELMB channel 58 [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (0.001 V) in ELMB channel 59 [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 42.37
Channel 61, temperature 42.83
Channel 62, temperature 41.42
Channel 63, temperature 41.11

Set all regulators ON

 TEST is finished: temperature,current or voltage error. 03.09.2012 12:56:50
