============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 18
CALIBRATION FILE: /det/dcs/lvtemp/lvppCalib/card_018/LVPP_CALIB_018.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
                temperature range: <15.10 C - 60.00 C> 

TEST is started: 06.09.2012 13:48:56

ELMB <1> init OK
ELMB NB: 1 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.

Current channels
******** 0. 0.003288    1. 0.003154  || 0.006442 
******** 2. 0.002746    3. 0.002226  || 0.004973 
******** 4. 0.000332    5. 0.002683  || 0.003015 
******** 6. 0.000185    7. 0.000829  || 0.001014 
******** 8. 0.000507    9. 0.000833  || 0.001340 
******** 10. 0.001506    11. 0.000167  || 0.001673 
******** 12. 0.003005    13. 0.000950  || 0.003956 
******** 14. 0.000365    15. 0.000383  || 0.000748 
******** 16. 0.005595    17. 0.002532  || 0.008127 
******** 18. 0.000175    19. 0.002697  || 0.002872 
******** 20. 0.002951    21. 0.009120  || 0.012071 
******** 22. 0.000095    23. 0.003006  || 0.003101 
******** 24. 0.000374    25. 0.002879  || 0.003253 
******** 26. 0.000262    27. 0.002404  || 0.002666 
******** 28. 0.000036    29. 0.000527  || 0.000563 
******** 30. 0.002861    31. 0.000501  || 0.003362 
******** 32. 0.002870    33. 0.002881  || 0.005751 
******** 34. 0.002979    35. 0.002441  || 0.005420 

Voltage channels:
ELMB Channel 36:   0.005 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.004 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.005 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.004 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.006 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.006 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.004 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.005 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.003 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.000 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.001 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.002 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 40.83
Channel 61, temperature 41.75
Channel 62, temperature 39.94
Channel 63, temperature 40.61

Set all regulators ON

Current channels
******** 0. 0.365402    1. 0.368535  || 0.733936 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.365 A): ELMB channel 0 [I AN_POS dtmroc 1]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.369 A): ELMB channel 1 [I AN_POS dtmroc 1]
******** 2. 0.528973    3. 0.530155  || 1.059128 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.529 A): ELMB channel 2 [I AN_POS dtmroc 2]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.530 A): ELMB channel 3 [I AN_POS dtmroc 2]
******** 4. 0.721039    5. 0.721982  || 1.443021 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.722 A): ELMB channel 5 [I AN_POS dtmroc 3]
******** 6. 0.553891    7. 0.555484  || 1.109375 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.554 A): ELMB channel 6 [I AN_POS dtmroc 4]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.555 A): ELMB channel 7 [I AN_POS dtmroc 4]
******** 8. 0.662541    9. 0.657522  || 1.320063 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.658 A): ELMB channel 9 [I AN_POS dtmroc 5]
******** 10. 0.003048    11. 0.003339  || 0.006387 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.003 A): ELMB channel 10 [I AN_POS dtmroc 6]
******** 12. 0.723401    13. 0.727527  || 1.450928 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.728 A): ELMB channel 13 [I AN_NEG dtmroc 1]
******** 14. 0.349002    15. 0.349239  || 0.698241 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.349 A): ELMB channel 14 [I AN_NEG dtmroc 2]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.349 A): ELMB channel 15 [I AN_NEG dtmroc 2]
******** 16. 0.283878    17. 0.280461  || 0.564339 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.284 A): ELMB channel 16 [I AN_NEG dtmroc 3]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.280 A): ELMB channel 17 [I AN_NEG dtmroc 3]
******** 18. 0.373356    19. 0.379908  || 0.753264 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.373 A): ELMB channel 18 [I AN_NEG dtmroc 4]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.380 A): ELMB channel 19 [I AN_NEG dtmroc 4]
******** 20. 0.527252    21. 0.527320  || 1.054573 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.527 A): ELMB channel 20 [I AN_NEG dtmroc 5]
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.527 A): ELMB channel 21 [I AN_NEG dtmroc 5]
******** 22. 0.005236    23. 0.008469  || 0.013705 
checkAnalogReadWheelA: ERROR: curr not in range ON (1)  (0.005 A): ELMB channel 22 [I AN_NEG dtmroc 6]
******** 24. 0.563412    25. 0.562802  || 1.126214 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.563 A): ELMB channel 25 [I DIGI dtmroc 9]
******** 26. 0.651142    27. 0.654515  || 1.305657 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.655 A): ELMB channel 27 [I DIGI dtmroc 10]
******** 28. 0.657226    29. 0.656607  || 1.313833 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.657 A): ELMB channel 29 [I DIGI dtmroc 11]
******** 30. 0.647329    31. 0.650811  || 1.298140 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.651 A): ELMB channel 31 [I DIGI dtmroc 12]
******** 32. 0.580270    33. 0.575131  || 1.155400 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.575 A): ELMB channel 33 [I DIGI dtmroc 13]
******** 34. 0.573185    35. 0.570486  || 1.143671 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.570 A): ELMB channel 35 [I DIGI dtmroc 14]

Voltage channels:
ELMB Channel 36:   3.744 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.758 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.752 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.750 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.755 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.742 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.770 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.758 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.557 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.558 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.577 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.572 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.570 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.560 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.586 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.578 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.273 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.249 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.243 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.227 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.246 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.230 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.235 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.233 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 40.77
Channel 61, temperature 41.66
Channel 62, temperature 39.89
Channel 63, temperature 40.53

Set all regulators ON

Current channels
******** 0. 0.400804    1. 0.397201  || 0.798006 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.401 A): ELMB channel 0 [I AN_POS dtmroc 1]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.397 A): ELMB channel 1 [I AN_POS dtmroc 1]
******** 2. 0.240125    3. 0.241646  || 0.481771 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.240 A): ELMB channel 2 [I AN_POS dtmroc 2]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.242 A): ELMB channel 3 [I AN_POS dtmroc 2]
******** 4. 0.056329    5. 0.052419  || 0.108748 
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.052 A): ELMB channel 5 [I AN_POS dtmroc 3]
******** 6. 0.211947    7. 0.213340  || 0.425287 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.212 A): ELMB channel 6 [I AN_POS dtmroc 4]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.213 A): ELMB channel 7 [I AN_POS dtmroc 4]
******** 8. 0.104257    9. 0.107407  || 0.211664 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.104 A): ELMB channel 8 [I AN_POS dtmroc 5]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.107 A): ELMB channel 9 [I AN_POS dtmroc 5]
******** 10. 0.756391    11. 0.751990  || 1.508381 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.756 A): ELMB channel 10 [I AN_POS dtmroc 6]
******** 12. 0.000253    13. 0.001701  || 0.001954 
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.002 A): ELMB channel 13 [I AN_NEG dtmroc 1]
******** 14. 0.380176    15. 0.376984  || 0.757160 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.380 A): ELMB channel 14 [I AN_NEG dtmroc 2]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.377 A): ELMB channel 15 [I AN_NEG dtmroc 2]
******** 16. 0.444215    17. 0.447057  || 0.891272 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.444 A): ELMB channel 16 [I AN_NEG dtmroc 3]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.447 A): ELMB channel 17 [I AN_NEG dtmroc 3]
******** 18. 0.352656    19. 0.352990  || 0.705646 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.353 A): ELMB channel 18 [I AN_NEG dtmroc 4]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.353 A): ELMB channel 19 [I AN_NEG dtmroc 4]
******** 20. 0.204899    21. 0.207101  || 0.412001 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.205 A): ELMB channel 20 [I AN_NEG dtmroc 5]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.207 A): ELMB channel 21 [I AN_NEG dtmroc 5]
******** 22. 0.718214    23. 0.720755  || 1.438969 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.718 A): ELMB channel 22 [I AN_NEG dtmroc 6]
******** 24. 0.104233    25. 0.103613  || 0.207846 
checkAnalogReadWheelA: ERROR: curr too high REG OFF (0.104 A): ELMB channel 24 [I DIGI dtmroc 9]
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.104 A): ELMB channel 25 [I DIGI dtmroc 9]
******** 26. 0.000522    27. 0.003903  || 0.004425 
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.004 A): ELMB channel 27 [I DIGI dtmroc 10]
******** 28. 0.000676    29. 0.000403  || 0.001079 
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.000 A): ELMB channel 29 [I DIGI dtmroc 11]
******** 30. 0.001185    31. 0.000282  || 0.001467 
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.000 A): ELMB channel 31 [I DIGI dtmroc 12]
******** 32. 0.083776    33. 0.085639  || 0.169415 
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.086 A): ELMB channel 33 [I DIGI dtmroc 13]
******** 34. 0.079499    35. 0.080062  || 0.159561 
checkAnalogReadWheelA: ERROR: curr not in range ON (2) (0.080 A): ELMB channel 35 [I DIGI dtmroc 14]

Voltage channels:
ELMB Channel 36:   3.739 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.760 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.764 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.751 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.763 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.764 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.717 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.717 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.615 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.557 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.567 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.564 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.567 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.577 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.566 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.570 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.249 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.268 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.259 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.260 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.229 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.226 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.220 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.231 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 40.73
Channel 61, temperature 41.63
Channel 62, temperature 39.89
Channel 63, temperature 40.53

Set all regulators ON

Current channels
******** 0. 0.048472    1. 0.050990  || 0.099462 
checkAnalogReadWheelA: ERROR: not in range ON  (0.099 A): ELMB channels 0 1 [I AN_POS dtmroc 1]
******** 2. 0.057652    3. 0.059936  || 0.117588 
checkAnalogReadWheelA: ERROR: not in range ON  (0.118 A): ELMB channels 2 3 [I AN_POS dtmroc 2]
******** 4. 0.041338    5. 0.045345  || 0.086683 
checkAnalogReadWheelA: ERROR: not in range ON  (0.087 A): ELMB channels 4 5 [I AN_POS dtmroc 3]
******** 6. 0.044808    7. 0.041550  || 0.086358 
checkAnalogReadWheelA: ERROR: not in range ON  (0.086 A): ELMB channels 6 7 [I AN_POS dtmroc 4]
******** 8. 0.046728    9. 0.045635  || 0.092363 
checkAnalogReadWheelA: ERROR: not in range ON  (0.092 A): ELMB channels 8 9 [I AN_POS dtmroc 5]
******** 10. 0.000248    11. 0.003317  || 0.003564 
checkAnalogReadWheelA: ERROR: not in range ON  (0.004 A): ELMB channels 10 11 [I AN_POS dtmroc 6]
******** 12. 0.000256    13. 0.002317  || 0.002573 
checkAnalogReadWheelA: ERROR: not in range ON  (0.003 A): ELMB channels 12 13 [I AN_NEG dtmroc 1]
******** 14. 0.051257    15. 0.049874  || 0.101131 
checkAnalogReadWheelA: ERROR: not in range ON  (0.101 A): ELMB channels 14 15 [I AN_NEG dtmroc 2]
******** 16. 0.060494    17. 0.058202  || 0.118695 
checkAnalogReadWheelA: ERROR: not in range ON  (0.119 A): ELMB channels 16 17 [I AN_NEG dtmroc 3]
******** 18. 0.045044    19. 0.043673  || 0.088717 
checkAnalogReadWheelA: ERROR: not in range ON  (0.089 A): ELMB channels 18 19 [I AN_NEG dtmroc 4]
******** 20. 0.003762    21. 0.003947  || 0.007709 
checkAnalogReadWheelA: ERROR: not in range ON  (0.008 A): ELMB channels 20 21 [I AN_NEG dtmroc 5]
******** 22. 0.025364    23. 0.025040  || 0.050404 
checkAnalogReadWheelA: ERROR: not in range ON  (0.050 A): ELMB channels 22 23 [I AN_NEG dtmroc 6]
******** 24. 0.480905    25. 0.483537  || 0.964442 
checkAnalogReadWheelA: ERROR: not in range ON  (0.964 A): ELMB channels 24 25 [I DIGI dtmroc 9]
******** 26. 0.003184    27. 0.001799  || 0.004983 
checkAnalogReadWheelA: ERROR: not in range ON  (0.005 A): ELMB channels 26 27 [I DIGI dtmroc 10]
******** 28. 0.000540    29. 0.001491  || 0.002031 
checkAnalogReadWheelA: ERROR: not in range ON  (0.002 A): ELMB channels 28 29 [I DIGI dtmroc 11]
******** 30. 0.000723    31. 0.001388  || 0.002111 
checkAnalogReadWheelA: ERROR: not in range ON  (0.002 A): ELMB channels 30 31 [I DIGI dtmroc 12]
******** 32. 0.323477    33. 0.322111  || 0.645588 
******** 34. 0.322894    35. 0.320439  || 0.643332 

Voltage channels:
ELMB Channel 36:   3.749 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.769 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.765 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.759 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.767 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.768 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.769 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.759 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.618 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.567 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.582 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.580 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.580 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.584 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.586 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.579 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.273 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.263 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.266 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.267 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.251 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.240 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.240 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.243 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 40.69
Channel 61, temperature 41.77
Channel 62, temperature 40.11
Channel 63, temperature 40.59

 TEST interrupted at 01.06.   9 2012:13:49 because of errros, after 47 loops
