//
// calibration program for LVPP boards,
// created 2008 (B. Kisielewski, E. Banas) and embedded in 'lvppCompleteTest' program
//
// modified for Linux: September 2012 (E. Banas) as standalone program
//

#include "lvppSharedLib.h"
#include "lvppTest.h"
#include "lvppTestLib.h"

// global wide declarations
int portCGlobal; // keeps content of port C - global wide 

void printUsage(char *programName)
{

  printf ("Program %s\n", programName);
  printf ("Usage: %s -c 'CAN port' -e ELMB ID -b lvpp card number -v \n");
  
}

main (int argc, char **argv)
{
int channel;
int handle;
int elmbNb;
char dtmrocs[64];
int cardNb;
unsigned char elmbSerialNumber[10];

int status;
lvppSharedLib zasilacz;
int elmbTimeOut = 10000;
 
char textBuf[64];
FILE  *logFile;
FILE *calibFile;

// time
time_t actualTime;
struct tm *brokenTime;

int opt;
bool verbose = false;
 
 printf ("=====================================================\n");			
 printf ("Calibration: card TYPE Wheel A\n");
 printf ("=====================================================\n");	

 while ((opt = getopt(argc, argv, "hvc:e:b:")) != -1) { 
   switch (opt) {
     case 'c':
       channel = atoi(optarg);
       if ((channel > 5) && (channel < 0)) {
	 printf ("Bad CAN port: %d - CAN Port should be 0 or 1\n", channel);
	 exit(1);
       }
       break;
     case 'e':
       elmbNb = atoi(optarg);
       if ((elmbNb < 0) && (elmbNb > 64)) {
	 printf ("Bad ELMB ID: %d \n", elmbNb);
	 exit(1);
       }
       break;
     case 'b':
       cardNb = atoi(optarg);
       if ((cardNb < 1) || (elmbNb > 564)) {
	 printf ("Bad LVPP card number: %d \n", elmbNb);
	 exit(1);
       }
       break;
     case 'v':
       verbose = true;
       break;
     default:
       printUsage(basename(argv[0]));
       exit(1);
       break;
   }
 }

       	if (optind < 7) {
	  printf ("optind %d\n", optind);
		printUsage(basename(argv[0]));
	 	exit(0);
	}
	
 portCGlobal=0;

 printf ("CAN PORT %d  ELMB ID %d   Card Number %d \n", channel, elmbNb, cardNb);

 // exit (0);

 
// create directory for a log file
 sprintf (textBuf, "/localdisk/LVPP_TEST/lvtemp/lvppTestData/card_%03d", cardNb);

 if ((mkdir (textBuf, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH))) {  
   if (errno != 17) { 
     printf ("directory not created : %s\n", strerror (errno )); 
   }
   else
     printf ("Directory %s created.\n", textBuf); 
 } 

// open log file

 actualTime = time(NULL);
 brokenTime = localtime(&actualTime); 
 sprintf (textBuf, "/localdisk/LVPP_TEST/lvtemp/lvppTestData/card_%03d/LVPP_CALIB_LOG_%02d%02dH%02d%02d.txt",
	  cardNb, brokenTime->tm_mday, brokenTime->tm_mon+1,
	   brokenTime->tm_hour, brokenTime->tm_min);

 if (!(logFile =fopen(textBuf,"w"))) {
   printf ("File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
   return 0;
 }

// init CANBUS, ELMB and DTMROCs

  if ((status = initLVPP (channel, elmbNb, logFile, &handle)) < 0) {
    printf ("initLVPP exit with error: %d\n", status);
    return status;
  }  
 printf ("Calibration log file name: '%s'\n", textBuf);


// create directory for a calibration file
 sprintf (textBuf, "/localdisk/LVPP_TEST/lvtemp/lvppCalib/card_%03d", cardNb);

 if ((mkdir (textBuf, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH))) {  
   if (errno != 17) { 
     printf ("directory not created : %s\n", strerror (errno )); 
   }
   else
     printf ("Directory %s created.\n", textBuf); 
 } 

  // open calib file
  sprintf (textBuf, "/localdisk/LVPP_TEST/lvtemp/lvppCalib/card_%03d/LVPP_CALIB_%03d.csv", cardNb, cardNb);

  if (!(calibFile =fopen(textBuf,"w"))) {
    printf ("File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
    fprintf (logFile, "File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
    fclose (logFile);
    return 0;
  }
  actualTime = time(NULL);
  brokenTime = localtime(&actualTime); 
  //  srand (brokenTime->seconds );

  fprintf (logFile, "=====================================================\n");			
  fprintf (logFile, "Calibration: card TYPE Wheel A\n");
  fprintf (logFile, "=====================================================\n");			
  printf ("Calibration File Name: '%s'\n\n", textBuf); 
  fprintf (logFile, "Calibration File Name: %s\n\n", textBuf); 
  printf ("Calibration started: %02d.%02d.%4d %02d:%02d:%02d\n\n",
	  brokenTime->tm_mday, brokenTime->tm_mon+1, brokenTime->tm_year+1900, 
	  brokenTime->tm_hour, brokenTime->tm_min,
	  brokenTime->tm_sec); 
  fprintf (logFile, "Calibration started: %02d.%02d.%4d %02d:%02d:%02d\n",
	   brokenTime->tm_mday, brokenTime->tm_mon+1, brokenTime->tm_year+1900, 
	   brokenTime->tm_hour, brokenTime->tm_min,
	   brokenTime->tm_sec); 
			
  fprintf (calibFile, "Calibration done: %02d.%02d.%4d %02d:%02d:%02d\n",
	   brokenTime->tm_mday, brokenTime->tm_mon+1, brokenTime->tm_year+1900, 
	   brokenTime->tm_hour, brokenTime->tm_min,
	   brokenTime->tm_sec);

  // read ELMB serial number
  if ((status = zasilacz.Get_ELMB_SerialNumber (handle, elmbNb, elmbSerialNumber)) < 0) {
    printf ("Get_ELMB_SerialNumber exit with error: %d\n", status);
    printf ("%s\n", zasilacz.GetErrorText ());
  }
  fprintf (logFile, "ELMB serial number: %s\n", elmbSerialNumber);
  fprintf (calibFile, "%s\n", elmbSerialNumber);
  fprintf (calibFile, "Current channel, calibration factor\n");
			
  // set all regulators ON
  if (checkInhibitsBroadcast (handle, elmbNb, portCGlobal,  3, elmbTimeOut, logFile) == false) {
  // make calibration
    calibCurrentWheelA (handle, elmbNb, elmbTimeOut, logFile, calibFile);
  }
  else {
    printf ("Errors when switching voltages on. Please check log file. Calibration not done\n");
  }
  if (checkInhibitsBroadcast (handle, elmbNb, portCGlobal,  0, elmbTimeOut, logFile) == true) 
    printf ("Problems with switching regulators OFF\n");

  if ((status = zasilacz.CloseCan (handle)) < 0) {
    printf ("CloseCan exit with error: %d\n", status);
    printf ("%s\n", zasilacz.GetErrorText ());
    fprintf (logFile, "CloseCan exit with error: %d\n", status);
    fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
  } 

  actualTime = time(NULL);
  brokenTime = localtime(&actualTime); 
 
  printf ("Calibration finished: %02d.%02d.%4d %02d:%02d:%02d\n\n",
	  brokenTime->tm_mday, brokenTime->tm_mon+1, brokenTime->tm_year+1900, 
	  brokenTime->tm_hour, brokenTime->tm_min,
	  brokenTime->tm_sec); 
  fprintf (logFile, "Calibration started: %02d.%02d.%4d %02d:%02d:%02d\n\n",
	   brokenTime->tm_mday, brokenTime->tm_mon+1, brokenTime->tm_year+1900, 
	   brokenTime->tm_hour, brokenTime->tm_min,
	   brokenTime->tm_sec); 

  fclose (calibFile);
  fclose (logFile);

}

