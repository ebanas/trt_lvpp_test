//
// Set of low level library calls for accessing 
//    Low Voltage Patch Panel -> a card which supplies 1/32 of TRT detector 
//    in ATLAS experiment
// 
// ###################################################################
//
//  Second version developed for systec hardware, 
//  using systec SocketCAN Driver for USB-CANmodul series,
//  utilities and libriaries
//   
//   Systec firmware version: 4.14
//
//  August 2012, Elzbieta Banas  Elzbieta.Banas@ifj.edu.pl
//
// ###################################################################
// First version was developed for KVASER CAN bus interface 
// working with KVASER CANLIB SDK.
//  
//  It was based on the work (Master Thesis) of:
//         Dawid Szymanski, 
//         student of Jagiellonian University
//         Cracow
//         first version (working OK): 15 July 2005
//
// upgrades:
// ****************************************************
// 1.   2005 - 2010
//  E. Banas, B. Kisielewski
//  IFJ PAN Cracow, Atlas group
//
// - 'dtmrocAddrress' and 'elmbAddress' introduction
// - symbolic names of CAN options
// - function GetErrorText
// - introduction of packet internal error codes
// - function SetInternalErrorText
// - function GetInhibit 
//
// ****************************************************
// 2. October 2011
// E. Banas 
//
// a new way of creating DTMROC serial protocol
// based on SPI register: ELMB firmware 4.4TRT, author Henk Boterenbrood
// *****************************************************
// ########################################################################
// 
// standard includes

#include "lvppSharedLib.h"
  
//#define PRINT

// common information to all functions:
//
//	1. all functions return an integer value:
//		when success - either 0 or positive value
//              when error - negative value
//     The error description is placed in an internal klass member,
//      and can be retrieved with a function: 
//                   GetErrorText()
//


// class constructor:
//
//  perfoms no special action
//

lvppSharedLib::lvppSharedLib(void)

{

}


//
//  class destructor
//  performs no special action
//

lvppSharedLib::~lvppSharedLib(void)

{

}


// list of functions calls

//**********************Init_CANBus************************//

//
// Syntax: int lvppSharedLib::Init_CANBus(int channel, int *handle);
//
// Function:
//
//	- open socket
//
// Input parameters:
//
//	int channel - number of SYSTEC Device, starting from "0". 
//                    this value appended to the string "CAN" forms a device name, e.g. CAN0
//
// Output paramter:
//
//  int *handle - socket id
//
// Return value:
//
//	0 (canOK) - success
//  <0 - error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::Init_CANBus(int channel, int *handle)

{
int s; /* can raw socket */ 
struct sockaddr_can addr;
struct ifreq ifr;
char sCanDeviceName[16];

 sprintf (sCanDeviceName, "can%d", channel);

/* open socket */
	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("socket");
		return s;
	}

	addr.can_family = AF_CAN;
#ifdef PRINT        
        printf ("Can name: %s  ", sCanDeviceName);
#endif
	strcpy(ifr.ifr_name, sCanDeviceName);
	if (ioctl(s, SIOCGIFINDEX, &ifr) < 0) {
		perror("SIOCGIFINDEX");
		return -1;
	}
	addr.can_ifindex = ifr.ifr_ifindex;

	/* disable default receive filter on this RAW socket */
	/* This is obsolete as we do not read from the socket at all, but for */
	/* this reason we can remove the receive list in the Kernel to save a */
	/* little (really a very little!) CPU usage. 
                         */
	// we do not set this now, as we will use this socket to read and write
	//	setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0);

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("bind");
		return -1;
	}

        *handle = s;
 
	return 0;
}


//
//	internal function
//
//  syntax: lvCanWrite (int handle, ...)
//
int lvppSharedLib::socketCanWrite (int handle, long id, void *msg, unsigned int dlc, unsigned int flag)
{ 
struct can_frame frame;
int iNbytes;
 int i, myErrno;

/* prepare frame */
 frame.can_id = id+flag;
 frame.can_dlc = dlc;
 
#ifdef PRINT
 printf ("SocketCanWrite -  handle: %d can_id: %X flag: %X dlc: %d \n", handle, id, flag, dlc);
#endif

 //for (i=0; i<dlc; i++) frame.data[i] = (__u8 *)*(msg++);
   memcpy(frame.data, msg, dlc);

#ifdef PRINT
 for (i=0; i<dlc; i++) printf (" %X ", frame.data[i]);
 printf ("\n");
#endif
	/* send frame */
 iNbytes = write(handle, &frame, sizeof(struct can_frame));
#ifdef PRINT
 perror ("write");
#endif
 if (iNbytes < sizeof(struct can_frame)) {
   perror ("write");
		 printf ("socketCanWrite error: %d bytes written, should be %d\n", iNbytes,  sizeof(struct can_frame) );
		 //   perror("write");
		return (-1);
	}
#ifdef PRINT
 printf ("socketCanWrite: %d bytes written OK \n", iNbytes);
#endif
	return 0;
}

//
//	internal function
//
//  syntax: socketCanRead (int handle, ...)
//
int lvppSharedLib::socketCanRead (int handle, int *can_id, void *msg, unsigned int *dlc, unsigned int *flag, unsigned int *time)
{
  //   	canMessage cmsg;
   	struct can_frame  sockmsg;
	//   	CSockCanScan *ppcs = reinterpret_cast<CSockCanScan *>(pCanScan);
   	bool runCan = true;
//   	struct msghdr msg;
   	unsigned int nbytes;

  	while (runCan) {

		/* these settings may be modified by recvmsg() */
	  nbytes = read(handle, &sockmsg, sizeof(sockmsg));
#ifdef PRINT
	  printf ("socketCanRead - after read - nbytes %d (size of can_frame: %d)\n", nbytes, sizeof(struct can_frame));
#endif
		if (nbytes < 0) {
			perror("read");
			continue;
		}

		if (nbytes < sizeof(struct can_frame)) {
			printf("read: incomplete CAN frame\n");
			continue;
		}
		/*
		cmsg.c_id = sockmsg.can_id;
		cmsg.c_time= time(NULL);
		cmsg.c_dlc = sockmsg.can_dlc;
		memcpy(&cmsg.c_data[0],&sockmsg.data[0],8); 
		*/
                *can_id = sockmsg.can_id;
		//              *time = time(NULL);
                *dlc = sockmsg.can_dlc;
                memcpy (msg, &sockmsg.data[0], 8);
		//	ppcs->cb->FireOnChange(sock, &cmsg);
                runCan = false;
#ifdef PRINT
	  printf("socketCanRead: socket - %d  canID 0x%x\n", handle, *can_id);
#endif
	}
 
	return 0;

#ifdef CAN_UTILS 
struct can_frame frame;
int iNbytes;
int i, dropcnt, lastDropcnt; 
struct msghdr msg;
struct iovec iov;
fd_set rdfs;
struct cmsghdr *cmsg;
struct sockaddr_can addr;
char ctrlmsg[CMSG_SPACE(sizeof(struct timeval)) + CMSG_SPACE(sizeof(__u32))];
int running = 1;

	/* these settings are static and can be held out of the hot path */
	iov.iov_base = &frame;
	msg.msg_name = &addr;
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = &ctrlmsg;

	FD_ZERO (&rfs);
        FD_SET (handle, &rdfs);

 	if ((ret = select(handle, &rdfs, NULL, NULL, NULL)) < 0) {
			//perror("select");
			running = 0;
			continue;
		}
#ifdef PRINT
        printf ("After select: ret - %d \n", ret);
#endif

      while (running) {
	if (FD_ISSET(handle, &rdfs)) {

           int idx;

				/* these settings may be modified by recvmsg() */
           iov.iov_len = sizeof(frame);
	   msg.msg_namelen = sizeof(addr);
	   msg.msg_controllen = sizeof(ctrlmsg);  
	   msg.msg_flags = 0;

	   nbytes = recvmsg(handle, &msg, 0);
           if (nbytes < 0) {
		perror("read");
		return -11;
	   }

	   if (nbytes < sizeof(struct can_frame)) {
		fprintf(stderr, "read: incomplete CAN frame\n");
		return -1;
	   }

	   if (count && (--count == 0))
				running = 0;
		    
	   for (cmsg = CMSG_FIRSTHDR(&msg);
	     cmsg && (cmsg->cmsg_level == SOL_SOCKET);
	     cmsg = CMSG_NXTHDR(&msg,cmsg)) {
		if (cmsg->cmsg_type == SO_TIMESTAMP)
		  tv = *(struct timeval *)CMSG_DATA(cmsg);
		else if (cmsg->cmsg_type == SO_RXQ_OVFL)
			dropcnt = *(__u32 *)CMSG_DATA(cmsg);
	   }

	   /* check for (unlikely) dropped frames on this specific socket */
	   if (dropcnt != last_dropcnt) {

		__u32 frames;
		if (dropcnt > lastDropcnt)
     		  frames = dropcnt - lastDropcnt;
		else
		  frames = 4294967295U - lastDropcnt + dropcnt; /* 4294967295U == UINT32_MAX */

		lastDropcnt = dropcnt;
	   }
	   //	   idx = idx2dindex(addr.can_ifindex, handle);
	}
       
        *dlc = sizeof (can_frame.data);
        memcpy (buffer, &frame.data[0], *dlc);
        *time = tv->tv_sec; 
        running= 0;
      } 
#endif
	return 0;
}



//**********************Init_ELMB************************//

// Funkcja inicjalizuje ELMB, na poczatku jest resetowane a nastepnie
// wprowadzane w satn Operational tak aby byla mozliwa komunickacja PDO
// Wszystkie operacja odbywaja sie na stalym adresie 3f
// Sprawdzany jest status opercji za kadym razem
//

//
// Syntax: KlasaI::Init_ELMB (int handle, int elmbAddress);
//
// Function:
//
//	initialization of ELMB:
//	 - set into operational state
//	 - read back state
//
// Input parameters: 
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63)
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::Init_ELMB (int handle, int elmbAddress)

{	
  int status;
  int loopCnt = 200;
  int state;
  bool keepChecking = true;

  if ((status = Set_ELMB_State (handle, elmbAddress, nmtCS_StartNode)) < 0) {
    AddToErrorText((char *)" from Init_ELMB");
    return status;
  }

  while (keepChecking && (loopCnt--)) {
    if ((status = Read_ELMB_State (handle, elmbAddress, &state)) < 0) {
      AddToErrorText((char *)" from Init_ELMB");
      return status;
    }
    if ((state == 5) || (state == 133)) keepChecking = false;
  }
  
  if (loopCnt) status = lvppSL_OK;
  else status = lvppSL_ELMB_InitializingTimeout;

#ifdef PRINT
  printf ("Init_ELMB: there was %d loops\n", 200-loopCnt);
#endif

  return status;

}


//**********************Set_ELMB_State************************//
//
// Syntax: lvppSharedLib::Set_ELMB_State (int handle, int elmbAddress, int nmtCS);
//
// Function:
//
//	sends a command to ELMB: 
//			StartNode			1
//			StopNode			2
//			PreOpState			128
//			ResetNode			129
//			ResetCommunication	130
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63)
//  int nmtCS	- command code 
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//


int lvppSharedLib::Set_ELMB_State (int handle, int elmbAddress, int nmtCS)

{	
  int command;
  int status, i;
  unsigned char buffer[16];
  unsigned int length, flag;
  unsigned int time;
  int elmbState;
  int can_id;
  bool keepChecking; 

  if ((nmtCS != nmtCS_StartNode) &&			
      (nmtCS != nmtCS_StopNode) &&	
      (nmtCS != nmtCS_PreOpState) &&
      (nmtCS != nmtCS_ResetNode) &&
      (nmtCS != nmtCS_ResetCommunication)) 
    return lvppSL_ELMB_BadNMTCommandSpecifier;

  command = (elmbAddress << 8)|nmtCS;	

  if ((status = socketCanWrite (handle,0x0,&command,2,0)) < 0) {
    SetErrorText ((char *)"socketCanWrite command in Set_ELMB_State", status);
    return status;
  }

	return status;	
}


//**********************Read_ELMB_State************************//
//
// Syntax: lvppSharedLib::Read_ELMB_State (int handle, int elmbAddress, int *state);
//
// Function:
//
//	reads ELMB state
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63)
//
// Output parameter:
//
//	int *state - ELMB state is returned here
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::Read_ELMB_State (int handle, int elmbAddress, int *state)

{	
  bool keepChecking; 
  int status;
  //unsigned char buffer = '\0';
  unsigned char buffer[16];
  unsigned int length, flag;
  unsigned int time;
  int i, can_id;

  //read back actual ELMB state
  if ((status = socketCanWrite (handle,cobID_ELMB_BootUp|elmbAddress,NULL,0,CAN_RTR_FLAG)) < 0) {
    SetErrorText ((char *)"canWrite read back command in Read_ELMB_State", status);
    return status;
  }

#ifdef PRINT
  printf ("Read_ELMB_Stat Write: can_id %X, flag: %X\n",cobID_ELMB_BootUp|elmbAddress, CAN_RTR_FLAG);
#endif

  // read socket
  keepChecking = true;
  while (keepChecking) {
    if ((status = socketCanRead (handle, &can_id, (void *)&buffer, &length, &flag, &time)) < 0) {
      SetErrorText ((char *)"canWrite read back command in Set_ELMB_State", status);
      return status;
    }
    if (can_id == (cobID_ELMB_BootUp|elmbAddress)) keepChecking = false;
  }
#ifdef PRINT
  printf ("Read_ELMB_Stat Read: can_id %X, length: %d, time: %d  state %d\n", can_id, length, time, buffer[0]);
  for (i=0; i<length; i++) printf (" %X ", buffer[i]);  printf ("\n");
#endif
  if (length == 0) {
    status = lvppSL_canReadZeroLength;
    SetInternalErrorText ((char *)"Read_ELMB_State", status);
  }
  else 
    *state = buffer[0];

  return status;	
	 
}

//**********************StoreELMB_ParamAllToEEPROM************************//
//
// Syntax: int lvppSharedLib::StoreELMB_ParamAllToEEPROM (int handle, int elmbAddress, 
//                                                         bool bResetCalibrateADC, int elmbTimeOut);
//
// Function:
//
//	Stores all parameters to EEPROM
//		
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//      int elmbAddress - address of elmb (from 0 to 63)
//
// Return value:
//
//	0  - success
//      <0 (-1, -11) coming from socket write/read functions 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//


int lvppSharedLib::StoreELMB_ParamAllToEEPROM (int handle, int elmbAddress, int elmbTimeOut)
{
int status;
int index = 0x1010;
int subindex = 1;
int nbBytes = 4;
int ctrlWord = 0x2f;

	if ((status = sendWriteSDO(handle, elmbAddress, index, subindex, 1702257011, nbBytes,
						      ctrlWord, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from StoreELMB_ParamAllToEEPROM");
	}
	return (status);

}

//**********************ReadELMB_ADCResetAndCalibrate************************//
//
// Syntax: int lvppSharedLib::ReadELMB_ADCResetAndCalibrate(int handle, int elmbAddress, 
//                                                          int elmbTimeOut, bool *bResetCalibrateADC);
//
// Function:
//
//	Set/Reset option:
//                       ADC reset and calibration before each channel scan
//                       This is default option for the TRT LVPP software
//		
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//      int elmbAddress - address of elmb (from 0 to 63)
//
// Output parameters:
//	bool bResetCalibrateADC -  0 -> reset
//                                 1 -> set
//
// Return value:
//
//	0  - success
//      <0 (-1, -11) coming from socket write/read functions 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//


int lvppSharedLib::ReadELMB_ADCResetAndCalibrate (int handle, int elmbAddress, int elmbTimeOut, bool *bResetCalibrateADC)
{
int status;
int index = 0x2120;
int subindex = 0;
int nbBytes = 1;
int ctrlWord = 0x43;
unsigned long uData;

	if ((status = sendReadSDO(handle, elmbAddress, index, subindex, &uData, nbBytes,
						      ctrlWord, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from ReadELMB_ADCResetAndCalibrate");
		return status;
	}

        if (uData == 1) *bResetCalibrateADC = true;
        else if (uData == 0) *bResetCalibrateADC = false;
	else status = -2015;
       
	return (status);

}


//**********************SetELMB_ADCResetAndCalibrate************************//
//
// Syntax: int lvppSharedLib::SetELMB_ADCResetAndCalibrate(int handle, int elmbAddress, 
//                                                         bool bResetCalibrateADC, int elmbTimeOut);
//
// Function:
//
//	Set/Reset option:
//                       ADC reset and calibration before each channel scan
//                       This is default option for the TRT LVPP software
//		
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//      int elmbAddress - address of elmb (from 0 to 63)
//	bool bResetCalibrateADC -  0 -> reset
//                                 1 -> set
//
// Return value:
//
//	0  - success
//      <0 (-1, -11) coming from socket write/read functions 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//


int lvppSharedLib::SetELMB_ADCResetAndCalibrate (int handle, int elmbAddress, bool bResetCalibrateADC, int elmbTimeOut)
{
int status;
int index = 0x2120;
int subindex = 0;
int nbBytes = 1;
int ctrlWord = 0x2f;

	if ((status = sendWriteSDO(handle, elmbAddress, index, subindex, bResetCalibrateADC, nbBytes,
						      ctrlWord, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from SetELMB_ADCResetAndCalibrate");
	}
	return (status);

}

//**********************SetELMB_TPD03TransmissionType************************//
//
// Syntax: int lvppSharedLib::SetELMB_TPD03TransmissionType(int handle, int elmbAddress, 
//                                                         unsigned char ucTransmType, int elmbTimeOut);
//
// Function:
//
//	Set transmission type for TPD03 messages (ADC readout values)	
//		
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//      int elmbAddress - address of elmb (from 0 to 63)
//	unigned char  ucTransmType -  1 - SYNC -> Opc Server
//                                    255 - RTR (Remote Transmission Request) -> test program
//
// Return value:
//
//	0  - success
//      <0 (-1, -11) coming from socket write/read functions 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//


int lvppSharedLib::SetELMB_TPD03TransmissionType (int handle, int elmbAddress, unsigned char ucTransmType, int elmbTimeOut)
{
int status;
int index = 0x1802;
int subindex = 0x2;
int nbBytes = 1;
int ctrlWord = 0x2f;

	if ((status = sendWriteSDO(handle, elmbAddress, index, subindex, ucTransmType, nbBytes,
						      ctrlWord, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from SetELMB_PD03TransmissionType");
	}
	return (status);

}



int lvppSharedLib::initELMB_SPI (int handle, int elmbAddress, int elmbTimeOut)
{
	int status;

	// set SPI SCLK signal period (frequency) to 30 us
	if ((status = sendWriteSDO(handle, elmbAddress, 0x2602, 0x1, 0x20,
						      1, 0x2f, elmbTimeOut)) < 0) {
	  AddToErrorText ((char *)" from initELMB_SPI");
		return status;
	}

	// set SPI SCLK Rising Edge to 0: SPI Data are shifted on falling edge
	if ((status = sendWriteSDO(handle, elmbAddress, 0x2602, 0x2, 0x0,
						      1, 0x2f, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from initELMB_SPI");
		return status;
	}

	return status;
}

int lvppSharedLib::readSPI_period (int handle, int elmbAddress, int elmbTimeOut, unsigned long *period)
{
int status;
int index = 0x2602;
int subindex = 0x1;

int nbBytes = 1;
int ctrlWord = 0x43;

	if ((status = sendReadSDO(handle, elmbAddress, index, subindex, period, nbBytes, 
		ctrlWord, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from readSPI_period");
	}

	return status;
}

int lvppSharedLib::setSPI_period (int handle, int elmbAddress, unsigned long period, int elmbTimeOut)
{
int status;
int index = 0x2602;
int subindex = 0x1;
int nbBytes = 1;
int ctrlWord = 0x2f;

	// set SPI SCLK signal period (frequency) to 30 us
	if ((status = sendWriteSDO(handle, elmbAddress, index, subindex, period, nbBytes,
						      ctrlWord, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from setSPI_period");		
	}

	return status;

}

//**********************Init_DTMROC************************//

// Funkcja inicjalizuje DTMROC'i na poczatku posylane jest 300 zegarow

// nastepnie wywoluje funkjce Software_Rese_DTMROC (reset DTMROC'ow)

// i kolejne 200 zegarow, nastepnie odpytuje kazdy adres DTMROC'a 

// jezeli odpowie to jest komnikacja (musi odpowiedziec wlasnym adresem 

// bo przy odpytywaniu po adresach sprwadzam Common Status Register

// pierwszych bitow to adres DTMROC'a

//
//**********************Init_DTMROC************************//
//
// Syntax: int lvppSharedLib::Init_DTMROC(int handle, int elmbAddress, int portC, char *dtmrocRetAddr);
//
// Function:
//
//	initializarion of all dtmrocs in the board:
//	
//		1. Hardware reset 
//		2. 300 clocks
//		3. Software reset - command 'Soft Reset'
//		4. 300 clocks
//		5. read status of each dtmroc
//		
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63)
//	int portC - a value to be written to port C (port A and C are written with the same command)
//
// Output parameter:
//
//	char *dtmrocRetAddr - string with returned addresses of all 12 dtmrocs, in the order:
//                         dtmroc 1, 2, 3, 4, 5, 6, 9, 10, 11, 12, 13, 14
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::Init_DTMROC(int handle, int elmbAddress, int portC, int elmbTimeOut, char *dtmrocRetAddr)
{	
int status;
int Field6;
int i;
char tmp[20];

	dtmrocRetAddr[0] = '\0';

	if ((status = Hardware_Reset_DTMROC(handle, elmbAddress, portC, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from Init_DTMROC");
		return status;
	}


	if ((status = Software_Reset_DTMROC(handle, elmbAddress, portC, elmbTimeOut)) < 0) {	//Fukcja resetujaca
		AddToErrorText ((char *)" from Init_DTMROC");
		return status;
	}


	// read status of each dtmroc
	for (i = 0; i < 12; i++) {
		if ((status = GetDtmrocState(handle, elmbAddress, dtmrocAddress(i), portC, elmbTimeOut, &Field6)) < 0) {
			AddToErrorText ((char *)" from Init_DTMROC");
			return status;
		}
#ifdef PRINT
		printf ("Init_DTMROC: i %d  dtmroc %d  %X \n", i, dtmrocAddress(i), Field6);
#endif
		sprintf (tmp, "%d " , Field6);
		strcat (dtmrocRetAddr, tmp);
	}

	return status;
}

//**********************HardSoftResetDtmrocs************************//
//
// Syntax: int lvppSharedLib::HardSoftResetDtmrocs(int handle, int elmbAddress, int portC);
//
// Function:
//
//	Hard and soft reset of all dtmrocs in the board:
//	
//		1. Hardware reset 
//		2. 300 clocks
//		3. Software reset - command 'Soft Reset'
//		4. 300 clocks
//		
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63)
//	int portC - a value to be written to port C (port A and C are written with the same command)
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//


int lvppSharedLib::HardSoftResetDtmrocs(int handle, int elmbAddress, int portC, int elmbTimeOut)
{
int status;

	if ((status = Hardware_Reset_DTMROC(handle, elmbAddress, portC, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from HardSoftResetDtmrocs");
		return status;
	}	


	if ((status = Software_Reset_DTMROC(handle, elmbAddress, portC, elmbTimeOut)) < 0) {	//Fukcja resetujaca
		AddToErrorText ((char *)" from HardSoftResetDtmrocs");
		return status;
	}

	return status;

}

//**********************GetDtmrocState************************//
//
// Syntax: lvppSharedLib::GetDtmrocState (int handle, int elmbAddress, int dtmrocAddress, 
//                                 int *state);
//
// Function:
//
//	read status register of given dtmroc
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63)
//  int dtmrocAddress - address of dtmroc (1-6, 9-14)
//
// Output value:
//
//	int *state - last four bits of dtmroc's status register,
//               these bits contain dtmroc address
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()

int lvppSharedLib::GetDtmrocState (int handle, int elmbAddress, int dtmrocAddress, int portC, int elmbTimeOut, int *state)
{
int status;
int Field6;

	if ((status = Read(handle, elmbAddress, dtmrocAddress, StatusRegister, portC, elmbTimeOut, &Field6)) < 0) {
		AddToErrorText ((char *)" GetDtmrocState");
		return status;
	}
	if (*state & 0xC0) { // previous command had with unrecognised fields, we do SOFT RESET
						 // and redo reading status
		if ((status = Software_Reset_DTMROC(handle, elmbAddress, portC, elmbTimeOut)) < 0) {
			AddToErrorText ((char *)" from GetDtmrocState");
			return status;
		}
		if ((status = Read(handle, elmbAddress, dtmrocAddress, StatusRegister, portC, elmbTimeOut, &Field6)) < 0) {
			AddToErrorText ((char *)" GetDtmrocState");
			return status;
		}
	}

	*state = 0xFF & Field6;

	return status;
	
}

//**********************Software_Reset_DTMROC************************//
//
// Syntax: lvppSharedLib::Software_Reset_DTMROC(int handle, int elmbAddress, int portC); 
//
// Function:
//
//	send command 'Soft Reset' to all dtmroc 
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63)
//	int portC - a value to be written to port C (port A and C are written with the same command)
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()

int lvppSharedLib::Software_Reset_DTMROC(int handle, int elmbAddress, int portC, int elmbTimeOut)

{
int clocks = 7;   
unsigned long FieldSoft = 0x54;
int status;

	status = SPI_write (handle, elmbAddress, FieldSoft, clocks, elmbTimeOut);
	if (status < 0) {
		AddToErrorText ((char *)" from Software_Reset_DTMROC");
		return (status);
	}

	return status;

}

//**********************BC************************//
//
// Syntax: lvppSharedLib::BC(int handle, int clocks, int elmbAddress, int portC); 
//
// Function:
//
//	send requested number of clocks - simulate a serial protocol for dtmroc
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//	int clocks - number of clocks requested
//  int elmbAddress - address of elmb (from 0 to 63);
//	int portC - a value to be written to port C 
//				(port A and C are written with the same command)
//  int elmbTimeOut - timeout in miliseconds 
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()

int lvppSharedLib::BC(int handle, int clockNb, int elmbAddress, int portC, int elmbTimeOut)
{
int i, status;

	for (i=0; i<clockNb/32; i++) {
		if ((status = SPI_write (handle, elmbAddress, 0, 32, elmbTimeOut)) < 0) {
			AddToErrorText ((char *)" from BC");
			return (status);
		}
	}
	if (clockNb%32) 
		if ((status = SPI_write (handle, elmbAddress, 0, clockNb%32, elmbTimeOut)) < 0) {
			AddToErrorText ((char *)" from BC");
			return (status);
		}
	return status;

}



//**********************CloseCan************************//
//
// Syntax: lvppSharedLib::CloseCan(int handle); 
//
// Function:
//
//	close opened KVASER channel
//  close can communication
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::CloseCan(int handle)
{

  // close socket       
      
  close (handle);
	return (0);
}


//**********************Init************************//
//
// Syntax: lvppSharedLib::Init(int handle, int channel, int elmbAddress, int portC); 
//
// Function:
//
//	combined Init (init of CANBus, ELMB and dtmrocs):
//
//        1. IF requested KVASER channel is opened -> close it
//		  2. open a channel
//        3. initialize ELMB
//        4. initialize DTMROC
//		
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//	int channel - KVASER channel number (0 - 3)
//  int elmbAddress - address of elmb (from 0 to 63);
//	int portC - a value to be written to port C 
//				(port A and C are written with the same command)
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::Init(int handle, int channel, int elmbAddress, int portC, int elmbTimeOut)

{
int status;
char dtmrocAddresses[12];

//	canBusOff(handle);
//	canClose(handle);

	if ((status = Init_CANBus (channel, &handle)) < 0) {
		AddToErrorText ((char *)" from Init");
		return status;
	}

	if ((status = Init_ELMB (handle, elmbAddress)) < 0) {
		AddToErrorText ((char *)" from Init");
		return status;
	}


	if ((status = Init_DTMROC (handle, elmbAddress, portC, elmbTimeOut, dtmrocAddresses)) < 0) {
		AddToErrorText ((char *)" from Init");
		return status;
	}

	return status;
}



//**********************WriteDacBrdc************************//
//
// Syntax: int lvppSharedLib::WriteDacBrdc (int handle, int elmbAddress, int dacs, 
//									 int Field6, int portC);
//
//
// Function:
//
//	Write the same value to DACs with broadcast, without check:
//
//		parameter 'dacs': 1 -> DACs 0 1
//		parameter 'dacs': 2 -> DACS 2 3
//		parameter 'dacs': 3 -> all DACs: 0 1 2 3
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int dacs - which dacs should be written (see above);
//  int Field6 - data to be written;
//	int portC - a value to be written to port C 
//				(port A and C are written with the same command)
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::WriteDacBrdc (int handle, int elmbAddress, int dacs, int Field6, int portC, int elmbTimeOut)
{
unsigned long Field12345 []= {0x5724000,0x571C00A,0x571C00C};//
unsigned long Field;
int status;
int a;	//liczba zegarow podawana do funkcji send
int checksNb = 102;
int i;
bool overrun = false;

if (dacs == 3) {
		for (i=1; i<=2; i++) {
				a = 27;	//liczba zegarow 
				Field = (Field12345 [i] + (0x3F << 6));	// (command)
				if ((status = SPI_write (handle, elmbAddress, Field, a, elmbTimeOut))<0) {	
					AddToErrorText ((char *)" from WriteDacBrdc");
					return status;
				}
				a = 16;	//liczba zegarow 
				if ((status = SPI_write (handle, elmbAddress, Field6, a, elmbTimeOut)) < 0)	{ //wywaolnie funkcji send
					AddToErrorText ((char *)" from WriteDacBrdc");
					return status;
				}
				a = 2;	//liczba zegarow 
				if ((status = SPI_write (handle, elmbAddress, 0, a, elmbTimeOut)) < 0)	{ //wywaolnie funkcji send
					AddToErrorText ((char *)" from WriteDacBrdc");
					return status;
				}

		}
	}
	else {
			a = 27;	//liczba zegarow podawana do funkcji send
			Field = (Field12345 [dacs] + (0x3F << 6));	//liczba ktora checy wyslac (command)
			if ((status = SPI_write (handle, elmbAddress, Field, a, elmbTimeOut))<0) {	
				AddToErrorText ((char *)" from WriteDacBrdc");
				return status;
			}
			a = 16;	//liczba zegarow 
			if ((status = SPI_write (handle, elmbAddress, Field6, a, elmbTimeOut)) < 0)	{ //wywaolnie funkcji send
				AddToErrorText ((char *)" from WriteDacBrdc");
				return status;
			}
			a = 2;	//liczba zegarow 
			if ((status = SPI_write (handle, elmbAddress, 0, a, elmbTimeOut)) < 0)	{ //wywaolnie funkcji send
				AddToErrorText ((char *)" from WriteDacBrdc");
				return status;
			}
	}

	return status;

}

//**********************WriteDacBrdcCheck************************//
//
// Syntax: int lvppSharedLib::WriteDacBrdcCheck (int handle, int elmbAddress, int dacs, 
//										  int Field6, int portC)
//
//
// Function:
//
//	Write the same value to DACs with broadcast, with check:
//
//		parameter 'dacs': 1 -> DACs 0 1
//		parameter 'dacs': 2 -> DACS 2 3
//		parameter 'dacs': 3 -> all DACs: 0 1 2 3
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int dacs - which dacs should be written (see above);
//  int Field6 - data to be written;
//	int portC - a value to be written to port C 
//				(port A and C are written with the same command)
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::WriteDacBrdcCheck (int handle, int elmbAddress, int dacs, int Field6, int portC, int elmbTimeOut)
{
int status;

	// broadcast write 
	if ((status = WriteDacBrdc (handle, elmbAddress, dacs, Field6, portC, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from WriteDacBrdcCheck");
		return status;
	}	
	
	// check;
	if ((status = DacBrdcCheck (handle, elmbAddress, dacs, Field6, portC, elmbTimeOut)) < 0) { 
		AddToErrorText ((char *)" from WriteDacBrdcCheck");
		return status;
	}

	return (status);

}


//**********************DacBrdcCheck************************//
//
// Syntax: int lvppSharedLib::DacBrdcCheck (int handle, int elmbAddress, int dacs, 
//									 int Field6, int portC)
//
//
// Function:
//
//	check values written to all dtmrocs in a board with broadcast:
//
//		parameter 'dacs': 1 -> DACs 0 1
//		parameter 'dacs': 2 -> DACS 2 3
//		parameter 'dacs': 3 -> all DACs: 0 1 2 3
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int dacs - which dacs should be written (see above);
//  int Field6 - data to be written;
//	int portC - a value to be written to port C 
//				(port A and C are written with the same command)
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::DacBrdcCheck (int handle, int elmbAddress, int dacs, int Field6, int portC, int elmbTimeOut)
{
int status;
int i, dac0, dac1;
int fieldCheck;
char text[80];

	if (dacs == 3) {
		for (i=0; i<12; i++) {
			if ((status = Read (handle, elmbAddress, dtmrocAddress(i), DAC_01, portC, elmbTimeOut, &fieldCheck)) < 0 ) {
				AddToErrorText ((char *)" from WriteDacBrdcCheck");
				return status;
			}
			if (fieldCheck != Field6) {
				sprintf (text, "in dtmroc #%d dacs 0 1: is - 0x%X should be - 0x%X", 
								dtmrocAddress(i), fieldCheck, Field6);
				SetInternalErrorText (text, lvppSL_DTMROC_Write_Brdc_BadSet);
				return lvppSL_DTMROC_Write_Brdc_BadSet;
			}
		}
		for (i=0; i<12; i++) {
			if ((status = Read(handle, elmbAddress, dtmrocAddress(i), DAC_23, portC, elmbTimeOut, &fieldCheck)) < 0 ) {
				AddToErrorText ((char *)" from WriteDacBrdcCheck");
				return status;
			}
			if (fieldCheck != Field6) {
				sprintf (text, "in dtmroc #%d dacs 2 3: is - 0x%X should be - 0x%X", 
								dtmrocAddress(i), fieldCheck, Field6);

				SetInternalErrorText (text, lvppSL_DTMROC_Write_Brdc_BadSet);
				return lvppSL_DTMROC_Write_Brdc_BadSet;
			}
		}
	}
	else {

		for (i=0; i<12; i++) {
			if ((status = Read(handle, elmbAddress, dtmrocAddress(i), dacs+1, portC, elmbTimeOut, &fieldCheck)) < 0 ) {
				AddToErrorText ((char *)" from WriteDacBrdcCheck");
				return status;
			}
			if (fieldCheck != Field6) {
				if (dacs == 1) {dac0 = 0; dac1 = 1;} else {dac0 = 2; dac1 = 3;}
				sprintf (text, "in dtmroc #%d dacs %d %d: is - 0x%X should be - 0x%X", 
								dtmrocAddress(i), dac0, dac1, fieldCheck, Field6);

				SetInternalErrorText (text, lvppSL_DTMROC_Write_Brdc_BadSet);
				return lvppSL_DTMROC_Write_Brdc_BadSet;
			}
		}

	}

	return status;

}


//**********************WriteDac************************//
//
// Syntax: int lvppSharedLib::WriteDac(int handle, int elmbAddress, int dtmrocAddress, int dacNb, 
//								int Field6, int portC);
//
//
// Function:
//
//	Write one dedicated DAC
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int dtmrocAddress - address of dtmroc (1-6, 9-14);
//  int dacNb - which dac (0 | 1 | 2 | 3);
//  int Field6 - data to be written;
//	int portC - a value to be written to port C 
//				(port A and C are written with the same command)
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//


int lvppSharedLib::WriteDac(int handle, int elmbAddress, int dtmrocAddress, int dacNb, int Field6, int portC, int elmbTimeOut)
{
int status;
int value, valueCheck;
int dacs;
int licz;

	if ((dacNb < 0 ) || (dacNb > 3))
		return lvppSL_DTMROC_DAC_BadDacNumber;

	// check which dacs (0-1 or 2-3) should be read-out

	if ((dacNb == 0) || (dacNb == 1))
		dacs = 2;
	else 
		dacs = 3;

	// now we read dacs

	licz = 4;
	do {
		if ((status = Read(handle, elmbAddress, dtmrocAddress, dacs, portC, elmbTimeOut, &value)) < 0) {
			AddToErrorText ((char *)" from WriteDac");
			return status;
		} 
		if (!licz--) break;
	} while (!value);

	if ((dacNb == 0) || (dacNb == 2)) {
		value &= 0xFF00; value |= Field6;
	}
	else {
		value &= 0xFF; value |=  (Field6 << 8);
	}
      
	// and now we write

	if ((status = Write(handle, elmbAddress, dtmrocAddress, dacs, value, portC, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from WriteDac");
		return status;
	}

	// and now check...
	
	licz = 4;
	do {
		if ((status = Read(handle, elmbAddress, dtmrocAddress, dacs, portC, elmbTimeOut, &valueCheck)) < 0) {
			AddToErrorText ((char *)" from WriteDac");
			return status;
		}
		if (!licz--) break;
	} while (value != valueCheck);

       	
	if (value != valueCheck) {
	  SetInternalErrorText ((char *) "WriteDac", lvppSL_DTMROC_Write_NotCorrect);
		status = lvppSL_DTMROC_Write_NotCorrect;
	}

	return (status);

}

//**********************WriteAllDacsWheelA************************//
//
// Syntax: lvppSharedLib::WriteAllDacsWheelA(int handle, int elmbAddress, int *values, 
//									int loopOverrun, int portC)
//
// Function:
//
//	Write all DACs in a 'Wheel A' type board, data are taken from a given array,
//				18 integers in the following order:
//		dtmroc 1 - 6:  dacs 01  dacs 23 -> 12 words
//		dtmroc 9 - 14: dacs 01			->  6 words		
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int *values - data to be written - array of 18 integers
//	int loopOverrun	- how many time repeat write/read procedure...
//	int portC - a value to be written to port C 
//				(port A and C are written with the same command)
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::WriteAllDacsWheelA(int handle, int elmbAddress, int portC, int elmbTimeOut, int *values)
{
int status;
int i;

	// analog part
	for (i=0; i<6; i++) {
		if ((status = Write(handle, elmbAddress, i+1, 2, *(values+2*i), portC, elmbTimeOut)) < 0) {
			AddToErrorText ((char *)" from WriteAllDacsWheelA");
			return status;
		}

		if ((status = Write(handle, elmbAddress, i+1, 3, *(values+2*i+1), portC, elmbTimeOut)) < 0) {
			AddToErrorText ((char *)" from WriteAllDacsWheelA");
			return status;
		}

	}


	// digital part

	for (i=9; i<15; i++) {
		if ((status = Write(handle, elmbAddress, i, 2, *(values+i+3), portC, elmbTimeOut)) < 0) {
			AddToErrorText ((char *)" from WriteAllDacsWheelA");
			return status;
		}

	}

	return status;


}

//**********************WriteAllDacsWheelB************************//
//
// Syntax: lvppSharedLib::WriteAllDacsWheelB(int handle, int elmbAddress, int *values, 
//									int loopOverrun, int portC)
//
// Function:
//
//	Write all DACs in a 'Wheel B' type board, data are taken from a given array,
//				18 integers in the following order:
//		dtmroc 1 - 4:  dacs 01			-> 4 words
//		dtmroc 5 - 6:  dacs 01  dacs 23 -> 4 words
//		dtmroc 9 - 14: dacs 01			-> 6 words		
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int *values - data to be written - array of 18 integers
//	int loopOverrun	- how many time repeat write/read procedure...
//	int portC - a value to be written to port C 
//				(port A and C are written with the same command)
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::WriteAllDacsWheelB(int handle, int elmbAddress, int portC, int elmbTimeOut, int *values)
{
int status;
int i;
int cnt;

	// analog part
	for (i=0; i<4; i++) {
		if ((status = Write(handle, elmbAddress, i+1, 2, *(values+i), portC, elmbTimeOut)) < 0) {
			AddToErrorText ((char *)" from WriteAllDacsWheelB");
			return status;
		}
	}

	for (i=4; i<6; i++) {
		if (i==4) cnt = 4; else cnt = 6;
		if ((status = Write(handle, elmbAddress, i+1, 2, *(values+cnt), portC, elmbTimeOut)) < 0) {
			AddToErrorText ((char *)" from WriteAllDacsWheelB");
			return status;
		}
		
		if ((status = Write(handle, elmbAddress, i+1, 3, *(values+cnt+1), portC, elmbTimeOut)) < 0) {
			AddToErrorText ((char *)" from WriteAllDacsWheelB");
			return status;
		}
	}


	// digital part

	for (i=9; i<15; i++) {
		if ((status = Write(handle, elmbAddress, i, 2, *(values+i-1), portC, elmbTimeOut)) < 0) {
			AddToErrorText ((char *)" from WriteAllDacsWheelB");
			return status;
		}
	}

	return status;


}




//**********************Hardware_Reset_DTMROC************************//
//
// Syntax: lvppSharedLib::Hardware_Reset_DTMROC(int handle, int elmbAddress, int portC);
//
//
// Function:
//
//	Hardware reset off all dtmrocs in the board:
//		set level "0" on port A bit #2  for 5 BC clocks:
//      maximum length of BC clock is: 255us*2 -> 510 us * 5 -> 2550 us
//      so level 0 is set for 10 msec...
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//



int lvppSharedLib::Hardware_Reset_DTMROC(int handle, int elmbAddress, int portC, int elmbTimeOut)
{
int i;
int status;

	if ((status = sendWriteSDO(handle, elmbAddress, 0x6220, 0x12, 1,
						      1, 0x2f, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from Hardware_Reset_DTMROC");
		return status;
	}	
    // 5 clocks
	if ((status = SPI_write (handle,  elmbAddress, 0,  32, elmbTimeOut)) < 0 )	{ 
		AddToErrorText ((char *)" from Hardware_Reset_DTMROC");
		return (status);
	}

	if ((status = sendWriteSDO(handle, elmbAddress, 0x6220, 0x12, 0,
						      1, 0x2f, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from Hardware_Reset_DTMROC");
		return status;
	}

    // 320 clocks
	for (i=0; i<32; i++)
		if ((status = SPI_write (handle,  elmbAddress, 0,  32, elmbTimeOut)) < 0 )	{ 
			AddToErrorText ((char *)" from Hardware_Reset_DTMROC");
			return (status);
		}	

	return status;

}


	

//**********************ReadAllDacsWheelA************************//
//
// Syntax: lvppSharedLib::ReadAllDacsWheelA 9int handle, int elmbAddress, 
//					 int portC, int elmbTimeOut, int *values)
//
// Function:
//
//	Read all DACs from a 'Wheel A' type board, data are stored in an array,
//				18 integers in the following order:
//		dtmroc 1 - 6:  dacs 01  dacs 23 -> 12 words
//		dtmroc 9 - 14: dacs 01			->  6 words		
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int *values - array of 18 integers, place for data
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::ReadAllDacsWheelA(int handle, int elmbAddress, int portC, int elmbTimeOut, int *values)
{
int status;
int i;
int data;
char s[40];

	// analog part
	for (i=0; i<6; i++) {
		if ((status = Read(handle, elmbAddress, i+1, DAC_01, portC, elmbTimeOut, &data)) < 0 ) {
			sprintf (s, " from ReadAllDacsWheelA in dtmroc %d", i+1);
			AddToErrorText (s);
			return status;
		}			
		*(values+2*i) = data;

		if ((status = Read(handle, elmbAddress, i+1, DAC_23, portC, elmbTimeOut, &data)) < 0 ) {
			sprintf (s, " from ReadAllDacsWheelA in dtmroc %d", i+1);
			AddToErrorText (s);
			return status;
		}			
		*(values+2*i+1) = data;
	}


	// digital part

	for (i=9; i<15; i++) {
		if ((status = Read(handle, elmbAddress, i, DAC_01, portC, elmbTimeOut, &data)) < 0 ) {
			sprintf (s, " from ReadAllDacsWheelA in dtmroc %d", i);
			AddToErrorText (s);
			return status;
		}			 
		*(values+i+3) = data;
	}

	return status;


}

//**********************ReadAllDacsWheelB************************//
//
// Syntax: lvppSharedLib::ReadAllDacsWheelB(int handle, int elmbAddress, int *values, 
//									int loopOverrun, int portC)
//
// Function:
//
//	Read all DACs from a 'Wheel B' type board, data are stored in an array,
//				14 integers in the following order:
//		dtmroc 1 - 4:  dacs 01			-> 4 words
//		dtmroc 5 - 6:  dacs 01  dacs 23 -> 4 words
//		dtmroc 9 - 14: dacs 01			-> 6 words		
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int *values - array of 18 integers for data
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::ReadAllDacsWheelB(int handle, int elmbAddress, int portC, int elmbTimeOut, int *values)
{
int status;
int i;
int cnt;
int data;
char s[40];

//int lvppSharedLib::Read(int handle, int elmbAddress, int dtmrocAddress, int object, int portC, int elmbTimeOut, int *value)

	// analog part
	for (i=0; i<4; i++) {
		if ((status = Read(handle, elmbAddress, i+1, DAC_01, portC, elmbTimeOut, &data)) < 0 ) {
			sprintf (s, " from ReadAllDacsWheelB in dtmroc %d\n", i+1);
			AddToErrorText (s);
			return status;
		}
		*(values+i) = data;
	}

	for (i=4; i<6; i++) {
		if (i==4) cnt = 4; else cnt = 6;
		if ((status = Read(handle, elmbAddress, i+1, DAC_01, portC, elmbTimeOut, &data)) < 0 ) {
			sprintf (s, " from ReadAllDacsWheelB in dtmroc %d\n", i+1);
			AddToErrorText (s);
			return status;
		}
		*(values+cnt) = data;

		if ((status = Read(handle, elmbAddress, i+1, DAC_23, portC, elmbTimeOut, &data)) < 0) {
			sprintf (s, " from ReadAllDacsWheelB in dtmroc %d\n", i+1);
			AddToErrorText (s);
			return status;
		}
		*(values+cnt+1) = data;

	}

	// digital part

	for (i=9; i<15; i++) {
		if ((status = Read(handle, elmbAddress, i, DAC_01, portC, elmbTimeOut, &data)) < 0 ) {
			sprintf (s, " from ReadAllDacsWheelB in dtmroc %d\n", i);
			AddToErrorText (s);
			return status;
		}
		*(values+i-1) = data;
	}

	return status;


}


//**********************GetErrorText************************//
//
// Syntax: char *lvppSharedLib::GetErrorText()
//
// Function:
//
//		return last eror information
//		
//
// Input parameters:
//
//	none
//
// Return value:
//
//	error message

char *lvppSharedLib::GetErrorText()

{
	return (errorText);
}


//**********************SetErrorText************************//
//
// Syntax: void lvppSharedLib::SetErrorText(char* info, int status)
//
// Function:
//
//		set error message in case of error coming from canlib library
//		error message consists of: function name, can error code, can message
//
// Input parameters:
//
//	char *info - extra information (e.g. name of a method where can error ocuured);
//  canStatus stat - canlib error code;
//
// Return value:
//
//		NONE

void lvppSharedLib::SetErrorText(char* info, int status)
{
char buf[50];

	if (status != canOK) {
        buf[0] = '\0';
        GetErrorText(status, buf, sizeof(buf));
		sprintf(errorText, "Error %d <%s> in %s\n", status, buf, info);
   }
	return;
}


//**********************SetInternalErrorText************************//
//
// Syntax: void lvppSharedLib::SetInternalErrorText(char* info, int status);
//
// Function:
//
//		set error message in case of internal class error
//		error message consists of: function name, can error code, can message
//
// Input parameters:
//
//	char *info - extra information (e.g. name of a method where can error ocuured);
//  int status - class error code;
//
// Return value:
//
//		NONE



void lvppSharedLib::SetInternalErrorText(char* info, int status)

{
//bk	int dalej;
	switch (status) {

		case lvppSL_canReadZeroLength:
			sprintf (errorText, "Error %d <Zero bytes read in CAN Read Function> in %s\n", 
				status, info);
			break;

		case lvppSL_ELMB_BadNMTCommandSpecifier:
			sprintf (errorText, "Error %d <Bad NMT Command Specifier> in %s\n", 
				status, info);
			break;

		case lvppSL_DTMROC_DAC_BadDacNumber:
			sprintf (errorText, "Error %d <Bad dac number> in %s\n", 
				status, info);
			break;

		case lvppSL_DTMROC_INH_BadInhNumber:
			sprintf (errorText, "Error %d <Bad inhibit number> in %s\n", 
				status, info);
			break;

		case lvppSL_DTMROC_INH_BadInhValue:
			sprintf (errorText, "Error %d <Bad inhibit value> in %s\n", 
				status, info);
			break;

		case lvppSL_DTMROC_INH_BadSet:
			sprintf (errorText, "Error %d <Inhibit was not properly set> in %s\n", 
				status, info);
			break;

		case lvppSL_DTMROC_Write_NotCorrect:
			sprintf (errorText, "Error %d <Write to DTMROC procedure was not correct> in %s\n", 
				status, info);
			break;

		case lvppSL_DTMROC_Write_Brdc_BadSet:
			sprintf (errorText, "Error %d <Broadcast write procedure was not correct> in %s\n", 
				status, info);
 //bk           printf ("Stop ! ");
	//bk		scanf ("%x", &dalej);

			break;

		case lvppSL_DTMROC_INH_Brdc_BadSet:
			sprintf (errorText, "Error %d <Broadcast set INHIBIT was not correct> in %s\n", 
				status, info);
			break;


		case lvppSL_DTMROC_INH_Read_Failed:

			sprintf (errorText, "Error %d <Read inhibit failed completely> in %s\n", 

				status, info);

			break;


		default:

			sprintf (errorText, "Error %d <Unknown error code> in %s\n", 

				status, info);
	   }

	return;

}



//**********************AddToErrorText************************//
//
// Syntax: void lvppSharedLib::AddToErrorText (char *text)
//
// Function:
//
//		add additional information to error message
//
// Input parameters:
//
//	char *text - additional information (e.g. name of a method where an error ocuured);
//
// Return value:
//
//		NONE


//**********************AddToErrorText************************//

// internal (socketate) function for adding messages in nested class functions

//

//

void lvppSharedLib::AddToErrorText (char *text)
{
  	strcat (errorText, text);
}




//**********************Get_INHIBIT************************//
//
// Syntax: int lvppSharedLib::Get_INHIBIT(int handle, int elmbAddress, int dtmrocAddress, 
//									int portC, int *inhibits)
//
// Function:
//
//	read both inhibits from one dtmroc:
//		inhibit == 0 -> regulator is OFF
//		inhibit == 1 -> regulator is ON
//
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int dtmrocAddress - address of dtmroc (1-6, 9-14);
//	int portC - a value to be written to port C 
//				(port A and C are written with the same command)
//
// Output value:
//
//	int *inhibits - inhibit state: 2 lowest bits are valid, so:
//
//			0 - inhibit0 = 0 and inhibit1 = 0
//          1 - inhibit0 = 1 and inhibit1 = 0
//          2 - inhibit0 = 0 and inhibit1 = 1
//          3 - inhibit0 = 1 and inhibit1 = 1
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()


int lvppSharedLib::Get_INHIBIT(int handle, int elmbAddress, int address, int portC, int elmbTimeOut, int *inhibits)
{
int Field6;
int bitout = 0;
int status;



	if ((status = Read(handle, elmbAddress, address, ConfigRegister, portC, elmbTimeOut, &Field6)) < 0) {//przeczytanie Configuration Register
		AddToErrorText ((char *)" from Get_INHIBIT");
		return (status);
	}

	// printf("\nGet_INHIBIT Configuration Register %x\n",Field6);
	bitout = Field6 & 0x20;	//wyzerowanie niepotrzebnych bitow (oprocz 5 (INHIBIT 0))
	*inhibits = bitout >> 5;	//przesuniecie w prawo na pozycje 0
	bitout = Field6 & 0x10;	//wyzerowanie niepotrzebnych bitow(oprocz 4 (INHIBIT 1))
	bitout = bitout >> 3;	//przesuniecie w prawo na pozycje 1

	*inhibits |= bitout;

	return (status);

}

//**********************GetAllInhibitsWheelA************************//
//
// Syntax: int lvppSharedLib::GetAllInhibitsWheelA(int handle, int elmbAddress, 
//				int portC, int elmbTimeOut, int *values)
//
//
// Function:
//
//	Read all inhibits for a 'Wheel A' type board, data are coded in one integer:
//		24 bits: bit 1: dtmroc #1, regulator 1
//               bit 2: dtmroc #2, regulator 2,
//						...
//               bit 24: dtmroc #14, regulator 2
//		if bit is set, the regulator is switched on.
//		
//	
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int elmbTimeOut - foreseen for future tests
//  int *values - 1 integer
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::GetAllInhibitsWheelA(int handle, int elmbAddress, int portC, int elmbTimeOut, int *values)
{
int status;
int i, j;
int data;
int inhibit;

	// analog part
    inhibit = 0;
	for (i=0; i<6; i++) {
		if ((status = Get_INHIBIT(handle, elmbAddress, i+1, portC, elmbTimeOut, &data)) < 0 ) {
			AddToErrorText ((char *)" from GetAllInhibitsWheelA");
			return status;
		}			
		inhibit |= (data << (i*2)); 

	}


	// digital part
	i = 12;
	for (j=9; j<15; j++) {
		if ((status = Get_INHIBIT(handle, elmbAddress, j, portC, elmbTimeOut, &data)) < 0 ) {
			AddToErrorText ((char *)" from GetAllInhibitsWheelA");
			return status;
		}			 
		inhibit |= (data << i); 
		i+=2;
	}

	*values = inhibit;
	return status;


}

//**********************GetAllInhibitsWheelB************************//
//
/// Syntax: int lvppSharedLib::GetAllInhibitsWheelA(int handle, int elmbAddress, 
//				int portC, int elmbTimeOut, int *values)
//
//
// Function:
//
//	Read all inhibits for a 'Wheel A' type board, data are coded in one integer:
//		16 bits: bit 1: dtmroc #1, regulator 1
//               bit 2: dtmroc #2, regulator 2,
//						...
//               bit 16: dtmroc #14, regulator 2
//		if bit is set, the regulator is switched on.
//		
//	
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int elmbTimeOut - foreseen for future tests
//  int *values - 1 integer
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::GetAllInhibitsWheelB(int handle, int elmbAddress, int portC, int elmbTimeOut, int *values)
{
int status;
int i, j;
int data;
int inhibit;

	// analog part
    inhibit = 0;
	for (i=0; i<4; i++) {
		if ((status = Get_INHIBIT(handle, elmbAddress, i+1, portC, elmbTimeOut, &data)) < 0 ) {
			AddToErrorText ((char *)" from GetAllInhibitsWheelB");
			return status;
		}			
		inhibit |= ((data&0x1) << i); 
	}

	
	for (j=5; j<7; j++) {
		if ((status = Get_INHIBIT(handle, elmbAddress, j, portC, elmbTimeOut, &data)) < 0 ) {
			AddToErrorText ((char *)" from GetAllInhibitsWheelB");
			return status;
		}			
		inhibit |= (data << i);
		i+=2;
	}


	// digital part

	for (j=9; j<13; j++) {
		if ((status = Get_INHIBIT(handle, elmbAddress, j, portC, elmbTimeOut, &data)) < 0 ) {
			AddToErrorText ((char *)" from GetAllInhibitsWheelB");
			return status;
		}			 
		inhibit |= ((data&0x1) << i); 
		i++;

	}

	for (j=13; j<15; j++) {
		if ((status = Get_INHIBIT(handle, elmbAddress, j, portC, elmbTimeOut, &data)) < 0 ) {
			AddToErrorText ((char *)" from GetAllInhibitsWheelB");
			return status;
		}			 
		inhibit |= (data << i); 
		i+=2;

	}

	*values = inhibit;
	return status;


}


//**********************Set_INHIBIT************************//
//
// Syntax: int lvppSharedLib::Set_INHIBIT(int handle, int elmbAddress, int dtmrocAddress, 
//							int portC, int inhibitNb, int inhibitValue)
//
// Function:
//
//	set requested inhibit (0, 1 or both) in one dtmroc:
//		inhibitNb == 1 -> inhibit0 is set/reset
//		inhibitNb == 2 -> inhibit 1 is set/reset
//		inhibitNb == 3 -> both inhibits are set/reset
//
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int dtmrocAddress - address of dtmroc (1-6, 9-14);
//	int inhibitNb - which inhibit set/reset
//  int inhibitValue - 0 or 1
//
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()

int lvppSharedLib::Set_INHIBIT(int handle, int elmbAddress, int address, int portC, int inhibitNb, 
						int inhibitValue, int elmbTimeOut)
{
int Field6;
int inh1, inh0;
int status;

	if ((inhibitNb != 1) && (inhibitNb != 2) && (inhibitNb != 3)) {
	  SetInternalErrorText ((char *)"Set_INHIBIT", lvppSL_DTMROC_INH_BadInhNumber);
		return lvppSL_DTMROC_INH_BadInhNumber;
	}

	if ((inhibitValue != 0) && (inhibitValue != 1)) {
	  SetInternalErrorText ((char *)"Set_INHIBIT", lvppSL_DTMROC_INH_BadInhValue);
		return lvppSL_DTMROC_INH_BadInhValue;
	}

	// now we read Configuration register - to know status of inhibits
//	BC (handle, 300, elmbAddress, portC);
	if ((status = Read (handle, elmbAddress, address, ConfigRegister, portC, elmbTimeOut, &Field6)) < 0) {//przecztanie Configuration Register
		AddToErrorText ((char *)" from Set_INHIBIT");
		return (status);
	}
		
//	printf("\nSet Inhibit Configuration Register before setting %x\n",Field6);

	inh0 = Field6 & 0x20;	//wyzerowanie niepotrzebnych bitow (oprocz 5 (INHIBIT 0))
	inh0 = inh0 >> 5;	//przesuniecie w prawo 

	inh1 = Field6 & 0x10;	//wyzerowanie niepotrzebnych bitow(oprocz 4 (INHIBIT 1))
	inh1 = inh1 >> 4;	//przesuniecie w prawo

//	printf("\n\n\tINHIBIT_0 = %d    INHIBIT_1 = %d  \n", inh0, inh1);
	switch (inhibitNb) {
		case 1:		// inhibit 0 is changed, inhibit 1 remains without change
			if (inhibitValue == 0) {
				Field6 = Field6 & 0xFFDF;
			}
			else {
				Field6 = Field6 | 0x20;
			}
			break;

		case 2:		// inhibit 0 is changed, inhibit 1 remains without change
			if (inhibitValue == 0) 
				Field6 = Field6 & 0xFFEF;
			else 
				Field6 = Field6 | 0x10;
			break;

		case 3:		// both inhibits are changed
			if (inhibitValue == 0) 
				Field6 = Field6 & 0xFFCF;
			else 
				Field6 = Field6 | 0x30;
			break;

	}

//	printf ("Set_INIHIBIT - Field6 to be written: %X\n", Field6);
	if ((status = Write (handle, elmbAddress, address, ConfigRegister, Field6, portC, elmbTimeOut)) < 0 ) {	//zapisanie nowej wartosci do Configuration Register
		AddToErrorText ((char *)" from Set_INHIBIT");
		return (status);
	}
/*
	licz = 4;
	do {

		if ((status = Read(handle, elmbAddress, address, 1, portC, &Field6Tmp)) < 0 ) {//przeczytanie na nowo Configuration Register

			AddToErrorText ((char *)" from Set_INHIBIT");

			return (status);

		}
		if (!licz--) break;
	} while (!Field6);
*/

	return status;

}

//**********************SetInhBrdc************************//
//
// Syntax: lvppSharedLib::SetInhBrdc(int handle, int elmbAddress, int portC, int inhibitValue);					int portC, int inhibitNb, int inhibitValue)
//
// Function:
//
//	set/reset all inhibits in entire board - broadcast mode
//  without check
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int dtmrocAddress - address of dtmroc (1-6, 9-14);
//  int inhibitValue - 0 - set or 1 - reset 
//
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()

int lvppSharedLib::SetInhBrdc(int handle, int elmbAddress, int portC, int inhibitValue, int elmbTimeOut)
{
	int Field6;
	int status;
	char s[80];

	if ((inhibitValue < 0) || (inhibitValue > 3)) {
		sprintf (s, "Bad inhibit value: %d (should be: 0, 1, 2, or 3)", inhibitValue); 
		SetInternalErrorText (s, lvppSL_DTMROC_INH_BadInhValue);
		return (lvppSL_DTMROC_INH_BadInhValue);
	}

	if (inhibitValue == 0) 
		Field6 = 0;
	if (inhibitValue == 1)	// INHIBIT 0
		Field6 = 0x20;
	if (inhibitValue == 2)	// INHIBIT 1
		Field6 = 0x10;
	if (inhibitValue == 3)
		Field6 = 0x30;

	
 //   printf ("Field6(INH): 0x%X\n", Field6); //bk

	if ((status = Write (handle, elmbAddress, 0x3F, ConfigRegister, Field6, portC, elmbTimeOut)) < 0 ) {	//zapisanie nowej wartosci do Configuration Register
		AddToErrorText ((char *)" from Set_INHIBIT");
		return (status);
	}

	return status;
}

//**********************InhBrdcCheck************************//
//
// Syntax:lvppSharedLib::InhBrdcCheck(int handle, int elmbAddress, int portC, int inhibitValue);
//
// Function:
//
//	check, whether all inhibits in board where properly set/reset
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int dtmrocAddress - address of dtmroc (1-6, 9-14);
//  int inhibitValue - 0 - set or 1 - reset 
//
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()

int lvppSharedLib::InhBrdcCheck(int handle, int elmbAddress, int portC, int inhibitValue, int elmbTimeOut)
{
	int Field6;
	int checkInh;
	int status;
	int i;
	char text[80];

	// now we read Configuration register - to know status of inhibits
	for (i=0; i<12; i++) {
		if ((status = Read(handle, elmbAddress, dtmrocAddress(i), ConfigRegister, portC, elmbTimeOut, &Field6)) < 0) {//przecztanie Configuration Register
			AddToErrorText ((char *)" from Set_INHIBIT");
			return (status);
		}
//	printf("\nSet Inhibit Configuration Register before setting %x\n",Field6);
		checkInh = Field6 & 0x30;	//wyzerowanie niepotrzebnych bitow (oprocz 5 (INHIBIT 0))

		switch (inhibitValue) {
			case 3:
				if (checkInh != 0x30) {
					sprintf (text, "dtmroc #%d: both regulators should be ON.", 
									dtmrocAddress(i));

					SetInternalErrorText (text, lvppSL_DTMROC_INH_Brdc_BadSet);
					return lvppSL_DTMROC_INH_Brdc_BadSet;
				}
				break;
			case 2:
				if (checkInh != 0x10) {	// INHIBIT 0
					sprintf (text, "dtmroc #%d: 'second' regulator should be ON.", 
									dtmrocAddress(i));

					SetInternalErrorText (text, lvppSL_DTMROC_INH_Brdc_BadSet);
					return lvppSL_DTMROC_INH_Brdc_BadSet;
				}
				break;
			case 1:
				if (checkInh != 0x20) { // INHIBIT 1
					sprintf (text, "dtmroc #%d: first regulator should be ON.", 
									dtmrocAddress(i));

					SetInternalErrorText (text, lvppSL_DTMROC_INH_Brdc_BadSet);
					return lvppSL_DTMROC_INH_Brdc_BadSet;
				}
				break;
			case 0:
				if (checkInh != 0) {
					sprintf (text, "dtmroc #%d: both regulators should be OFF.", 
									dtmrocAddress(i));

					SetInternalErrorText (text, lvppSL_DTMROC_INH_Brdc_BadSet);
					return lvppSL_DTMROC_INH_Brdc_BadSet;
				}
				break;
		}
	}
	return status;
}

//**********************SetInhBrdcCheck************************//
//
// Syntax: lvppSharedLib::SetInhBrdcCheck (int handle, int elmbAddress, int portC, int inhibitValue);					int portC, int inhibitNb, int inhibitValue)
//
// Function:
//
//	set/reset all inhibits in entire board - broadcast mode
//  with check
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int dtmrocAddress - address of dtmroc (1-6, 9-14);
//  int inhibitValue - 0 - set or 1 - reset 
//
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()


int lvppSharedLib::SetInhBrdcCheck (int handle, int elmbAddress, int portC, int inhibitValue, int elmbTimeOut)
{
int status;

	// broadcast set 
	if ((status = SetInhBrdc (handle, elmbAddress, portC, inhibitValue, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from WriteDacBrdcCheck");
		return status;
	}	
	
	// check;
	if ((status = InhBrdcCheck (handle, elmbAddress, portC, inhibitValue, elmbTimeOut)) < 0) { 
		AddToErrorText ((char *)" from WriteDacBrdcCheck");
		return status;
	}

	return (status);
}

//**********************SetInhibitsAll************************//
//
// Syntax: lvppSharedLib::SetInhibitsAll(int handle, int elmbAddress, int portC, int inhibitValue);					int portC, int inhibitNb, int inhibitValue)
//
// Function:
//
//	set/reset all inhibits in entire board - one-by-one 
//  after set - a check is performed.
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int portC	- port C value;
//  int boardType: 6 - Wheel A
//                 8 - WheelB/Barrel
//  int channelSkip - address of dtmroc (1-6, 9-14) to be skipped, none: 0;
//  int inhibitValue: 0 - both inhibits set
//                    1 - inhibit 1 not set, 2 - set
//                    2 - inhibit 2 set, 2 - not set 
//                    3 - both inhibits not set
//
// Output parameters:
//  int badChannels - if a channel is NOT set/rest properly - 1
//                    otherwise - 0.
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()

int lvppSharedLib::SetInhibitsAllOn(int handle, int elmbAddress, int portC, 
						   int boardType, int channelSkip, int elmbTimeOut, int *inhibits)
{
int i, iWB=0;
int dtmrocOff = 0;	// switch off a skipped channel completely before switching on entire board
int inhibitNb[12];
int inhibitsAll[12];
int dtmroc[12];
int data;
int status;
   
	*inhibits = 0;
    if (boardType == 6) {
		for (i=0; i<12; i++) inhibitNb[i] = 3;
		for (i=0; i<12; i++) dtmroc[i]= dtmrocAddress(i);
	}
	else {
		for (i=0; i<4; i++) {inhibitNb[i] = 1; dtmroc[i]= dtmrocAddress(i);}
		for (i=6; i<11; i++) {inhibitNb[i] = 1; dtmroc[i]= dtmrocAddress(i);}
		dtmroc[4]=5; dtmroc[5]=6;
		dtmroc[10]=13; dtmroc[11]=14;
		inhibitNb[4]=3; inhibitNb[5]=3; 
		inhibitNb[10]=3; inhibitNb[11]=3; 
		if (channelSkip >4) {
			if (channelSkip == 5)  {inhibitNb[4] = 2; dtmrocOff=4;}
			if (channelSkip == 6)  {inhibitNb[4] = 1; dtmrocOff=4;}
			if (channelSkip == 7)  {inhibitNb[5] = 2; dtmrocOff=5;}
			if (channelSkip == 8)  {inhibitNb[5] = 1; dtmrocOff=5;}

			printf ("CH skip: %d inhibit[4] = %d  inhibit[5] = %d\n", channelSkip,inhibitNb[4], inhibitNb[5]);
			channelSkip = 0;
		}
	}

	if (dtmrocOff) {
		if ((status = Set_INHIBIT(handle, elmbAddress, dtmrocAddress(dtmrocOff), portC, 3, 0, elmbTimeOut)) < 0 ) {
			usleep(100);
			if ((status = Set_INHIBIT(handle, elmbAddress, dtmrocAddress(dtmrocOff), portC, 3, 0, elmbTimeOut)) < 0 ) {
				AddToErrorText ((char *)" from SetInhibitsAll");
				return status;
			}
		}
	}	
	for (i=0; i<12; i++) {
		if (i != (channelSkip -1)) {
			if ((status = Set_INHIBIT(handle, elmbAddress, dtmroc[i], portC, inhibitNb[i], 1, elmbTimeOut)) < 0 ) {
				usleep(100);
				if ((status = Set_INHIBIT(handle, elmbAddress, dtmroc[i], portC, inhibitNb[i], 1, elmbTimeOut)) < 0 ) {
					AddToErrorText ((char *)" from SetInhibitsAll");
					return status;
				}
			}
			inhibitsAll[i] = inhibitNb[i];
		}
		else {
			if ((status = Set_INHIBIT(handle, elmbAddress, dtmroc[i], portC, inhibitNb[i], 0, elmbTimeOut)) < 0 ) {
				usleep(100);
				if ((status = Set_INHIBIT(handle, elmbAddress, dtmroc[i], portC, inhibitNb[i], 0, elmbTimeOut)) < 0 ) {
					AddToErrorText ((char *)" from SetInhibitsAll");
					return status;
				}
			}
			inhibitsAll[i] = 0;
		}

	}


	for (i=0; i<12; i++) {
		if ((status = Get_INHIBIT(handle, elmbAddress, dtmrocAddress(i), portC, elmbTimeOut, &data)) < 0 ) {
			AddToErrorText ((char *)" from GetAllInhibitsWheelA");
			return status;
		}
		if (data != inhibitsAll[i]) 
			*inhibits |=0x8000000;			
		if (boardType == 8) {
			if ((i<4) || ((i>5) && (i<10)))  {*inhibits |= ((0x01&data) << iWB); iWB++;}
			else {*inhibits |= (data << iWB); iWB+=2;}
		}
		else 
			*inhibits |= (data << (i*2));
	}
  
	return status;
}



//**********************Read_OCM************************//
//
// Syntax: int lvppSharedLib::Read_OCM(int handle, int elmbAddress, int section, 
//						int portC, int *digOCM, int *negativeOCM, int *positiveOCM);
//
//
// Function:
//
///	read OCM values for one section, e.g. 1 from 12:
//				analog positive, analog negative and digital
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int section - 0 - 11 ;
//	int portC - a value to be written to port C 
//				(port A and C are written with the same command)
//
// Output value:
//
//	int *digOCM - OCM for digital part: bit set to 1 - Overcurrent occured
//	int *negativeOCM - OCM for analog negative part:  bit set to 1 - Overcurrent occured
//	int *positiveOCM - OCM for analog positive part:  bit set to 1 - Overcurrent occured
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()

int lvppSharedLib::Read_OCM(int handle, int elmbAddress, int section, int portC, int *ocmValues)

{
int licznik = 0;
char BufferRead[16];
int Field6 = 0x0000;
int DIGITAL_OCM;
int NEGATIVE_OCM;
int POSITIVE_OCM;
unsigned int Time;
unsigned int Length = 0x0000;
unsigned int Flag = 0x0000;
int L;
int status;
int address;
int canID;

/*
	if ((section < 0) ||  (section > 11)) {	// error - we have only 12 sections!
		SetInternalErrorText ("Read_OCM", lvppSL_DTMROC_OCM_BadSection);
		return lvppSL_DTMROC_OCM_BadSection;
	}
*/  
//Bartek 
	// first regulator
	address = (section + portC);
	if ((status = socketCanWrite (handle, cobID_RPDO1|elmbAddress, &address, 1, 0)) < 0) {
		SetErrorText ((char *)"canWrite Address in Read_OCM", status);
		return status;
	}

	// read request
	if ((status = socketCanWrite (handle, cobID_TPDO1|elmbAddress, 0, 1, CAN_RTR_FLAG)) < 0) {
		SetErrorText ((char *)"canWrite Read Request in Read_OCM", status);
		return status;
	}

        // read socket
        canID = cobID_TPDO1|elmbAddress;
	if ((status = socketCanRead (handle, &canID, (void *) &BufferRead[0] ,&Length, &Flag, &Time))< 0) {
	  SetErrorText ((char *)"socketCanRead in sendWriteSDO", status);
		   return status;
          };

	/* obsolete KVASER
	stat = canReadSyncSpecific(handle,cobID_TPDO1|elmbAddress,2000);

	if (stat < 0) {
		SetErrorText ((char *)"canReadSyncSpecific in Read_OCM", stat);
		return 0;
	}

	stat = canReadSpecificSkip(handle,cobID_TPDO1|elmbAddress, &BufferRead ,&Length, &Flag, &Time);

	if (stat < 0) {
		SetErrorText ((char *)"canReadSpecificSkip in Read_OCM", stat);
		return 0;
	}
	*/
 //   L = (BufferRead & 0xFF);
	L = (int) (BufferRead[0] & 0x7);
	// printf ("L %X BufferRead %X\n", L, BufferRead);

	DIGITAL_OCM = L & 0x4;
	NEGATIVE_OCM = L & 0x2;
	POSITIVE_OCM = L & 0x1;

//    DIG_OCM      = L & 0xff;
//	NEGATIVE_OCM = L & 0xFF;
//	POSITIVE_OCM = L & 0xFF;

#ifdef _CONSOLE
	
	//printf ("\n\n\t\tDIG_OCM = %d",DIG_OCM);

	//printf ("\n\n\t\tAnalog NEGATIVE_OCM = %d",NEGATIVE_OCM);

	//printf ("\n\n\t\tAnalog POSITIVE_OCM = %d\n",POSITIVE_OCM); 
	
	// bartek

#endif

	ocmValues[0] = DIGITAL_OCM;
	ocmValues[1] = POSITIVE_OCM;
	ocmValues[2] = NEGATIVE_OCM;

	return status;	

}


//**********************ocmDigitalSectionNb************************//
//
// Syntax: int lvppSharedLib::ocmDigitalSectionNb(int section);
//
//
// Function:
//
///	returns real address for mutiplexer in Digital_OCM
//
// Input parameters:
//
//	int section - section number as it should be: started from 0 to 11;
//
// Return value:
//
// "real" section address in the multiplexer
//

int lvppSharedLib::ocmAnalogSectionNb (int section)
{
int ocmAn = section;

	if (section == 1) ocmAn = 8;
	if (section == 2) ocmAn = 4;
	if (section == 3) ocmAn = 12;
	if (section == 4) ocmAn = 2;
	if (section == 5) ocmAn = 10;
	if (section == 7) ocmAn = 15;
	if (section == 8) ocmAn = 1;
	if (section == 10) ocmAn = 5;
	if (section == 11) ocmAn = 13;
	return (ocmAn);

}

//**********************Read_OCM_All_WheelA************************//
//
// Syntax: int lvppSharedLib::Read_OCM_All_WheelA(int handle, int elmbAddress, int section, 
//						int portC, int *digOCM, int *negativeOCM, int *positiveOCM);
//
//
// Function:
//
///	read OCM values for one section, e.g. 1 from 12:
//				analog positive, analog negative and digital
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int section - 0 - 11 ;
//	int portC - a value to be written to port C 
//				(port A and C are written with the same command)
//
// Output value:
//
//	int *digOCM - OCM for digital part: bit set to 1 - Overcurrent occured
//	int *negativeOCM - OCM for analog negative part:  bit set to 1 - Overcurrent occured
//	int *positiveOCM - OCM for analog positive part:  bit set to 1 - Overcurrent occured
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()

int lvppSharedLib::Read_OCM_All_WheelA(int handle, int elmbAddress, int portC, int *ocmValues)

{
int status;
int section;
int ocmValuesSingle[3];
int DIGITAL_OCM_ALL = 0;;
int NEGATIVE_OCM_ALL = 0;
int POSITIVE_OCM_ALL = 0;

// int lvppSharedLib::Read_OCM(int handle, int elmbAddress, int section, int portC, int *digOCM, int *negativeOCM, int *positiveOCM)

	for(section = 0; section < 12; section++) {	
		// analog part
		if ((status = Read_OCM (handle, elmbAddress, ocmAnalogSectionNb(section), portC, ocmValuesSingle)) < 0) {
			AddToErrorText ((char *)" from Read_OCM");
			return (status);			
		}
		if (ocmValuesSingle[1] == 1) POSITIVE_OCM_ALL |= (1<<section);

		if (ocmValuesSingle[2] == 2) NEGATIVE_OCM_ALL |= (1<<section);

		// digital part
		if ((status = Read_OCM (handle, elmbAddress, section, portC, ocmValuesSingle)) < 0) {
			AddToErrorText ((char *)" from Read_OCM");
			return (status);			
		}
		if (ocmValuesSingle[3] == 4) DIGITAL_OCM_ALL |= (1<<section);
	}

	ocmValues[0] = DIGITAL_OCM_ALL;
	ocmValues[1] = POSITIVE_OCM_ALL;
	ocmValues[2] = NEGATIVE_OCM_ALL;

	return (status);	

}

//**********************Read_OCM_All_WheelB************************//
//
// Syntax: int lvppSharedLib::Read_OCM_All(int handle, int elmbAddress, int section, 
//						int portC, int *digOCM, int *negativeOCM, int *positiveOCM);
//
//
// Function:
//
///	read OCM values for one section, e.g. 1 from 12:
//				analog positive, analog negative and digital
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int section - 0 - 11 ;
//	int portC - a value to be written to port C 
//				(port A and C are written with the same command)
//
// Output value:
//
//	int *digOCM - OCM for digital part: bit set to 1 - Overcurrent occured
//	int *negativeOCM - OCM for analog negative part:  bit set to 1 - Overcurrent occured
//	int *positiveOCM - OCM for analog positive part:  bit set to 1 - Overcurrent occured
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()

int lvppSharedLib::Read_OCM_All_WheelB(int handle, int elmbAddress, int portC, int *ocmValues)

{
int status;
int section;
int ocmValuesSingle[3];
int DIGITAL_OCM_ALL = 0;;
int NEGATIVE_OCM_ALL = 0;
int POSITIVE_OCM_ALL = 0;

// int lvppSharedLib::Read_OCM(int handle, int elmbAddress, int section, int portC, int *digOCM, int *negativeOCM, int *positiveOCM)

	for(section = 0; section < 4; section++) {	
		// analog part
		if ((status = Read_OCM (handle, elmbAddress, ocmAnalogSectionNb(section*2), portC, ocmValuesSingle)) < 0) {
			AddToErrorText ((char *)" from Read_OCM");
			return (status);			
		}
		if (ocmValuesSingle[1] == 1) POSITIVE_OCM_ALL |= (1<<section);
		if (ocmValuesSingle[2] == 2) NEGATIVE_OCM_ALL |= (1<<section);

		// digital part
		if ((status = Read_OCM (handle, elmbAddress, section*2, portC, ocmValuesSingle)) < 0) {
			AddToErrorText ((char *)" from Read_OCM");
			return (status);			
		}
		if (ocmValuesSingle[0] == 4) DIGITAL_OCM_ALL |= (1<<section);
	}
	
	for(section = 8; section < 12; section++) {	
		// analog part
		if ((status = Read_OCM (handle, elmbAddress, ocmAnalogSectionNb(section), portC, ocmValuesSingle)) < 0) {
			AddToErrorText ((char *)" from Read_OCM");
			return (status);			
		}
		//sectionB = section-4;
		if (ocmValuesSingle[1] == 1) POSITIVE_OCM_ALL |= (1<<(section-4));
		if (ocmValuesSingle[2] == 2) NEGATIVE_OCM_ALL |= (1<<(section-4));

		// digital part
		if ((status = Read_OCM (handle, elmbAddress, section, portC, ocmValuesSingle)) < 0) {
			AddToErrorText ((char *)" from Read_OCM");
			return (status);			
		}
		if (ocmValuesSingle[0] == 4) DIGITAL_OCM_ALL |= (1<<(section-4));
	}

	ocmValues[0] = DIGITAL_OCM_ALL;
	ocmValues[1] = POSITIVE_OCM_ALL;
	ocmValues[2] = NEGATIVE_OCM_ALL;

	return (status);	

}






//**********************Write_C************************//
//
// Syntax: int lvppSharedLib::Write_C(int handle, int elmbAddress, int Port_C);
//
// Function:
//
//	write data to ELMB port C (8 bits)
//		
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//	int portC - a value to be written to port C 
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//

int lvppSharedLib::Write_C(int handle, int elmbAddress, int Port_C)

{
int status;
int licznik = 0;

	if ((status = socketCanWrite (handle, cobID_RPDO1|elmbAddress, &Port_C, 1, 0)) < 0) {
		SetErrorText ((char *)"canWrite Port_C in Write_C", status);
		return status;
	}

	return status;

}


//**********************Get_ELMB_CANBufferOverrun************************//
//
// Syntax: bool lvppSharedLib::Get_ELMB_CANBufferOverrun (int handle, int elmbAddress);
//
// Function:
//
//	ead ELMB Error register
//		
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//
// Return value:
//
//			overrun - TRUE - in case of CAN buffer overrun Emergency Error
//					  FALSE - buffer not full
//

int lvppSharedLib::Get_ELMB_CANBufferOverrun (int handle, int elmbAddress, bool *overrun)
{	
  int status;
  unsigned char buffer[16];
  unsigned int length, flag;
  unsigned int time;
  bool keepChecking = true;
  int loopCnt = 2000;
  int canID;

  // ask for overrun error code
  if ((status = socketCanWrite (handle, cobID_Emergency|elmbAddress,NULL,0,CAN_RTR_FLAG)) < 0) {
    SetErrorText ((char *)"canWrite read back command in Read_ELMB_State", status);
    return status;
  }

  // socket read
  while (keepChecking && (loopCnt--)) {
    if ((status = socketCanRead (handle, &canID, (void *) &buffer, &length, &flag, &time))< 0) {
      SetErrorText ((char *)"socketCanRead in sendWriteSDO", status);
      return false;
    };
    if (canID ==  (cobID_Emergency|elmbAddress)) keepChecking = false;
  }
  
  if (loopCnt > 0) status = lvppSL_OK;
  else status = lvppSL_ELMB_ReadSocketTimeout;

  // prepare output value
  if (buffer[0] == 0x8110) *overrun =  true;
  else *overrun = false;
	      
  return status;

}

int lvppSharedLib::Get_ELMB_EmergencyError (int handle, int elmbAddress, unsigned char *emErr)
{	
 int status;
 unsigned char buffer[16];
 unsigned int length, flag;
 unsigned int time;
 int canID;
 bool keepChecking;
 int loopCnt = 20000;
 int i;

	// ask for emergency error code
  if ((status = socketCanWrite (handle, cobID_Emergency|elmbAddress,NULL,0,CAN_RTR_FLAG)) < 0) {
    SetErrorText ((char *)"canWrite read back command in Read_ELMB_State", status);
    return status;
  }

#ifdef PRINT
  printf ("Get_ELMB_EmergencyError: socketCanWrite cobID 0x%X\n", cobID_Emergency|elmbAddress);
#endif

        // read socket - check this code
  keepChecking = true;
  while (keepChecking && (loopCnt--)) {
    if ((status = socketCanRead (handle, &canID, (void *) &buffer, &length, &flag, &time))< 0) {
      SetErrorText ((char *) "socketCanRead in sendWriteSDO", status);
      return status;
    };
    if (canID ==  (cobID_Emergency|elmbAddress)) keepChecking = false;
  }
#ifdef PRINT
  printf ("Get_ELMB_EmergencyError: canID 0x%X  length %d  keepChecking %d  loopCnt %d \n",
	  canID, length, keepChecking, loopCnt);
  for (i=0; i<length; i++) printf (" %X ", buffer[i]);
  printf ("\n");
#endif 
 
  if (loopCnt > 0) {memcpy(emErr, buffer, 8); status = lvppSL_OK; }
  else status = lvppSL_ELMB_ReadSocketTimeout;

  return status;

}

int lvppSharedLib::Get_ELMB_SerialNumber (int handle, int elmbAddress, unsigned char *elmbSerialNumber)
{
unsigned long data;
int elmbTimeOut = 2000;
int i, status;

	if ((status = sendReadSDO(handle, elmbAddress, 0x3100, 0, &data,
		4, 0x43, elmbTimeOut)) < 0) {
		AddToErrorText((char *)" from Get_ELMB_SerialNumber");
		return status;
	}
    for (i=0; i<5; i++) elmbSerialNumber[i] = 0;

	for (i=0; i<4; i++) {
		elmbSerialNumber[i] |= (data >> 8*i);
	}
	elmbSerialNumber[4] =  '\0';
	return status;

}


//**********************Get_ELMB_CANBufferOverrun************************//
//
// Syntax: bool lvppSharedLib::Get_ELMB_CANBufferOverrun (int handle, int elmbAddress);
//
// Function:
//
//	ead ELMB Error register
//		
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//
// Return value:
//
//			overrun - TRUE - in case of CAN buffer overrun Emergency Error
//					  FALSE - buffer not full
//

int lvppSharedLib::ReadADC_Counts (int handle, int elmbAddress, int *values, bool *convStat)
{
int status;
int chan;
unsigned int adcValue;
unsigned int length, flag;
unsigned int time;
int canID;

	// read request for ADC
	if ((status = socketCanWrite (handle, cobID_TPDO2|elmbAddress, 0, 4, CAN_RTR_FLAG)) < 0) {
		SetErrorText ((char *)"canWrite COMMAND in ReadADC_Counts", status);
		return status;
	}

	for (chan = 0; chan < 64; chan++) {
		 // wait for data
	  // read socket
          canID =  cobID_TPDO2|elmbAddress;
	  if ((status = socketCanRead (handle, &canID, (void *) &adcValue, &length, &flag, &time))< 0) {
	    SetErrorText ((char *) "socketCanRead in sendWriteSDO", status);
		   return status;
          };
	  /* obsolete KVASER 
         stat = canReadSyncSpecific (handle, cobID_TPDO2|elmbAddress,2000);
           
		if (stat < 0) {
			SetErrorText ("canReadSyncSpecific in ReadADC_Counts", stat);
			return (int) stat;
		}

        
		stat = canReadSpecificSkip(handle, cobID_TPDO2|elmbAddress, &adcValue, &length,&flag,&time);
		if (stat < 0) {
			SetErrorText ("canReadSpecificSkip in ReadADC_Counts", stat);
			return (int) stat;
		}
	  */
	//   printf ("adc chan: %d  value 0x%X\n", chan, adcValue);
	   if (adcValue & 0x8000) convStat[chan] = true; else convStat[chan] = false;
	   values[chan] = (adcValue >> 16) & 0xFFFF;

	}

	return status;
	
}


int lvppSharedLib::ReadADC_uVolts (int handle, int elmbAddress, int *values, bool *convStat)
{
int status;
int chan;
unsigned short adcValue[3];
unsigned int length, flag;
unsigned int time;
int canID;

 	// read request for ADC
	if ((status = socketCanWrite (handle, cobID_TPDO3|elmbAddress, NULL, 0, CAN_RTR_FLAG)) < 0) {
		SetErrorText ((char *)"canWrite COMMAND in ReadADC_Counts", status);
		return status;
	}
   //     printf ("After s socketCanWrite\n");
 

	for (chan = 0; chan < 64; chan++) {
		 // wait for data
	  // read socket
          canID =  cobID_TPDO3|elmbAddress; 
	  if ((status = socketCanRead (handle, &canID, (void *) adcValue, &length, &flag, &time))< 0) {
	    SetErrorText ((char *)"socketCanRead in sendWriteSDO", status);
		   return status;
          };
	  /* obsolete KVASER
         stat = canReadSyncSpecific (handle, cobID_TPDO3|elmbAddress,10000);
           
		if (stat < 0) {
			SetErrorText ("canReadSyncSpecific in _uVolts", stat);
			return (int) stat;
		}

        
		stat = canReadSpecificSkip(handle, cobID_TPDO3|elmbAddress, adcValue, &length,&flag,&time);
		if (stat < 0) {
			SetErrorText ("canReadSpecificSkip in ReadADC_uVolts", stat);
			return (int) stat;
		}
              */
	   if (adcValue[0] & 0x8000) convStat[chan] = true; else convStat[chan] = false;

	   values[chan] = (adcValue[2] << 16) | adcValue[1];

	 //  printf ("adc chan: %d  value 0x%X 0x%X 0x%X\n", chan, adcValue[0], adcValue[1], adcValue[2]);

	}

	return status;
	
}

int lvppSharedLib::sendWriteSDO(int handle, int elmbAddress, int index, int subIndex, unsigned long data,
						 int nbBytes, int ctrlWord, int elmbTimeOut)
{
bool keepChecking;
int indexHigh, indexLow;
int i;
int status;
unsigned char buffer[16], bufferOut[16];
int sdoID;
unsigned int length, flag;
unsigned int time;

	sdoID = cobID_SDORX | elmbAddress; 
	indexLow = index & 0xFF; indexHigh = (index>>8) & 0xFF;
	buffer[0] = ctrlWord; buffer[1] = indexLow; buffer[2] = indexHigh; buffer[3] = subIndex;
	
	for (i=0; i<4; i++) buffer[4+i]= 0;
	for( i=0; i<nbBytes; i++ ) buffer[4+i] = (data >> 8*i) & 0xFF;

	// send SDO message
	if ((status = socketCanWrite (handle, sdoID, &buffer, 8, 0)) < 0) {
		SetErrorText ((char *)"canWrite in sendWriteSDO", status);
		return status;
	}
#ifdef PRINT
    printf ("sendWriteSDO CAN message sent: 0x%X 0x%X  0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X\n",
		     data, sdoID, buffer[0], buffer[1], buffer[2], buffer[3], 
			 buffer[4], buffer[5], buffer[6], buffer[7]);
#endif

     sdoID = cobID_SD0TX | elmbAddress;

	 keepChecking = true;
	while (keepChecking) {
	        // read socket 
	  if ((status = socketCanRead (handle, &sdoID, (void *) bufferOut, &length, &flag, &time))< 0) {
	    SetErrorText ((char *)"socketCanRead in sendWriteSDO", status);
		   return status;
          };
          /* obsolete KVASER
		if ((stat = canReadSyncSpecific (handle, sdoID, elmbTimeOut))< 0) {
			SetErrorText ("canReadSyncSpecific in sendWriteSDO", stat);
			return  (int) stat;
		}
		if ((stat = canReadSpecificSkip (handle, sdoID, bufferOut, &length, &flag, &time))< 0) {
		   SetErrorText ("canReadSyncSpecific in sendWriteSDO", stat);
		   return (int) stat;
		}
		*/
#ifdef PRINT
		printf ("sendWriteSDO CAN message reply: 0x%X  0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X length: %d\n",
		     sdoID, bufferOut[0], bufferOut[1], bufferOut[2], bufferOut[3], 
			        bufferOut[4], bufferOut[5], bufferOut[6], bufferOut[7], length);
#endif
		// Check if the reply contains the proper index/subindex
		if( bufferOut[1] == buffer[1] &&
	      bufferOut[2] == buffer[2] &&
		  bufferOut[3] == buffer[3] ) {
		  keepChecking = false;
		}
	}

	  return status;

}

int lvppSharedLib::sendReadSDO(int handle, int elmbAddress, int index, int subIndex,  unsigned long *data,
						 int nbBytes, int ctrlWord, int elmbTimeOut)
{
bool keepChecking;
int indexHigh, indexLow;
int i;
int status;
unsigned char buffer[16], bufferOut[16];
int sdoID;
unsigned int length, flag;
unsigned int time;

	sdoID = cobID_SDORX | elmbAddress; 
	indexLow = index & 0xFF; indexHigh = (index>>8) & 0xFF;
	buffer[0] = ctrlWord; buffer[1] = indexLow; buffer[2] = indexHigh; buffer[3] = subIndex;
	
	for (i=0; i<4; i++) buffer[4+i]= 0;

	// send SDO message
	if ((status = socketCanWrite (handle, sdoID, &buffer, 8, 0)) < 0) {
		SetErrorText ((char *)"canWrite in SPI_read", status);
		return status;
	}
#ifdef PRINT
    printf ("sendReadSDO CAN message sent: 0x%X 0x%X  0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X\n",
		     data, sdoID, buffer[0], buffer[1], buffer[2], buffer[3], 
			        buffer[4], buffer[5], buffer[6], buffer[7]);
#endif

      	sdoID = cobID_SD0TX | elmbAddress;
#ifdef PRINT
    printf ("SPI_read CAN message sent: 0x%X 0x%X  0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X\n",
		     data, sdoID, buffer[0], buffer[1], buffer[2], buffer[3], 
			        buffer[4], buffer[5], buffer[6], buffer[7]);
#endif
	keepChecking = true;
	while (keepChecking) {
	        // read socket
	  if ((status = socketCanRead (handle, &sdoID, (void *)bufferOut, &length, &flag, &time))< 0) {
	    SetErrorText ((char *)"socketCanRead in sendWriteSDO", status);
		   return status;
          };
                 /* obsolete KVASER 
		if ((stat = canReadSyncSpecific (handle, sdoID, elmbTimeOut))< 0) {
			SetErrorText ("canReadSyncSpecific in sendReadSDO", stat);
			return  (int) stat;
		}
		if ((stat = canReadSpecificSkip (handle, sdoID, bufferOut, &length, &flag, &time))< 0) {
		   SetErrorText ("canReadSyncSpecific in sendReadSDO", stat);
		   return (int) stat;
		}
                */
#ifdef PRINT
		printf ("SPI_read CAN message reply: 0x%X  0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X length: %d\n",
		     sdoID, bufferOut[0], bufferOut[1], bufferOut[2], bufferOut[3], 
			        bufferOut[4], bufferOut[5], bufferOut[6], bufferOut[7], length);
#endif
		// Check if the reply contains the proper index/subindex
		*data = 0;
		if( bufferOut[1] == buffer[1] &&
	      bufferOut[2] == buffer[2] &&
		  bufferOut[3] == buffer[3] ) {
		  for (i=0; i<nbBytes; i++) *data |= (bufferOut[i+4] << 8*i); 
		 // printf ("Dane: %X, nbBytes %d\n", *data, nbBytes);
		  keepChecking = false;
		}
	}

    return status;

}

int lvppSharedLib::SPI_write (int handle, int elmbAddress, unsigned long data, int clocks, int elmbTimeOut)
{
int i, extraByte;
int status;


    if (clocks%8) extraByte = 1; else extraByte =0;
	int nbBytes = clocks/8 + extraByte;
	
	if ((status = sendWriteSDO(handle, elmbAddress, 0x2603, clocks, data,
						      nbBytes, 0x23, elmbTimeOut)) < 0) {
		AddToErrorText ((char *)" from SPI_write");
		return status;
	}	
	return status;

}

int lvppSharedLib::SPI_read (int handle, int elmbAddress, int *data, int clocks, int elmbTimeOut)
{
bool keepChecking;
int i, extraByte;
int loop=1;
int status;
unsigned char buffer[16], bufferOut[16];
unsigned int uMask;
int sdoID;
unsigned int length, flag;
unsigned int time;

	sdoID = cobID_SDORX | elmbAddress; 
	buffer[0] = 0x40; buffer[1] = 0x03; buffer[2] = 0x26; buffer[3] = clocks;
	
	for (i=0; i<4; i++) buffer[4+i]= 0;

	// send SDO message
	if ((status = socketCanWrite (handle, sdoID, &buffer, 8, 0)) < 0) {
		SetErrorText ((char *)"canWrite in SPI_read", status);
		return status;
	}
#ifdef PRINT
    printf ("SPI_read CAN message sent: 0x%X 0x%X  0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X\n",
		     data, sdoID, buffer[0], buffer[1], buffer[2], buffer[3], 
			        buffer[4], buffer[5], buffer[6], buffer[7]);
#endif

	switch (clocks) {
		case 32:
			uMask = 0xFFFFFFFF;
			break;
		case 24+DTMROC_READ_EXTRA_CLOCKS:
			uMask = 0xFFFFFF;
			break;
		case 16+DTMROC_READ_EXTRA_CLOCKS:
			uMask = 0xFFFF;
			break;
		case DTMROC_READ_EXTRA_CLOCKS:
			uMask = 0xF;
			break;
		default:
			uMask = 0xFFFFFFFF;
			break;

	}

    if ((clocks-DTMROC_READ_EXTRA_CLOCKS)%8) extraByte = 1; else extraByte =0;
	if (clocks == DTMROC_READ_EXTRA_CLOCKS) extraByte = 1;
	int nbBytes = (clocks-DTMROC_READ_EXTRA_CLOCKS)/8 + extraByte;
	if (clocks == 32) nbBytes = 4;

      	sdoID = cobID_SD0TX | elmbAddress;
#ifdef PRINT
    printf ("SPI_read CAN message sent: 0x%X 0x%X  0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X\n",
		     data, sdoID, buffer[0], buffer[1], buffer[2], buffer[3], 
			        buffer[4], buffer[5], buffer[6], buffer[7]);
#endif
	keepChecking = true;
	while (keepChecking) {
	        // read socket
	  if ((status = socketCanRead (handle, &sdoID, (void *)bufferOut, &length, &flag, &time))< 0) {
	    SetErrorText ((char *)"socketCanRead in sendWriteSDO", status);
		   return status;
          };

                /* obsolete KVASER
		if ((stat = canReadSyncSpecific (handle, sdoID, elmbTimeOut))< 0) {
			SetErrorText ("canReadSyncSpecific in SPI_read", stat);
			return  (int) stat;
		}
		if ((stat = canReadSpecificSkip (handle, sdoID, bufferOut, &length, &flag, &time))< 0) {
		   SetErrorText ("canReadSyncSpecific in SPI_read", stat);
		   return (int) stat;
		}
                */
#ifdef PRINT
		printf ("SPI_read CAN message reply: 0x%X  0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X length: %d\n",
		     sdoID, bufferOut[0], bufferOut[1], bufferOut[2], bufferOut[3], 
			        bufferOut[4], bufferOut[5], bufferOut[6], bufferOut[7], length);
#endif
		// Check if the reply contains the proper index/subindex
		*data = 0;
		if( bufferOut[1] == buffer[1] &&
	      bufferOut[2] == buffer[2] &&
		  bufferOut[3] == buffer[3] && bufferOut[0] == 0x43 ) {
		  for (i=0; i<nbBytes; i++) *data |= (bufferOut[i+4] << 8*i); *data &= uMask;
		 // printf ("Dane: %X, nbBytes %d\n", *data, nbBytes);
		  keepChecking = false;
		}
	}
         
	  return status;
  }
	    



int lvppSharedLib::Read(int handle, int elmbAddress, int dtmrocAddress, int object, int portC, int elmbTimeOut, int *value)
{
canStatus stat;
int status;
int Field;
int Field6 = 0;
int dataClocks;
int commandClocks = 27;
int data;


	switch (object) {
		case ConfigRegister:
			Field = FIELD_12345_READ | (dtmrocAddress << 6) | CONF_REG;
			dataClocks = 24;
			break;

		case DAC_01:
			Field = FIELD_12345_READ | (dtmrocAddress << 6) | DAC01_REG;
			dataClocks = 16;
			break;

		case DAC_23:
			Field = FIELD_12345_READ | (dtmrocAddress << 6) | DAC23_REG;
			dataClocks = 16;
			break;

		case StatusRegister:
			Field = FIELD_12345_READ | (dtmrocAddress << 6) | COMM_STAT_REG;
			dataClocks = 32;
			break;
	}

	// send an SDO:
	// 1. command
	if ((status = SPI_write (handle,  elmbAddress, Field,  commandClocks, elmbTimeOut)) < 0 )	{ 
		AddToErrorText ((char *)" from Read");
		return (status);
	}
	// 2. data
	if (object == StatusRegister) {
		// here we send 2 requests, because the data word is 32 bits:
		// 1st we send 5 clocks 
		if ((status = SPI_read (handle,  elmbAddress, &data, 4, elmbTimeOut)) < 0 )	{ 
			AddToErrorText ((char *)" from Read");
			return (status);
		}

		if ((status = SPI_read (handle,  elmbAddress, value,  dataClocks, elmbTimeOut)) < 0 )	{ 
			AddToErrorText ((char *)" from Read");
			return (status);
		}
	}
	else {
		if ((status = SPI_read (handle,  elmbAddress, value,  dataClocks+4, elmbTimeOut)) < 0 )	{ 
			AddToErrorText ((char *)" from Read");
			return (status);
		}
	}

        return status;

}


//**********************Write************************//
//
// Syntax: int lvppSharedLib::Write(int handle, int elmbAddress, int dtmrocAddress, int object, 
//               int Field6, int portC)
//
// Function:
//
//	write data to dedicated dtmroc, in 'object' parameter is set, what to write:
//
//        "object" = 1 -> Configuration Register
//		  "object" = 2 -> DAC 0 & DAC 1
//        "object" = 3 -> DAC 2 & DAC 3
//		
//
// Input parameters:
//
//	int handle - a handle for KVASER channel;
//  int elmbAddress - address of elmb (from 0 to 63);
//  int dtmrocAddress - address of dtmroc (1-6, 9-14);
//  int object - what to write (see above);
//  int Field6 - data to be written;
//	int portC - a value to be written to port C 
//				(port A and C are written with the same command)
//
// Return value:
//
//	0 (canOK) - success
//  <0 - a canlib error code 
//       error message is placed in internal class member and can be retrieved
//       with: GetErrorText()
//
int lvppSharedLib::Write(int handle, int elmbAddress, int dtmrocAddress, int object, int Field6, int portC, int elmbTimeOut, bool bCheck)
{
int dataClocks;
int commandClocks = 27;
unsigned long Field;
unsigned long dataField;
int status;
int value;
char s[50];
   
start:
	switch (object) {
		case ConfigRegister:
			Field = (FIELD_12345_WRITE_CFG | (dtmrocAddress << 6) | CONF_REG);
			dataClocks = 24;
			dataField = (Field6);
			break;

		case DAC_01:
			Field = (FIELD_12345_WRITE_DAC | (dtmrocAddress << 6) | DAC01_REG);
			dataClocks = 16;
			dataField = (Field6);
			break;

		case DAC_23:
			Field = (FIELD_12345_WRITE_DAC | (dtmrocAddress << 6) | DAC23_REG);
			dataClocks = 16;
			dataField = (Field6);
			break;

	}
#ifdef PRINT
		printf ("<Write>  Command: Clocks: %d, Field: 0x%X\n", commandClocks, Field); 
#endif
//int lvppSharedLib::SPI_write (int handle, int elmbAddress, unsigned long data, int clocks)
		if ((status = SPI_write (handle, elmbAddress, Field, commandClocks, elmbTimeOut)) < 0 )	{ 
			AddToErrorText ((char *)" from Write");
			return (status);
		}
#ifdef PRINT
		printf ("<Write>  Data: Clocks: %d, Field: 0x%X\n", dataClocks, dataField); 
#endif
		if ((status = SPI_write (handle,  elmbAddress, dataField,  dataClocks, elmbTimeOut)) < 0 )	{ 
			AddToErrorText ((char *)" from Write");
			return (status);
		}
		if ((status = SPI_write(handle, elmbAddress, 0, 2, elmbTimeOut)) < 0 ) {
			AddToErrorText ((char *)" from Write");
			return (status);
		}
// read back in a case of writing DACs:
		bCheck = false;
		if (bCheck) {
			if ((object == DAC_01) || (object == DAC_01)) {
				if ((status = Read(handle, elmbAddress, dtmrocAddress, object, portC, elmbTimeOut, &value)) < 0 ) {
					AddToErrorText ((char *)" from Write");
					return (status);
				}
				if (value != dataField) {
					sprintf(s, "Write - error in writing to DACs: check 0x%X should be 0x%x", value, dataField);
					SetInternalErrorText(s, lvppSL_DTMROC_Write_NotCorrect);
					return lvppSL_DTMROC_Write_NotCorrect;
				}
			}
			return status;
		}
}
