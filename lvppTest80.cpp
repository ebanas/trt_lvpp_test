/*
  80
*/

#include "lvppSharedLib.h"
#include "lvppTest.h"
#include "lvppTestLib.h"

// global wide declarations
int portCGlobal; // keeps content of port C - global wide 

void printUsage(char *programName)
{

  printf ("Program %s\n", programName);
  printf ("Usage: %s -c 'CAN port' -e ELMB ID -b lvpp card number -v \n");
  
}

main (int argc, char **argv)
{			
FILE *fp80;
FILE *calibFile;

int status;
int i;

lvppSharedLib zasilacz;

int channel;
int cardNb;
int elmbNb;
int handle;
int portCInternal;   // to be suppressed in later version of the library

int adcValues[64];	// array for ADC values (counts or microVolts)
bool adcConvStat[64];	// array for ADC convertion status (OK (0) or error (1))

char dtmrocs[64];
char textBuf[64];

// for calibration
double calibCoeff[36], calibCurrentOffset[36];
double CurrentFactor = CURRENT_FACTOR;		// 
double VoltageFactor = VOLTAGE_FACTOR;  // 

int opt;
bool verbose = false;
		
  fp80 = fopen ("/det/dcs/lvtemp/lvppTestData/IoutVout_80.txt","w");

 while ((opt = getopt(argc, argv, "hvc:e:b:")) != -1) { 
   switch (opt) {
     case 'c':
       channel = atoi(optarg);
       if ((channel < 0 ) ||  (channel > 5)) {
	 printf ("Bad CAN port: %d \n", channel);
	 exit(1);
       }
       break;
     case 'e':
       elmbNb = atoi(optarg);
       if ((elmbNb < 0) && (elmbNb > 64)) {
	 printf ("Bad ELMB ID: %d \n", elmbNb);
	 exit(1);
       }
       break;
     case 'b':
       cardNb = atoi(optarg);
       if ((cardNb < 1) || (cardNb > 564)) {
	 printf ("Bad LVPP card number: %d \n", cardNb);
	 exit(1);
       }
       break;
     case 'v':
       verbose = true;
       break;
     default:
       printUsage(basename(argv[0]));
       exit(1);
       break;
   }
 }

// create calibfilename
  sprintf (textBuf, "/localdisk/LVPP_TEST/lvtemp/lvppCalib/card_%03d/LVPP_CALIB_%03d.csv", cardNb, cardNb);
			
  if ((calibFile = fopen (textBuf, "r")) == NULL) {
    printf ("Calibration file does not exist for card %d - please make calibration\n", cardNb); 
    exit (0);
  }

  readCalibration(calibFile, calibCoeff, calibCurrentOffset, 36);
			
  fclose (calibFile);

  // init CAN, ELMB and DTMROCS
  if ((status = initLVPP (channel, elmbNb, fp80, &handle)) < 0) {
    printf ("initLVPP exit with error: %d\n", status);
    return status;
  }  

  if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
    printf ("%s\n", zasilacz.GetErrorText ());
  }
  else {

    printf ("---------------------------------------------\n");
    fprintf (fp80, "----------------------------------------\n");
    //----------------------------------------------------------------------------------------------------
    fprintf (fp80, "Iout without calibration\n");
    printf ("Iout without calibration\n");

    for (i=0; i<36; i++) {
      fprintf (fp80, " %d\t %f\t \n",i, 
	       adcValues[i]*CurrentFactor);
      printf ("%d\t %d\t %f\n", i, adcConvStat[i], 
	      adcValues[i]*CurrentFactor);
    }

    printf ("---------------------------------------------\n");
    fprintf (fp80, "----------------------------------------\n");
    fprintf (fp80, "Iout calibrated with calibCoeff=adcValue/Vout\n");
    printf ("Iout calibrated with calibCoeff=adcValue/Vout\n");

    for (i=0; i<36; i++) {				
      //	printf ("%d. Conversion Status: %d \t %f\n", 
      //		i, adcConvStat[i], adcValues[i]*CurrentFactor);
//			fprintf (fp, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*CurrentFactor);
//            fprintf (fp80, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*CurrentFactor);
      fprintf (fp80, " %d\t %f\t \n",i, 
	       adcValues[i]*CurrentFactor - (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000);

      printf ("%d\t %d\t %f\n", i, adcConvStat[i], 
	      adcValues[i]*CurrentFactor - (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000);

    // sumowanie pradow
  //              printf (" %d\t %d\t %f\t %f\t %f\n",i,i+1,adcValues[i]*CurrentFactor,adcValues[i+1]*CurrentFactor,
	//		     (adcValues[i]*CurrentFactor)+ (adcValues[i+1]*CurrentFactor));
//			if(i % 2==0){printf ("i+(i+1)= %d\t %d\t %f\n", i, adcConvStat[i],(adcValues[i]*CurrentFactor+adcValues[i+1]*CurrentFactor));}

    }
    printf ("---------------------------------------------\n");
    fprintf (fp80, "----------------------------------------\n");  //Current offset for DAC = 128 counts
    fprintf (fp80, "Current offset for DAC = 128 counts\n");
    printf ("Current offset for DAC = 128 counts\n");

    for (i=0; i<36; i++) {
      fprintf (fp80, " %d\t %f\t \n",i, 
	       adcValues[i]*CurrentFactor - calibCurrentOffset[i]);
      printf ("%d\t %d\t %f\n", i, adcConvStat[i], 
	      adcValues[i]*CurrentFactor - calibCurrentOffset[i]);
    }

    printf ("---------------------------------------------\n");
    fprintf (fp80, "----------------------------------------\n");

    for (i=36; i<60; i++) {
		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*VoltageFactor);
//	fprintf (fp80, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*VoltageFactor);
	
      fprintf (fp80, " %d \t %f\n",i, adcValues[i]*VoltageFactor);
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
 //    fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor);       

    }
    printf ("---------------------------------------------\n");
    for (i=60; i<64; i++) {
//	printf ("%d. Conversion Status: %d \t %d\n", 
//		i, adcConvStat[i], adcValues[i]);
//			fprintf (fp, "%d. Conversion Status: %d \t %d\n",i, adcConvStat[i], adcValues[i]);
      fprintf (fp80, "%d\t %d\t %f\n",i,adcConvStat[i], temperature (adcValues[i]));
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],temperature (adcValues[i]));
    }
  }
			
  fclose (fp80);

  exit (0);

//------------------------------------------------------------------------------------------------
}
