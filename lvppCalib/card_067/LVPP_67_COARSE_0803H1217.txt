============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 67
CALIBRATION FILE: C:\LVPP_TESTS\data\card_067\LVPP_CALIB_067.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
      temperature range: <15.10 C - 67.00 C> 

TEST is started: 08.03.2012 12:17:10

ELMB <7> init OK
ELMB NB: 7 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.006199    1. 0.002542  || 0.008741 
******** 2. 0.003115    3. 0.003193  || 0.006308 
******** 4. 0.000086    5. 0.000316  || 0.000402 
******** 6. 0.000101    7. 0.000291  || 0.000392 
******** 8. 0.000097    9. 0.000222  || 0.000319 
******** 10. 0.000204    11. 0.000008  || 0.000211 
  RANGE: 0.65  -  0.85
******** 12. 0.000085    13. 0.000282  || 0.000367 
******** 14. 0.000227    15. 0.000018  || 0.000245 
******** 16. 0.008793    17. 0.003123  || 0.011917 
******** 18. 0.003358    19. 0.002939  || 0.006296 
******** 20. 0.000157    21. 0.012240  || 0.012397 
******** 22. 0.002975    23. 0.000023  || 0.002998 
  RANGE: 0.55  -  0.75
******** 24. 0.000048    25. 0.000177  || 0.000225 
******** 26. 0.000209    27. 0.000149  || 0.000358 
******** 28. 0.000118    29. 0.000111  || 0.000229 
******** 30. 0.000004    31. 0.000077  || 0.000081 
******** 32. 0.005963    33. 0.003079  || 0.009042 
******** 34. 0.003001    35. 0.003043  || 0.006044 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   0.005 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.005 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.005 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.004 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.005 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.005 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.004 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.001 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.001 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.001 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.002 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 36.61
Channel 61, temperature 37.61
Channel 62, temperature 35.73
Channel 63, temperature 37.12

Set all regulators ON

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.740032    1. 0.009969  || 0.750001 
******** 2. 0.771455    3. 0.005394  || 0.776848 
******** 4. 0.784864    5. 0.007291  || 0.792155 
******** 6. 0.751986    7. 0.001809  || 0.753796 
******** 8. 0.784790    9. 0.003685  || 0.788474 
******** 10. 0.742575    11. 0.002955  || 0.745530 
  RANGE: 0.65  -  0.85
******** 12. 0.748770    13. 0.006663  || 0.755433 
******** 14. 0.764470    15. 0.000804  || 0.765274 
******** 16. 0.710962    17. 0.001650  || 0.712613 
******** 18. 0.731526    19. 0.007985  || 0.739511 
******** 20. 0.724133    21. 0.039660  || 0.763793 
******** 22. 0.740946    23. 0.005627  || 0.746573 
  RANGE: 0.55  -  0.75
******** 24. 0.666313    25. 0.028042  || 0.694355 
******** 26. 0.628326    27. 0.005425  || 0.633751 
******** 28. 0.637687    29. 0.013852  || 0.651538 
******** 30. 0.662580    31. 0.019300  || 0.681881 
******** 32. 0.652169    33. 0.021741  || 0.673910 
******** 34. 0.627855    35. 0.009038  || 0.636894 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.730 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.728 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.716 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.759 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.728 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.721 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.718 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.722 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.549 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.612 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.553 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.595 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.599 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.593 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.566 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.557 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   3.235 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.219 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.225 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.240 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.245 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.232 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.222 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.212 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 36.69
Channel 61, temperature 37.56
Channel 62, temperature 35.75
Channel 63, temperature 37.08

Set all regulators ON

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.016465    1. 0.754628  || 0.771094 
******** 2. 0.014763    3. 0.751320  || 0.766083 
******** 4. 0.033760    5. 0.753628  || 0.787388 
******** 6. 0.013180    7. 0.758687  || 0.771868 
******** 8. 0.031055    9. 0.755565  || 0.786619 
******** 10. 0.007195    11. 0.756898  || 0.764093 
  RANGE: 0.65  -  0.85
******** 12. 0.019369    13. 0.735627  || 0.754996 
******** 14. 0.026703    15. 0.722585  || 0.749288 
******** 16. 0.008992    17. 0.718652  || 0.727644 
******** 18. 0.003296    19. 0.721746  || 0.725042 
******** 20. 0.005377    21. 0.676938  || 0.682314 
******** 22. 0.019152    23. 0.725284  || 0.744436 
  RANGE: 0.55  -  0.75
******** 24. 0.019661    25. 0.614943  || 0.634605 
******** 26. 0.006165    27. 0.632425  || 0.638590 
******** 28. 0.007435    29. 0.631099  || 0.638534 
******** 30. 0.018656    31. 0.627124  || 0.645780 
******** 32. 0.002935    33. 0.628021  || 0.630956 
******** 34. 0.016058    35. 0.646466  || 0.662524 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.746 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.728 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.733 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.723 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.726 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.734 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.738 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.751 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.542 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.560 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.549 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.571 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.522 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.535 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.626 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.626 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   3.245 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.222 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.246 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.228 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.228 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.229 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.202 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.205 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 36.73
Channel 61, temperature 37.66
Channel 62, temperature 35.92
Channel 63, temperature 37.15

Set all regulators ON

 ======================  TEST LOOP for 2 loops =============================

 Loop: 1
DAC Settings:

Analog Positive 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:  128 [80]    129 [81]              -1
Supply Channel 2:  128 [80]    129 [81]              -1
Supply Channel 3:  128 [80]    129 [81]              -1
Supply Channel 4:  128 [80]    127 [7F]              1
Supply Channel 5:  128 [80]    129 [81]              -1
Supply Channel 6:  128 [80]    129 [81]              -1

Analog Negative 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:   128 [80]     129 [81]             -1
Supply Channel 2:   128 [80]     129 [81]             -1
Supply Channel 3:   128 [80]     127 [7F]             1
Supply Channel 4:   128 [80]     129 [81]             -1
Supply Channel 5:   128 [80]     129 [81]             -1
Supply Channel 6:   128 [80]     127 [7F]             1

Digital 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:   128 [80]     129 [81]             -1
Supply Channel 2:   128 [80]     129 [81]             -1
Supply Channel 3:   128 [80]     129 [81]             -1
Supply Channel 4:   128 [80]     127 [7F]             1
Supply Channel 5:   128 [80]     127 [7F]             1
Supply Channel 6:   128 [80]     127 [7F]             1

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.016515    1. 0.754857  || 0.771372 
******** 2. 0.319951    3. 0.446437  || 0.766388 
******** 4. 0.033800    5. 0.750380  || 0.784180 
******** 6. 0.755086    7. 0.001291  || 0.756377 
******** 8. 0.354642    9. 0.431928  || 0.786569 
******** 10. 0.007304    11. 0.753903  || 0.761207 
  RANGE: 0.65  -  0.85
******** 12. 0.330829    13. 0.431292  || 0.762121 
******** 14. 0.764561    15. 0.000811  || 0.765372 
******** 16. 0.353370    17. 0.358848  || 0.712218 
******** 18. 0.545794    19. 0.181134  || 0.726928 
******** 20. 0.723554    21. 0.045558  || 0.769112 
******** 22. 0.019194    23. 0.722237  || 0.741431 
  RANGE: 0.55  -  0.75
******** 24. 0.132453    25. 0.502073  || 0.634526 
******** 26. 0.256683    27. 0.364145  || 0.620828 
******** 28. 0.007179    29. 0.627858  || 0.635036 
******** 30. 0.345192    31. 0.301331  || 0.646524 
******** 32. 0.395472    33. 0.232330  || 0.627803 
******** 34. 0.444759    35. 0.186268  || 0.631027 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.744 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.736 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.731 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.759 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.735 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.737 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.736 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.748 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.559 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.614 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.561 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.600 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.603 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.596 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.624 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.624 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   3.245 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.227 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.242 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.246 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.249 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.242 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.226 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.219 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 37.50
Channel 61, temperature 38.89
Channel 62, temperature 37.30
Channel 63, temperature 37.89

 Loop: 2
DAC Settings:

Analog Positive 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:  128 [80]    130 [82]              -2
Supply Channel 2:  128 [80]    130 [82]              -2
Supply Channel 3:  128 [80]    130 [82]              -2
Supply Channel 4:  128 [80]    126 [7E]              2
Supply Channel 5:  128 [80]    130 [82]              -2
Supply Channel 6:  128 [80]    130 [82]              -2

Analog Negative 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:   128 [80]     128 [80]             0
Supply Channel 2:   128 [80]     130 [82]             -2
Supply Channel 3:   128 [80]     128 [80]             0
Supply Channel 4:   128 [80]     130 [82]             -2
Supply Channel 5:   128 [80]     130 [82]             -2
Supply Channel 6:   128 [80]     126 [7E]             2

Digital 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:   128 [80]     130 [82]             -2
Supply Channel 2:   128 [80]     130 [82]             -2
Supply Channel 3:   128 [80]     130 [82]             -2
Supply Channel 4:   128 [80]     126 [7E]             2
Supply Channel 5:   128 [80]     126 [7E]             2
Supply Channel 6:   128 [80]     126 [7E]             2

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.026161    1. 0.718372  || 0.744532 
******** 2. 0.368827    3. 0.400596  || 0.769423 
******** 4. 0.039940    5. 0.738033  || 0.777973 
******** 6. 0.755079    7. 0.001313  || 0.756392 
******** 8. 0.403442    9. 0.386179  || 0.789621 
******** 10. 0.007399    11. 0.756906  || 0.764305 
  RANGE: 0.65  -  0.85
******** 12. 0.385724    13. 0.373275  || 0.758998 
******** 14. 0.761522    15. 0.003916  || 0.765438 
******** 16. 0.283107    17. 0.423014  || 0.706120 
******** 18. 0.491115    19. 0.235996  || 0.727111 
******** 20. 0.726444    21. 0.045477  || 0.771921 
******** 22. 0.022352    23. 0.725198  || 0.747550 
  RANGE: 0.55  -  0.75
******** 24. 0.223929    25. 0.407561  || 0.631490 
******** 26. 0.348034    27. 0.275539  || 0.623573 
******** 28. 0.005238    29. 0.621553  || 0.626791 
******** 30. 0.268895    31. 0.377670  || 0.646564 
******** 32. 0.322113    33. 0.308823  || 0.630936 
******** 34. 0.368460    35. 0.268811  || 0.637271 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.742 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.735 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.729 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.760 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.735 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.736 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.733 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.746 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.558 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.616 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.564 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.603 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.604 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.597 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.622 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.622 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   3.244 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.225 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.238 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.247 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.251 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.245 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.228 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.223 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 38.38
Channel 61, temperature 39.94
Channel 62, temperature 38.23
Channel 63, temperature 38.80

 CARD 67: TEST is finished:  08.03.2012 12:22:10

 NO ERRORS  in   2 loops
