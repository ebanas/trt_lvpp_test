============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 67
CALIBRATION FILE: C:\LVPP_TESTS\data\card_067\LVPP_CALIB_067.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
      temperature range: <15.10 C - 67.00 C> 

TEST is started: 08.03.2012 12:13:41

Init_ELMB 1 exit with error: -7
Error -7 <Timeout occurred> in canReadSyncSpecific in Set_ELMB_State
 from INIT_ELMB
Test end -> INIT with errors ELMB nb 1
