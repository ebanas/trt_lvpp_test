=====================================================
Check basic functions of a card TYPE Wheel B/Barrel
=====================================================
CALIBRATION FILE: C:\LVPP_TESTS\data\card_312\LVPP_CALIB_312.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
TEST is started: 01.10.2009 15:20:24

ELMB <1> init OK
ELMB NB: 1 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 

Single Write/Read DACs 01 23 in all DTMROCS
Write/Read DACs BROADCAST 01 23 in all DTMROCS

Check set INHIBITS

Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.
Current channels
******** 0. 0.002984  
******** 2. 0.003009  
******** 4. 0.000000  
******** 6. 0.000000  
******** 8. 0.003002  
******** 9. 0.000010  
******** 10. 0.002976  
******** 11. 0.003003  
******** 12. 0.003039  
******** 14. 0.000022  
******** 16. 0.009109  
******** 18. 0.000008  
******** 20. 0.003000  
******** 21. 0.012200  
******** 22. 0.000001  
******** 23. 0.000002  
******** 24. 0.003004  
******** 26. 0.003002  
******** 28. 0.000003  
******** 30. 0.003016  
******** 32. 0.002996  
******** 33. 0.002998  
******** 34. 0.003003  
******** 35. 0.003004  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   0.005 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.004 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.004 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.004 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.004 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.004 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.004 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.001 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.001 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.002 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]
Channel 60, temperature 40.80
Channel 61, temperature 42.41
Channel 62, temperature 38.21
Channel 63, temperature 42.80
Temperature channels

Set all regulators ON
Current channels
******** 0. 0.735689  
******** 2. 0.730483  
******** 4. 0.747337  
******** 6. 0.741500  
******** 8. 0.739961  
******** 9. 0.738043  
******** 10. 0.732225  
******** 11. 0.731829  
******** 12. 0.725789  
******** 14. 0.746157  
******** 16. 0.746095  
******** 18. 0.728534  
******** 20. 0.738500  
******** 21. 0.751397  
******** 22. 0.749609  
******** 23. 0.730019  
******** 24. 0.701226  
******** 26. 0.693659  
******** 28. 0.708652  
******** 30. 0.715905  
******** 32. 0.711583  
******** 33. 0.711725  
******** 34. 0.720983  
******** 35. 0.719561  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   3.640 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.642 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.633 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.654 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.652 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.643 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.623 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.612 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.613 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.665 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.617 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.614 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.628 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.697 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.686 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.637 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.617 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.599 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.632 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.618 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.590 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.583 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.623 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.576 V   [V DIGI dtmroc 14]
Channel 60, temperature 41.41
Channel 61, temperature 44.99
Channel 62, temperature 40.33
Channel 63, temperature 46.74
Temperature channels
Current channels
******** 0. 0.735688  
******** 2. 0.733585  
******** 4. 0.747337  
******** 6. 0.744600  
******** 8. 0.742962  
******** 9. 0.741146  
******** 10. 0.735323  
******** 11. 0.731829  
******** 12. 0.728817  
******** 14. 0.749282  
******** 16. 0.749104  
******** 18. 0.731627  
******** 20. 0.741500  
******** 21. 0.751405  
******** 22. 0.752723  
******** 23. 0.733035  
******** 24. 0.704226  
******** 26. 0.696660  
******** 28. 0.711652  
******** 30. 0.715907  
******** 32. 0.714582  
******** 33. 0.711725  
******** 34. 0.720984  
******** 35. 0.722663  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   3.640 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.642 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.633 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.654 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.654 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.644 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.623 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.612 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.617 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.670 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.621 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.618 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.634 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.705 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.692 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.643 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.618 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.600 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.632 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.618 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.591 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.583 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.623 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.576 V   [V DIGI dtmroc 14]
Channel 60, temperature 42.79
Channel 61, temperature 48.21
Channel 62, temperature 42.93
Channel 63, temperature 50.82
Temperature channels
Current channels
******** 0. 0.738688  
******** 2. 0.733585  
******** 4. 0.747337  
******** 6. 0.744600  
******** 8. 0.746062  
******** 9. 0.741147  
******** 10. 0.735323  
******** 11. 0.734929  
******** 12. 0.728840  
******** 14. 0.752297  
******** 16. 0.749110  
******** 18. 0.731621  
******** 20. 0.744600  
******** 21. 0.760510  
******** 22. 0.752733  
******** 23. 0.736148  
******** 24. 0.704227  
******** 26. 0.696660  
******** 28. 0.711652  
******** 30. 0.715907  
******** 32. 0.714582  
******** 33. 0.714825  
******** 34. 0.720984  
******** 35. 0.722663  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   3.640 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.643 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.633 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.654 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.654 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.645 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.623 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.612 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.619 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.673 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.624 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.621 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.639 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.710 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.697 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.647 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.618 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.600 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.632 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.618 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.591 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.583 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.623 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.576 V   [V DIGI dtmroc 14]
Channel 60, temperature 44.23
Channel 61, temperature 50.79
Channel 62, temperature 44.96
Channel 63, temperature 53.76
Temperature channels
Current channels
******** 0. 0.738688  
******** 2. 0.733585  
******** 4. 0.747337  
******** 6. 0.744600  
******** 8. 0.749162  
******** 9. 0.744149  
******** 10. 0.735323  
******** 11. 0.734929  
******** 12. 0.728862  
******** 14. 0.752311  
******** 16. 0.752214  
******** 18. 0.734616  
******** 20. 0.747700  
******** 21. 0.757514  
******** 22. 0.755841  
******** 23. 0.733057  
******** 24. 0.704227  
******** 26. 0.696660  
******** 28. 0.711652  
******** 30. 0.719010  
******** 32. 0.717683  
******** 33. 0.714825  
******** 34. 0.720984  
******** 35. 0.722663  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   3.640 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.643 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.633 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.654 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.655 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.645 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.623 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.612 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.622 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.676 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.626 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.623 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.642 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.714 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.700 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.651 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.618 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.600 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.632 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.619 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.590 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.583 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.623 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.576 V   [V DIGI dtmroc 14]
Channel 60, temperature 45.57
Channel 61, temperature 52.87
Channel 62, temperature 46.58
Channel 63, temperature 55.86
Temperature channels
Current channels
******** 0. 0.738687  
******** 2. 0.733585  
******** 4. 0.750337  
******** 6. 0.747700  
******** 8. 0.749162  
******** 9. 0.744149  
******** 10. 0.735323  
******** 11. 0.737929  
******** 12. 0.731979  
******** 14. 0.752320  
******** 16. 0.752218  
******** 18. 0.734613  
******** 20. 0.747700  
******** 21. 0.760517  
******** 22. 0.755847  
******** 23. 0.736162  
******** 24. 0.704227  
******** 26. 0.698177  
******** 28. 0.711697  
******** 30. 0.719010  
******** 32. 0.714430  
******** 33. 0.714839  
******** 34. 0.724097  
******** 35. 0.691280  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   3.640 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.643 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.633 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.653 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.655 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.645 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.623 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.612 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.624 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.678 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.628 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.625 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.644 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.717 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.703 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.652 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.618 V   [V DIGI dtmroc 9]
ELMB Channel 53:   4.979 V   [V DIGI dtmroc 10]
ERROR: voltage too LOW (4.979 V) in ELMB channel 53 [V DIGI dtmroc 10]
ELMB Channel 54:   3.602 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.619 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.654 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.574 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.683 V   [V DIGI dtmroc 14]
ELMB Channel 59:   4.117 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (4.117 V) in ELMB channel 59 [V DIGI dtmroc 14]
Channel 60, temperature 46.72
Channel 61, temperature 54.36
Channel 62, temperature 47.66
Channel 63, temperature 57.38
Temperature channels

 TEST is finished: 01.10.2009 15:26:23
