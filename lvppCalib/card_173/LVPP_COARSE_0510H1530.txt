=====================================================
Check basic functions of a card TYPE Wheel B/Barrel
=====================================================
CALIBRATION FILE: C:\LVPP_TESTS\data\card_173\LVPP_CALIB_173.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
TEST is started: 05.10.2009 15:30:08

ELMB <1> init OK
ELMB NB: 1 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 

Single Write/Read DACs 01 23 in all DTMROCS
Write/Read DACs BROADCAST 01 23 in all DTMROCS

Check set INHIBITS

Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.
Current channels
******** 0. 0.003457  
******** 2. 0.002892  
******** 4. 0.000065  
******** 6. 0.003131  
******** 8. 0.000414  
******** 9. 0.002791  
******** 10. 0.003282  
******** 11. 0.000158  
******** 12. 0.003185  
******** 14. 0.000275  
******** 16. 0.008759  
******** 18. 0.002850  
******** 20. 0.000052  
******** 21. 0.009078  
******** 22. 0.000103  
******** 23. 0.000040  
******** 24. 0.000152  
******** 26. 0.000190  
******** 28. 0.003101  
******** 30. 0.000111  
******** 32. 0.003028  
******** 33. 0.002738  
******** 34. 0.002887  
******** 35. 0.003018  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   0.005 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.004 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.004 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.004 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.005 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.004 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.001 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.001 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]
Channel 60, temperature 28.96
Channel 61, temperature 28.34
Channel 62, temperature 30.56
Channel 63, temperature 28.63
Temperature channels

Set all regulators ON
Current channels
******** 0. 0.735215  
******** 2. 0.733262  
******** 4. 0.738503  
******** 6. 0.744463  
******** 8. 0.744210  
******** 9. 0.736892  
******** 10. 0.740969  
******** 11. 0.714884  
******** 12. 0.730881  
******** 14. 0.736621  
******** 16. 0.751106  
******** 18. 0.734679  
******** 20. 0.728118  
******** 21. 0.726074  
******** 22. 0.731450  
******** 23. 0.728855  
******** 24. 0.703764  
******** 26. 0.710235  
******** 28. 0.722054  
******** 30. 0.728900  
******** 32. 0.722888  
******** 33. 0.728489  
******** 34. 0.726426  
******** 35. 0.729766  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   3.636 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.670 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.660 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.668 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.666 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.658 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.647 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.646 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.598 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.626 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.651 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.661 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.585 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.652 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.639 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.599 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.697 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.677 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.684 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.700 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.634 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.647 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.659 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.663 V   [V DIGI dtmroc 14]
Channel 60, temperature 30.22
Channel 61, temperature 30.26
Channel 62, temperature 32.07
Channel 63, temperature 30.76
Temperature channels
Current channels
******** 0. 0.735215  
******** 2. 0.733253  
******** 4. 0.741597  
******** 6. 0.747443  
******** 8. 0.741114  
******** 9. 0.739876  
******** 10. 0.744069  
******** 11. 0.714884  
******** 12. 0.734043  
******** 14. 0.736424  
******** 16. 0.753961  
******** 18. 0.731463  
******** 20. 0.730532  
******** 21. 0.725759  
******** 22. 0.735115  
******** 23. 0.731655  
******** 24. 0.706803  
******** 26. 0.710311  
******** 28. 0.725071  
******** 30. 0.728856  
******** 32. 0.719799  
******** 33. 0.728425  
******** 34. 0.726370  
******** 35. 0.729778  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   3.636 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.670 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.660 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.669 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.667 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.659 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.647 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.646 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.600 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.629 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.655 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.664 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.588 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.657 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.643 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.602 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.698 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.678 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.685 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.701 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.635 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.647 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.660 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.664 V   [V DIGI dtmroc 14]
Channel 60, temperature 31.65
Channel 61, temperature 32.30
Channel 62, temperature 33.54
Channel 63, temperature 32.98
Temperature channels
Current channels
******** 0. 0.738349  
******** 2. 0.733253  
******** 4. 0.741587  
******** 6. 0.747423  
******** 8. 0.741019  
******** 9. 0.739860  
******** 10. 0.740969  
******** 11. 0.714873  
******** 12. 0.737105  
******** 14. 0.736227  
******** 16. 0.753766  
******** 18. 0.731347  
******** 20. 0.733099  
******** 21. 0.725490  
******** 22. 0.738630  
******** 23. 0.731476  
******** 24. 0.709772  
******** 26. 0.710348  
******** 28. 0.725088  
******** 30. 0.731856  
******** 32. 0.722904  
******** 33. 0.731458  
******** 34. 0.726342  
******** 35. 0.729784  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   3.637 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.670 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.661 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.669 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.668 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.659 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.647 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.646 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.602 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.633 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.657 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.667 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.592 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.661 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.646 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.605 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.698 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.678 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.685 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.701 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.635 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.648 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.660 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.664 V   [V DIGI dtmroc 14]
Channel 60, temperature 33.10
Channel 61, temperature 34.30
Channel 62, temperature 34.82
Channel 63, temperature 35.04
Temperature channels
Current channels
******** 0. 0.738349  
******** 2. 0.733253  
******** 4. 0.744582  
******** 6. 0.747403  
******** 8. 0.740955  
******** 9. 0.739828  
******** 10. 0.740969  
******** 11. 0.714884  
******** 12. 0.740279  
******** 14. 0.739050  
******** 16. 0.753547  
******** 18. 0.731232  
******** 20. 0.732619  
******** 21. 0.728342  
******** 22. 0.739092  
******** 23. 0.734416  
******** 24. 0.709742  
******** 26. 0.713525  
******** 28. 0.725105  
******** 30. 0.731833  
******** 32. 0.722910  
******** 33. 0.731394  
******** 34. 0.726286  
******** 35. 0.732796  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   3.637 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.670 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.661 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.670 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.669 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.660 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.647 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.646 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.604 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.636 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.661 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.671 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.595 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.665 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.649 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.608 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.699 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.679 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.685 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.701 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.635 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.648 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.661 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.665 V   [V DIGI dtmroc 14]
Channel 60, temperature 34.50
Channel 61, temperature 36.15
Channel 62, temperature 35.92
Channel 63, temperature 36.90
Temperature channels
Current channels
******** 0. 0.738414  
******** 2. 0.733245  
******** 4. 0.747677  
******** 6. 0.750493  
******** 8. 0.740891  
******** 9. 0.742912  
******** 10. 0.740990  
******** 11. 0.714873  
******** 12. 0.740316  
******** 14. 0.738893  
******** 16. 0.753377  
******** 18. 0.731140  
******** 20. 0.735193  
******** 21. 0.728118  
******** 22. 0.742603  
******** 23. 0.737256  
******** 24. 0.709681  
******** 26. 0.713563  
******** 28. 0.728221  
******** 30. 0.731811  
******** 32. 0.722916  
******** 33. 0.734327  
******** 34. 0.729386  
******** 35. 0.732802  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   3.637 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.670 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.662 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.670 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.670 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.660 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.647 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.646 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.605 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.638 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.663 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.673 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.597 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.668 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.652 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.611 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.699 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.679 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.686 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.702 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.636 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.648 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.661 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.665 V   [V DIGI dtmroc 14]
Channel 60, temperature 35.81
Channel 61, temperature 37.81
Channel 62, temperature 36.88
Channel 63, temperature 38.57
Temperature channels
Current channels
******** 0. 0.738414  
******** 2. 0.733245  
******** 4. 0.747672  
******** 6. 0.747383  
******** 8. 0.740827  
******** 9. 0.745896  
******** 10. 0.740990  
******** 11. 0.714873  
******** 12. 0.740366  
******** 14. 0.738755  
******** 16. 0.756206  
******** 18. 0.731047  
******** 20. 0.737974  
******** 21. 0.727938  
******** 22. 0.742964  
******** 23. 0.740236  
******** 24. 0.712781  
******** 26. 0.710501  
******** 28. 0.728238  
******** 30. 0.731789  
******** 32. 0.722921  
******** 33. 0.734263  
******** 34. 0.729329  
******** 35. 0.732802  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   3.637 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.670 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.662 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.671 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.670 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.661 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.647 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.646 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.606 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.641 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.665 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.676 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.599 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.671 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.654 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.613 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.699 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.679 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.686 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.702 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.636 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.649 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.661 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.665 V   [V DIGI dtmroc 14]
Channel 60, temperature 37.03
Channel 61, temperature 39.32
Channel 62, temperature 37.76
Channel 63, temperature 40.04
Temperature channels
Current channels
******** 0. 0.741414  
******** 2. 0.733245  
******** 4. 0.750667  
******** 6. 0.750462  
******** 8. 0.737664  
******** 9. 0.745896  
******** 10. 0.737990  
******** 11. 0.714873  
******** 12. 0.743415  
******** 14. 0.741718  
******** 16. 0.756084  
******** 18. 0.730978  
******** 20. 0.737600  
******** 21. 0.730759  
******** 22. 0.746325  
******** 23. 0.740116  
******** 24. 0.712781  
******** 26. 0.713601  
******** 28. 0.728255  
******** 30. 0.734867  
******** 32. 0.722927  
******** 33. 0.737363  
******** 34. 0.729329  
******** 35. 0.732808  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   3.637 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.670 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.662 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.671 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.671 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.661 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.647 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.646 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.607 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.643 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.667 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.678 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.602 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.673 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.657 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.615 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.699 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.679 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.686 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.702 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.636 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.649 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.661 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.665 V   [V DIGI dtmroc 14]
Channel 60, temperature 38.15
Channel 61, temperature 40.65
Channel 62, temperature 38.51
Channel 63, temperature 41.42
Temperature channels
Current channels
******** 0. 0.741446  
******** 2. 0.733245  
******** 4. 0.750667  
******** 6. 0.750453  
******** 8. 0.740731  
******** 9. 0.745879  
******** 10. 0.740990  
******** 11. 0.711861  
******** 12. 0.743453  
******** 14. 0.741600  
******** 16. 0.755913  
******** 18. 0.730909  
******** 20. 0.737335  
******** 21. 0.727601  
******** 22. 0.746633  
******** 23. 0.743017  
******** 24. 0.712750  
******** 26. 0.713639  
******** 28. 0.728255  
******** 30. 0.734867  
******** 32. 0.726033  
******** 33. 0.737296  
******** 34. 0.729273  
******** 35. 0.732814  
Voltage channels 3.540000  3.880000
ELMB Channel 36:   3.638 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.670 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.662 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.672 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.671 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.661 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.647 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.646 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.608 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.645 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.669 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.680 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.603 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.676 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.659 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.617 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.700 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.680 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.686 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.702 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.637 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.649 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.662 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.666 V   [V DIGI dtmroc 14]
Channel 60, temperature 39.16
Channel 61, temperature 41.89
Channel 62, temperature 39.22
Channel 63, temperature 42.62
Temperature channels
Current channels
******** 0. 0.003489  
ERROR: current not in range (0.003 A): ELMB channel 0 [I AN_POS dtmroc 1]
******** 2. 0.002884  
ERROR: current not in range (0.003 A): ELMB channel 2 [I AN_POS dtmroc 2]
******** 4. 0.000070  
ERROR: current not in range (0.000 A): ELMB channel 4 [I AN_POS dtmroc 3]
******** 6. 0.000131  
ERROR: current not in range (0.000 A): ELMB channel 6 [I AN_POS dtmroc 4]
******** 8. 0.002554  
ERROR: current not in range (0.003 A): ELMB channel 8 [I AN_POS dtmroc 5]
******** 9. 0.000225  
ERROR: current not in range (0.000 A): ELMB channel 9 [I AN_POS dtmroc 5]
******** 10. 0.003302  
ERROR: current not in range (0.003 A): ELMB channel 10 [I AN_POS dtmroc 6]
******** 11. 0.000158  
ERROR: current not in range (0.000 A): ELMB channel 11 [I AN_POS dtmroc 6]
******** 12. 0.003185  
ERROR: current not in range (0.003 A): ELMB channel 12 [I AN_NEG dtmroc 1]
******** 14. 0.002705  
ERROR: current not in range (0.003 A): ELMB channel 14 [I AN_NEG dtmroc 2]
******** 16. 0.008735  
ERROR: current not in range (0.009 A): ELMB channel 16 [I AN_NEG dtmroc 3]
******** 18. 0.002838  
ERROR: current not in range (0.003 A): ELMB channel 18 [I AN_NEG dtmroc 4]
******** 20. 0.000106  
ERROR: current not in range (0.000 A): ELMB channel 20 [I AN_NEG dtmroc 5]
******** 21. 0.009078  
ERROR: current not in range (0.009 A): ELMB channel 21 [I AN_NEG dtmroc 5]
******** 22. 0.000103  
ERROR: current not in range (0.000 A): ELMB channel 22 [I AN_NEG dtmroc 6]
******** 23. 0.000040  
ERROR: current not in range (0.000 A): ELMB channel 23 [I AN_NEG dtmroc 6]
******** 24. 0.000183  
ERROR: current not in range (0.000 A): ELMB channel 24 [I DIGI dtmroc 9]
******** 26. 0.003190  
ERROR: current not in range (0.003 A): ELMB channel 26 [I DIGI dtmroc 10]
******** 28. 0.003084  
ERROR: current not in range (0.003 A): ELMB channel 28 [I DIGI dtmroc 11]
******** 30. 0.002889  
ERROR: current not in range (0.003 A): ELMB channel 30 [I DIGI dtmroc 12]
******** 32. 0.003022  
ERROR: current not in range (0.003 A): ELMB channel 32 [I DIGI dtmroc 13]
******** 33. 0.002805  
ERROR: current not in range (0.003 A): ELMB channel 33 [I DIGI dtmroc 13]
******** 34. 0.002887  
ERROR: current not in range (0.003 A): ELMB channel 34 [I DIGI dtmroc 14]
******** 35. 0.003018  
ERROR: current not in range (0.003 A): ELMB channel 35 [I DIGI dtmroc 14]
Voltage channels 3.540000  3.880000
ELMB Channel 36:   0.005 V   [V AN_POS dtmroc 1]
ERROR: voltage too LOW (0.005 V) in ELMB channel 36 [V AN_POS dtmroc 1]
ELMB Channel 37:   0.005 V   [V AN_POS dtmroc 2]
ERROR: voltage too LOW (0.005 V) in ELMB channel 37 [V AN_POS dtmroc 2]
ELMB Channel 38:   0.005 V   [V AN_POS dtmroc 3]
ERROR: voltage too LOW (0.005 V) in ELMB channel 38 [V AN_POS dtmroc 3]
ELMB Channel 39:   0.004 V   [V AN_POS dtmroc 4]
ERROR: voltage too LOW (0.004 V) in ELMB channel 39 [V AN_POS dtmroc 4]
ELMB Channel 40:   0.005 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (0.005 V) in ELMB channel 40 [V AN_POS dtmroc 5]
ELMB Channel 41:   0.005 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (0.005 V) in ELMB channel 41 [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ERROR: voltage too LOW (0.005 V) in ELMB channel 42 [V AN_POS dtmroc 6]
ELMB Channel 43:   0.005 V   [V AN_POS dtmroc 6]
ERROR: voltage too LOW (0.005 V) in ELMB channel 43 [V AN_POS dtmroc 6]
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ERROR: voltage too LOW (0.005 V) in ELMB channel 44 [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ERROR: voltage too LOW (0.005 V) in ELMB channel 45 [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.005 V   [V AN_NEG dtmroc 3]
ERROR: voltage too LOW (0.005 V) in ELMB channel 46 [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.005 V   [V AN_NEG dtmroc 4]
ERROR: voltage too LOW (0.005 V) in ELMB channel 47 [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.001 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (0.001 V) in ELMB channel 48 [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (0.000 V) in ELMB channel 49 [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ERROR: voltage too LOW (0.001 V) in ELMB channel 50 [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
ERROR: voltage too LOW (0.001 V) in ELMB channel 51 [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ERROR: voltage too LOW (0.002 V) in ELMB channel 52 [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ERROR: voltage too LOW (0.002 V) in ELMB channel 53 [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ERROR: voltage too LOW (0.002 V) in ELMB channel 54 [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ERROR: voltage too LOW (0.002 V) in ELMB channel 55 [V DIGI dtmroc 12]
ELMB Channel 56:   0.001 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (0.001 V) in ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   0.001 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (0.001 V) in ELMB channel 57 [V DIGI dtmroc 13]
ELMB Channel 58:   0.001 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (0.001 V) in ELMB channel 58 [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (0.001 V) in ELMB channel 59 [V DIGI dtmroc 14]
Channel 60, temperature 39.83
Channel 61, temperature 41.94
Channel 62, temperature 39.29
Channel 63, temperature 42.47
Temperature channels

 TEST is finished: 05.10.2009 15:39:01
