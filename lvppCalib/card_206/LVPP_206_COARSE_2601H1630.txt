=====================================================
Check basic functions of a card TYPE Wheel B/Barrel
=====================================================
CALIBRATION FILE: C:\LVPP_TESTS\data\card_206\LVPP_CALIB_206.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
TEST is started: 26.01.2011 16:30:40

ELMB <1> init OK
ELMB NB: 1 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 

Single Write/Read DACs 01 23 in all DTMROCS
Write/Read DACs BROADCAST 01 23 in all DTMROCS

Check set INHIBITS

Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.
Current channels
******** 0. 0.002983  
******** 2. 0.002998  
******** 4. 0.000019  
******** 6. 0.000015  
******** 8. 0.002987  
******** 9. 0.000025  
******** 10. 0.003006  
******** 11. 0.000000  
******** 12. 0.000008  
******** 14. 0.000011  
******** 16. 0.009123  
******** 18. 0.000015  
******** 20. 0.000008  
******** 21. 0.009100  
******** 22. 0.000002  
******** 23. 0.000001  
******** 24. 0.000010  
******** 26. 0.000010  
******** 28. 0.000000  
******** 30. 0.000019  
******** 32. 0.002995  
******** 33. 0.002994  
******** 34. 0.003006  
******** 35. 0.002994  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   0.005 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.004 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.004 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.004 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.004 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.004 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.004 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.004 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.004 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.001 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.000 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.000 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.001 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.001 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.002 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]
Channel 60, temperature 36.93
Channel 61, temperature 37.56
Channel 62, temperature 37.47
Channel 63, temperature 36.87
Temperature channels

Set all regulators ON
Current channels
******** 0. 0.717318  
******** 2. 0.721387  
******** 4. 0.745004  
******** 6. 0.735784  
******** 8. 0.735781  
******** 9. 0.731783  
******** 10. 0.740175  
******** 11. 0.729300  
******** 12. 0.723560  
******** 14. 0.741632  
******** 16. 0.731727  
******** 18. 0.719306  
******** 20. 0.731736  
******** 21. 0.737822  
******** 22. 0.732737  
******** 23. 0.727506  
******** 24. 0.693700  
******** 26. 0.688902  
******** 28. 0.698442  
******** 30. 0.704326  
******** 32. 0.713365  
******** 33. 0.704620  
******** 34. 0.713384  
******** 35. 0.705410  
Voltage channels 3.000000  3.920000
ELMB Channel 36:   7.557 V   [V AN_POS dtmroc 1]
ERROR: voltage too LOW (7.557 V) in ELMB channel 36 [V AN_POS dtmroc 1]
ELMB Channel 37:   3.627 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.652 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.701 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.612 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.646 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.673 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.628 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.589 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.627 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.574 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.601 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.640 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.680 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.584 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.588 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.597 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.572 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.580 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.579 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.593 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.541 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.589 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.541 V   [V DIGI dtmroc 14]
Channel 60, temperature 37.41
Channel 61, temperature 38.41
Channel 62, temperature 38.19
Channel 63, temperature 37.82
Temperature channels

CARD 206:   TEST is finished: 26.01.2011 16:33:20
THERE WERE ERRORS !!!  in  the 1 loop.
