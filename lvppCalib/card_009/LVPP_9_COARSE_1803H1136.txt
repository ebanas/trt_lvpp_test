============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 9
CALIBRATION FILE: E:\LVPP_TESTS_SPI\data\card_009\LVPP_CALIB_009.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
      temperature range: <15.10 C - 67.00 C> 

TEST is started: 18.03.2014 11:36:01

ELMB <7> init OK
ELMB NB: 7 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.003133    1. 0.002905  || 0.006038 
******** 2. 0.003234    3. 0.003078  || 0.006311 
******** 4. 0.000311    5. 0.000511  || 0.000822 
******** 6. 0.000234    7. 0.000040  || 0.000274 
******** 8. 0.002989    9. 0.003285  || 0.006274 
******** 10. 0.002873    11. 0.003016  || 0.005890 
  RANGE: 0.65  -  0.85
******** 12. 0.000111    13. 0.000205  || 0.000316 
******** 14. 0.000118    15. 0.000304  || 0.000423 
******** 16. 0.009086    17. 0.003163  || 0.012250 
******** 18. 0.003276    19. 0.003111  || 0.006387 
******** 20. 0.002950    21. 0.009055  || 0.012005 
******** 22. 0.000015    23. 0.000011  || 0.000026 
  RANGE: 0.55  -  0.75
******** 24. 0.002999    25. 0.006120  || 0.009119 
******** 26. 0.003008    27. 0.002978  || 0.005986 
******** 28. 0.002968    29. 0.000177  || 0.003145 
******** 30. 0.003009    31. 0.002966  || 0.005975 
******** 32. 0.003095    33. 0.003018  || 0.006113 
******** 34. 0.002972    35. 0.002977  || 0.005949 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   0.005 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.004 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.004 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.004 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.005 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.005 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.004 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.001 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.002 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.002 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 43.62
Channel 61, temperature 43.82
Channel 62, temperature 44.06
Channel 63, temperature 42.24

Set all regulators ON

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.756294    1. 0.015522  || 0.771816 
******** 2. 0.774920    3. 0.017899  || 0.792819 
******** 4. 0.756831    5. 0.000542  || 0.757373 
******** 6. 0.758334    7. 0.013162  || 0.771496 
******** 8. 0.744054    9. 0.120225  || 0.864279 
ERROR: curr too high REG OFF (0.120 A): ELMB channel 9 [I AN_POS dtmroc 5]
******** 10. 0.726363    11. 0.026600  || 0.752963 
  RANGE: 0.65  -  0.85
******** 12. 0.721660    13. 0.012031  || 0.733691 
******** 14. 0.750634    15. 0.018774  || 0.769408 
******** 16. 0.728151    17. 0.011285  || 0.739436 
******** 18. 0.726773    19. 0.025384  || 0.752157 
******** 20. 0.740575    21. 0.017834  || 0.758410 
******** 22. 0.743946    23. 0.005195  || 0.749141 
  RANGE: 0.55  -  0.75
******** 24. 0.654795    25. 0.001852  || 0.656647 
******** 26. 0.618668    27. 0.022080  || 0.640748 
******** 28. 0.628528    29. 0.038098  || 0.666626 
******** 30. 0.640570    31. 0.002149  || 0.642719 
******** 32. 0.630538    33. 0.000073  || 0.630611 
******** 34. 0.639464    35. 0.001504  || 0.640968 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.743 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.747 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.719 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.756 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.748 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.751 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.727 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.714 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.557 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.632 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.569 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.572 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.635 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.616 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.641 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.630 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   3.263 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.248 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.248 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.227 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.195 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.183 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.198 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.180 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 43.65
Channel 61, temperature 43.77
Channel 62, temperature 44.01
Channel 63, temperature 42.25

Set all regulators ON

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.015363    1. 0.793477  || 0.808839 
******** 2. 0.006641    3. 0.790138  || 0.796779 
******** 4. 0.009964    5. 0.775123  || 0.785088 
******** 6. 0.015772    7. 0.781799  || 0.797572 
******** 8. 0.028031    9. 0.892774  || 0.920805 
ERROR: curr not in range ON (2) (0.893 A): ELMB channel 9 [I AN_POS dtmroc 5]
******** 10. 0.034045    11. 0.742400  || 0.776445 
  RANGE: 0.65  -  0.85
******** 12. 0.022924    13. 0.753502  || 0.776426 
******** 14. 0.002704    15. 0.762425  || 0.765130 
******** 16. 0.001859    17. 0.734391  || 0.736250 
******** 18. 0.007855    19. 0.771503  || 0.779359 
******** 20. 0.001830    21. 0.731936  || 0.733765 
******** 22. 0.001466    23. 0.732324  || 0.733790 
  RANGE: 0.55  -  0.75
******** 24. 0.001698    25. 0.657176  || 0.658874 
******** 26. 0.031383    27. 0.625058  || 0.656441 
******** 28. 0.018574    29. 0.617543  || 0.636117 
******** 30. 0.006527    31. 0.645574  || 0.652101 
******** 32. 0.016569    33. 0.643822  || 0.660391 
******** 34. 0.004573    35. 0.645286  || 0.649859 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.755 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.761 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.750 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.720 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.743 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.760 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.746 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.743 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.556 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.619 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.566 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.627 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.646 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.645 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.575 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.576 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   3.255 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.233 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.255 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.192 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.191 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.193 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.201 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.195 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 43.65
Channel 61, temperature 43.79
Channel 62, temperature 44.04
Channel 63, temperature 42.32

Set all regulators ON

 ======================  TEST LOOP for 2 loops =============================

 Loop: 1
DAC Settings:

Analog Positive 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:  128 [80]    128 [80]              0
Supply Channel 2:  128 [80]    128 [80]              0
Supply Channel 3:  128 [80]    128 [80]              0
Supply Channel 4:  128 [80]    128 [80]              0
Supply Channel 5:  128 [80]    128 [80]              0
Supply Channel 6:  128 [80]    128 [80]              0

Analog Negative 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:   128 [80]     128 [80]             0
Supply Channel 2:   128 [80]     128 [80]             0
Supply Channel 3:   128 [80]     128 [80]             0
Supply Channel 4:   128 [80]     128 [80]             0
Supply Channel 5:   128 [80]     128 [80]             0
Supply Channel 6:   128 [80]     128 [80]             0

Digital 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:   128 [80]     128 [80]             0
Supply Channel 2:   128 [80]     128 [80]             0
Supply Channel 3:   128 [80]     128 [80]             0
Supply Channel 4:   128 [80]     128 [80]             0
Supply Channel 5:   128 [80]     128 [80]             0
Supply Channel 6:   128 [80]     128 [80]             0

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.012056    1. 0.766063  || 0.778120 
******** 2. 0.028059    3. 0.771844  || 0.799903 
******** 4. 0.009940    5. 0.778083  || 0.788023 
******** 6. 0.758354    7. 0.013165  || 0.771519 
******** 8. 0.332035    9. 0.539213  || 0.871248 
ERROR: not in range ON  (0.871 A): ELMB channels 8 9 [I AN_POS dtmroc 5]
******** 10. 0.034045    11. 0.742398  || 0.776444 
  RANGE: 0.65  -  0.85
******** 12. 0.263722    13. 0.470171  || 0.733894 
******** 14. 0.488369    15. 0.281422  || 0.769791 
******** 16. 0.285623    17. 0.457123  || 0.742746 
******** 18. 0.007763    19. 0.771540  || 0.779304 
******** 20. 0.049667    21. 0.679773  || 0.729441 
******** 22. 0.746961    23. 0.005173  || 0.752134 
  RANGE: 0.55  -  0.75
******** 24. 0.392293    25. 0.266597  || 0.658890 
******** 26. 0.481277    27. 0.115194  || 0.596471 
******** 28. 0.075961    29. 0.519489  || 0.595450 
******** 30. 0.640574    31. 0.002163  || 0.642736 
******** 32. 0.267672    33. 0.366182  || 0.633853 
******** 34. 0.151039    35. 0.489535  || 0.640574 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.756 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.761 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.750 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.757 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.756 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.766 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.746 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.742 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.567 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.638 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.578 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.628 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.650 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.648 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.642 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.631 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   3.268 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.251 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.259 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.227 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.202 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.198 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.207 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.199 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 44.00
Channel 61, temperature 44.39
Channel 62, temperature 44.65
Channel 63, temperature 42.87

 CARD 9: TEST is finished:  18.03.2014 11:37:36

 ERRORS  in loop 1, test stopped. 
 Total number of loops foreseen in test: 2
