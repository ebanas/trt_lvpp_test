============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 2
CALIBRATION FILE: C:\LVPP_TESTS\data\card_002\LVPP_CALIB_002.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
                temperature range: <15.10 C - 60.00 C> 

TEST is started: 07.10.2009 09:07:18

ELMB <2> init OK
ELMB NB: 2 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.

Current channels
******** 0. 0.000000    1. 0.003000  || 0.003000 
******** 2. 0.000002    3. 0.000000  || 0.000002 
******** 4. 0.003000    5. 0.006114  || 0.009114 
******** 6. 0.002990    7. 0.003007  || 0.005997 
******** 8. 0.003006    9. 0.002997  || 0.006003 
******** 10. 0.003011    11. 0.002967  || 0.005978 
******** 12. 0.003041    13. 0.003003  || 0.006044 
******** 14. 0.002981    15. 0.002981  || 0.005962 
******** 16. 0.003000    17. 0.000000  || 0.003000 
******** 18. 0.003035    19. 0.000014  || 0.003049 
******** 20. 0.003000    21. 0.006100  || 0.009100 
******** 22. 0.003001    23. 0.002999  || 0.005999 
******** 24. 0.003007    25. 0.002986  || 0.005993 
******** 26. 0.003000    27. 0.002995  || 0.005995 
******** 28. 20.160507    29. 0.003001  || 20.163508 
ERROR: curr too high REG OFF (20.164 A): ELMB channels 28 29 [I DIGI dtmroc 11]
******** 30. 20.908206    31. 0.061006  || 20.969211 
ERROR: curr too high REG OFF (20.969 A): ELMB channels 30 31 [I DIGI dtmroc 12]
******** 32. 0.000000    33. 0.000001  || 0.000001 
******** 34. 0.000007    35. 0.000000  || 0.000008 

Voltage channels:
ELMB Channel 36:   0.004 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.004 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.004 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.004 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.004 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.004 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.004 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.004 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.004 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.004 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.000 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.000 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.001 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.001 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.001 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.001 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.001 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 27.77
Channel 61, temperature 27.63
Channel 62, temperature 30.01
Channel 63, temperature 26.85

Set all regulators ON

Current channels
******** 0. 0.790400    1. 0.006100  || 0.796500 
******** 2. 0.774432    3. 0.012200  || 0.786632 
******** 4. 0.778200    5. 0.004144  || 0.782344 
******** 6. 0.813645    7. 0.023089  || 0.836734 
******** 8. 0.800136    9. 0.021663  || 0.821799 
******** 10. 0.752356    11. 0.049919  || 0.802275 
******** 12. 0.754136    13. 0.036586  || 0.790722 
******** 14. 0.842073    15. 0.005973  || 0.848046 
ERROR: curr not in range ON  (0.842 A): ELMB channel 14 [I AN_NEG dtmroc 2]
******** 16. 0.802600    17. 0.024400  || 0.827000 
******** 18. 0.762841    19. 0.024309  || 0.787151 
******** 20. 0.799500    21. 0.194912  || 0.994412 
ERROR: curr too high REG OFF (0.195 A): ELMB channel 21 [I AN_NEG dtmroc 5]
******** 22. 0.753815    23. 0.045325  || 0.799140 
******** 24. 0.734225    25. 0.005551  || 0.739775 
******** 26. 0.766000    27. 0.022680  || 0.788680 
******** 28. 0.777779    29. 0.005816  || 0.783595 
******** 30. 1.017444    31. 0.019444  || 1.036887 
ERROR: curr not in range ON  (1.017 A): ELMB channel 30 [I DIGI dtmroc 12]
******** 32. 0.805600    33. 0.029970  || 0.835570 
******** 34. 0.833234    35. 0.008881  || 0.842115 
ERROR: curr not in range ON  (0.833 A): ELMB channel 34 [I DIGI dtmroc 14]

Voltage channels:
ELMB Channel 36:   3.920 V   [V AN_POS dtmroc 1]
ERROR: voltage too LOW (3.920 V) in ELMB channel 36 [V AN_POS dtmroc 1]
ELMB Channel 37:   3.887 V   [V AN_POS dtmroc 2]
ERROR: voltage too LOW (3.887 V) in ELMB channel 37 [V AN_POS dtmroc 2]
ELMB Channel 38:   3.895 V   [V AN_POS dtmroc 3]
ERROR: voltage too LOW (3.895 V) in ELMB channel 38 [V AN_POS dtmroc 3]
ELMB Channel 39:   3.945 V   [V AN_POS dtmroc 4]
ERROR: voltage too LOW (3.945 V) in ELMB channel 39 [V AN_POS dtmroc 4]
ELMB Channel 40:   3.903 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (3.903 V) in ELMB channel 40 [V AN_POS dtmroc 5]
ELMB Channel 41:   3.910 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (3.910 V) in ELMB channel 41 [V AN_POS dtmroc 5]
ELMB Channel 42:   3.905 V   [V AN_POS dtmroc 6]
ERROR: voltage too LOW (3.905 V) in ELMB channel 42 [V AN_POS dtmroc 6]
ELMB Channel 43:   3.904 V   [V AN_POS dtmroc 6]
ERROR: voltage too LOW (3.904 V) in ELMB channel 43 [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.858 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.867 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.865 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.900 V   [V AN_NEG dtmroc 4]
ERROR: voltage too LOW (-3.900 V) in ELMB channel 47 [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.871 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.863 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.803 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.801 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   4.018 V   [V DIGI dtmroc 9]
ERROR: voltage too LOW (4.018 V) in ELMB channel 52 [V DIGI dtmroc 9]
ELMB Channel 53:   4.007 V   [V DIGI dtmroc 10]
ERROR: voltage too LOW (4.007 V) in ELMB channel 53 [V DIGI dtmroc 10]
ELMB Channel 54:   4.022 V   [V DIGI dtmroc 11]
ERROR: voltage too LOW (4.022 V) in ELMB channel 54 [V DIGI dtmroc 11]
ELMB Channel 55:   4.011 V   [V DIGI dtmroc 12]
ERROR: voltage too LOW (4.011 V) in ELMB channel 55 [V DIGI dtmroc 12]
ELMB Channel 56:   4.003 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (4.003 V) in ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   3.978 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (3.978 V) in ELMB channel 57 [V DIGI dtmroc 13]
ELMB Channel 58:   3.988 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (3.988 V) in ELMB channel 58 [V DIGI dtmroc 14]
ELMB Channel 59:   3.973 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (3.973 V) in ELMB channel 59 [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 28.12
Channel 61, temperature 27.79
Channel 62, temperature 30.20
Channel 63, temperature 27.09

Set all regulators ON

Current channels
******** 0. 0.006100    1. 0.787300  || 0.793400 
******** 2. 0.012849    3. 0.805600  || 0.818449 
******** 4. 0.009100    5. 0.786271  || 0.795371 
******** 6. 0.020119    7. 0.770329  || 0.790448 
******** 8. 0.009760    9. 0.765635  || 0.775395 
******** 10. 0.031899    11. 0.840566  || 0.872466 
ERROR: curr not in range ON  (0.841 A): ELMB channel 11 [I AN_POS dtmroc 6]
******** 12. 0.035620    13. 0.741568  || 0.777189 
******** 14. 0.060583    15. 0.771683  || 0.832266 
******** 16. 0.030500    17. 0.750700  || 0.781200 
******** 18. 0.024127    19. 0.805472  || 0.829599 
******** 20. 0.015200    21. 0.982381  || 0.997581 
ERROR: curr not in range ON  (0.982 A): ELMB channel 21 [I AN_NEG dtmroc 5]
******** 22. 0.015281    23. 0.820699  || 0.835980 
******** 24. 0.062182    25. 0.805064  || 0.867247 
******** 26. 0.027400    27. 0.770724  || 0.798124 
******** 28. 0.021723    29. 0.793684  || 0.815407 
******** 30. 2.378549    31. 0.783051  || 3.161600 
ERROR: curr too high REG OFF (2.379 A): ELMB channel 30 [I DIGI dtmroc 12]
******** 32. 0.003000    33. 0.769536  || 0.772536 
******** 34. 0.012255    35. 0.778428  || 0.790683 

Voltage channels:
ELMB Channel 36:   3.906 V   [V AN_POS dtmroc 1]
ERROR: voltage too LOW (3.906 V) in ELMB channel 36 [V AN_POS dtmroc 1]
ELMB Channel 37:   3.918 V   [V AN_POS dtmroc 2]
ERROR: voltage too LOW (3.918 V) in ELMB channel 37 [V AN_POS dtmroc 2]
ELMB Channel 38:   3.891 V   [V AN_POS dtmroc 3]
ERROR: voltage too LOW (3.891 V) in ELMB channel 38 [V AN_POS dtmroc 3]
ELMB Channel 39:   3.935 V   [V AN_POS dtmroc 4]
ERROR: voltage too LOW (3.935 V) in ELMB channel 39 [V AN_POS dtmroc 4]
ELMB Channel 40:   3.885 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (3.885 V) in ELMB channel 40 [V AN_POS dtmroc 5]
ELMB Channel 41:   3.907 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (3.907 V) in ELMB channel 41 [V AN_POS dtmroc 5]
ELMB Channel 42:   3.926 V   [V AN_POS dtmroc 6]
ERROR: voltage too LOW (3.926 V) in ELMB channel 42 [V AN_POS dtmroc 6]
ELMB Channel 43:   3.934 V   [V AN_POS dtmroc 6]
ERROR: voltage too LOW (3.934 V) in ELMB channel 43 [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.789 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.805 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.865 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.865 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.860 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.870 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.863 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.872 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   4.020 V   [V DIGI dtmroc 9]
ERROR: voltage too LOW (4.020 V) in ELMB channel 52 [V DIGI dtmroc 9]
ELMB Channel 53:   4.009 V   [V DIGI dtmroc 10]
ERROR: voltage too LOW (4.009 V) in ELMB channel 53 [V DIGI dtmroc 10]
ELMB Channel 54:   4.023 V   [V DIGI dtmroc 11]
ERROR: voltage too LOW (4.023 V) in ELMB channel 54 [V DIGI dtmroc 11]
ELMB Channel 55:   4.012 V   [V DIGI dtmroc 12]
ERROR: voltage too LOW (4.012 V) in ELMB channel 55 [V DIGI dtmroc 12]
ELMB Channel 56:   3.981 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (3.981 V) in ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   3.974 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (3.974 V) in ELMB channel 57 [V DIGI dtmroc 13]
ELMB Channel 58:   3.955 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (3.955 V) in ELMB channel 58 [V DIGI dtmroc 14]
ELMB Channel 59:   3.960 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (3.960 V) in ELMB channel 59 [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 28.30
Channel 61, temperature 27.98
Channel 62, temperature 30.42
Channel 63, temperature 27.32

Set all regulators ON

Current channels
******** 0. 0.790400    1. 0.006100  || 0.796500 
******** 2. 0.774433    3. 0.012200  || 0.786633 
******** 4. 0.781200    5. 0.004145  || 0.785345 
******** 6. 0.813647    7. 0.023091  || 0.836739 
******** 8. 0.800135    9. 0.021663  || 0.821798 
******** 10. 0.752355    11. 0.049922  || 0.802276 
******** 12. 0.757101    13. 0.036589  || 0.793690 
******** 14. 0.845190    15. 0.002990  || 0.848181 
ERROR: curr not in range ON  (0.845 A): ELMB channel 14 [I AN_NEG dtmroc 2]
******** 16. 0.805600    17. 0.024400  || 0.830000 
******** 18. 0.765907    19. 0.024324  || 0.790230 
******** 20. 0.802600    21. 0.194940  || 0.997540 
ERROR: curr too high REG OFF (0.195 A): ELMB channel 21 [I AN_NEG dtmroc 5]
******** 22. 0.753812    23. 0.042232  || 0.796045 
******** 24. 0.737325    25. 0.002451  || 0.739775 
******** 26. 0.766000    27. 0.022679  || 0.788679 
******** 28. 0.777777    29. 0.005816  || 0.783593 
******** 30. 0.333745    31. 0.019445  || 0.353190 
ERROR: curr not in range ON  (0.334 A): ELMB channel 30 [I DIGI dtmroc 12]
******** 32. 0.808700    33. 0.032971  || 0.841671 
******** 34. 0.836336    35. 0.011882  || 0.848218 
ERROR: curr not in range ON  (0.836 A): ELMB channel 34 [I DIGI dtmroc 14]

Voltage channels:
ELMB Channel 36:   3.920 V   [V AN_POS dtmroc 1]
ERROR: voltage too LOW (3.920 V) in ELMB channel 36 [V AN_POS dtmroc 1]
ELMB Channel 37:   3.888 V   [V AN_POS dtmroc 2]
ERROR: voltage too LOW (3.888 V) in ELMB channel 37 [V AN_POS dtmroc 2]
ELMB Channel 38:   3.895 V   [V AN_POS dtmroc 3]
ERROR: voltage too LOW (3.895 V) in ELMB channel 38 [V AN_POS dtmroc 3]
ELMB Channel 39:   3.946 V   [V AN_POS dtmroc 4]
ERROR: voltage too LOW (3.946 V) in ELMB channel 39 [V AN_POS dtmroc 4]
ELMB Channel 40:   3.903 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (3.903 V) in ELMB channel 40 [V AN_POS dtmroc 5]
ELMB Channel 41:   3.911 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (3.911 V) in ELMB channel 41 [V AN_POS dtmroc 5]
ELMB Channel 42:   3.906 V   [V AN_POS dtmroc 6]
ERROR: voltage too LOW (3.906 V) in ELMB channel 42 [V AN_POS dtmroc 6]
ELMB Channel 43:   3.904 V   [V AN_POS dtmroc 6]
ERROR: voltage too LOW (3.904 V) in ELMB channel 43 [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.862 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.870 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.868 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.904 V   [V AN_NEG dtmroc 4]
ERROR: voltage too LOW (-3.904 V) in ELMB channel 47 [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.874 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.866 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.805 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.803 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   4.018 V   [V DIGI dtmroc 9]
ERROR: voltage too LOW (4.018 V) in ELMB channel 52 [V DIGI dtmroc 9]
ELMB Channel 53:   4.008 V   [V DIGI dtmroc 10]
ERROR: voltage too LOW (4.008 V) in ELMB channel 53 [V DIGI dtmroc 10]
ELMB Channel 54:   4.023 V   [V DIGI dtmroc 11]
ERROR: voltage too LOW (4.023 V) in ELMB channel 54 [V DIGI dtmroc 11]
ELMB Channel 55:   4.011 V   [V DIGI dtmroc 12]
ERROR: voltage too LOW (4.011 V) in ELMB channel 55 [V DIGI dtmroc 12]
ELMB Channel 56:   4.003 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (4.003 V) in ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   3.979 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (3.979 V) in ELMB channel 57 [V DIGI dtmroc 13]
ELMB Channel 58:   3.988 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (3.988 V) in ELMB channel 58 [V DIGI dtmroc 14]
ELMB Channel 59:   3.974 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (3.974 V) in ELMB channel 59 [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 29.44
Channel 61, temperature 29.36
Channel 62, temperature 31.69
Channel 63, temperature 28.55

Current channels
******** 0. 0.009100    1. 0.787300  || 0.796400 
******** 2. 0.012849    3. 0.805600  || 0.818449 
******** 4. 0.009100    5. 0.786271  || 0.795371 
******** 6. 0.017120    7. 0.773328  || 0.790449 
******** 8. 0.009760    9. 0.765635  || 0.775395 
******** 10. 0.031899    11. 0.843566  || 0.875466 
ERROR: curr not in range ON  (0.844 A): ELMB channel 11 [I AN_POS dtmroc 6]
******** 12. 0.035652    13. 0.744666  || 0.780318 
******** 14. 0.060597    15. 0.771697  || 0.832294 
******** 16. 0.030500    17. 0.750700  || 0.781200 
******** 18. 0.021149    19. 0.805481  || 0.826631 
******** 20. 0.015200    21. 0.982409  || 0.997609 
ERROR: curr not in range ON  (0.982 A): ELMB channel 21 [I AN_NEG dtmroc 5]
******** 22. 0.015286    23. 0.823813  || 0.839099 
******** 24. 0.062182    25. 0.805064  || 0.867247 
******** 26. 0.027400    27. 0.770724  || 0.798124 
******** 28. 0.021725    29. 0.793684  || 0.815408 
******** 30. 1.813951    31. 0.783049  || 2.597000 
ERROR: curr too high REG OFF (1.814 A): ELMB channel 30 [I DIGI dtmroc 12]
******** 32. 0.003000    33. 0.769535  || 0.772535 
******** 34. 0.012257    35. 0.781528  || 0.793785 

Voltage channels:
ELMB Channel 36:   3.906 V   [V AN_POS dtmroc 1]
ERROR: voltage too LOW (3.906 V) in ELMB channel 36 [V AN_POS dtmroc 1]
ELMB Channel 37:   3.919 V   [V AN_POS dtmroc 2]
ERROR: voltage too LOW (3.919 V) in ELMB channel 37 [V AN_POS dtmroc 2]
ELMB Channel 38:   3.891 V   [V AN_POS dtmroc 3]
ERROR: voltage too LOW (3.891 V) in ELMB channel 38 [V AN_POS dtmroc 3]
ELMB Channel 39:   3.936 V   [V AN_POS dtmroc 4]
ERROR: voltage too LOW (3.936 V) in ELMB channel 39 [V AN_POS dtmroc 4]
ELMB Channel 40:   3.886 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (3.886 V) in ELMB channel 40 [V AN_POS dtmroc 5]
ELMB Channel 41:   3.907 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (3.907 V) in ELMB channel 41 [V AN_POS dtmroc 5]
ELMB Channel 42:   3.926 V   [V AN_POS dtmroc 6]
ERROR: voltage too LOW (3.926 V) in ELMB channel 42 [V AN_POS dtmroc 6]
ELMB Channel 43:   3.934 V   [V AN_POS dtmroc 6]
ERROR: voltage too LOW (3.934 V) in ELMB channel 43 [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.793 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.808 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.868 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.867 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.863 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.873 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.866 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.875 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   4.020 V   [V DIGI dtmroc 9]
ERROR: voltage too LOW (4.020 V) in ELMB channel 52 [V DIGI dtmroc 9]
ELMB Channel 53:   4.009 V   [V DIGI dtmroc 10]
ERROR: voltage too LOW (4.009 V) in ELMB channel 53 [V DIGI dtmroc 10]
ELMB Channel 54:   4.023 V   [V DIGI dtmroc 11]
ERROR: voltage too LOW (4.023 V) in ELMB channel 54 [V DIGI dtmroc 11]
ELMB Channel 55:   4.012 V   [V DIGI dtmroc 12]
ERROR: voltage too LOW (4.012 V) in ELMB channel 55 [V DIGI dtmroc 12]
ELMB Channel 56:   3.981 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (3.981 V) in ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   3.975 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (3.975 V) in ELMB channel 57 [V DIGI dtmroc 13]
ELMB Channel 58:   3.955 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (3.955 V) in ELMB channel 58 [V DIGI dtmroc 14]
ELMB Channel 59:   3.960 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (3.960 V) in ELMB channel 59 [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 29.66
Channel 61, temperature 29.61
Channel 62, temperature 31.90
Channel 63, temperature 28.80

Current channels
******** 0. 0.552300    1. 0.231900  || 0.784200 
******** 2. 0.012849    3. 0.808700  || 0.821549 
******** 4. 0.354000    5. 0.426129  || 0.780129 
******** 6. 0.447465    7. 0.346096  || 0.793562 
******** 8. 0.607930    9. 0.170542  || 0.778473 
******** 10. 0.031899    11. 0.843566  || 0.875466 
ERROR: not in range ON  (0.875 A): ELMB channels 10 11 [I AN_POS dtmroc 6]
******** 12. 0.754098    13. 0.036590  || 0.790687 
******** 14. 0.845194    15. 0.005994  || 0.851187 
ERROR: not in range ON  (0.851 A): ELMB channels 14 15 [I AN_NEG dtmroc 2]
******** 16. 0.302100    17. 0.482100  || 0.784200 
******** 18. 0.768897    19. 0.021328  || 0.790225 
******** 20. 0.442500    21. 0.555298  || 0.997798 
ERROR: not in range ON  (0.998 A): ELMB channels 20 21 [I AN_NEG dtmroc 5]
******** 22. 0.012288    23. 0.823817  || 0.836104 
ERROR: not in range ON  (0.836 A): ELMB channels 22 23 [I AN_NEG dtmroc 6]
******** 24. 0.200082    25. 0.545736  || 0.745818 
******** 26. 0.225800    27. 0.520448  || 0.746248 
******** 28. 0.237623    29. 0.537277  || 0.774900 
******** 30. 2.860789    31. 0.569411  || 3.430200 
ERROR: not in range ON  (3.430 A): ELMB channels 30 31 [I DIGI dtmroc 12]
******** 32. 0.634700    33. 0.143919  || 0.778619 
******** 34. 0.818040    35. 0.006318  || 0.824358 

Voltage channels:
ELMB Channel 36:   3.925 V   [V AN_POS dtmroc 1]
ERROR: voltage too LOW (3.925 V) in ELMB channel 36 [V AN_POS dtmroc 1]
ELMB Channel 37:   3.919 V   [V AN_POS dtmroc 2]
ERROR: voltage too LOW (3.919 V) in ELMB channel 37 [V AN_POS dtmroc 2]
ELMB Channel 38:   3.903 V   [V AN_POS dtmroc 3]
ERROR: voltage too LOW (3.903 V) in ELMB channel 38 [V AN_POS dtmroc 3]
ELMB Channel 39:   3.952 V   [V AN_POS dtmroc 4]
ERROR: voltage too LOW (3.952 V) in ELMB channel 39 [V AN_POS dtmroc 4]
ELMB Channel 40:   3.907 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (3.907 V) in ELMB channel 40 [V AN_POS dtmroc 5]
ELMB Channel 41:   3.918 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (3.918 V) in ELMB channel 41 [V AN_POS dtmroc 5]
ELMB Channel 42:   3.926 V   [V AN_POS dtmroc 6]
ERROR: voltage too LOW (3.926 V) in ELMB channel 42 [V AN_POS dtmroc 6]
ELMB Channel 43:   3.934 V   [V AN_POS dtmroc 6]
ERROR: voltage too LOW (3.934 V) in ELMB channel 43 [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.862 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.871 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.879 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.905 V   [V AN_NEG dtmroc 4]
ERROR: voltage too LOW (-3.905 V) in ELMB channel 47 [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.882 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (-3.882 V) in ELMB channel 48 [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.883 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (-3.883 V) in ELMB channel 49 [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.867 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.876 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   4.029 V   [V DIGI dtmroc 9]
ERROR: voltage too LOW (4.029 V) in ELMB channel 52 [V DIGI dtmroc 9]
ELMB Channel 53:   4.018 V   [V DIGI dtmroc 10]
ERROR: voltage too LOW (4.018 V) in ELMB channel 53 [V DIGI dtmroc 10]
ELMB Channel 54:   4.033 V   [V DIGI dtmroc 11]
ERROR: voltage too LOW (4.033 V) in ELMB channel 54 [V DIGI dtmroc 11]
ELMB Channel 55:   4.022 V   [V DIGI dtmroc 12]
ERROR: voltage too LOW (4.022 V) in ELMB channel 55 [V DIGI dtmroc 12]
ELMB Channel 56:   4.006 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (4.006 V) in ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   3.986 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (3.986 V) in ELMB channel 57 [V DIGI dtmroc 13]
ELMB Channel 58:   3.989 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (3.989 V) in ELMB channel 58 [V DIGI dtmroc 14]
ELMB Channel 59:   3.974 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (3.974 V) in ELMB channel 59 [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 29.91
Channel 61, temperature 29.88
Channel 62, temperature 32.17
Channel 63, temperature 29.03

 TEST is finished: temperature,current or voltage error. 07.10.2009 09:10:57
