============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 69
CALIBRATION FILE: E:\LVPP_TESTS_SPI\data\card_069\LVPP_CALIB_069.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
      temperature range: <15.10 C - 67.00 C> 

TEST is started: 10.03.2014 10:46:39

ELMB <1> init OK
ELMB NB: 1 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.

Current channels
  RANGE: 0.65  -  0.85
******** 0. 50433660.516900    1. 50433650.467200  || 100867310.984100 
ERROR: curr too high REG OFF (10.050 A): ELMB channels 0 1 [I AN_POS dtmroc 1]
******** 2. 53976998.278200    3. 53976998.275200  || 107953996.553400 
******** 4. 46772191.065600    5. 46772191.065600  || 93544382.131200 
******** 6. 43228843.257600    7. 43228843.257600  || 86457686.515200 
******** 8. 50433650.467200    9. 50433650.467200  || 100867300.934400 
******** 10. 50433650.470200    11. 50433650.467200  || 100867300.937400 
  RANGE: 0.65  -  0.85
******** 12. 53976998.275200    13. 53976998.275200  || 107953996.550400 
******** 14. 50433650.467200    15. 50433650.470200  || 100867300.937400 
******** 16. 46772191.074700    17. 46772191.065600  || 93544382.140300 
******** 18. 46772191.065600    19. 46772191.068600  || 93544382.134200 
******** 20. 3543347.808000    21. 0.009100  || 3543347.817100 
******** 22. 7204807.212600    23. 7204807.209600  || 14409614.422200 
  RANGE: 0.55  -  0.75
******** 24. 21614421.628800    25. 21614421.631800  || 43228843.260600 
******** 26. 25157769.436800    27. 25157769.436800  || 50315538.873600 
******** 28. 21614421.631800    29. 21614421.628800  || 43228843.260600 
******** 30. 21614421.628800    31. 21614421.628800  || 43228843.257600 
******** 32. 21614421.631800    33. 14409614.422200  || 36024036.054000 
******** 34. 21614421.628800    35. 14409614.422200  || 36024036.051000 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   0.005 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.005 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.004 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.004 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.005 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.004 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.004 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.001 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.002 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 32.87
Channel 61, temperature 31.70
Channel 62, temperature 32.80
Channel 63, temperature 31.23

Set all regulators ON

Current channels
  RANGE: 0.65  -  0.85
******** 0. 39967664048.163002    1. 39967664046.747002  || 79935328094.910004 
ERROR: curr not in range ON (1)  (39967664048.163 A): ELMB channel 0 [I AN_POS dtmroc 1]
ERROR: curr too high REG OFF (39967664046.747 A): ELMB channel 1 [I AN_POS dtmroc 1]
******** 2. 40403732051.136101    3. 40403732050.385300  || 80807464101.521393 
ERROR: curr not in range ON (1)  (40403732051.136 A): ELMB channel 2 [I AN_POS dtmroc 2]
ERROR: curr too high REG OFF (40403732050.385 A): ELMB channel 3 [I AN_POS dtmroc 2]
******** 4. 40356959860.131500    5. 40356959859.322800  || 80713919719.454300 
ERROR: curr not in range ON (1)  (40356959860.132 A): ELMB channel 4 [I AN_POS dtmroc 3]
ERROR: curr too high REG OFF (40356959859.323 A): ELMB channel 5 [I AN_POS dtmroc 3]
******** 6. 40061326541.274406    7. 40061326540.932602  || 80122653082.207001 
ERROR: curr not in range ON (1)  (40061326541.274 A): ELMB channel 6 [I AN_POS dtmroc 4]
ERROR: curr too high REG OFF (40061326540.933 A): ELMB channel 7 [I AN_POS dtmroc 4]
******** 8. 40295659942.830307    9. 40255974447.093803  || 80551634389.924103 
ERROR: curr not in range ON (1)  (40295659942.830 A): ELMB channel 8 [I AN_POS dtmroc 5]
ERROR: curr too high REG OFF (40255974447.094 A): ELMB channel 9 [I AN_POS dtmroc 5]
******** 10. 40111760192.068199    11. 39938844818.146599  || 80050605010.214798 
ERROR: curr not in range ON (1)  (40111760192.068 A): ELMB channel 10 [I AN_POS dtmroc 6]
ERROR: curr too high REG OFF (39938844818.147 A): ELMB channel 11 [I AN_POS dtmroc 6]
  RANGE: 0.65  -  0.85
******** 12. 37949018801.042099    13. 37949018800.587395  || 75898037601.629486 
ERROR: curr not in range ON (1)  (37949018801.042 A): ELMB channel 12 [I AN_NEG dtmroc 1]
ERROR: curr too high REG OFF (37949018800.587 A): ELMB channel 13 [I AN_NEG dtmroc 1]
******** 14. 37999570563.920799    15. 37999570562.999100  || 75999141126.919891 
ERROR: curr not in range ON (1)  (37999570563.921 A): ELMB channel 14 [I AN_NEG dtmroc 2]
ERROR: curr too high REG OFF (37999570562.999 A): ELMB channel 15 [I AN_NEG dtmroc 2]
******** 16. 38392409724.252701    17. 38392409722.409500  || 76784819446.662201 
ERROR: curr not in range ON (1)  (38392409724.253 A): ELMB channel 16 [I AN_NEG dtmroc 3]
ERROR: curr too high REG OFF (38392409722.410 A): ELMB channel 17 [I AN_NEG dtmroc 3]
******** 18. 38021184985.375603    19. 38021184984.362404  || 76042369969.738007 
ERROR: curr not in range ON (1)  (38021184985.376 A): ELMB channel 18 [I AN_NEG dtmroc 4]
ERROR: curr too high REG OFF (38021184984.362 A): ELMB channel 19 [I AN_NEG dtmroc 4]
******** 20. 37934609186.580200    21. 37949018800.337196  || 75883627986.917389 
ERROR: curr not in range ON (1)  (37934609186.580 A): ELMB channel 20 [I AN_NEG dtmroc 5]
ERROR: curr too high REG OFF (37949018800.337 A): ELMB channel 21 [I AN_NEG dtmroc 5]
******** 22. 37931065839.599205    23. 37869765921.315300  || 75800831760.914505 
ERROR: curr not in range ON (1)  (37931065839.599 A): ELMB channel 22 [I AN_NEG dtmroc 6]
ERROR: curr too high REG OFF (37869765921.315 A): ELMB channel 23 [I AN_NEG dtmroc 6]
  RANGE: 0.55  -  0.75
******** 24. 34333622921.098701    25. 34333622920.524902  || 68667245841.623604 
ERROR: curr not in range ON (1)  (34333622921.099 A): ELMB channel 24 [I DIGI dtmroc 9]
ERROR: curr too high REG OFF (34333622920.525 A): ELMB channel 25 [I DIGI dtmroc 9]
******** 26. 34935637713.574097    27. 34935637713.079697  || 69871275426.653793 
ERROR: curr not in range ON (1)  (34935637713.574 A): ELMB channel 26 [I DIGI dtmroc 10]
ERROR: curr too high REG OFF (34935637713.080 A): ELMB channel 27 [I DIGI dtmroc 10]
******** 28. 34784218650.673500    29. 34784218650.008194  || 69568437300.681702 
ERROR: curr not in range ON (1)  (34784218650.674 A): ELMB channel 28 [I DIGI dtmroc 11]
ERROR: curr too high REG OFF (34784218650.008 A): ELMB channel 29 [I DIGI dtmroc 11]
******** 30. 34488703443.132301    31. 34488703442.735703  || 68977406885.868011 
ERROR: curr not in range ON (1)  (34488703443.132 A): ELMB channel 30 [I DIGI dtmroc 12]
ERROR: curr too high REG OFF (34488703442.736 A): ELMB channel 31 [I DIGI dtmroc 12]
******** 32. 34405789104.895103    33. 34095746171.560799  || 68501535276.455902 
ERROR: curr not in range ON (1)  (34405789104.895 A): ELMB channel 32 [I DIGI dtmroc 13]
ERROR: curr too high REG OFF (34095746171.561 A): ELMB channel 33 [I DIGI dtmroc 13]
******** 34. 34485041983.974899    35. 34366103608.728302  || 68851145592.703201 
ERROR: curr not in range ON (1)  (34485041983.975 A): ELMB channel 34 [I DIGI dtmroc 14]
ERROR: curr too high REG OFF (34366103608.728 A): ELMB channel 35 [I DIGI dtmroc 14]

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.722 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.763 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.759 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.731 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.753 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.749 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.736 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.720 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.534 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.539 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.576 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.541 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.533 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.534 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.533 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.527 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   3.198 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.254 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.240 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.212 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.204 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.175 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.212 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.201 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 32.69
Channel 61, temperature 31.57
Channel 62, temperature 32.62
Channel 63, temperature 31.10

Set all regulators ON

Current channels
  RANGE: 0.65  -  0.85
******** 0. 40450622353.572502    1. 40450622353.758606  || 80901244707.331116 
ERROR: curr too high REG OFF (40450622353.573 A): ELMB channel 0 [I AN_POS dtmroc 1]
ERROR: curr not in range ON (2) (40450622353.759 A): ELMB channel 1 [I AN_POS dtmroc 1]
******** 2. 40118964998.203499    3. 40118964998.972603  || 80237929997.176102 
ERROR: curr too high REG OFF (40118964998.203 A): ELMB channel 2 [I AN_POS dtmroc 2]
ERROR: curr not in range ON (2) (40118964998.973 A): ELMB channel 3 [I AN_POS dtmroc 2]
******** 4. 40000026623.497093    5. 40000026624.229599  || 80000053247.726685 
ERROR: curr too high REG OFF (40000026623.497 A): ELMB channel 4 [I AN_POS dtmroc 3]
ERROR: curr not in range ON (2) (40000026624.230 A): ELMB channel 5 [I AN_POS dtmroc 3]
******** 6. 40346093592.684097    7. 40346093593.874298  || 80692187186.558395 
ERROR: curr too high REG OFF (40346093592.684 A): ELMB channel 6 [I AN_POS dtmroc 4]
ERROR: curr not in range ON (2) (40346093593.874 A): ELMB channel 7 [I AN_POS dtmroc 4]
******** 8. 40173060107.919800    9. 40277588869.482605  || 80450648977.402405 
ERROR: curr too high REG OFF (40173060107.920 A): ELMB channel 8 [I AN_POS dtmroc 5]
ERROR: curr not in range ON (2) (40277588869.483 A): ELMB channel 9 [I AN_POS dtmroc 5]
******** 10. 40032507312.008705    11. 39956797781.127594  || 79989305093.136292 
ERROR: curr too high REG OFF (40032507312.009 A): ELMB channel 10 [I AN_POS dtmroc 6]
ERROR: curr not in range ON (2) (39956797781.128 A): ELMB channel 11 [I AN_POS dtmroc 6]
  RANGE: 0.65  -  0.85
******** 12. 37866104461.605598    13. 37866104462.609497  || 75732208924.215088 
ERROR: curr too high REG OFF (37866104461.606 A): ELMB channel 12 [I AN_NEG dtmroc 1]
ERROR: curr not in range ON (2) (37866104462.609 A): ELMB channel 13 [I AN_NEG dtmroc 1]
******** 14. 38608672051.407906    15. 38608672051.935905  || 77217344103.343811 
ERROR: curr too high REG OFF (38608672051.408 A): ELMB channel 14 [I AN_NEG dtmroc 2]
ERROR: curr not in range ON (2) (38608672051.936 A): ELMB channel 15 [I AN_NEG dtmroc 2]
******** 16. 37812127464.163399    17. 37812127463.775902  || 75624254927.939301 
ERROR: curr too high REG OFF (37812127464.163 A): ELMB channel 16 [I AN_NEG dtmroc 3]
ERROR: curr not in range ON (2) (37812127463.776 A): ELMB channel 17 [I AN_NEG dtmroc 3]
******** 18. 38464457795.433105    19. 38464457795.878700  || 76928915591.311798 
ERROR: curr too high REG OFF (38464457795.433 A): ELMB channel 18 [I AN_NEG dtmroc 4]
ERROR: curr not in range ON (2) (38464457795.879 A): ELMB channel 19 [I AN_NEG dtmroc 4]
******** 20. 38334771264.973801    21. 38532962519.824898  || 76867733784.798706 
ERROR: curr too high REG OFF (38334771264.974 A): ELMB channel 20 [I AN_NEG dtmroc 5]
ERROR: curr not in range ON (2) (38532962519.825 A): ELMB channel 21 [I AN_NEG dtmroc 5]
******** 22. 37912994765.055099    23. 37967089875.161995  || 75880084640.217102 
ERROR: curr too high REG OFF (37912994765.055 A): ELMB channel 22 [I AN_NEG dtmroc 6]
ERROR: curr not in range ON (2) (37967089875.162 A): ELMB channel 23 [I AN_NEG dtmroc 6]
  RANGE: 0.55  -  0.75
******** 24. 34589570743.792000    25. 34589570744.500099  || 69179141488.292099 
ERROR: curr too high REG OFF (34589570743.792 A): ELMB channel 24 [I DIGI dtmroc 9]
ERROR: curr not in range ON (2) (34589570744.500 A): ELMB channel 25 [I DIGI dtmroc 9]
******** 26. 34856266722.031097    27. 34856266722.827499  || 69712533444.858597 
ERROR: curr too high REG OFF (34856266722.031 A): ELMB channel 26 [I DIGI dtmroc 10]
ERROR: curr not in range ON (2) (34856266722.827 A): ELMB channel 27 [I DIGI dtmroc 10]
******** 28. 34603980358.195999    29. 34603980358.827698  || 69207960717.023697 
ERROR: curr too high REG OFF (34603980358.196 A): ELMB channel 28 [I DIGI dtmroc 11]
ERROR: curr not in range ON (2) (34603980358.828 A): ELMB channel 29 [I DIGI dtmroc 11]
******** 30. 34528270826.347496    31. 34528270827.235497  || 69056541653.582993 
ERROR: curr too high REG OFF (34528270826.347 A): ELMB channel 30 [I DIGI dtmroc 12]
ERROR: curr not in range ON (2) (34528270827.235 A): ELMB channel 31 [I DIGI dtmroc 12]
******** 32. 34499451597.979004    33. 34337284381.129002  || 68836735979.108002 
ERROR: curr too high REG OFF (34499451597.979 A): ELMB channel 32 [I DIGI dtmroc 13]
ERROR: curr not in range ON (2) (34337284381.129 A): ELMB channel 33 [I DIGI dtmroc 13]
******** 34. 34820242685.967804    35. 34838313760.597305  || 69658556446.565109 
ERROR: curr too high REG OFF (34820242685.968 A): ELMB channel 34 [I DIGI dtmroc 14]
ERROR: curr not in range ON (2) (34838313760.597 A): ELMB channel 35 [I DIGI dtmroc 14]

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.767 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.736 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.725 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.758 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.741 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.751 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.728 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.721 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.527 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.596 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.522 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.582 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.570 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.589 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.531 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.536 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   3.221 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.246 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.223 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.216 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.213 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.198 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.243 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.245 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 32.70
Channel 61, temperature 31.73
Channel 62, temperature 32.74
Channel 63, temperature 31.25

Set all regulators ON

 ======================  TEST LOOP for 1 loops =============================

 Loop: 1
DAC Settings:

Analog Positive 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:  128 [80]    128 [80]              0
Supply Channel 2:  128 [80]    128 [80]              0
Supply Channel 3:  128 [80]    128 [80]              0
Supply Channel 4:  128 [80]    128 [80]              0
Supply Channel 5:  128 [80]    128 [80]              0
Supply Channel 6:  128 [80]    128 [80]              0

Analog Negative 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:   128 [80]     128 [80]             0
Supply Channel 2:   128 [80]     128 [80]             0
Supply Channel 3:   128 [80]     128 [80]             0
Supply Channel 4:   128 [80]     128 [80]             0
Supply Channel 5:   128 [80]     128 [80]             0
Supply Channel 6:   128 [80]     128 [80]             0

Digital 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:   128 [80]     128 [80]             0
Supply Channel 2:   128 [80]     128 [80]             0
Supply Channel 3:   128 [80]     128 [80]             0
Supply Channel 4:   128 [80]     128 [80]             0
Supply Channel 5:   128 [80]     128 [80]             0
Supply Channel 6:   128 [80]     128 [80]             0

Current channels
  RANGE: 0.65  -  0.85
******** 0. 40454283812.815399    1. 40454283813.160202  || 80908567625.975601 
ERROR: not in range ON  (80908567625.976 A): ELMB channels 0 1 [I AN_POS dtmroc 1]
******** 2. 40407393510.537697    3. 40407393509.786896  || 80814787020.324585 
ERROR: not in range ON  (80814787020.325 A): ELMB channels 2 3 [I AN_POS dtmroc 2]
******** 4. 40353298400.729897    5. 40353298399.921196  || 80706596800.651093 
ERROR: not in range ON  (80706596800.651 A): ELMB channels 4 5 [I AN_POS dtmroc 3]
******** 6. 40353298399.893700    7. 40353298401.086998  || 80706596800.980698 
ERROR: not in range ON  (80706596800.981 A): ELMB channels 6 7 [I AN_POS dtmroc 4]
******** 8. 40353298400.226303    9. 40371369474.331001  || 80724667874.557312 
ERROR: not in range ON  (80724667874.557 A): ELMB channels 8 9 [I AN_POS dtmroc 5]
******** 10. 40173060108.820000    11. 40050578386.021805  || 80223638494.841797 
ERROR: not in range ON  (80223638494.842 A): ELMB channels 10 11 [I AN_POS dtmroc 6]
  RANGE: 0.65  -  0.85
******** 12. 38031933139.431900    13. 38031933139.614998  || 76063866279.046906 
ERROR: not in range ON  (76063866279.047 A): ELMB channels 12 13 [I AN_NEG dtmroc 1]
******** 14. 38630286473.036705    15. 38630286473.564705  || 77260572946.601410 
ERROR: not in range ON  (77260572946.601 A): ELMB channels 14 15 [I AN_NEG dtmroc 2]
******** 16. 38421228953.091095    17. 38421228951.247894  || 76842457904.338989 
ERROR: not in range ON  (76842457904.339 A): ELMB channels 16 17 [I AN_NEG dtmroc 3]
******** 18. 38496938483.676201    19. 38496938484.121803  || 76993876967.798004 
ERROR: not in range ON  (76993876967.798 A): ELMB channels 18 19 [I AN_NEG dtmroc 4]
******** 20. 38385204915.441002    21. 38583514281.888695  || 76968719197.329697 
ERROR: not in range ON  (76968719197.330 A): ELMB channels 20 21 [I AN_NEG dtmroc 5]
******** 22. 38046342754.525505    23. 38053547561.387199  || 76099890315.912704 
ERROR: not in range ON  (76099890315.913 A): ELMB channels 22 23 [I AN_NEG dtmroc 6]
  RANGE: 0.55  -  0.75
******** 24. 34593232203.193604    25. 34593232203.904701  || 69186464407.098297 
ERROR: not in range ON  (69186464407.098 A): ELMB channels 24 25 [I DIGI dtmroc 9]
******** 26. 35000480978.185799    27. 35000480978.249893  || 70000961956.435699 
ERROR: not in range ON  (70000961956.436 A): ELMB channels 26 27 [I DIGI dtmroc 10]
******** 28. 34820242686.611702    29. 34820242686.175201  || 69640485372.786896 
ERROR: not in range ON  (69640485372.787 A): ELMB channels 28 29 [I DIGI dtmroc 11]
******** 30. 34600437010.153107    31. 34600437010.809105  || 69200874020.962219 
ERROR: not in range ON  (69200874020.962 A): ELMB channels 30 31 [I DIGI dtmroc 12]
******** 32. 34531932286.270905    33. 34355237343.304302  || 68887169629.575211 
ERROR: not in range ON  (68887169629.575 A): ELMB channels 32 33 [I DIGI dtmroc 13]
******** 34. 34823904145.369400    35. 34841857108.405304  || 69665761253.774704 
ERROR: not in range ON  (69665761253.775 A): ELMB channels 34 35 [I DIGI dtmroc 14]

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.768 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.763 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.758 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.758 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.758 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.760 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.741 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.730 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.542 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.598 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.578 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.585 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.575 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.593 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.543 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.544 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   3.222 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.260 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.243 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.222 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.216 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.200 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.243 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.245 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 33.49
Channel 61, temperature 33.40
Channel 62, temperature 34.13
Channel 63, temperature 32.48

 CARD 69: TEST is finished:  10.03.2014 11:07:35

 ERRORS  in loop 1, test stopped. 
 Total number of loops foreseen in test: 1
