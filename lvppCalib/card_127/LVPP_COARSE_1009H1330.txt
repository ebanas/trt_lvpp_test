=====================================================
Check basic functions of a card TYPE Wheel B/Barrel
=====================================================
CALIBRATION FILE: C:\LVPP_TESTS\data\card_127\LVPP_CALIB_127.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
TEST is started: 10.09.2009 13:30:48

ELMB <1> init OK
ELMB NB: 1 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 

Single Write/Read DACs 01 23 in all DTMROCS
Write/Read DACs BROADCAST 01 23 in all DTMROCS

Check set INHIBITS

Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.
Current channels
******** 0. 0.002933  
******** 2. 0.003198  
******** 4. 0.000277  
******** 6. 0.002869  
******** 8. 0.003051  
******** 9. 0.003010  
******** 10. 0.002907  
******** 11. 0.003179  
******** 12. 0.003742  
******** 14. 0.003717  
******** 16. 0.008858  
******** 18. 0.000102  
******** 20. 0.002970  
******** 21. 0.009111  
******** 22. 0.000072  
******** 23. 0.000020  
******** 24. 0.003096  
******** 26. 0.003011  
******** 28. 0.006375  
******** 30. 0.002998  
******** 32. 0.002953  
******** 33. 0.003139  
******** 34. 0.002928  
******** 35. 0.003031  
******** 0. 0.002933  
******** 2. 0.003198  
******** 4. 0.000277  
******** 6. 0.002869  
******** 8. 0.003051  
******** 9. 0.003010  
******** 10. 0.002907  
******** 11. 0.003179  
Voltage channels
ELMB Channel 36:   0.004 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.004 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.004 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.004 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.005 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.004 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.001 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.002 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]
Temperature channels
Channel 60, temperature 33.24
Channel 61, temperature 35.46
Channel 62, temperature 30.93
Channel 63, temperature 35.76

Set all regulators ON
Current channels
******** 0. 0.762403  
******** 2. 0.784322  
******** 4. 0.772515  
******** 6. 0.782118  
******** 8. 0.780325  
******** 9. 0.783910  
******** 10. 0.769583  
******** 11. 0.770885  
******** 12. 0.742848  
******** 14. 0.762232  
******** 16. 0.776412  
******** 18. 0.758222  
******** 20. 0.756035  
******** 21. 0.740845  
******** 22. 0.772276  
******** 23. 0.734208  
******** 24. 0.743097  
******** 26. 0.729617  
******** 28. 0.750425  
******** 30. 0.774738  
******** 32. 0.781107  
******** 33. 0.758327  
******** 34. 0.749703  
******** 35. 0.760088  
******** 0. 0.762403  
******** 2. 0.784322  
******** 4. 0.772515  
******** 6. 0.782118  
******** 8. 0.780325  
******** 9. 0.783910  
******** 10. 0.769583  
******** 11. 0.770885  
Voltage channels
ELMB Channel 36:   3.798 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.825 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.840 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.821 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.840 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.830 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.805 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.813 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.707 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.693 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.709 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.721 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.683 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.710 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.703 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.702 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.856 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.792 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.819 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.847 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.818 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.826 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.829 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.823 V   [V DIGI dtmroc 14]
Temperature channels
Channel 60, temperature 33.02
Channel 61, temperature 34.74
Channel 62, temperature 30.68
Channel 63, temperature 34.89
Current channels
******** 0. 0.765419  
******** 2. 0.784337  
******** 4. 0.772558  
******** 6. 0.785197  
******** 8. 0.783441  
******** 9. 0.787013  
******** 10. 0.772690  
******** 11. 0.773847  
******** 12. 0.740096  
******** 14. 0.762488  
******** 16. 0.776325  
******** 18. 0.758199  
******** 20. 0.755944  
******** 21. 0.740909  
******** 22. 0.775421  
******** 23. 0.737168  
******** 24. 0.740129  
******** 26. 0.729619  
******** 28. 0.753570  
******** 30. 0.777738  
******** 32. 0.781088  
******** 33. 0.758431  
******** 34. 0.749703  
******** 35. 0.760130  
******** 0. 0.765419  
******** 2. 0.784337  
******** 4. 0.772558  
******** 6. 0.785197  
******** 8. 0.783441  
******** 9. 0.787013  
******** 10. 0.772690  
******** 11. 0.773847  
Voltage channels
ELMB Channel 36:   3.797 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.825 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.841 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.822 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.841 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.832 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.805 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.812 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.710 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.695 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.710 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.722 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.684 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.712 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.705 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.703 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.857 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.793 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.819 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.846 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.818 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.827 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.829 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.825 V   [V DIGI dtmroc 14]
Temperature channels
Channel 60, temperature 32.74
Channel 61, temperature 35.69
Channel 62, temperature 30.79
Channel 63, temperature 36.40
Current channels
******** 0. 0.765419  
******** 2. 0.787453  
******** 4. 0.772579  
******** 6. 0.785186  
******** 8. 0.783445  
******** 9. 0.790014  
******** 10. 0.772690  
******** 11. 0.776947  
******** 12. 0.743394  
******** 14. 0.765793  
******** 16. 0.776256  
******** 18. 0.758175  
******** 20. 0.758774  
******** 21. 0.744094  
******** 22. 0.775674  
******** 23. 0.737080  
******** 24. 0.743145  
******** 26. 0.729621  
******** 28. 0.753616  
******** 30. 0.780838  
******** 32. 0.781088  
******** 33. 0.761565  
******** 34. 0.752789  
******** 35. 0.760140  
******** 0. 0.765419  
******** 2. 0.787453  
******** 4. 0.772579  
******** 6. 0.785186  
******** 8. 0.783445  
******** 9. 0.790014  
******** 10. 0.772690  
******** 11. 0.776947  
Voltage channels
ELMB Channel 36:   3.797 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.825 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.841 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.822 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.841 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.832 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.805 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.812 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.711 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.696 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.712 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.723 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.687 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.715 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.707 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.706 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.857 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.793 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.819 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.846 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.818 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.827 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.829 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.825 V   [V DIGI dtmroc 14]
Temperature channels
Channel 60, temperature 32.98
Channel 61, temperature 37.14
Channel 62, temperature 31.96
Channel 63, temperature 38.34
Current channels
******** 0. 0.768519  
******** 2. 0.790453  
******** 4. 0.775658  
******** 6. 0.788275  
******** 8. 0.780353  
******** 9. 0.793115  
******** 10. 0.775690  
******** 11. 0.779947  
******** 12. 0.743544  
******** 14. 0.763000  
******** 16. 0.776135  
******** 18. 0.758128  
******** 20. 0.758503  
******** 21. 0.744191  
******** 22. 0.779028  
******** 23. 0.740092  
******** 24. 0.743161  
******** 26. 0.732621  
******** 28. 0.753616  
******** 30. 0.780838  
******** 32. 0.784178  
******** 33. 0.758465  
******** 34. 0.752789  
******** 35. 0.760140  
******** 0. 0.768519  
******** 2. 0.790453  
******** 4. 0.775658  
******** 6. 0.788275  
******** 8. 0.780353  
******** 9. 0.793115  
******** 10. 0.775690  
******** 11. 0.779947  
Voltage channels
ELMB Channel 36:   3.797 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.825 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.841 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.822 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.842 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.833 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.805 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.812 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.712 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.698 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.714 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.725 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.690 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.718 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.710 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.709 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.857 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.793 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.819 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.846 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.819 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.827 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.829 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.825 V   [V DIGI dtmroc 14]
Temperature channels
Channel 60, temperature 33.39
Channel 61, temperature 38.40
Channel 62, temperature 32.91
Channel 63, temperature 39.89
Current channels
******** 0. 0.768524  
******** 2. 0.790453  
******** 4. 0.775679  
******** 6. 0.788264  
******** 8. 0.780353  
******** 9. 0.793116  
******** 10. 0.772690  
******** 11. 0.783047  
******** 12. 0.743643  
******** 14. 0.766255  
******** 16. 0.776083  
******** 18. 0.758112  
******** 20. 0.758352  
******** 21. 0.747223  
******** 22. 0.779137  
******** 23. 0.740062  
******** 24. 0.743178  
******** 26. 0.732623  
******** 28. 0.753616  
******** 30. 0.783838  
******** 32. 0.784178  
******** 33. 0.758465  
******** 34. 0.752789  
******** 35. 0.760151  
******** 0. 0.768524  
******** 2. 0.790453  
******** 4. 0.775679  
******** 6. 0.788264  
******** 8. 0.780353  
******** 9. 0.793116  
******** 10. 0.772690  
******** 11. 0.783047  
Voltage channels
ELMB Channel 36:   3.797 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.825 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.841 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.823 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.842 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.833 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.805 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.812 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.713 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.699 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.715 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.726 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.692 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.719 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.711 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.710 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.858 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.793 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.819 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.846 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.819 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.827 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.829 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.825 V   [V DIGI dtmroc 14]
Temperature channels
Channel 60, temperature 33.73
Channel 61, temperature 39.43
Channel 62, temperature 33.19
Channel 63, temperature 41.00
Current channels
******** 0. 0.768524  
******** 2. 0.790453  
******** 4. 0.775679  
******** 6. 0.788264  
******** 8. 0.780353  
******** 9. 0.796116  
******** 10. 0.772690  
******** 11. 0.786047  
******** 12. 0.743742  
******** 14. 0.763102  
******** 16. 0.779100  
******** 18. 0.758104  
******** 20. 0.761352  
******** 21. 0.747266  
******** 22. 0.779173  
******** 23. 0.740052  
******** 24. 0.743178  
******** 26. 0.732623  
******** 28. 0.753661  
******** 30. 0.783838  
******** 32. 0.784178  
******** 33. 0.758501  
******** 34. 0.752789  
******** 35. 0.760151  
******** 0. 0.768524  
******** 2. 0.790453  
******** 4. 0.775679  
******** 6. 0.788264  
******** 8. 0.780353  
******** 9. 0.796116  
******** 10. 0.772690  
******** 11. 0.786047  
Voltage channels
ELMB Channel 36:   3.797 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.825 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.841 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.823 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.842 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.833 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.805 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.812 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.713 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.699 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.715 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.726 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.692 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.720 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.711 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.711 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.858 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.793 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.820 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.846 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.819 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.828 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.829 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.825 V   [V DIGI dtmroc 14]
Temperature channels
Channel 60, temperature 33.98
Channel 61, temperature 40.23
Channel 62, temperature 33.44
Channel 63, temperature 41.67
Current channels
******** 0. 0.768519  
******** 2. 0.790453  
******** 4. 0.775679  
******** 6. 0.791264  
******** 8. 0.780357  
******** 9. 0.796117  
******** 10. 0.772697  
******** 11. 0.786034  
******** 12. 0.743742  
******** 14. 0.766255  
******** 16. 0.776066  
******** 18. 0.758081  
******** 20. 0.758231  
******** 21. 0.747287  
******** 22. 0.779282  
******** 23. 0.736904  
******** 24. 0.743193  
******** 26. 0.732623  
******** 28. 0.753661  
******** 30. 0.786938  
******** 32. 0.784178  
******** 33. 0.758501  
******** 34. 0.752774  
******** 35. 0.760151  
******** 0. 0.768519  
******** 2. 0.790453  
******** 4. 0.775679  
******** 6. 0.791264  
******** 8. 0.780357  
******** 9. 0.796117  
******** 10. 0.772697  
******** 11. 0.786034  
Voltage channels
ELMB Channel 36:   3.797 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.825 V   [V AN_POS dtmroc 2]
ELM