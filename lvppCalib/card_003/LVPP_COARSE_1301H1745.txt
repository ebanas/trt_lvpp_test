============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 3
CALIBRATION FILE: C:\LVPP_TESTS\data\card_003\LVPP_CALIB_003.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
                temperature range: <15.10 C - 60.00 C> 

TEST is started: 13.01.2011 17:45:43

ELMB <7> init OK
ELMB NB: 7 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.

Current channels
******** 0. 0.006093    1. 0.006095  || 0.012188 
******** 2. 0.003039    3. 0.006096  || 0.009135 
******** 4. 0.002993    5. 0.000001  || 0.002994 
******** 6. 0.003008    7. 0.003000  || 0.006008 
******** 8. 0.003026    9. 0.000004  || 0.003029 
******** 10. 0.003010    11. 0.000033  || 0.003044 
******** 12. 0.003058    13. 0.003003  || 0.006061 
******** 14. 0.003002    15. 0.003003  || 0.006005 
******** 16. 0.012205    17. 0.003001  || 0.015207 
******** 18. 0.003001    19. 0.006104  || 0.009105 
******** 20. 0.000004    21. 0.012197  || 0.012201 
******** 22. 0.003001    23. 0.002998  || 0.005999 
******** 24. 0.003005    25. 0.003001  || 0.006006 
******** 26. 0.000007    27. 0.000000  || 0.000007 
******** 28. 0.002992    29. 0.000003  || 0.002996 
******** 30. 0.002976    31. 0.003004  || 0.005979 
******** 32. 0.006100    33. 0.003008  || 0.009108 
******** 34. 0.006098    35. 0.006099  || 0.012198 

Voltage channels:
ELMB Channel 36:   0.006 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.005 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.005 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.005 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.005 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.005 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.001 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.000 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.003 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.003 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.003 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.002 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 50.09
Channel 61, temperature 50.10
Channel 62, temperature 50.73
Channel 63, temperature 48.82

Set all regulators ON

Current channels
******** 0. 0.776355    1. 0.018182  || 0.794537 
******** 2. 0.806991    3. 0.003109  || 0.810100 
******** 4. 0.751990    5. 0.005730  || 0.757720 
******** 6. 0.771969    7. 0.000373  || 0.772342 
******** 8. 0.769721    9. 0.002981  || 0.772702 
******** 10. 0.771098    11. 0.023616  || 0.794714 
******** 12. 0.758591    13. 0.010877  || 0.769469 
******** 14. 0.739589    15. 0.003922  || 0.743511 
******** 16. 0.724096    17. 0.017237  || 0.741334 
******** 18. 0.736486    19. 0.018405  || 0.754891 
******** 20. 0.764140    21. 0.000070  || 0.764210 
******** 22. 0.731131    23. 0.028140  || 0.759271 
******** 24. 0.652615    25. 0.002048  || 0.654662 
******** 26. 0.636234    27. 0.009100  || 0.645334 
******** 28. 0.603027    29. 0.004400  || 0.607428 
******** 30. 0.596964    31. 0.009883  || 0.606847 
******** 32. 0.625600    33. 0.017422  || 0.643022 
******** 34. 0.625456    35. 0.002192  || 0.627648 

Voltage channels:
ELMB Channel 36:   3.727 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.739 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.700 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.730 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.732 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.726 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.726 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.749 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.555 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.630 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.542 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.619 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.619 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.615 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.605 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.600 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.175 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.181 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.143 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.153 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.150 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.135 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.144 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.132 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 49.96
Channel 61, temperature 49.80
Channel 62, temperature 50.45
Channel 63, temperature 48.57

Set all regulators ON

Current channels
******** 0. 0.010396    1. 0.741644  || 0.752040 
******** 2. 0.040983    3. 0.769110  || 0.810093 
******** 4. 0.004799    5. 0.748069  || 0.752868 
******** 6. 0.005940    7. 0.763271  || 0.769211 
******** 8. 0.009882    9. 0.762901  || 0.772783 
******** 10. 0.014212    11. 0.774100  || 0.788312 
******** 12. 0.026761    13. 0.758604  || 0.785364 
******** 14. 0.001920    15. 0.731460  || 0.733380 
******** 16. 0.006960    17. 0.718180  || 0.725140 
******** 18. 0.005035    19. 0.707741  || 0.712776 
******** 20. 0.025292    21. 0.726684  || 0.751976 
******** 22. 0.001158    23. 0.707231  || 0.708390 
******** 24. 0.017950    25. 0.632658  || 0.650608 
******** 26. 0.004643    27. 0.625600  || 0.630243 
******** 28. 0.025602    29. 0.633013  || 0.658616 
******** 30. 0.028555    31. 0.641586  || 0.670141 
******** 32. 0.009100    33. 0.655339  || 0.664439 
******** 34. 0.006136    35. 0.629504  || 0.635641 

Voltage channels:
ELMB Channel 36:   3.695 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.738 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.692 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.713 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.743 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.751 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.687 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.718 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.607 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.600 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.600 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.551 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.554 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.568 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.610 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.615 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.194 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.184 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.152 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.155 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.160 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.161 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.136 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.137 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 49.87
Channel 61, temperature 49.77
Channel 62, temperature 50.42
Channel 63, temperature 48.54

Set all regulators ON

Current channels
******** 0. 0.779455    1. 0.018182  || 0.797637 
******** 2. 0.388937    3. 0.424204  || 0.813141 
******** 4. 0.370481    5. 0.378771  || 0.749252 
******** 6. 0.530876    7. 0.244473  || 0.775349 
******** 8. 0.110586    9. 0.662100  || 0.772686 
******** 10. 0.771099    11. 0.026614  || 0.797713 
******** 12. 0.029828    13. 0.758602  || 0.788430 
******** 14. 0.733489    15. 0.002178  || 0.735668 
******** 16. 0.003957    17. 0.718179  || 0.722136 
******** 18. 0.736486    19. 0.018404  || 0.754890 
******** 20. 0.764144    21. 0.000075  || 0.764219 
******** 22. 0.258153    23. 0.453927  || 0.712081 
******** 24. 0.014842    25. 0.629557  || 0.644399 
******** 26. 0.248648    27. 0.381400  || 0.630048 
******** 28. 0.105493    29. 0.504915  || 0.610408 
******** 30. 0.200196    31. 0.409593  || 0.609789 
******** 32. 0.079300    33. 0.566831  || 0.646131 
******** 34. 0.225649    35. 0.400598  || 0.626247 

Voltage channels:
ELMB Channel 36:   3.727 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.745 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.707 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.735 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.743 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.750 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.727 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.748 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.604 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.631 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.597 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.620 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.619 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.615 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.615 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.617 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.190 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.187 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.154 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.161 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.160 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.159 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.151 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.146 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 49.77
Channel 61, temperature 49.94
Channel 62, temperature 50.79
Channel 63, temperature 48.64

Current channels
******** 0. 0.779455    1. 0.015181  || 0.794637 
******** 2. 0.437735    3. 0.372304  || 0.810039 
******** 4. 0.416282    5. 0.329871  || 0.746153 
******** 6. 0.485076    7. 0.290274  || 0.775350 
******** 8. 0.156286    9. 0.616398  || 0.772684 
******** 10. 0.771098    11. 0.023616  || 0.794714 
******** 12. 0.026692    13. 0.758601  || 0.785292 
******** 14. 0.687689    15. 0.050979  || 0.738668 
******** 16. 0.003954    17. 0.715178  || 0.719133 
******** 18. 0.736486    19. 0.018404  || 0.754890 
******** 20. 0.764146    21. 0.000080  || 0.764226 
******** 22. 0.316152    23. 0.392831  || 0.708982 
******** 24. 0.030134    25. 0.620456  || 0.650590 
******** 26. 0.343346    27. 0.286800  || 0.630146 
******** 28. 0.187901    29. 0.422512  || 0.610413 
******** 30. 0.273415    31. 0.336390  || 0.609805 
******** 32. 0.152500    33. 0.490516  || 0.643016 
******** 34. 0.320250    35. 0.306000  || 0.626250 

Voltage channels:
ELMB Channel 36:   3.727 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.745 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.706 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.735 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.743 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.748 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.726 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.749 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.601 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.632 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.595 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.620 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.620 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.616 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.615 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.616 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.186 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.186 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.151 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.159 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.158 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.156 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.150 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.143 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 49.89
Channel 61, temperature 50.15
Channel 62, temperature 50.91
Channel 63, temperature 48.87

Current channels
******** 0. 0.751954    1. 0.009218  || 0.761173 
******** 2. 0.388942    3. 0.424203  || 0.813146 
******** 4. 0.370481    5. 0.375671  || 0.746152 
******** 6. 0.442378    7. 0.336074  || 0.778452 
******** 8. 0.202080    9. 0.570597  || 0.772677 
******** 10. 0.771099    11. 0.023616  || 0.794715 
******** 12. 0.029763    13. 0.758599  || 0.788362 
******** 14. 0.635790    15. 0.102880  || 0.738669 
******** 16. 0.003950    17. 0.715177  || 0.719127 
******** 18. 0.736486    19. 0.021503  || 0.757990 
******** 20. 0.764147    21. 0.003080  || 0.767228 
******** 22. 0.383249    23. 0.325734  || 0.708983 
******** 24. 0.118631    25. 0.531955  || 0.650587 
******** 26. 0.248651    27. 0.381400  || 0.630051 
******** 28. 0.267305    29. 0.340110  || 0.607415 
******** 30. 0.349723    31. 0.263189  || 0.612912 
******** 32. 0.225800    33. 0.417299  || 0.643099 
******** 34. 0.228749    35. 0.400598  || 0.629347 

Voltage channels:
ELMB Channel 36:   3.727 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.746 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.707 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.736 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.742 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.746 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.727 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.749 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.598 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.633 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.591 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.621 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.620 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.616 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.613 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.614 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.184 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.188 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.150 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.158 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.157 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.152 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.151 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.146 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 50.01
Channel 61, temperature 50.37
Channel 62, temperature 51.07
Channel 63, temperature 49.10

Current channels
******** 0. 0.706154    1. 0.054918  || 0.761072 
******** 2. 0.440737    3. 0.372304  || 0.813041 
******** 4. 0.416282    5. 0.329871  || 0.746153 
******** 6. 0.396579    7. 0.378774  || 0.775352 
******** 8. 0.247876    9. 0.524796  || 0.772672 
******** 10. 0.771099    11. 0.026614  || 0.797713 
******** 12. 0.026626    13. 0.758598  || 0.785224 
******** 14. 0.574790    15. 0.163881  || 0.738671 
******** 16. 0.003948    17. 0.715177  || 0.719124 
******** 18. 0.736486    19. 0.018404  || 0.754890 
******** 20. 0.764147    21. 0.003080  || 0.767228 
******** 22. 0.319153    23. 0.392829  || 0.711983 
******** 24. 0.210127    25. 0.440355  || 0.650482 
******** 26. 0.346347    27. 0.286800  || 0.633147 
******** 28. 0.346610    29. 0.263808  || 0.610418 
******** 30. 0.276507    31. 0.336392  || 0.612898 
******** 32. 0.299000    33. 0.343988  || 0.642988 
******** 34. 0.323351    35. 0.306000  || 0.629351 

Voltage channels:
ELMB Channel 36:   3.728 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.745 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.706 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.737 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.742 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.745 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.727 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.748 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.595 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.634 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.589 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.621 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.620 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.616 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.615 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.616 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.182 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.186 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.