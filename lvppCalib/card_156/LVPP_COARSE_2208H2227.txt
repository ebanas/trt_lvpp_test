=====================================================
Check basic functions of a card TYPE Wheel B/Barrel
=====================================================
CALIBRATION FILE: C:\LVPP_TESTS\data\card_156\LVPP_CALIB_156.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
TEST is started: 22.08.2009 22:27:24

ELMB <1> init OK
ELMB NB: 1 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 

Single Write/Read DACs 01 23 in all DTMROCS
Write/Read DACs BROADCAST 01 23 in all DTMROCS

Check set INHIBITS

Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.
Current channels
******** 0. 0.009058  
******** 2. 0.013598  
******** 4. 0.007320  
******** 6. 0.007725  
******** 8. 0.007857  
******** 9. 0.007769  
******** 10. 0.006063  
******** 11. 0.008343  
******** 12. 0.008290  
******** 14. 0.007294  
******** 16. 0.014316  
******** 18. 0.007804  
******** 20. 0.007399  
******** 21. 0.015508  
******** 22. 0.007404  
******** 23. 0.008001  
******** 24. 0.003128  
******** 26. 0.006349  
******** 28. 0.004042  
******** 30. 0.007438  
******** 32. 0.007183  
******** 33. 0.013017  
******** 34. 0.007997  
******** 35. 0.007979  
Voltage channels
ELMB Channel 36:   0.140 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.139 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.140 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.139 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.143 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.141 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.138 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.138 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.115 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.113 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.113 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.113 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.112 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.110 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.109 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.108 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.179 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.176 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.180 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.180 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.181 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.180 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.180 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.180 V   [V DIGI dtmroc 14]
Temperature channels
Channel 60, temperature 31.97
Channel 61, temperature 30.60
Channel 62, temperature 30.76
Channel 63, temperature 29.54

Set all regulators ON
Current channels
ERROR: ADC convertion failed for ELMB channel 0 [I AN_POS dtmroc 1 regulator 1]
******** 2. 0.010268  
ERROR: current not in range (0.010 A): ELMB channel 2 [I AN_POS dtmroc 2]
******** 4. 0.007652  
ERROR: current not in range (0.008 A): ELMB channel 4 [I AN_POS dtmroc 3]
******** 6. 0.003393  
ERROR: current not in range (0.003 A): ELMB channel 6 [I AN_POS dtmroc 4]
******** 8. 0.006740  
ERROR: current not in range (0.007 A): ELMB channel 8 [I AN_POS dtmroc 5]
ERROR: ADC convertion failed for ELMB channel 9 [I AN_POS dtmroc 5 regulator 2]
******** 10. 0.011685  
ERROR: current not in range (0.012 A): ELMB channel 10 [I AN_POS dtmroc 6]
ERROR: ADC convertion failed for ELMB channel 11 [I AN_POS dtmroc 6 regulator 2]
******** 12. 0.030500  
ERROR: current not in range (0.031 A): ELMB channel 12 [I AN_NEG dtmroc 1]
******** 14. 0.000000  
ERROR: current not in range (0.000 A): ELMB channel 14 [I AN_NEG dtmroc 2]
ERROR: ADC convertion failed for ELMB channel 16 [I AN_NEG dtmroc 3 regulator 1]
******** 18. 0.015200  
ERROR: current not in range (0.015 A): ELMB channel 18 [I AN_NEG dtmroc 4]
ERROR: ADC convertion failed for ELMB channel 20 [I AN_NEG dtmroc 5 regulator 1]
******** 21. 0.015200  
ERROR: current not in range (0.015 A): ELMB channel 21 [I AN_NEG dtmroc 5]
******** 22. 0.000000  
ERROR: current not in range (0.000 A): ELMB channel 22 [I AN_NEG dtmroc 6]
******** 23. 0.015200  
ERROR: current not in range (0.015 A): ELMB channel 23 [I AN_NEG dtmroc 6]
ERROR: ADC convertion failed for ELMB channel 24 [I DIGI dtmroc 9 regulator 1]
******** 26. 0.007595  
ERROR: current not in range (0.008 A): ELMB channel 26 [I DIGI dtmroc 10]
ERROR: ADC convertion failed for ELMB channel 28 [I DIGI dtmroc 11 regulator 1]
******** 30. 0.004672  
ERROR: current not in range (0.005 A): ELMB channel 30 [I DIGI dtmroc 12]
******** 32. 0.023051  
ERROR: current not in range (0.023 A): ELMB channel 32 [I DIGI dtmroc 13]
******** 33. 0.014568  
ERROR: current not in range (0.015 A): ELMB channel 33 [I DIGI dtmroc 13]
******** 34. 0.007099  
ERROR: current not in range (0.007 A): ELMB channel 34 [I DIGI dtmroc 14]
******** 35. 0.006747  
ERROR: current not in range (0.007 A): ELMB channel 35 [I DIGI dtmroc 14]
Voltage channels
ELMB Channel 36:   3.772 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.751 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.774 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.770 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.745 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.769 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.758 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.761 V   [V AN_POS dtmroc 6]
ERROR: ADC convertion failed for ELMB channel 44 [V AN_NEG dtmroc 1]
ERROR: ADC convertion failed for ELMB channel 45 [V AN_NEG dtmroc 2]
ERROR: ADC convertion failed for ELMB channel 46 [V AN_NEG dtmroc 3]
ERROR: ADC convertion failed for ELMB channel 47 [V AN_NEG dtmroc 4]
ERROR: ADC convertion failed for ELMB channel 48 [V AN_NEG dtmroc 5]
ERROR: ADC convertion failed for ELMB channel 49 [V AN_NEG dtmroc 5]
ERROR: ADC convertion failed for ELMB channel 50 [V AN_NEG dtmroc 6]
ERROR: ADC convertion failed for ELMB channel 51 [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.226 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.226 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.236 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.253 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.239 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.209 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.227 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.213 V   [V DIGI dtmroc 14]
Temperature channels
Channel 60, temperature 32.00
Channel 61, temperature 30.62
Channel 62, temperature 30.79
Channel 63, temperature 29.57

 TEST is finished: 22.08.2009 22:29:30
