============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 309
CALIBRATION FILE: C:\LVPP_TESTS\data\card_309\LVPP_CALIB_309.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
                temperature range: <15.10 C - 60.00 C> 

TEST is started: 28.01.2010 23:13:14

ELMB <1> init OK
ELMB NB: 1 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.

Current channels
******** 0. 0.003000    1. 0.006089  || 0.009089 
******** 2. 0.002989    3. 0.003000  || 0.005989 
******** 4. 0.002986    5. 0.000021  || 0.003007 
******** 6. 0.000016    7. 0.002997  || 0.003014 
******** 8. 0.000031    9. 0.000013  || 0.000044 
******** 10. 0.000041    11. 0.000017  || 0.000058 
******** 12. 0.003026    13. 0.002996  || 0.006021 
******** 14. 0.003000    15. 0.000076  || 0.003076 
******** 16. 0.009092    17. 0.003016  || 0.012108 
******** 18. 0.002959    19. 0.006119  || 0.009077 
******** 20. 0.000001    21. 0.009101  || 0.009102 
******** 22. 0.000003    23. 0.000004  || 0.000007 
******** 24. 0.000006    25. 0.003008  || 0.003014 
******** 26. 0.000006    27. 0.000000  || 0.000006 
******** 28. 0.002992    29. 0.002992  || 0.005984 
******** 30. 0.003000    31. 0.000006  || 0.003006 
******** 32. 0.002995    33. 0.006101  || 0.009096 
******** 34. 0.006090    35. 0.002995  || 0.009085 

Voltage channels:
ELMB Channel 36:   0.005 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.005 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.005 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.005 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.005 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.005 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.001 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.001 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.002 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 49.33
Channel 61, temperature 50.55
Channel 62, temperature 49.23
Channel 63, temperature 48.11

Set all regulators ON

Current channels
******** 0. 0.744600    1. 0.001448  || 0.746048 
******** 2. 0.746452    3. 0.000000  || 0.746452 
******** 4. 0.743945    5. 0.008865  || 0.752810 
******** 6. 0.742068    7. 0.000811  || 0.742879 
******** 8. 0.750659    9. 0.002257  || 0.752916 
******** 10. 0.745368    11. 0.003364  || 0.748732 
******** 12. 0.755739    13. 0.012327  || 0.768065 
******** 14. 0.741500    15. 0.012212  || 0.753712 
******** 16. 0.741907    17. 0.000613  || 0.742520 
******** 18. 0.734263    19. 0.005326  || 0.739589 
******** 20. 0.738211    21. 0.011433  || 0.749643 
******** 22. 0.731717    23. 0.006862  || 0.738579 
******** 24. 0.739511    25. 0.021020  || 0.760531 
******** 26. 0.746587    27. 0.018300  || 0.764887 
******** 28. 0.740970    29. 0.015830  || 0.756800 
******** 30. 0.747700    31. 0.004011  || 0.751711 
******** 32. 0.755892    33. 0.006491  || 0.762382 
******** 34. 0.746184    35. 0.002691  || 0.748875 

Voltage channels:
ELMB Channel 36:   3.644 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.674 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.650 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.648 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.703 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.683 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.671 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.666 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.591 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.649 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.621 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.606 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.632 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.608 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.599 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.609 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.782 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.783 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.774 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.745 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.744 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.728 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.740 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.727 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 49.17
Channel 61, temperature 50.20
Channel 62, temperature 48.87
Channel 63, temperature 47.85

Set all regulators ON

Current channels
******** 0. 0.003000    1. 0.746145  || 0.749145 
******** 2. 0.001780    3. 0.747700  || 0.749480 
******** 4. 0.000759    5. 0.738829  || 0.739588 
******** 6. 0.005596    7. 0.745522  || 0.751119 
******** 8. 0.005837    9. 0.750044  || 0.755881 
******** 10. 0.002242    11. 0.741342  || 0.743584 
******** 12. 0.014288    13. 0.753919  || 0.768207 
******** 14. 0.003000    15. 0.763632  || 0.766632 
******** 16. 0.003309    17. 0.737882  || 0.741191 
******** 18. 0.001715    19. 0.756143  || 0.757859 
******** 20. 0.003206    21. 0.762413  || 0.765619 
******** 22. 0.002712    23. 0.744146  || 0.746859 
******** 24. 0.017092    25. 0.738898  || 0.755991 
******** 26. 0.013097    27. 0.735400  || 0.748497 
******** 28. 0.009780    29. 0.734920  || 0.744700 
******** 30. 0.006100    31. 0.746705  || 0.752805 
******** 32. 0.000987    33. 0.750316  || 0.751303 
******** 34. 0.007496    35. 0.748140  || 0.755635 

Voltage channels:
ELMB Channel 36:   3.645 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.660 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.651 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.630 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.657 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.650 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.660 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.664 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.602 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.706 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.619 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.661 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.684 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.678 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.691 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.712 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.818 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.740 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.759 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.739 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.736 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.737 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.721 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.721 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 49.08
Channel 61, temperature 50.12
Channel 62, temperature 48.83
Channel 63, temperature 47.83

Set all regulators ON

Current channels
******** 0. 0.204400    1. 0.541629  || 0.746029 
******** 2. 0.267262    3. 0.479100  || 0.746362 
******** 4. 0.441732    5. 0.293215  || 0.734947 
******** 6. 0.327050    7. 0.415914  || 0.742964 
******** 8. 0.128075    9. 0.624731  || 0.752806 
******** 10. 0.220504    11. 0.521527  || 0.742031 
******** 12. 0.118003    13. 0.650116  || 0.768119 
******** 14. 0.265500    15. 0.491355  || 0.756855 
******** 16. 0.308491    17. 0.435818  || 0.744309 
******** 18. 0.270277    19. 0.475265  || 0.745542 
******** 20. 0.246827    21. 0.502720  || 0.749547 
******** 22. 0.600333    23. 0.114918  || 0.715251 
******** 24. 0.345916    25. 0.369486  || 0.715402 
******** 26. 0.313287    27. 0.408900  || 0.722187 
******** 28. 0.508974    29. 0.215974  || 0.724948 
******** 30. 0.338700    31. 0.404881  || 0.743581 
******** 32. 0.356085    33. 0.393217  || 0.749303 
******** 34. 0.291568    35. 0.452036  || 0.743604 

Voltage channels:
ELMB Channel 36:   3.653 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.669 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.655 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.643 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.701 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.692 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.666 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.669 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.605 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.659 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.631 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.616 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.642 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.630 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.542 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.554 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.784 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.745 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.772 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.748 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.746 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.739 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.727 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.722 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 49.00
Channel 61, temperature 50.15
Channel 62, temperature 48.90
Channel 63, temperature 47.86

Current channels
******** 0. 0.204400    1. 0.541629  || 0.746029 
******** 2. 0.264263    3. 0.482100  || 0.746363 
******** 4. 0.444833    5. 0.290117  || 0.734950 
******** 6. 0.323950    7. 0.415914  || 0.739864 
******** 8. 0.125073    9. 0.630831  || 0.755904 
******** 10. 0.220498    11. 0.518526  || 0.739024 
******** 12. 0.118003    13. 0.653216  || 0.771219 
******** 14. 0.262400    15. 0.491355  || 0.753755 
******** 16. 0.311492    17. 0.432717  || 0.744208 
******** 18. 0.267169    19. 0.481369  || 0.748537 
******** 20. 0.246826    21. 0.499720  || 0.746546 
******** 22. 0.609435    23. 0.105714  || 0.715149 
******** 24. 0.352016    25. 0.366486  || 0.718502 
******** 26. 0.310287    27. 0.411900  || 0.722187 
******** 28. 0.508972    29. 0.215972  || 0.724944 
******** 30. 0.335700    31. 0.404881  || 0.740581 
******** 32. 0.356087    33. 0.393218  || 0.749305 
******** 34. 0.285468    35. 0.455138  || 0.740605 

Voltage channels:
ELMB Channel 36:   3.653 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.669 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.654 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.643 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.701 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.692 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.667 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.669 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.605 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.659 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.630 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.617 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.641 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.630 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.542 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.553 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.784 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.745 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.773 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.748 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.745 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.739 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.727 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.721 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 48.93
Channel 61, temperature 50.21
Channel 62, temperature 49.04
Channel 63, temperature 47.90

Current channels
******** 0. 0.204400    1. 0.544629  || 0.749029 
******** 2. 0.264262    3. 0.482100  || 0.746362 
******** 4. 0.444833    5. 0.290117  || 0.734950 
******** 6. 0.327051    7. 0.415914  || 0.742965 
******** 8. 0.121973    9. 0.630831  || 0.752803 
******** 10. 0.220498    11. 0.518526  || 0.739024 
******** 12. 0.114901    13. 0.653216  || 0.768118 
******** 14. 0.265500    15. 0.491365  || 0.756865 
******** 16. 0.311492    17. 0.432717  || 0.744208 
******** 18. 0.267169    19. 0.478269  || 0.745437 
******** 20. 0.246826    21. 0.499720  || 0.746546 
******** 22. 0.615535    23. 0.096516  || 0.712052 
******** 24. 0.352017    25. 0.366487  || 0.718504 
******** 26. 0.310287    27. 0.411900  || 0.722187 
******** 28. 0.508968    29. 0.215968  || 0.724937 
******** 30. 0.338700    31. 0.404880  || 0.743580 
******** 32. 0.356086    33. 0.393217  || 0.749304 
******** 34. 0.285463    35. 0.452036  || 0.737500 

Voltage channels:
ELMB Channel 36:   3.653 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.669 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.654 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.643 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.701 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.692 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.667 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.669 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.604 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.660 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.630 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.617 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.641 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.630 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.542 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.554 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.784 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.745 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.774 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.748 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.746 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.739 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.727 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.722 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 48.93
Channel 61, temperature 50.26
Channel 62, temperature 49.08
Channel 63, temperature 47.95

Current channels
******** 0. 0.204400    1. 0.544629  || 0.749029 
******** 2. 0.264262    3. 0.482100  || 0.746362 
******** 4. 0.444832    5. 0.290115  || 0.734947 
******** 6. 0.327050    7. 0.415914  || 0.742964 
******** 8. 0.121973    9. 0.630831  || 0.752804 
******** 10. 0.220498    11. 0.521526  || 0.742024 
******** 12. 0.121003    13. 0.650116  || 0.771119 
******** 14. 0.268500    15. 0.488365  || 0.756865 
******** 16. 0.305392    17. 0.438817  || 0.744208 
******** 18. 0.267169    19. 0.478269  || 0.745437 
******** 20. 0.237727    21. 0.508821  || 0.746548 
******** 22. 0.615537    23. 0.099615  || 0.715152 
******** 24. 0.348916    25. 0.366486  || 0.715402 
******** 26. 0.310287    27. 0.411900  || 0.722187 
******** 28. 0.508972    29. 0.215972  || 0.724944 
******** 30. 0.335700    31. 0.404881  || 0.740581 
******** 32. 0.356086    33. 0.393217  || 0.749304 
******** 34. 0.285468    35. 0.452036  || 0.737504 

Voltage channels:
ELMB Channel 36:   3.653 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.669 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.655 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.643 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.701 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.692 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.667 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.669 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.605 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.660 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.630 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.617 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.642 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.630 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.543 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.554 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.784 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.745 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.773 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.748 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.746 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.739 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.727 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.722 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 48.92
Channel 61, temperature 50.38
Channel 62, temperature 49.13
Channel 63, temperature 47.97

Current channels
******** 0. 0.204400    1. 0.544629  || 0.749029 
******** 2. 0.267262    3. 0.482100  || 0.749362 
******** 4. 0.444832    5. 0.290115  || 0.734947 
******** 6. 0.327050    7. 0.415914  || 0.742964 
******** 8. 0.121973    9. 0.633931  || 0.755904 
******** 10. 0.220498    11. 0.521526  || 0.742024 
******** 12. 0.118005    13. 0.653216  || 0.771220 
******** 14. 0.268500    15. 0.488360  || 0.756860 
******** 16. 0.308492    17. 0.435817  || 0.744308 
******** 18. 0.264169    19. 0.484369  || 0.748537 
******** 20. 0.237727    21. 0.508823  || 0.746549 
******** 22. 0.612537    23. 0.102716  || 0.715253 
******** 24. 0.352016    25. 0.366486  || 0.718502 
******** 26. 0.310286    27. 0.411900  || 0.722186 
******** 28. 0.512072    29. 0.215972  || 0.728044 
******** 30. 0.335700    31. 0.404881  || 0.740581 
******** 32. 0.356086    33. 0.393217  || 0.749304 
******** 34. 0.288468    35. 0.452038  || 0.740505 

Voltage channels:
ELMB Channel 36:   3.653 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.669 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.655 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.643 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.701 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.692 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.667 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.669 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.605 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.659 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.630 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.617 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.642 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.631 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.543 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.554 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.784 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.746 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.773 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.748 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.746 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.739 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.727 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.721 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 48.96
Channel 61, temperature 50.46
Channel 62, temperature 49.21
Channel 63, temperature 48.04

Current channels
******** 0. 0.204400    1. 0.544629  || 0.749029 
******** 2. 0.267262    3. 0.482100  || 0.749362 
******** 4. 0.444833    5. 0.290117  || 0.734950 
******** 6. 0.327050    7. 0.412814  || 0.739864 
******** 8. 0.121975    9. 0.633931  || 0.755905 
******** 10. 0.220498    11. 0.521525  || 0.742023 
******** 12. 0.118005    13. 0.653216  || 0.771220 
******** 14. 0.265500    15. 0.488365  || 0.753865 
******** 16. 0.311491    17. 0.435818  || 0.747309 
******** 18. 0.264172    19. 0.481367  || 0.745539 
******** 20. 0.240728    21. 0.508823  || 0.749551 
******** 22. 0.615535    23. 0.096515  || 0.712050 
******** 24. 0.352016    25. 0.366486  || 0.718502 
******** 26. 0.310286    27. 0.411900  || 0.722186 
******** 28. 0.508973    29. 0.215973  || 0.724946 
******** 30. 0.335700    31. 0.404882  || 0.740582 
******** 32. 0.356087    33. 0.393217  || 0.749304 
******** 34. 0.285468    35. 0.452041  || 0.737509 

Voltage channels:
ELMB Channel 36:   3.653 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.669 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.654 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.643 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.701 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.692 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.667 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.669 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.605 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.660 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.631 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.617 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.643 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.631 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.542 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.554 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.784 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.746 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.773 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.747 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.745 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.739 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.727 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.721 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 48.97
Channel 61, temperature 50.49
Channel 62, temperature 49.24
Channel 63, temperature 48.06

Current channels
******** 0. 0.204400    1. 0.541629  || 0.746029 
******** 2. 0.264262    3. 0.482100  || 0.746362 
******** 4. 0.444832    5. 0.290115  || 0.734947 
******** 6. 0.327050    7. 0.412814  || 0.739864 
******** 8. 0.121973    9. 0.630831  || 0.752804 
******** 10. 0.220498    11. 0.518526  || 0.739024 
******** 12. 0.121005    13. 0.650116  || 0.771120 
******** 14. 0.262400    15. 0.494460  || 0.756860 
******** 16. 0.314591    17. 0.432718  || 0.747309 
******** 18. 0.264169    19. 0.481369  || 0.745537 
******** 20. 0.240727    21. 0.508824  || 0.749551 
******** 22. 0.609437    23. 0.102716  || 0.712153 
******** 24. 0.352015    25. 0.366485  || 0.718500 
******** 26. 0.310286    27. 0.411900  || 0.722186 
******** 28. 0.508971    29. 0.215971  || 0.724942 
******** 30. 0.335700    31. 0.407981  || 0.743681 
******** 32. 0.356086    33. 0.393217  || 0.749304 
******** 34. 0.285468    35. 0.455138  || 0.740605 

Voltage channels:
ELMB Channel 36:   3.653 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.669 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.655 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.643 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.701 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.692 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.667 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.669 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.605 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.659 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.631 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.617 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.642 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.631 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.543 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.554 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.783 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.746 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.773 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.748 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.746 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.739 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.727 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.721 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 49.01
Channel 61, temperature 50.60
Channel 62, temperature 49.31
Channel 63, temperature 48.09

Current channels
******** 0. 0.204400    1. 0.541629  || 0.746029 
******** 2. 0.264262    3. 0.482100  || 0.746362 
******** 4. 0.444834    5. 0.293218  || 0.738052 
******** 6. 0.327051    7. 0.415914  || 0.742965 
******** 8. 0.121970    9. 0.630831  || 0.752801 
******** 10. 0.220501    11. 0.518527  || 0.739028 
******** 12. 0.114905    13. 0.653216  || 0.768120 
******** 14. 0.259400    15. 0.494465  || 0.753865 
******** 16. 0.308492    17. 0.438817  || 0.747308 
******** 18. 0.261063    19. 0.487471  || 0.748534 
******** 20. 0.237728    21. 0.511924  || 0.749652 
******** 22. 0.606437    23. 0.105716  || 0.712153 
******** 24. 0.352018    25. 0.366488  || 0.718506 
******** 26. 0.310286    27. 0.408900  || 0.719186 
******** 28. 0.508973    29. 0.215973  || 0.724946 
******** 30. 0.335700    31. 0.404882  || 0.740582 
******** 32. 0.356087    33. 0.393217  || 0.749305 
******** 34. 0.285468    35. 0.455138  || 0.7