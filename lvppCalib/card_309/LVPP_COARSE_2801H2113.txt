============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 309
CALIBRATION FILE: C:\LVPP_TESTS\data\card_309\LVPP_CALIB_309.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
                temperature range: <15.10 C - 60.00 C> 

TEST is started: 28.01.2010 21:13:16

ELMB <1> init OK
ELMB NB: 1 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.

Current channels
******** 0. 0.006100    1. 0.002988  || 0.009088 
******** 2. 0.006089    3. 0.003000  || 0.009089 
******** 4. 0.000014    5. 0.000021  || 0.000034 
******** 6. 0.000018    7. 0.002997  || 0.003015 
******** 8. 0.000031    9. 0.000012  || 0.000043 
******** 10. 0.000041    11. 0.000017  || 0.000058 
******** 12. 0.003026    13. 0.002996  || 0.006021 
******** 14. 0.003000    15. 0.000076  || 0.003076 
******** 16. 0.009092    17. 0.003016  || 0.012108 
******** 18. 0.002959    19. 0.003019  || 0.005977 
******** 20. 0.000001    21. 0.009103  || 0.009103 
******** 22. 0.003003    23. 0.000003  || 0.003006 
******** 24. 0.000006    25. 0.003008  || 0.003014 
******** 26. 0.000007    27. 0.000000  || 0.000007 
******** 28. 0.002992    29. 0.000008  || 0.003000 
******** 30. 0.003000    31. 0.000005  || 0.003005 
******** 32. 0.002995    33. 0.006101  || 0.009096 
******** 34. 0.002992    35. 0.006092  || 0.009084 

Voltage channels:
ELMB Channel 36:   0.006 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.005 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.005 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.005 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.005 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.005 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.001 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.003 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.001 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.001 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.002 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 47.46
Channel 61, temperature 48.53
Channel 62, temperature 47.35
Channel 63, temperature 46.66

Set all regulators ON

Current channels
******** 0. 0.744600    1. 0.001449  || 0.746049 
******** 2. 0.743353    3. 0.000000  || 0.743353 
******** 4. 0.740847    5. 0.011963  || 0.752809 
******** 6. 0.739067    7. 0.000811  || 0.739878 
******** 8. 0.747661    9. 0.002260  || 0.749921 
******** 10. 0.745366    11. 0.003366  || 0.748732 
******** 12. 0.752630    13. 0.012328  || 0.764959 
******** 14. 0.741500    15. 0.012189  || 0.753689 
******** 16. 0.741908    17. 0.000615  || 0.742523 
******** 18. 0.734278    19. 0.005319  || 0.739597 
******** 20. 0.735107    21. 0.008324  || 0.743431 
******** 22. 0.731717    23. 0.006863  || 0.738580 
******** 24. 0.739511    25. 0.021020  || 0.760531 
******** 26. 0.743586    27. 0.018300  || 0.761886 
******** 28. 0.740970    29. 0.015830  || 0.756800 
******** 30. 0.744600    31. 0.004013  || 0.748613 
******** 32. 0.755893    33. 0.009590  || 0.765483 
******** 34. 0.746177    35. 0.002693  || 0.748870 

Voltage channels:
ELMB Channel 36:   3.643 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.674 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.649 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.648 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.703 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.682 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.671 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.667 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.590 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.647 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.620 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.605 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.629 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.606 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.599 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.609 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.782 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.783 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.774 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.745 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.743 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.728 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.741 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.728 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 47.34
Channel 61, temperature 48.20
Channel 62, temperature 47.11
Channel 63, temperature 46.41

Set all regulators ON

Current channels
******** 0. 0.003000    1. 0.746144  || 0.749144 
******** 2. 0.001781    3. 0.747700  || 0.749481 
******** 4. 0.003758    5. 0.735730  || 0.739488 
******** 6. 0.005596    7. 0.745522  || 0.751119 
******** 8. 0.002837    9. 0.746944  || 0.749781 
******** 10. 0.005245    11. 0.741341  || 0.746585 
******** 12. 0.014277    13. 0.750920  || 0.765198 
******** 14. 0.000000    15. 0.763599  || 0.763599 
******** 16. 0.003313    17. 0.734774  || 0.738087 
******** 18. 0.001689    19. 0.756131  || 0.757820 
******** 20. 0.003211    21. 0.759300  || 0.762511 
******** 22. 0.000396    23. 0.747237  || 0.747633 
******** 24. 0.014092    25. 0.738898  || 0.752991 
******** 26. 0.013099    27. 0.735400  || 0.748499 
******** 28. 0.012780    29. 0.734920  || 0.747700 
******** 30. 0.006100    31. 0.746705  || 0.752805 
******** 32. 0.000989    33. 0.747216  || 0.748206 
******** 34. 0.007500    35. 0.745040  || 0.752539 

Voltage channels:
ELMB Channel 36:   3.646 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.660 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.651 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.630 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.657 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.650 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.660 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.665 V   [V AN_POS dtmroc 6]
ELMB Channel 44:   -3.599 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.704 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.617 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.658 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.681 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   -3.675 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.689 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.709 V   [V AN_NEG dtmroc 6]
ELMB Channel 52:   3.818 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.740 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.759 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.739 V   [V DIGI dtmroc 12]
ELMB Channel 56:   3.737 V   [V DIGI dtmroc 13]
ELMB Channel 57:   3.738 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.721 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.721 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 47.26
Channel 61, temperature 48.19
Channel 62, temperature 47.11
Channel 63, temperature 46.35

Set all regulators ON

Current channels
******** 0. 0.201400    1. 0.544630  || 0.746030 
******** 2. 0.267263    3. 0.476000  || 0.743263 
******** 4. 0.441734    5. 0.290118  || 0.731852 
******** 6. 0.323948    7. 0.415915  || 0.739863 
******** 8. 0.121975    9. 0.630832  || 0.752807 
******** 10. 0.217401    11. 0.521527  || 0.738928 
******** 12. 0.124095    13. 0.644018  || 0.768112 
******** 14. 0.271600    15. 0.482227  || 0.753827 
******** 16. 0.311494    17. 0.432712  || 0.744206 
******** 18. 0.282492    19. 0.463058  || 0.