============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 309
CALIBRATION FILE: C:\LVPP_TESTS\data\card_309\LVPP_CALIB_309.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
      temperature range: <15.10 C - 67.00 C> 

TEST is started: 11.06.2013 18:13:07

ELMB <7> init OK
ELMB NB: 7 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.003000    1. 0.000010  || 0.003010 
******** 2. 0.002991    3. 0.003000  || 0.005991 
******** 4. 0.000013    5. 0.000019  || 0.000032 
******** 6. 0.000014    7. 0.000002  || 0.000017 
******** 8. 0.000031    9. 0.000013  || 0.000044 
******** 10. 0.000035    11. 0.000017  || 0.000052 
  RANGE: 0.65  -  0.85
******** 12. 0.003024    13. 0.000004  || 0.003028 
******** 14. 0.000000    15. 0.003067  || 0.003067 
******** 16. 0.006092    17. 0.000015  || 0.006108 
******** 18. 0.000041    19. 0.000019  || 0.000060 
******** 20. 0.000001    21. 0.009101  || 0.009102 
******** 22. 0.000002    23. 0.000003  || 0.000005 
  RANGE: 0.55  -  0.75
******** 24. 0.000005    25. 0.000007  || 0.000012 
******** 26. 0.000005    27. 0.000000  || 0.000005 
******** 28. 0.002992    29. 0.002992  || 0.005984 
******** 30. 0.003000    31. 0.000005  || 0.003005 
******** 32. 0.000005    33. 0.000001  || 0.000007 
******** 34. 0.002992    35. 0.000005  || 0.002997 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   0.005 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.005 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.005 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.004 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.004 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.005 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.005 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.005 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.005 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.002 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ELMB Channel 56:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 57:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.001 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 27.58
Channel 61, temperature 27.25
Channel 62, temperature 28.00
Channel 63, temperature 26.73

Set all regulators ON

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.747700    1. 0.001292  || 0.748992 
******** 2. 0.746292    3. 0.000000  || 0.746292 
******** 4. 0.743741    5. 0.012275  || 0.756016 
******** 6. 0.000026    7. 0.000004  || 0.000030 
ERROR: curr not in range ON (1)  (0.000 A): ELMB channel 6 [I AN_POS dtmroc 4]
******** 8. 0.002951    9. 0.000021  || 0.002972 
ERROR: curr not in range ON (1)  (0.003 A): ELMB channel 8 [I AN_POS dtmroc 5]
******** 10. 0.747775    11. 0.000554  || 0.748329 
  RANGE: 0.65  -  0.85
******** 12. 0.724850    13. 0.009392  || 0.734242 
******** 14. 0.714100    15. 0.007825  || 0.721925 
******** 16. 0.711442    17. 0.000884  || 0.712326 
******** 18. 0.000047    19. 0.003021  || 0.003069 
ERROR: curr not in range ON (1)  (0.000 A): ELMB channel 18 [I AN_NEG dtmroc 4]
******** 20. 0.000001    21. 0.009103  || 0.009104 
ERROR: curr not in range ON (1)  (0.000 A): ELMB channel 20 [I AN_NEG dtmroc 5]
******** 22. 0.703890    23. 0.004278  || 0.708169 
  RANGE: 0.55  -  0.75
******** 24. 0.003002    25. 0.003002  || 0.006004 
ERROR: curr not in range ON (1)  (0.003 A): ELMB channel 24 [I DIGI dtmroc 9]
******** 26. 0.000003    27. 0.003000  || 0.003003 
ERROR: curr not in range ON (1)  (0.000 A): ELMB channel 26 [I DIGI dtmroc 10]
******** 28. 0.000002    29. 0.000002  || 0.000005 
ERROR: curr not in range ON (1)  (0.000 A): ELMB channel 28 [I DIGI dtmroc 11]
******** 30. 0.000000    31. 0.000000  || 0.000000 
ERROR: curr not in range ON (1)  (0.000 A): ELMB channel 30 [I DIGI dtmroc 12]
******** 32. 0.002996    33. 0.003001  || 0.005997 
ERROR: curr not in range ON (1)  (0.003 A): ELMB channel 32 [I DIGI dtmroc 13]
******** 34. 0.003000    35. 0.006103  || 0.009103 
ERROR: curr not in range ON (1)  (0.003 A): ELMB channel 34 [I DIGI dtmroc 14]

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.718 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.754 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.726 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.007 V   [V AN_POS dtmroc 4]
ERROR: voltage too LOW (0.007 V) in ELMB channel 39 [V AN_POS dtmroc 4]
ELMB Channel 40:   0.007 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (0.007 V) in ELMB channel 40 [V AN_POS dtmroc 5]
ELMB Channel 41:   0.008 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (0.008 V) in ELMB channel 41 [V AN_POS dtmroc 5]
ELMB Channel 42:   3.756 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.751 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.510 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.558 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.536 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.005 V   [V AN_NEG dtmroc 4]
ERROR: voltage too LOW (0.005 V) in ELMB channel 47 [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.001 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (0.001 V) in ELMB channel 48 [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.001 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (0.001 V) in ELMB channel 49 [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.497 V   [V AN_NEG dtmroc 6]
ERROR: voltage too LOW (-3.497 V) in ELMB channel 50 [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.505 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   0.001 V   [V DIGI dtmroc 9]
ERROR: voltage too LOW (0.001 V) in ELMB channel 52 [V DIGI dtmroc 9]
ELMB Channel 53:   0.001 V   [V DIGI dtmroc 10]
ERROR: voltage too LOW (0.001 V) in ELMB channel 53 [V DIGI dtmroc 10]
ELMB Channel 54:   0.001 V   [V DIGI dtmroc 11]
ERROR: voltage too LOW (0.001 V) in ELMB channel 54 [V DIGI dtmroc 11]
ELMB Channel 55:   0.000 V   [V DIGI dtmroc 12]
ERROR: voltage too LOW (0.000 V) in ELMB channel 55 [V DIGI dtmroc 12]
ELMB Channel 56:   0.001 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (0.001 V) in ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   0.001 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (0.001 V) in ELMB channel 57 [V DIGI dtmroc 13]
ELMB Channel 58:   0.000 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (0.000 V) in ELMB channel 58 [V DIGI dtmroc 14]
ELMB Channel 59:   -0.001 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (-0.001 V) in ELMB channel 59 [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 27.72
Channel 61, temperature 27.35
Channel 62, temperature 28.12
Channel 63, temperature 26.83

Set all regulators ON

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.000000    1. 0.745986  || 0.745986 
******** 2. 0.001369    3. 0.747700  || 0.749069 
******** 4. 0.003966    5. 0.735415  || 0.739381 
******** 6. 0.000023    7. 0.002996  || 0.003019 
ERROR: curr not in range ON (2) (0.003 A): ELMB channel 7 [I AN_POS dtmroc 4]
******** 8. 0.002958    9. 0.000017  || 0.002975 
ERROR: curr not in range ON (2) (0.000 A): ELMB channel 9 [I AN_POS dtmroc 5]
******** 10. 0.005908    11. 0.747160  || 0.753068 
  RANGE: 0.65  -  0.85
******** 12. 0.007782    13. 0.723486  || 0.731269 
******** 14. 0.003000    15. 0.731826  || 0.734826 
******** 16. 0.003445    17. 0.707111  || 0.710555 
******** 18. 0.000041    19. 0.003019  || 0.003060 
ERROR: curr not in range ON (2) (0.003 A): ELMB channel 19 [I AN_NEG dtmroc 4]
******** 20. 0.003001    21. 0.009101  || 0.012102 
ERROR: curr not in range ON (2) (0.009 A): ELMB channel 21 [I AN_NEG dtmroc 5]
******** 22. 0.002412    23. 0.722471  || 0.724883 
  RANGE: 0.55  -  0.75
******** 24. 0.000004    25. 0.000004  || 0.000008 
ERROR: curr not in range ON (2) (0.000 A): ELMB channel 25 [I DIGI dtmroc 9]
******** 26. 0.000003    27. 0.000000  || 0.000003 
ERROR: curr not in range ON (2) (0.000 A): ELMB channel 27 [I DIGI dtmroc 10]
******** 28. 0.002995    29. 0.000005  || 0.003000 
ERROR: curr not in range ON (2) (0.000 A): ELMB channel 29 [I DIGI dtmroc 11]
******** 30. 0.003000    31. 0.000001  || 0.003001 
ERROR: curr not in range ON (2) (0.000 A): ELMB channel 31 [I DIGI dtmroc 12]
******** 32. 0.002997    33. 0.003000  || 0.005998 
ERROR: curr not in range ON (2) (0.003 A): ELMB channel 33 [I DIGI dtmroc 13]
******** 34. 0.003002    35. 0.003005  || 0.006007 
ERROR: curr not in range ON (2) (0.003 A): ELMB channel 35 [I DIGI dtmroc 14]

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.721 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.734 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.728 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.007 V   [V AN_POS dtmroc 4]
ERROR: voltage too LOW (0.007 V) in ELMB channel 39 [V AN_POS dtmroc 4]
ELMB Channel 40:   0.006 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (0.006 V) in ELMB channel 40 [V AN_POS dtmroc 5]
ELMB Channel 41:   0.006 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (0.006 V) in ELMB channel 41 [V AN_POS dtmroc 5]
ELMB Channel 42:   3.742 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.747 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.517 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.615 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.535 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.005 V   [V AN_NEG dtmroc 4]
ERROR: voltage too LOW (0.005 V) in ELMB channel 47 [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.000 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (0.000 V) in ELMB channel 48 [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (0.000 V) in ELMB channel 49 [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.597 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.618 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   0.001 V   [V DIGI dtmroc 9]
ERROR: voltage too LOW (0.001 V) in ELMB channel 52 [V DIGI dtmroc 9]
ELMB Channel 53:   0.001 V   [V DIGI dtmroc 10]
ERROR: voltage too LOW (0.001 V) in ELMB channel 53 [V DIGI dtmroc 10]
ELMB Channel 54:   0.001 V   [V DIGI dtmroc 11]
ERROR: voltage too LOW (0.001 V) in ELMB channel 54 [V DIGI dtmroc 11]
ELMB Channel 55:   0.000 V   [V DIGI dtmroc 12]
ERROR: voltage too LOW (0.000 V) in ELMB channel 55 [V DIGI dtmroc 12]
ELMB Channel 56:   0.001 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (0.001 V) in ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   0.000 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (0.000 V) in ELMB channel 57 [V DIGI dtmroc 13]
ELMB Channel 58:   -0.000 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (-0.000 V) in ELMB channel 58 [V DIGI dtmroc 14]
ELMB Channel 59:   -0.001 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (-0.001 V) in ELMB channel 59 [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 27.85
Channel 61, temperature 27.43
Channel 62, temperature 28.23
Channel 63, temperature 26.98

Set all regulators ON

 ======================  TEST LOOP for 100 loops =============================

 Loop: 1
DAC Settings:

Analog Positive 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:  128 [80]    129 [81]              -1
Supply Channel 2:  128 [80]    127 [7F]              1
Supply Channel 3:  128 [80]    129 [81]              -1
Supply Channel 4:  128 [80]    127 [7F]              1
Supply Channel 5:  128 [80]    129 [81]              -1
Supply Channel 6:  128 [80]    127 [7F]              1

Analog Negative 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:   128 [80]     127 [7F]             1
Supply Channel 2:   128 [80]     127 [7F]             1
Supply Channel 3:   128 [80]     127 [7F]             1
Supply Channel 4:   128 [80]     129 [81]             -1
Supply Channel 5:   128 [80]     129 [81]             -1
Supply Channel 6:   128 [80]     127 [7F]             1

Digital 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:   128 [80]     129 [81]             -1
Supply Channel 2:   128 [80]     129 [81]             -1
Supply Channel 3:   128 [80]     129 [81]             -1
Supply Channel 4:   128 [80]     127 [7F]             1
Supply Channel 5:   128 [80]     129 [81]             -1
Supply Channel 6:   128 [80]     129 [81]             -1

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.250200    1. 0.495674  || 0.745874 
******** 2. 0.560084    3. 0.186100  || 0.746184 
******** 4. 0.234017    5. 0.500389  || 0.734406 
******** 6. 0.000026    7. 0.002996  || 0.003021 
ERROR: not in range ON  (0.003 A): ELMB channels 6 7 [I AN_POS dtmroc 4]
******** 8. 0.002954    9. 0.002983  || 0.005936 
ERROR: not in range ON  (0.006 A): ELMB channels 8 9 [I AN_POS dtmroc 5]
******** 10. 0.497440    11. 0.249620  || 0.747059 
  RANGE: 0.65  -  0.85
******** 12. 0.224407    13. 0.515982  || 0.740389 
******** 14. 0.003000    15. 0.731807  || 0.734807 
******** 16. 0.320825    17. 0.392749  || 0.713575 
******** 18. 0.000044    19. 0.000020  || 0.000064 
ERROR: not in range ON  (0.000 A): ELMB channels 18 19 [I AN_NEG dtmroc 4]
******** 20. 0.003001    21. 0.009100  || 0.012101 
ERROR: not in range ON  (0.012 A): ELMB channels 20 21 [I AN_NEG dtmroc 5]
******** 22. 0.002411    23. 0.722470  || 0.724881 
  RANGE: 0.55  -  0.75
******** 24. 0.000003    25. 0.003003  || 0.003006 
ERROR: not in range ON  (0.003 A): ELMB channels 24 25 [I DIGI dtmroc 9]
******** 26. 0.002996    27. 0.003000  || 0.005996 
ERROR: not in range ON  (0.006 A): ELMB channels 26 27 [I DIGI dtmroc 10]
******** 28. 0.000005    29. 0.000005  || 0.000009 
ERROR: not in range ON  (0.000 A): ELMB channels 28 29 [I DIGI dtmroc 11]
******** 30. 0.000000    31. 0.000002  || 0.000002 
ERROR: not in range ON  (0.000 A): ELMB channels 30 31 [I DIGI dtmroc 12]
******** 32. 0.002996    33. 0.003000  || 0.005997 
ERROR: not in range ON  (0.006 A): ELMB channels 32 33 [I DIGI dtmroc 13]
******** 34. 0.003002    35. 0.003002  || 0.006004 
ERROR: not in range ON  (0.006 A): ELMB channels 34 35 [I DIGI dtmroc 14]

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.727 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.758 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.734 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.007 V   [V AN_POS dtmroc 4]
ERROR: voltage too LOW (0.007 V) in ELMB channel 39 [V AN_POS dtmroc 4]
ELMB Channel 40:   0.007 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (0.007 V) in ELMB channel 40 [V AN_POS dtmroc 5]
ELMB Channel 41:   0.006 V   [V AN_POS dtmroc 5]
ERROR: voltage too LOW (0.006 V) in ELMB channel 41 [V AN_POS dtmroc 5]
ELMB Channel 42:   3.761 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.759 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.522 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.613 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.547 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.005 V   [V AN_NEG dtmroc 4]
ERROR: voltage too LOW (0.005 V) in ELMB channel 47 [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.001 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (0.001 V) in ELMB channel 48 [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.000 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (0.000 V) in ELMB channel 49 [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.597 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.618 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   0.001 V   [V DIGI dtmroc 9]
ERROR: voltage too LOW (0.001 V) in ELMB channel 52 [V DIGI dtmroc 9]
ELMB Channel 53:   0.001 V   [V DIGI dtmroc 10]
ERROR: voltage too LOW (0.001 V) in ELMB channel 53 [V DIGI dtmroc 10]
ELMB Channel 54:   0.001 V   [V DIGI dtmroc 11]
ERROR: voltage too LOW (0.001 V) in ELMB channel 54 [V DIGI dtmroc 11]
ELMB Channel 55:   0.001 V   [V DIGI dtmroc 12]
ERROR: voltage too LOW (0.001 V) in ELMB channel 55 [V DIGI dtmroc 12]
ELMB Channel 56:   0.001 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (0.001 V) in ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   0.001 V   [V DIGI dtmroc 13]
ERROR: voltage too LOW (0.001 V) in ELMB channel 57 [V DIGI dtmroc 13]
ELMB Channel 58:   -0.000 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (-0.000 V) in ELMB channel 58 [V DIGI dtmroc 14]
ELMB Channel 59:   -0.000 V   [V DIGI dtmroc 14]
ERROR: voltage too LOW (-0.000 V) in ELMB channel 59 [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 29.09
Channel 61, temperature 28.42
Channel 62, temperature 29.18
Channel 63, temperature 28.31

 CARD 309: TEST is finished:  11.06.2013 18:14:41

 ERRORS  in loop 1, test stopped. 
 Total number of loops foreseen in test: 100
