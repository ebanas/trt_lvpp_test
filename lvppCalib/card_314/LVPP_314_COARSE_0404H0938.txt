============================================
Check basic functions of a card TYPE A
============================================
Card Serial Number: 314
CALIBRATION FILE: C:\LVPP_TESTS\data\card_314\LVPP_CALIB_314.csv
Load: 5 ohms, valid current range: <0.520 A - 0.820 A> 
      temperature range: <15.10 C - 60.00 C> 

TEST is started: 04.04.2011 09:38:33

ELMB <7> init OK
ELMB NB: 7 Initializing of DTMROCS: 1 2 3 4 5 6 9 10 11 12 13 14 
Single Write/Read DACs 01 23 in all DTMROCS

Write to all DACs  - broadcast
Check set INHIBITS
Set all regulators ON one by one
Set all regulators OFF one by one
Set all regulators ON - BROADCAST
Set all regulators OFF - BROADCAST

Check ANALOG readout

Hard reset: all regulators are off, all DACs are set to 0xFF.

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.002988    1. 0.003013  || 0.006001 
******** 2. 0.006095    3. 0.003014  || 0.009109 
******** 4. 0.000008    5. 0.003002  || 0.003010 
******** 6. 0.003000    7. 0.000012  || 0.003012 
******** 8. 0.000002    9. 0.000003  || 0.000005 
******** 10. 0.002994    11. 0.000012  || 0.003006 
  RANGE: 0.65  -  0.85
******** 12. 0.003011    13. 0.000016  || 0.003026 
******** 14. 0.000012    15. 0.002992  || 0.003004 
******** 16. 0.009103    17. 0.002993  || 0.012096 
ERROR: ADC convertion failed for ELMB channel 18 [I AN_NEG dtmroc 4 regulator 1]
ERROR: curr too high REG OFF (124.146 A): ELMB channels 18 19 [I AN_NEG dtmroc 4]
******** 20. 0.000004    21. 0.022515  || 0.022519 
******** 22. 0.000002    23. 39.448200  || 39.448202 
ERROR: curr too high REG OFF (39.448 A): ELMB channels 22 23 [I AN_NEG dtmroc 6]
  RANGE: 0.55  -  0.75
******** 24. 0.000002    25. 0.003000  || 0.003002 
******** 26. 0.002985    27. 0.000004  || 0.002989 
******** 28. 0.000000    29. 0.000006  || 0.000006 
******** 30. 0.002992    31. 44.425697  || 44.428689 
ERROR: curr too high REG OFF (44.429 A): ELMB channels 30 31 [I DIGI dtmroc 12]
******** 32. 0.043701    33. 0.003005  || 0.046706 
******** 34. 0.002999    35. 0.003002  || 0.006001 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   0.005 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   0.005 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   0.004 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   0.004 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   0.004 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   0.005 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   0.005 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   0.004 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   0.005 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   0.004 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   0.004 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   0.004 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   0.001 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   3.327 V   [V AN_NEG dtmroc 5]
ELMB Channel 50:   0.001 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   0.001 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   0.002 V   [V DIGI dtmroc 9]
ELMB Channel 53:   0.002 V   [V DIGI dtmroc 10]
ELMB Channel 54:   0.003 V   [V DIGI dtmroc 11]
ELMB Channel 55:   0.002 V   [V DIGI dtmroc 12]
ERROR: ADC convertion failed for ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   0.002 V   [V DIGI dtmroc 13]
ELMB Channel 58:   0.001 V   [V DIGI dtmroc 14]
ELMB Channel 59:   0.001 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 33.27
Channel 61, temperature 32.83
Channel 62, temperature 33.52
Channel 63, temperature 31.87

Set all regulators ON

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.757043    1. 0.000603  || 0.757646 
******** 2. 0.756539    3. 0.005118  || 0.761658 
******** 4. 0.752804    5. 0.001867  || 0.754671 
******** 6. 0.756800    7. 0.000102  || 0.756902 
******** 8. 0.754929    9. 0.000382  || 0.755311 
******** 10. 0.751930    11. 0.000931  || 0.752861 
  RANGE: 0.65  -  0.85
******** 12. 0.730693    13. 0.001139  || 0.731832 
******** 14. 0.714395    15. 0.000230  || 0.714624 
******** 16. 0.725350    17. 0.000726  || 0.726075 
ERROR: ADC convertion failed for ELMB channel 18 [I AN_NEG dtmroc 4 regulator 1]
ERROR: curr not in range ON (1)  (45.660 A): ELMB channel 18 [I AN_NEG dtmroc 4]
ERROR: curr too high REG OFF (100.005 A): ELMB channel 19 [I AN_NEG dtmroc 4]
******** 20. 0.729140    21. 0.011142  || 0.740282 
******** 22. 0.717144    23. 43.920519  || 44.637663 
ERROR: curr too high REG OFF (43.921 A): ELMB channel 23 [I AN_NEG dtmroc 6]
  RANGE: 0.55  -  0.75
******** 24. 0.615927    25. 0.014884  || 0.630812 
******** 26. 0.617363    27. 0.013191  || 0.630554 
******** 28. 0.619500    29. 0.010304  || 0.629804 
******** 30. 0.625744    31. 32.475406  || 33.101150 
ERROR: curr too high REG OFF (32.475 A): ELMB channel 31 [I DIGI dtmroc 12]
******** 32. 0.675401    33. 0.003473  || 0.678875 
******** 34. 0.622751    35. 0.003424  || 0.626174 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.732 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.734 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.735 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.728 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.742 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.740 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.746 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.756 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.568 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.516 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.583 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.522 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.581 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   0.659 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (0.659 V) in ELMB channel 49 [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.562 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.547 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   3.159 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.193 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.176 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.173 V   [V DIGI dtmroc 12]
ERROR: ADC convertion failed for ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   3.117 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.166 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.154 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 33.46
Channel 61, temperature 32.92
Channel 62, temperature 33.61
Channel 63, temperature 31.97

Set all regulators ON

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.003243    1. 0.757403  || 0.760646 
******** 2. 0.002727    3. 0.754719  || 0.757446 
******** 4. 0.002002    5. 0.752568  || 0.754570 
******** 6. 0.000000    7. 0.753787  || 0.753787 
******** 8. 0.001874    9. 0.756432  || 0.758305 
******** 10. 0.001824    11. 0.749692  || 0.751516 
  RANGE: 0.65  -  0.85
******** 12. 0.001262    13. 0.718432  || 0.719694 
******** 14. 0.000400    15. 0.713867  || 0.714267 
******** 16. 0.002144    17. 0.720941  || 0.723084 
ERROR: ADC convertion failed for ELMB channel 18 [I AN_NEG dtmroc 4 regulator 1]
ERROR: curr too high REG OFF (40.017 A): ELMB channel 18 [I AN_NEG dtmroc 4]
ERROR: curr not in range ON (2) (100.005 A): ELMB channel 19 [I AN_NEG dtmroc 4]
******** 20. 0.000349    21. 0.703762  || 0.704111 
******** 22. 0.002998    23. 47.158609  || 47.161607 
ERROR: curr not in range ON (2) (47.159 A): ELMB channel 23 [I AN_NEG dtmroc 6]
  RANGE: 0.55  -  0.75
******** 24. 0.006566    25. 0.613717  || 0.620282 
******** 26. 0.011332    27. 0.618508  || 0.629840 
******** 28. 0.006100    29. 0.618310  || 0.624410 
******** 30. 0.000113    31. 30.213993  || 30.214106 
ERROR: curr not in range ON (2) (30.214 A): ELMB channel 31 [I DIGI dtmroc 12]
******** 32. 0.046801    33. 0.634414  || 0.681215 
******** 34. 0.002830    35. 0.628157  || 0.630987 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.732 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.748 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.736 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.701 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.747 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.760 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.711 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.731 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.506 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.519 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.573 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.518 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.536 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   1.367 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (1.367 V) in ELMB channel 49 [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.528 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.523 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   3.168 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.193 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.169 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.181 V   [V DIGI dtmroc 12]
ERROR: ADC convertion failed for ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   3.148 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.145 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.143 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 33.56
Channel 61, temperature 33.07
Channel 62, temperature 33.79
Channel 63, temperature 32.12

Set all regulators ON

 ======================  TEST LOOP for 30 loops =============================

 Loop: 1
DAC Settings:

Analog Positive 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:  128 [80]    129 [81]              -1
Supply Channel 2:  128 [80]    129 [81]              -1
Supply Channel 3:  128 [80]    129 [81]              -1
Supply Channel 4:  128 [80]    127 [7F]              1
Supply Channel 5:  128 [80]    129 [81]              -1
Supply Channel 6:  128 [80]    127 [7F]              1

Analog Negative 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:   128 [80]     129 [81]             -1
Supply Channel 2:   128 [80]     127 [7F]             1
Supply Channel 3:   128 [80]     129 [81]             -1
Supply Channel 4:   128 [80]     129 [81]             -1
Supply Channel 5:   128 [80]     129 [81]             -1
Supply Channel 6:   128 [80]     129 [81]             -1

Digital 
                     DAC I1       DAC I2     Delta (DAC I1 - DAC I2)

Supply Channel 1:   128 [80]     129 [81]             -1
Supply Channel 2:   128 [80]     129 [81]             -1
Supply Channel 3:   128 [80]     129 [81]             -1
Supply Channel 4:   128 [80]     129 [81]             -1
Supply Channel 5:   128 [80]     129 [81]             -1
Supply Channel 6:   128 [80]     127 [7F]             1

Current channels
  RANGE: 0.65  -  0.85
******** 0. 0.317525    1. 0.443022  || 0.760548 
******** 2. 0.042328    3. 0.715015  || 0.757344 
******** 4. 0.267489    5. 0.487071  || 0.754561 
******** 6. 0.695800    7. 0.063893  || 0.759693 
******** 8. 0.169024    9. 0.588532  || 0.757556 
******** 10. 0.751929    11. 0.002174  || 0.754102 
  RANGE: 0.65  -  0.85
******** 12. 0.733801    13. 0.001127  || 0.734928 
******** 14. 0.287223    15. 0.430052  || 0.717274 
******** 16. 0.359156    17. 0.369911  || 0.729067 
******** 18. 0.347012    19. 44.169138  || 44.516151 
ERROR: not in range ON  (44.516 A): ELMB channels 18 19 [I AN_NEG dtmroc 4]
******** 20. 0.732155    21. 0.013044  || 0.745200 
******** 22. 0.717139    23. 38.375320  || 39.092459 
ERROR: not in range ON  (39.092 A): ELMB channels 22 23 [I AN_NEG dtmroc 6]
  RANGE: 0.55  -  0.75
******** 24. 0.124535    25. 0.482417  || 0.606952 
******** 26. 0.312131    27. 0.295017  || 0.607148 
******** 28. 0.387500    29. 0.224587  || 0.612087 
******** 30. 0.128208    31. 27.476490  || 27.604698 
ERROR: not in range ON  (27.605 A): ELMB channels 30 31 [I DIGI dtmroc 12]
******** 32. 0.046801    33. 0.634404  || 0.681205 
******** 34. 0.528148    35. 0.094184  || 0.622332 

Voltage channels:
  RANGE: 3.65  -  3.85
ELMB Channel 36:   3.739 V   [V AN_POS dtmroc 1]
ELMB Channel 37:   3.746 V   [V AN_POS dtmroc 2]
ELMB Channel 38:   3.742 V   [V AN_POS dtmroc 3]
ELMB Channel 39:   3.729 V   [V AN_POS dtmroc 4]
ELMB Channel 40:   3.751 V   [V AN_POS dtmroc 5]
ELMB Channel 41:   3.760 V   [V AN_POS dtmroc 5]
ELMB Channel 42:   3.747 V   [V AN_POS dtmroc 6]
ELMB Channel 43:   3.758 V   [V AN_POS dtmroc 6]
  RANGE: 3.50  -  3.70
ELMB Channel 44:   -3.572 V   [V AN_NEG dtmroc 1]
ELMB Channel 45:   -3.527 V   [V AN_NEG dtmroc 2]
ELMB Channel 46:   -3.593 V   [V AN_NEG dtmroc 3]
ELMB Channel 47:   -3.534 V   [V AN_NEG dtmroc 4]
ELMB Channel 48:   -3.585 V   [V AN_NEG dtmroc 5]
ELMB Channel 49:   1.272 V   [V AN_NEG dtmroc 5]
ERROR: voltage too LOW (1.272 V) in ELMB channel 49 [V AN_NEG dtmroc 5]
ELMB Channel 50:   -3.565 V   [V AN_NEG dtmroc 6]
ELMB Channel 51:   -3.549 V   [V AN_NEG dtmroc 6]
  RANGE: 3.10  -  3.30
ELMB Channel 52:   3.169 V   [V DIGI dtmroc 9]
ELMB Channel 53:   3.198 V   [V DIGI dtmroc 10]
ELMB Channel 54:   3.180 V   [V DIGI dtmroc 11]
ELMB Channel 55:   3.182 V   [V DIGI dtmroc 12]
ERROR: ADC convertion failed for ELMB channel 56 [V DIGI dtmroc 13]
ELMB Channel 57:   3.144 V   [V DIGI dtmroc 13]
ELMB Channel 58:   3.168 V   [V DIGI dtmroc 14]
ELMB Channel 59:   3.158 V   [V DIGI dtmroc 14]

Temperature channels:
Channel 60, temperature 34.88
Channel 61, temperature 34.65
Channel 62, temperature 35.28
Channel 63, temperature 33.55

 CARD 314: TEST is finished:  04.04.2011 09:42:17

 ERRORS  in loop 1, test stopped. 
 Total number of loops foreseen in test: 30
