#include <sys/stat.h>
#include <sys/types.h>
#include <math.h>
#include <time.h>
#include <curses.h>


// funtion definitions
// initialization
int initLVPP (int channel, int elmbNb, FILE *logFile, int *handle);
// general purpose
int DacBrdcCheckValue (int handle, int elmbAddress, int Field6, int elmbTimeOut, FILE *fp);
double temperature (int adcCount);
bool checkTemperatures (int handle, int elmbNb);
int readElmbADC (int handle, int elmbNb, int *adcValues, bool *adcConvStat, 
		 double CurrentFactor, double VoltageFactor, FILE *fp, FILE *log);
int  elmbDtmrocInit (int handle, FILE *fp,  int elmb, int elmbTimeOut, int portC);
bool  burnInCheck (int handle,int loops, int elmbNb, int portC, int elmbTimeOut, FILE *burnFile);
bool  burnInCheckB (int handle, int elmbNb, int portC, int elmbTimeOut, FILE *burnFile);
// coarse tests
int returnVoltageChannelWheelA (int currChan);
void checkInhibitsSingle (int handle, int elmbNb, int dtmroc, int portC, int whichInh, 
			  int state, int elmbTimeOut, FILE *logFile);

bool checkInhibitsBroadcast (int handle, int elmbNb, int portC,  
			     int state, int elmbTimeOut, FILE *logFile);
void checkInhibitsBroadcastWheelB (int handle, int elmbNb, int portC,  
				   int state, int elmbTimeOut, FILE *logFile);
void checkWrite_SPI (int handle, int elmbNb, int dtmroc, int whichDacs,int dac, int elmbTimeOut, FILE *logFile);
bool checkWriteBroadcast (int handle, int elmbNb, int whichDacs, int dac, int elmbTimeOut, FILE *logFile);
bool checkWriteBroadcastOneDtmroc (int handle, int elmbNb, int whichDacs, int dac, int elmbTimeOut, 
				   FILE *logFile, int dtmroc, int item);
void decodeDtmrocWheelA(int elmbChannel, int *dtmroc, int *regulator, char *kind);
bool checkAnalogReadWheelA (int handle, int elmbNb, int state, int elmbTimeOut, 
			    FILE *logFile, double *calibCoeff);
bool checkAnalogReadWheelB (int handle, int elmbNb, int state, int elmbTimeOut, FILE *logFile, double *calibCoeff);
// calibration
void calibCurrentWheelA (int handle, int elmbNb, int elmbTimeOut, 
			 FILE *logFile, FILE *calibFile);
void calibCurrentWheelB (int handle, int elmbNb, int state, int elmbTimeOut, FILE *logFile);
void readCalibration(FILE *calibFile, double *calibCoeff, double *calibCurrentOffset, int lines);
// current division
void currentDivision (int handle, int elmbNb, int elmbTimeOut, double *calibCoeff, int cardNb);
