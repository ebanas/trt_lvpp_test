echo "Starting CAN bus ports for SYSTEC"
sudo ip link set can4 type can bitrate 50000
sudo ifconfig can4 up
echo "Port can4 speed 50k -> started"
sudo ip link set can5 type can bitrate 50000
sudo ifconfig can5 up
echo "Port can5 speed 50k -> started"
