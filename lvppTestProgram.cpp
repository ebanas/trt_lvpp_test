//

#include <stdio.h>
#include <conio.h>
#include <math.h>

#include <sys/timeb.h>
#include <time.h>

#include <errno.h> 
#include <direct.h> 
#include <string.h> 
#include <iostream>

// gpib
// #define MULTI
#ifdef MULTI
#	include "pop_string.h"
#	include "GraemeGPIB.h"
#	include "decl-32.h"
#endif

#include "KlasaI.h"
#include "lvppCompleteTest.h"

double currentpedestal[36];

#ifdef MULTI
// global variables
const Addr4882_t address = 6 ; //the address of the multimeter (change to fit your configuration)
const Addr4882_t address_sw = 7;//7001 SWITCH


double read2000MUL (char *buffer, int count, Addr4882_t address, Addr4882_t address_sw)
{
	short result;
	int j;
	char tmp[16];	

	My_Send(address, "*RST");
	My_Send(address, "STATUS:PRESET");
	My_Send(address, "*CLS");
	My_Send(address, "STAT:MEAS:ENAB 512");
	My_Send(address, "FUNCTION \'VOLTAGE:DC\'");
	My_Send(address, "TRIG:COUNT INF; SOURCE IMM;"); 
	if((My_Send(address, "TRACE:FEED:CONTROL NEXT") & ERR) //fill up the buffer on trigger
		|| (My_Send(address, "INIT") & ERR)  //pull the trigger
	)
		gpib_error(3, "Problem sending measurement trigger");

	WaitSRQ(0, & result);
	if(ibsta & ERR || result == 0)
		gpib_error(4, "Wait for SRQ failed");
	
//	cout << "Asking for data ..." << endl;
//    cout << "Press  EXT TRIG on 2000 MULTIMETER " << endl;
	My_Send(address, "STAT:MEAS?"); //un-assert SRQ
	
  //  Receive(0, address, buffer, BUF_SIZE, STOPend); //read in and ignore
 //   Receive(0, address, buffer, 100, STOPend); //read in and ignore
      Receive(0, address, buffer, count, STOPend); //read in and ignore

    if(My_Send(address, "DATA:DATA?") & ERR) //ask for data
		gpib_error(5, "Problem asking for data");


 //   Receive(0, address, buffer, BUF_SIZE, STOPend); //read in measurements
      Receive(0, address, buffer, count, STOPend); //read in measurements
  
    if(ibsta & ERR) 
		gpib_error(6, "Problem receiving data");

	buffer[ibcnt] = '\0'; //null-terminate response
	for (j=0; j<16; j++)
		tmp[j] = buffer[j+16];

	return ( atof (tmp));

}


void initMulSwitch(Addr4882_t address, Addr4882_t address_sw) 
{
    cout << "Vout[V] from 2000 MULTIMETER " << endl; 
	
	
//	        FILE *fp90;
//			fp90 = fopen ("2000_Vout.txt","w");


    SendIFC(0); // Resets the GPIB bus to its initial state.
    if(ibsta & ERR) //check for error
		gpib_error(1, "Could not send IFC");
		
//	DevClear(0, address); //clear the device
 //   DevClear(0, address_sw); //clear the device
 //   cout << "...next 1" << endl;

	if    
		   ((My_Send(address, "*RST") & ERR) //returns to default conditions
		|| (My_Send(address, "STATUS:PRESET") & ERR) //clear status bits
		|| (My_Send(address, "*CLS") & ERR) //clears all event registers
		|| (My_Send(address, "STAT:MEAS:ENAB 512") & ERR) //enable measurement complete status bit
		|| (My_Send(address, "*SRE 1") & ERR)   //generate SRQ on measurement complete
			//set function to read DC voltage
		|| (My_Send(address, "FUNCTION \'VOLTAGE:DC\'") & ERR) 
			//clear the buffer, read w/o calculation, set buffer length to 20
//		|| (My_Send(address, "TRACE:CLEAR;FEED SENS;POINTS 5") & ERR)
		//set trigger to 20, sourced to timer with 1 sec interval
		|| (My_Send(address, "TRIG:COUNT INF; SOURCE IMM;") & ERR)  //tryger: IMMediate
			//an error occured in one of the Send() calls  
			)
		gpib_error(2, "Problem sending measurement setup commands");


        if 
			
		  
        ((My_Send(address_sw, "*RST") & ERR) //send a reset// error -211
		||   (My_Send(address_sw, "STATUS:PRESET") & ERR) //clear status bits
  	    || (My_Send(address_sw, "SYST:PRES") & ERR) //clear status bits
        || (My_Send(address_sw, "CLOSE(@1!1)") & ERR)
		|| (My_Send(address_sw, "OPEN(@1!1)") & ERR)
		|| (My_Send(address_sw, "*CLS") & ERR) //send a clear
		|| (My_Send(address_sw, "STAT:MEAS:ENAB 512") & ERR) //enable measurement complete status bit
		|| (My_Send(address_sw, "*SRE 1") & ERR)   //generate SRQ on measurement complete
		//set function to read DC voltage
		|| (My_Send(address, "FUNCTION \'VOLTAGE:DC\'") & ERR) 
	
		//clear the buffer, read w/o calculation, set buffer length to 20
		|| (My_Send(address, "TRACE:CLEAR;FEED SENS;POINTS 5") & ERR)
	
		//set trigger to 20, sourced to timer with 1 sec interval


			//an error occured in one of the Send() calls
        
			)
		gpib_error(2, "Problem sending measurement setup commands");


	
	//cout << "Taking measurements ..." << endl;


	/* Wait for SRQ.  We have set the measurement status bit
	so that the Keithley will assert the SRQ line when a measurement
	has been successfully completed.  So when the SRQ is asserted,
	we know the readings have been taken, and we can read them 
	out.  The WaitSRQ() function will return with zero result
	if the timeout expired instead of the SRQ asserting.
	So you need to set the timeout on the board to greater than
	our timer run, which is 20 seconds. */

}


#endif
// 
// returns dtmroc address 
//

int dtmrocAddress (int dac) 
{
		if (dac < 6) 
			return (dac+1);
		else 
			return (dac+3);
}



//
//	check, whether all DACs are set to the same value
//  if not -> write error information to a file
//

int DacBrdcCheckOur (int handle, int elmbAddress, int Field6, int elmbTimeOut, FILE *fp)
{
int status = 0;
int i;
int fieldCheck;
char text[80];
KlasaI zasilacz;
bool badCheckStatus = false;

	for (i=0; i<12; i++) {

		if ((status = zasilacz.Read_SPI (handle, elmbAddress, 
			dtmrocAddress(i), 2,elmbTimeOut, &fieldCheck)) < 0 ) {
			printf ("Read exit with error: %d\n", status);
			printf ("%s\n", zasilacz.GetErrorText ());
			return status;
		}
		if (fieldCheck != Field6) {
			sprintf (text, "dtmroc #%d dacs 01: R - 0x%X  W - 0x%X", 
							dtmrocAddress(i), fieldCheck, Field6);
			fprintf (fp, "%s\n", text);
			badCheckStatus = true;
		}
	}


	for (i=0; i<12; i++) {
		if ((status = zasilacz.Read_SPI (handle, elmbAddress, 
			dtmrocAddress(i), 3, elmbTimeOut, &fieldCheck)) < 0 ) {
			printf ("Read exit with error: %d\n", status);
			printf ("%s\n", zasilacz.GetErrorText ());
			return status;
		}
		if (fieldCheck != Field6) {
			sprintf (text, "dtmroc #%d dacs 23: R - 0x%X W - 0x%X", 
							dtmrocAddress(i), fieldCheck, Field6);

			fprintf (fp, "%s\n", text);
			badCheckStatus = true;
		}
	}
  
	if (badCheckStatus)
		status = kl_DTMROC_Write_Brdc_BadSet;
	else 
		status = kl_OK;

	return status;
}


//
// double temperature (int adcCount)
//
// calculates temperature in Celsius degrees from adc readout (in mikroVolts)
//

double temperature (int adcCount) 
{
double Vref = 2.5;

double C1 = -0.1357;	//-0.1455;
double C2 =	5.4856;		// 6.0525;
double C3 = -89.55;		// -100.45;
double C4 = 490.21;		//550.21;
double Rref_S1 = 100000;
double Toff_S1 = 0;
double Resistance_S1;
double Temperature_S1;

	Resistance_S1 = Rref_S1*adcCount/(1000000*(Vref-adcCount/1000000));
	Temperature_S1 = C1*log(Resistance_S1)*log(Resistance_S1)*log(Resistance_S1)+C2*log(Resistance_S1)*log(Resistance_S1)+C3*log(Resistance_S1)+C4-Toff_S1;

	return (Temperature_S1);
}

//
// check board temperature - if this is over limit -> switches off all regulators
//

bool checkTemperatures (int handle, int elmbNb)
{
int i;
int status;
int adcValues[64];
bool adcConvStat[64];
bool stopFlag = false;
KlasaI zasilacz;

	if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
		printf ("%s\n", zasilacz.GetErrorText ());
		return (true);
	}

	for (i=60; i<64; i++) {
//		printf ("%d\t %d \t %f\n", i, adcConvStat[i],temperature (adcValues[i]));
		printf ("%f ", temperature (adcValues[i]));
		if (adcConvStat[i] == true ) stopFlag = true;
		if (temperature (adcValues[i]) > TEMPERATURE_LIMIT) {
			printf ("Temperature %d is too high: %f\n", i, temperature (adcValues[i]));
			stopFlag = true;
		}
	}
	printf ("\n");
	return stopFlag;
}

//
//	function reads ADCs and checks, whether voltages and current are of proper Level
//

int readElmbADC (int handle, int elmbNb, int *adcValues, bool *adcConvStat, 
				 double CurrentFactor, double VoltageFactor, FILE *fp, FILE *log)
{
int i;
int status;
KlasaI zasilacz;

    printf ("Read all ADCs:\n");
	if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (log, "%s\n", zasilacz.GetErrorText ());
		return (-1);
	}
	else {
//		g=0;
		for (i=0; i<36; i++) {
		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*CurrentFactor);
//			fprintf (fp, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*CurrentFactor);

            fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*CurrentFactor);
		}
		for (i=36; i<60; i++) {
		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*VoltageFactor);
//	fprintf (fp, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*VoltageFactor);
     fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor);       

		}
		for (i=60; i<64; i++) {
		//	printf ("%d. Conversion Status: %d \t %d\n", 
		//		i, adcConvStat[i], adcValues[i]);
//			fprintf (fp, "%d. Conversion Status: %d \t %d\n",i, adcConvStat[i], adcValues[i]);
          fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]);
		}
	}

	return (0);
}


//
// inicjalizaja CANbusu
//
int  canbusOnlyInit (int channel)
{
int handle;
int status;
KlasaI zasilacz;

// initialisation

//	printf ("CAN Bus init:\n");
	if ((status = zasilacz.Init_CANBus(channel, &handle)) < 0) {
		printf ("Init_CANBus exit with error: %d\n", status);
		printf ("%s\n", zasilacz.GetErrorText ());
//		fprintf (fp, "Init_CANBus exit with error: %d\n", status);
//		fprintf (fp, "%s\n", zasilacz.GetErrorText ());
		return (-1);
	}
	else {
//		fprintf (fp, "CANBUS init OK\n");
	}
	return (handle);
}

//
// inicjalizacja jednej karty
// all errors are store in the log file
//
int  elmbDtmrocInit (int handle, FILE *fp,  int elmb, int elmbTimeOut, int portC)
{
char dtmrocs[64];
int status;
KlasaI zasilacz;

// initialisation
 
 	if ((status = zasilacz.Init_ELMB(handle, elmb)) < 0) {
		printf ("Init_ELMB exit with error: %d\n", status);
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (fp, "Init_ELMB %d exit with error: %d\n", elmb, status);
		fprintf (fp, "%s\n", zasilacz.GetErrorText ());
		return -1;
	}
	else 
		fprintf (fp, "ELMB <%d> init OK\n", elmb);

	if ((status = zasilacz.Init_DTMROC(handle, elmb, elmbTimeOut, dtmrocs)) < 0) {
		printf ("Init_DTMROC exit with error: %d\n", status);
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (fp, "Init_DTMROC for elmb %d exit with error: %d\n", elmb, status);
		fprintf (fp, "%s\n", zasilacz.GetErrorText ());
		return -1;
	}
	else {
		fprintf (fp, "ELMB NB: %d Initializing of DTMROCS: %s\n", elmb, dtmrocs);
	}
	
	return (1);

}

// 
// rutynka sprawdzajaca karte w czasie burn-in -> jeden krok
//
//	zwracana wartosc: true - temperatura karty < limitu
//					  false - temperatura karty > limitu - nalezy zakonczyc test i wylaczyc natychmiast regulatory.
//

bool  burnInCheck (int handle,int loops, int elmbNb, int portC, int elmbTimeOut, FILE *burnFile)
{

int status;
KlasaI zasilacz;
int i;

int adcValues[64];	// array for ADC values (counts or microVolts)
bool adcConvStat[64];	// array for ADC convertion status (OK (0) or error (1))

double CurrMin;
double CurrMax;

double VoltMin;
double VoltMax;
int dacRead;
double CurrentFactor = CURRENT_FACTOR;
double VoltageFactor = VOLTAGE_FACTOR;
bool stopFlag = false;
int Inhibit;

 // struct __timeb64 timebuffer;
 //  char *timeline;
            
     for (Inhibit=1; Inhibit<4; Inhibit++){

//	if ((status = zasilacz.SetInhBrdc (handle, elmbNb, portC, 3, elmbTimeOut)) < 0) {
	if ((status = zasilacz.SetInhBrdc (handle, elmbNb, Inhibit, elmbTimeOut)) < 0) {
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (burnFile, "%s\n", zasilacz.GetErrorText ());
	}
	else {
			printf ("All regulators should be switched on.\n");
	}
	Sleep (1000);
//-----------------------------------------------------------------------------------------------

	if ((status = zasilacz.GetAllInhibitsWheelA (handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (burnFile, "%s\n", zasilacz.GetErrorText ());
	}
	printf ("Inhibit word: 0x%X\n", dacRead);
	if ((dacRead != 0x555555)&&(dacRead != 0xAAAAAA)&&(dacRead != 0xFFFFFF)) {
		fprintf (burnFile, "Inhibit error: read back value is 0x%X, should be: 0xFFFFFF\n",
			dacRead);
	}
			
//------------------------------------Write 0x5555 to DAC-----------------------------------------------------------
	if ((status = zasilacz.WriteDacBrdc(handle, elmbNb, 3, 0x5555, elmbTimeOut)) < 0) { 
			printf ("WriteDacBrdc exit with error: %d\n", status);
			fprintf (burnFile, "WriteDacBrdc exit with error: %d\n", status);
			printf ("%s\n", zasilacz.GetErrorText ());
			fprintf (burnFile, "%s\n", zasilacz.GetErrorText ());					
	}
	if ((status =  DacBrdcCheckOur (handle, elmbNb, 
					0x5555, elmbTimeOut, burnFile)) < 0) 
	{ 
		printf ("DacBrdcCheckOur exit with error: %d\n", status);
        fprintf (burnFile, "DacBrdcCheckOur exit with error: %d\n", status);
	} 

             printf ("All DAC are set = 0x5555\n"); 
//			 Sleep(10000);
//------------------------------------Write 0xFFFF to DAC--------------------------------------------
    if ((status = zasilacz.WriteDacBrdc(handle, elmbNb, 3, 0xffff, elmbTimeOut)) < 0) { 
					printf ("WriteDacBrdc exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					fprintf (burnFile, "WriteDacBrdc exit with error: %d\n", status);
					fprintf (burnFile, "%s\n", zasilacz.GetErrorText ());
					
	}
	if ((status =  DacBrdcCheckOur (handle, elmbNb, 
					0xFFFF, elmbTimeOut, burnFile)) < 0) 
	{ 
		printf ("DacBrdcCheckOur exit with error: %d\n", status);
        fprintf (burnFile, "DacBrdcCheckOur exit with error: %d\n", status);
	} 

    printf ("All DAC are set = 0xFFFF\n");
//             Sleep(10000);
//-----------------------------------------------------------------------------------------------
//      #ifdef SOFTING
	if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (burnFile, "%s\n", zasilacz.GetErrorText ());
	}
	else {
//	 printf ("------------------------Current-------------------------\n");
		for (i=0; i<36; i++) {

		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*CurrentFactor);
//			fprintf (fp, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*CurrentFactor);
//            fprintf (burnFile, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*CurrentFactor);
//              printf ("%d\t %d \t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
            
           
		}
   
//------------------------------------------------------------------------------------------------------
		// check
        fprintf(burnFile,"...................................................................................\n");
		CurrMin = CURRENT_PLUS_MIN; CurrMax = CURRENT_PLUS_MAX;
		for (i=0; i<11;i+=2){	
			if (!adcConvStat[i] && !adcConvStat[i+1]) {
				if (((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)) < CurrMin) ||
					((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)) > CurrMax))
					{ printf ("Current Error in channel:%d\t %d\t %f\n",i,i+1,((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor))));
                   fprintf(burnFile,"<Positive> Current Reading Error: ELMB: %d\t Ch: %d %d \t I[A]: %2.3f\n",
					   elmbNb, i,i+1, fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor));
					}
					else
/*
              fprintf (fp80, " %d\t %f\t \n",i, 
			  adcValues[i]*CurrentFactor - (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000);

              printf ("%d\t %d\t %f\n", i, adcConvStat[i], 
			  adcValues[i]*CurrentFactor - (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000);

*/
                  printf (" ch(n)+ch(n+1)=%d\t %d\t %f\t %f\t %f\n",i,i+1,adcValues[i]*CurrentFactor,adcValues[i+1]*CurrentFactor,
			     (adcValues[i]*CurrentFactor)+ (adcValues[i+1]*CurrentFactor));
				/*
                 printf (" ch(n)+ch(n+1)=%d\t %d\t %f\t %f\t %f\n",i,i+1,adcValues[i]*CurrentFactor- (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000),adcValues[i+1]*CurrentFactor- (calibCoeff[i+1]*adcValues[returnVoltageChannelWheelA(i+1)]*11)/10000),
			     (adcValues[i]*CurrentFactor- (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000))+ (adcValues[i+1]*CurrentFactor- (calibCoeff[i+1]*adcValues[returnVoltageChannelWheelA(i+1)]*11)/10000)));
				 */
				if(loops==1){
                 fprintf (burnFile," ch(n)+ch(n+1)=%d\t %d\t %f\t %f\t %f\n",i,i+1,adcValues[i]*CurrentFactor,adcValues[i+1]*CurrentFactor,
					 (adcValues[i]*CurrentFactor)+ (adcValues[i+1]*CurrentFactor));}
//				printf (" ch(n)+ch(n+1)=%d\t %d\t %f\n",i,i+1,
//				(fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)));
 //                fprintf (burnFile," ch(n)+ch(n+1)=%d\t %d\t %f\n",i,i+1,
//				(fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)));
			}
			else {
				if (adcConvStat[i])
                Sleep(1000);
					fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
				if (adcConvStat[i+1])
					Sleep(1000);
					fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i+1);
			}
		}
  

//-----------------------------------------------------------------------------------------------------
        
		CurrMin = CURRENT_MINUS_MIN; CurrMax = CURRENT_MINUS_MAX;
		for (i=12; i<23;i+=2){		      
 			if (!adcConvStat[i] && !adcConvStat[i+1] ) {
               if (((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)) < CurrMin) ||
				 ((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)) > CurrMax))
				  { printf ("Current Error in channel:%d\t %d\t %f\n",i,i+1,((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor))));
                    fprintf(burnFile,"Current Reading Error: ELMB: %d\t Ch: %d  %d \t I[A]: %2.3f\n",
					   elmbNb, i,i+1, fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor));
				  }
				 else
                  if(loops==1){
                 fprintf (burnFile," ch(n)+ch(n+1)=%d\t %d\t %f\t %f\t %f\n",i,i+1,adcValues[i]*CurrentFactor,adcValues[i+1]*CurrentFactor,
					 (adcValues[i]*CurrentFactor)+ (adcValues[i+1]*CurrentFactor));}
                printf (" ch(n)+ch(n+1)=%d\t %d\t %f\t %f\t %f\n",i,i+1,adcValues[i]*CurrentFactor,adcValues[i+1]*CurrentFactor,
	   		     (adcValues[i]*CurrentFactor)+ (adcValues[i+1]*CurrentFactor));
       //          fprintf (burnFile," ch(n)+ch(n+1)=%d\t %d\t %f\n",i,i+1,
		//	    (fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)));
			}
			else {
				if (adcConvStat[i])Sleep(1000);
					fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
				if (adcConvStat[i+1])Sleep(1000);
					fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i+1);
			}
		}
   

//------------------------------------------------------------------------------------------------------
           
		CurrMin = CURRENT_DIG_MIN; CurrMax = CURRENT_DIG_MAX;
		for (i=24; i<35;i+=2){		      
 			if (!adcConvStat[i] && !adcConvStat[i+1]) {
               if (((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)) < CurrMin) ||
				 ((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)) > CurrMax))
				  { printf ("Current Error in channel:%d\t %d\t %f\n",i,i+1,((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor))));
			   fprintf(burnFile,"Current Reading Error: ELMB: %d\t Ch: %d %d \t I[A]: %2.3f\n",
					   elmbNb, i,i+1,(fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)) );
				  }
			   else {
                 if(loops==1){
                 fprintf (burnFile," ch(n)+ch(n+1)=%d\t %d\t %f\t %f\t %f\n",i,i+1,adcValues[i]*CurrentFactor,adcValues[i+1]*CurrentFactor,
					 (adcValues[i]*CurrentFactor)+ (adcValues[i+1]*CurrentFactor));}
                printf (" ch(n)+ch(n+1)=%d\t %d\t %f\t %f\t %f\n",i,i+1,adcValues[i]*CurrentFactor,adcValues[i+1]*CurrentFactor,
			     (adcValues[i]*CurrentFactor)+ (adcValues[i+1]*CurrentFactor));
 //               printf (" ch(n)+ch(n+1)=%d\t %d\t %f\n",i,i+1,
//			    (fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)));
 //              fprintf (burnFile," ch(n)+ch(n+1)=%d\t %d\t %f\n",i,i+1,
//			    (fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)));
			   }
			}
			else {
				if (adcConvStat[i])Sleep(1000);
					fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
				if (adcConvStat[i+1])Sleep(1000);
					fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i+1);
			}
		}
    

//------------------------------------------------------------------------------------------------------
		VoltMin = VOLTAGE_PLUS_MIN; VoltMax = VOLTAGE_PLUS_MAX;
	  for (i=36; i<44; i++) {
   //     fprintf (burnFile, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor); 
		printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
		if (!adcConvStat[i]) {
			if (VoltMin>(adcValues[i]*VoltageFactor)|| (adcValues[i]*VoltageFactor)>VoltMax) {
          //      fprintf (burnFile, "Loop nb: %d  %s",item++, timeline);
				printf ("Voltage Error in channel:%d\n",i);
				fprintf(burnFile,"Voltage Reading Error: E: %d\t Ch: %d \t V: %2.3f\n",
					elmbNb, i, adcValues[i]*VoltageFactor);
			}
		}
		else {
				fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
		} 
	  }
    

 	 VoltMin = VOLTAGE_MINUS_MIN; VoltMax = VOLTAGE_MINUS_MAX;
	  for (i=44; i<52; i++) {
  //      fprintf (burnFile, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor);
		printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
 //        fprintf (burnFile,"%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
		if (!adcConvStat[i]) {
			if (VoltMin>(adcValues[i]*VoltageFactor)|| (adcValues[i]*VoltageFactor)>VoltMax) {
           //     fprintf (burnFile, "Loop nb: %d  %s",item++, timeline);
				printf ("Voltage Error in channel:%d\n",i);
				fprintf(burnFile,"Voltage Reading Error: E: %d\t Ch: %d \t V: %2.3f\n",
							elmbNb, i, adcValues[i]*VoltageFactor);
			}
		}
		else {
			fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
		}
	  }
    

	  VoltMin = VOLTAGE_DIG_MIN; VoltMax = VOLTAGE_DIG_MAX;
	  for (i=52; i<60; i++) {
   //     fprintf (burnFile, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor);
		printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
 //       fprintf (burnFile,"%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
		if (!adcConvStat[i]) {
			if (VoltMin>(adcValues[i]*VoltageFactor)|| (adcValues[i]*VoltageFactor)>VoltMax) {
    //            fprintf (burnFile, "Loop nb: %d  %s",item++, timeline);
				printf ("Voltage Error in channel:%d\n",i);
				fprintf(burnFile,"Voltage Reading Error: E: %d\t Ch: %d \t V: %2.3f\n",
							elmbNb, i, adcValues[i]*VoltageFactor);
			}   
		}
		else {
			fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
		}
	  }
       

// temperatury !
	  printf ("\n");
	  for (i=60; i<64; i++) {
     //     fprintf (burnFile, "%d\t %d\t %f\n",i,adcConvStat[i], temperature (adcValues[i]));
           printf ("%d\t %d \t %f\n", i, adcConvStat[i],temperature (adcValues[i]));
		   if (temperature (adcValues[i]) > TEMPERATURE_LIMIT) stopFlag = true;
	  }
	  }
// temperatury

//   #endif

//-----------------------------------------------------------------------------------------------
// EB 2 marzec 07
//		if ((status = zasilacz.SetInhBrdc (handle, elmbNb, portC, 0, elmbTimeOut)) < 0) {
//			printf ("%s\n", zasilacz.GetErrorText ());
//			fprintf (burnFile, "%s\n", zasilacz.GetErrorText ());
//		}

//			Sleep (60000);//10 sec
        Sleep (1000);
//---------------------------------------------------------------------------------------------------------
		if ((status = zasilacz.GetAllInhibitsWheelA (handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
			printf ("%s\n", zasilacz.GetErrorText ());
		}
		printf ("Inhibit word: 0x%X\n", dacRead);
//		if (dacRead != 0) {
//			fprintf (burnFile, "Inhibit error: read back value is 0x%X, should be: 0\n",
//				dacRead);
//		}
			
//-----------------------------------------------------------------------------------------------
            printf ("--------------------------------------------------\n");
//-----------------------------------------------------------------------------------------------
	}

	return stopFlag;
}





//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
bool  burnInCheckB (int handle, int elmbNb, int portC, int elmbTimeOut, FILE *burnFile)
{
int status;
KlasaI zasilacz;
int i;


int adcValues[64];	// array for ADC values (counts or microVolts)
bool adcConvStat[64];	// array for ADC convertion status (OK (0) or error (1))

double CurrMin;
double CurrMax;

double VoltMin;
double VoltMax;
int dacRead;
double CurrentFactor = CURRENT_FACTOR;
double VoltageFactor = VOLTAGE_FACTOR;
bool stopFlag = false;

 // struct __timeb64 timebuffer;
 //  char *timeline;

	if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 3, elmbTimeOut)) < 0) {
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (burnFile, "%s\n", zasilacz.GetErrorText ());
	}
	else {
			printf ("All regulators should be switched on.\n");
	}
	Sleep (1000);
//-----------------------------------------------------------------------------------------------

	if ((status = zasilacz.GetAllInhibitsWheelA (handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (burnFile, "%s\n", zasilacz.GetErrorText ());
	}
	printf ("Inhibit word: 0x%X\n", dacRead);
	if (dacRead != 0xFFFFFF) {
		fprintf (burnFile, "Inhibit error: read back value is 0x%X, should be: 0xFFFFFF\n",
			dacRead);
	}
			
//------------------------------------Write 0x5555 to DAC-----------------------------------------------------------
	if ((status = zasilacz.WriteDacBrdc(handle, elmbNb, 3, 0x5555, elmbTimeOut)) < 0) { 
			printf ("WriteDacBrdc exit with error: %d\n", status);
			fprintf (burnFile, "WriteDacBrdc exit with error: %d\n", status);
			printf ("%s\n", zasilacz.GetErrorText ());
			fprintf (burnFile, "%s\n", zasilacz.GetErrorText ());					
	}
	if ((status =  DacBrdcCheckOur (handle, elmbNb, 
					0x5555, elmbTimeOut, burnFile)) < 0) 
	{ 
		printf ("DacBrdcCheckOur exit with error: %d\n", status);
        fprintf (burnFile, "DacBrdcCheckOur exit with error: %d\n", status);
	} 

             printf ("All DAC are set = 0x5555\n"); 
//			 Sleep(10000);
//------------------------------------Write 0xFFFF to DAC--------------------------------------------
    if ((status = zasilacz.WriteDacBrdc(handle, elmbNb, 3, 0xffff, elmbTimeOut)) < 0) { 
					printf ("WriteDacBrdc exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					
	}
	if ((status =  DacBrdcCheckOur (handle, elmbNb, 
					0xFFFF, elmbTimeOut, burnFile)) < 0) 
	{ 
		printf ("DacBrdcCheckOur exit with error: %d\n", status);
        fprintf (burnFile, "DacBrdcCheckOur exit with error: %d\n", status);
	} 

    printf ("All DAC are set = 0xFFFF\n");
//             Sleep(10000);
//-----------------------------------------------------------------------------------------------

	if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (burnFile, "%s\n", zasilacz.GetErrorText ());
	}
	else {
//	 printf ("------------------------Current-------------------------\n");
		for (i=0; i<36; i++) {

		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*CurrentFactor);
//			fprintf (fp, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*CurrentFactor);
//            fprintf (burnFile, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*CurrentFactor);
              printf ("%d\t %d \t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
           
		}

//------------------------------------------------------------------------------------------------------

		// check
		CurrMin = CURRENT_PLUS_MIN; CurrMax = CURRENT_PLUS_MAX;
		for (i=0; i<7;i+=2){	
			if (!adcConvStat[i] && !adcConvStat[i+1]) {
				if (((fabs (adcValues[i]*CurrentFactor)) < CurrMin) ||
					((fabs (adcValues[i]*CurrentFactor)) > CurrMax))
					{ printf ("Current Error in channel:%d\t %f\n",i,((fabs (adcValues[i]*CurrentFactor))));
  //                 fprintf(burnFile,"Current Reading Error: E: %d\t Ch: %d \t V: %2.3f\n",
//					   elmbNb, i, adcValues[i]*CurrentFactor);
					}
					else
				printf (" ch(n)= %d\t %f\n",i,
				(fabs (adcValues[i]*CurrentFactor)));
			}
			else {
				if (adcConvStat[i])
Sleep(1000);
	//				fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
				if (adcConvStat[i+1])
					Sleep(1000);
	//				fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i+1);
			}
		}
//--------------------------------------------------------------------------------------------------
	CurrMin = CURRENT_PLUS_MIN; CurrMax = CURRENT_PLUS_MAX;
		for (i=8; i<12;i+=1){	
			if (!adcConvStat[i] && !adcConvStat[i+1]) {
				if ((fabs (adcValues[i]*CurrentFactor) < CurrMin) ||
					(fabs (adcValues[i]*CurrentFactor) > CurrMax))
					{ printf ("Current Error in channel:%d\t %f\n",i,(fabs (adcValues[i]*CurrentFactor)));
  //                 fprintf(burnFile,"Current Reading Error: E: %d\t Ch: %d \t V: %2.3f\n",
//					   elmbNb, i, adcValues[i]*CurrentFactor);
					}
					else
				printf (" ch(n)= %d\t %f\n",i,
				(fabs (adcValues[i]*CurrentFactor)));
			}
			else {
				if (adcConvStat[i])
Sleep(1000);
	//				fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
				if (adcConvStat[i+1])
					Sleep(1000);
	//				fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i+1);
			}
		}
		

//------------------------------------------------------------------------------------------------------
		CurrMin = CURRENT_MINUS_MIN; CurrMax = CURRENT_MINUS_MAX;
		for (i=12; i<19;i+=2){		      
 			if (!adcConvStat[i] && !adcConvStat[i+1] ) {
               if (((fabs (adcValues[i]*CurrentFactor)) < CurrMin) ||
				   ((fabs (adcValues[i]*CurrentFactor)) > CurrMax))
				  { printf ("Current Error in channel: %d\t %f\n",i,((fabs (adcValues[i]*CurrentFactor))));
     //               fprintf(burnFile,"Current Reading Error: E: %d\t Ch: %d \t V: %2.3f\n",
	//				   elmbNb, i, adcValues[i]*CurrentFactor);
				  }
				 else
                printf (" ch(n)= %d\t %f\n",i,
			    (fabs (adcValues[i]*CurrentFactor)));
			}
			else {
				if (adcConvStat[i])Sleep(1000);
	//				fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
				if (adcConvStat[i+1])Sleep(1000);
	//				fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i+1);
			}
		}

//------------------------------------------------------------------------------------------------------
		CurrMin = CURRENT_MINUS_MIN; CurrMax = CURRENT_MINUS_MAX;
		for (i=20; i<24;i+=1){	
			if (!adcConvStat[i] && !adcConvStat[i+1]) {
				if ((fabs (adcValues[i]*CurrentFactor) < CurrMin) ||
					(fabs (adcValues[i]*CurrentFactor) > CurrMax))
					{ printf ("Current Error in channel:%d\t %f\n",i,(fabs (adcValues[i]*CurrentFactor)));
  //                 fprintf(burnFile,"Current Reading Error: E: %d\t Ch: %d \t V: %2.3f\n",
//					   elmbNb, i, adcValues[i]*CurrentFactor);
					}
					else
				printf (" ch(n)= %d\t %f\n",i,
				(fabs (adcValues[i]*CurrentFactor)));
			}
			else {
				if (adcConvStat[i])
Sleep(1000);
	//				fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
				if (adcConvStat[i+1])
					Sleep(1000);
	//				fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i+1);
			}
		}
//-------------------------------------------------------------------------------------------------------------
		CurrMin = CURRENT_DIG_MIN; CurrMax = CURRENT_DIG_MAX;
		for (i=24; i<31;i+=2){		      
 			if (!adcConvStat[i] && !adcConvStat[i+1]) {
               if (((fabs (adcValues[i]*CurrentFactor)) < CurrMin) ||
				   ((fabs (adcValues[i]*CurrentFactor)) > CurrMax))
				  { printf ("Current Error in channel: %d\t %f\n",i,((fabs (adcValues[i]*CurrentFactor))));
   //                fprintf(burnFile,"Current Reading Error: E: %d\t Ch: %d \t V: %2.3f\n",
	//				   elmbNb, i, adcValues[i]*CurrentFactor);
				  }
			   else {
                printf (" ch(n)= %d\t %f\n",i,
			    (fabs (adcValues[i]*CurrentFactor)));
			   }
			}
			else {
				if (adcConvStat[i])Sleep(1000);
//					fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
				if (adcConvStat[i+1])Sleep(1000);
//					fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i+1);
			}
		}
//--------------------------------------------------------------------------------------------------------
	
    CurrMin = CURRENT_DIG_MIN; CurrMax = CURRENT_DIG_MAX;
		for (i=32; i<36;i+=1){	
			if (!adcConvStat[i] && !adcConvStat[i+1]) {
				if ((fabs (adcValues[i]*CurrentFactor) < CurrMin) ||
					(fabs (adcValues[i]*CurrentFactor) > CurrMax))
					{ printf ("Current Error in channel:%d\t %f\n",i,(fabs (adcValues[i]*CurrentFactor)));
  //                 fprintf(burnFile,"Current Reading Error: E: %d\t Ch: %d \t V: %2.3f\n",
//					   elmbNb, i, adcValues[i]*CurrentFactor);
					}
					else
				printf (" ch(n)=%d\t %f\n",i,
				(fabs (adcValues[i]*CurrentFactor)));
			}
			else {
				if (adcConvStat[i])
Sleep(1000);
	//				fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
				if (adcConvStat[i+1])
					Sleep(1000);
	//				fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i+1);
			}
		}
//------------------------------------------------Voltage------------------------------------------------------
		VoltMin = VOLTAGE_PLUS_MIN; VoltMax = VOLTAGE_PLUS_MAX;
	  for (i=36; i<44; i++) {
 //       fprintf (burnFile, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor); 
		printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
		if (!adcConvStat[i]) {
			if (VoltMin>(adcValues[i]*VoltageFactor)|| (adcValues[i]*VoltageFactor)>VoltMax) {
          //      fprintf (burnFile, "Loop nb: %d  %s",item++, timeline);
				printf ("Voltage Error in channel:%d\n",i);
				fprintf(burnFile,"Voltage Reading Error: E: %d\t Ch: %d \t V: %2.3f\n",
							elmbNb, i, adcValues[i]*VoltageFactor);
			}
		}
		else {
				fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
		} 
	  }

 	 VoltMin = VOLTAGE_MINUS_MIN; VoltMax = VOLTAGE_MINUS_MAX;
	  for (i=44; i<52; i++) {
 //       fprintf (burnFile, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor);
		printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);     
		if (!adcConvStat[i]) {
			if (VoltMin>(adcValues[i]*VoltageFactor)|| (adcValues[i]*VoltageFactor)>VoltMax) {
           //     fprintf (burnFile, "Loop nb: %d  %s",item++, timeline);
				printf ("Voltage Error in channel:%d\n",i);
				fprintf(burnFile,"Voltage Reading Error: E: %d\t Ch: %d \t V: %2.3f\n",
							elmbNb, i, adcValues[i]*VoltageFactor);
			}
		}
		else {
			fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
		}
	  }

	  VoltMin = VOLTAGE_DIG_MIN; VoltMax = VOLTAGE_DIG_MAX;
	  for (i=52; i<60; i++) {
 //       fprintf (burnFile, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor);
		printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
		if (!adcConvStat[i]) {
			if (VoltMin>(adcValues[i]*VoltageFactor)|| (adcValues[i]*VoltageFactor)>VoltMax) {
//                fprintf (burnFile, "Loop nb: %d  %s",item++, timeline);
				printf ("Voltage Error in channel:%d\n",i);
				fprintf(burnFile,"Voltage Reading Error: E: %d\t Ch: %d \t V: %2.3f\n",
							elmbNb, i, adcValues[i]*VoltageFactor);
			}   
		}
		else {
			fprintf(burnFile,"Conversion Error ELMB: %d\t Chan: %d\n",elmbNb, i);
		}
	  }

// temperatury !
	  printf ("\n");
	  for (i=60; i<64; i++) {
 //         fprintf (burnFile, "%d\t %d\t %f\n",i,adcConvStat[i], temperature (adcValues[i]));
           printf ("%d\t %d \t %f\n", i, adcConvStat[i],temperature (adcValues[i]));
		   if (temperature (adcValues[i]) > TEMPERATURE_LIMIT) stopFlag = true;
	  }

	        
//-----------------------------------------------------------------------------------------------

		if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 0, elmbTimeOut)) < 0) {
			printf ("%s\n", zasilacz.GetErrorText ());
			fprintf (burnFile, "%s\n", zasilacz.GetErrorText ());
		}

//			Sleep (60000);//10 sec
        Sleep (1000);
//---------------------------------------------------------------------------------------------------------
		if ((status = zasilacz.GetAllInhibitsWheelA (handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
			printf ("%s\n", zasilacz.GetErrorText ());
		}
		printf ("Inhibit word: 0x%X\n", dacRead);
		if (dacRead != 0) {
			fprintf (burnFile, "Inhibit error: read back value is 0x%X, should be: 0\n",
				dacRead);
		}
			
//-----------------------------------------------------------------------------------------------
            printf ("--------------------------------------------------\n");
//-----------------------------------------------------------------------------------------------
	}
	return stopFlag;
}


//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
// functions for initial - COARSE - checks

// for a given current channel returns appropriate voltage channel for WheelA

int returnVoltageChannelWheelA (int currChan)
{
int voltChannelsWheelA[36]; 
// chodzi o kanaly pradowe - od 0 do 36

	voltChannelsWheelA[0]=36; voltChannelsWheelA[1]=36; voltChannelsWheelA[2] = 37; voltChannelsWheelA[3] = 37;
	voltChannelsWheelA[4]=38; voltChannelsWheelA[5]=38; voltChannelsWheelA[6] = 39; voltChannelsWheelA[7] = 39;
	voltChannelsWheelA[8]=40; voltChannelsWheelA[9]=41; voltChannelsWheelA[10] = 42; voltChannelsWheelA[11] = 43;

	voltChannelsWheelA[12]=44; voltChannelsWheelA[13]=44; voltChannelsWheelA[14] = 45; voltChannelsWheelA[15] = 45;
	voltChannelsWheelA[16]=46; voltChannelsWheelA[17]=46; voltChannelsWheelA[18] = 47; voltChannelsWheelA[19] = 47;
	voltChannelsWheelA[20]=48; voltChannelsWheelA[21]=49; voltChannelsWheelA[22] = 50; voltChannelsWheelA[23] = 51;

	voltChannelsWheelA[24]=52; voltChannelsWheelA[25]=52; voltChannelsWheelA[26] = 53; voltChannelsWheelA[27] = 53;
	voltChannelsWheelA[28]=54; voltChannelsWheelA[29]=54; voltChannelsWheelA[30] = 55; voltChannelsWheelA[31] = 55;
	voltChannelsWheelA[32]=56; voltChannelsWheelA[33]=57; voltChannelsWheelA[34] = 58; voltChannelsWheelA[35] = 59;

	return voltChannelsWheelA[currChan];
}


void checkInhibitsSingle (int handle, int elmbNb, int dtmroc, int portC, int whichInh, 
						  int state, int elmbTimeOut, FILE *logFile)
{
KlasaI zasilacz;
int status;
int dacRead;

	if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, dtmrocAddress(dtmroc), whichInh, state, elmbTimeOut)) < 0) {
		printf ("Set_INHIBIT exit with error: %d <DTMROC %d INH %d>\n", 
			              status, dtmrocAddress(dtmroc), whichInh-1);
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (logFile, "Set_INHIBIT exit with error: %d <DTMROC %d INH %d>\n", 
			              status, dtmrocAddress(dtmroc), whichInh-1);
		fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
	}
	else {
		if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmrocAddress(dtmroc), elmbTimeOut, &dacRead)) < 0) {
			printf ("Get_INHIBIT exit with error: %d <DTMROC %d INH %d>\n", 
			              status, dtmrocAddress(dtmroc), whichInh-1);
			printf ("%s\n", zasilacz.GetErrorText ());
			fprintf (logFile, "Get_INHIBIT exit with error: %d<DTMROC %d INH %d>\n", 
			              status, dtmrocAddress(dtmroc), whichInh-1);
			fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
		}
		else {
			if (state) { // ON
				if (whichInh == 1) 
					if ((dacRead & whichInh) != 1) {
						printf ("ERROR: DTMROC %d  inhibit %d state is: %d, should be %d\n", 
								dtmrocAddress(dtmroc), whichInh-1, dacRead&whichInh, 1);
						fprintf (logFile, "ERROR: DTMROC %d  inhibit %d state is: %d, should be %d\n", 
								dtmrocAddress(dtmroc), whichInh-1, dacRead&whichInh, 1);
					}
				if (whichInh == 2) 
					if ((dacRead & whichInh) != 2) {
						printf ("ERROR: DTMROC %d  inhibit %d state is: %d, should be %d\n", 
								dtmrocAddress(dtmroc), whichInh-1, dacRead&whichInh, 2);
						fprintf (logFile, "ERROR: DTMROC %d  inhibit %d state is: %d, should be %d\n", 
								dtmrocAddress(dtmroc), whichInh-1, dacRead&whichInh, 2);
					}
			}
			else {
				if ((dacRead & whichInh) != 0) {
					printf ("ERROR: DTMROC %d  inhibit %d state is: %d, should be %d\n", 
							dtmrocAddress(dtmroc), whichInh-1, dacRead&whichInh, 0);
					fprintf (logFile, "ERROR: DTMROC %d  inhibit %d state is: %d, should be %d\n", 
							dtmrocAddress(dtmroc), whichInh-1, dacRead&whichInh, 0);
				}
			}
		}
		
	}
}

bool checkInhibitsBroadcast (int handle, int elmbNb, int portC,  
						  int state, int elmbTimeOut, FILE *logFile)
{
KlasaI zasilacz;
int status;
int dacRead;
int offing;
bool errorFlag = false;

	if ((status = zasilacz.SetInhBrdc (handle, elmbNb, state, elmbTimeOut)) < 0) {
				printf ("GetAllInhibitsWheelA exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				if (logFile) fprintf (logFile, "GetAllInhibitsWheelA exit with error: %d \n", status);
				if (logFile) fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
				errorFlag = true;
	}

	// read and check all once more
	if ((status = zasilacz.GetAllInhibitsWheelA(handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
				printf ("GetAllInhibitsWheelA exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				if (logFile) fprintf (logFile, "GetAllInhibitsWheelA exit with error: %d \n", status);
				if (logFile) fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
				errorFlag = true;
	}
	if (state == 3) offing = 0xFFFFFF;
	if (state == 2) offing = 0xAAAAAA;
	if (state == 1) offing = 0x555555;
	if (state == 0) offing = 0;

	if (dacRead != offing) {
		if (offing) {
			printf ("ERROR: not all regulators are ON: 0x%X\n", dacRead);
			if (logFile) fprintf (logFile, "ERROR: not all regulators are ON: 0x%X\n", dacRead);
			errorFlag = true;
		}
		else {
			printf ("ERROR: not all regulators are OFF: 0x%X\n", dacRead);
			if (logFile) fprintf (logFile, "ERROR: not all regulators are ON: 0x%X\n", dacRead);
			errorFlag = true;
		}
	}
	return (errorFlag);
}

void checkInhibitsBroadcastWheelB (int handle, int elmbNb, int portC,  
						  int state, int elmbTimeOut, FILE *logFile)
{
KlasaI zasilacz;
int status;
int dacRead;
int offing;

	if ((status = zasilacz.SetInhBrdc (handle, elmbNb, state, elmbTimeOut)) < 0) {
				printf ("GetAllInhibitsWheelA exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "GetAllInhibitsWheelA exit with error: %d \n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
	}

	// read and check all once more
	if ((status = zasilacz.GetAllInhibitsWheelB(handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
				printf ("GetAllInhibitsWheelB exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "GetAllInhibitsWheelB exit with error: %d \n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
	}
	if (state == 3) offing = 0xFFFF;
	if (state == 0) offing = 0;

	if (dacRead != offing) {
		if (offing) {
			printf ("ERROR: not all regulators are ON: 0x%X\n", dacRead);
			fprintf (logFile, "ERROR: not all regulators are ON: 0x%X\n", dacRead);
		}
		else {
			printf ("ERROR: not all regulators are OFF: 0x%X\n", dacRead);
			fprintf (logFile, "ERROR: not all regulators are ON: 0x%X\n", dacRead);
		}
	}

}

void checkWrite_SPI (int handle, int elmbNb, int dtmroc, int whichDacs,int dac, int elmbTimeOut, FILE *logFile)
{
char which[8];
int dacRead;
KlasaI zasilacz;
int status;

	if (whichDacs == 2) sprintf (which, "01"); else sprintf (which, "23");
	if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmrocAddress(dtmroc), whichDacs, dac, elmbTimeOut)) < 0) {
		printf ("Write exit with error: %d\n", status);
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (logFile, "Write exit with error: %d <DTMROC %d DACs %s>\n", 
				status, dtmrocAddress(dtmroc), which);
		fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
	} 
	else {
	// read
	if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmrocAddress(dtmroc), whichDacs, elmbTimeOut, &dacRead)) < 0) {
		printf ("Read exit with error: %d\n", status);
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (logFile, "Read exit with error: %d  <DTMROC %d DACs %s>\n", 
				status, dtmrocAddress(dtmroc), which);
		fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
	}
	if (dac != dacRead) {
		printf ("Error in DTMROC %d DAC %s  write: 0x%04X  read: 0x%04X FIRST READ\n",
				dtmrocAddress(dtmroc), which, dac, dacRead);	

		fprintf (logFile, "Error in DTMROC %d DAC %s  write: 0x%04X  read: 0x%04X FIRST READ\n",
				dtmrocAddress(dtmroc), which, dac, dacRead);
		if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmrocAddress(dtmroc), whichDacs, elmbTimeOut, &dacRead)) < 0) {
			printf ("SECOND READ: Read exit with error: %d\n", status);
			printf ("%s\n", zasilacz.GetErrorText ());
			fprintf (logFile, "SECOND READ: Read exit with error: %d  <DTMROC %d DACs %s>\n", 
					status, dtmrocAddress(dtmroc), which);
			fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
		}
		if (dac != dacRead) {
			printf ("Error in DTMROC %d DAC %s  write: 0x%04X  read: 0x%04X SECOND READ\n",
					dtmrocAddress(dtmroc), which, dac, dacRead);	
			fprintf (logFile, "Error in DTMROC %d DAC %s  write: 0x%04X  read: 0x%04X SECOND READ\n",
					dtmrocAddress(dtmroc), which, dac, dacRead);
		}
		else {
			printf ("\tDTMROC %d DAC %s  SECOND READ OK\n",
					dtmrocAddress(dtmroc), which);	
			fprintf (logFile, "\tDTMROC %d DAC %s  SECOND READ OK\n",
					dtmrocAddress(dtmroc), which);
		}

	}		
	}
}

bool checkWriteBroadcast (int handle, int elmbNb, int whichDacs, int dac, int elmbTimeOut, FILE *logFile)
{
int dacRead;
KlasaI zasilacz;
int status;
int dtmroc;
char which[8];
bool errorFlag = false;

	if (whichDacs == 2) sprintf (which, "01"); else sprintf (which, "23");
	if ((status = zasilacz.WriteDacBrdc(handle, elmbNb, whichDacs-1, dac, elmbTimeOut)) < 0) {
		printf ("WriteDacBrdc exit with error: %d\n", status);
		printf ("%s\n", zasilacz.GetErrorText ());
		if (logFile) fprintf (logFile, "WriteDacBrdc exit with error: %d  DACs %s>\n", 
				status, which);
		if (logFile) fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
		errorFlag = true;
	}

	
	// read
	for (dtmroc = 0; dtmroc <12; dtmroc++) {
		if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmrocAddress(dtmroc), whichDacs, elmbTimeOut, &dacRead)) < 0) {
			printf ("Read exit with error: %d\n", status);
			printf ("%s\n", zasilacz.GetErrorText ());
			if (logFile) fprintf (logFile, "Read exit with error: %d  <DTMROC %d DACs %s>\n", 
					status, dtmrocAddress(dtmroc), which);
			if (logFile) fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			errorFlag = true;
		}
		if (dac != dacRead) {
			errorFlag = true;
			printf ("Error in DTMROC %d DAC %s  write: 0x%04X  read: 0x%04X FIRST READ\n",
					dtmrocAddress(dtmroc), which, dac, dacRead);	

			if (logFile) fprintf (logFile, "Error in DTMROC %d DAC %s  write: 0x%04X  read: 0x%04X FIRST READ\n",
					dtmrocAddress(dtmroc), which, dac, dacRead);
			if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmrocAddress(dtmroc), whichDacs, elmbTimeOut, &dacRead)) < 0) {
				printf ("SECOND READ: Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				if (logFile) fprintf (logFile, "SECOND READ: Read exit with error: %d  <DTMROC %d DACs %s>\n", 
						status, dtmrocAddress(dtmroc), which);
				if (logFile) fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			}
			if (dac != dacRead) {
				printf ("Error in DTMROC %d DAC %s  write: 0x%04X  read: 0x%04X SECOND READ\n",
						dtmrocAddress(dtmroc), which, dac, dacRead);	
				if (logFile) fprintf (logFile, "Error in DTMROC %d DAC %s  write: 0x%04X  read: 0x%04X SECOND READ\n",
						dtmrocAddress(dtmroc), which, dac, dacRead);
			}
			else {
				printf ("\tDTMROC %d DAC %s  SECOND READ OK\n",
						dtmrocAddress(dtmroc), which);	
				if (logFile) fprintf (logFile, "\tDTMROC %d DAC %s  SECOND READ OK\n",
						dtmrocAddress(dtmroc), which);
			}

		}
	}
	return (errorFlag);
}

bool checkWriteBroadcastOneDtmroc (int handle, int elmbNb, int whichDacs, int dac, int elmbTimeOut, 
								   FILE *logFile, int dtmroc, int item)
{
int dacRead;
KlasaI zasilacz;
int status;
int regulator;
char which[8];
bool errorFlag = false;

	if (whichDacs == 2) sprintf (which, "01"); else sprintf (which, "23");
	if ((status = zasilacz.WriteDacBrdc(handle, elmbNb, whichDacs-1, dac, elmbTimeOut)) < 0) {
		printf ("WriteDacBrdc exit with error: %d\n", status);
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (logFile, "WriteDacBrdc exit with error: %d  DACs %s>\n", 
				status, which);
		fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
		errorFlag = true;
	}
	
	// read
	if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmrocAddress(dtmroc), whichDacs, elmbTimeOut, &dacRead)) < 0) {
		printf ("Read exit with error: %d\n", status);
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (logFile, "Read exit with error: %d  <DTMROC %d DACs %s>\n", 
				status, dtmrocAddress(dtmroc), which);
		fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
		errorFlag = true;
	}
	if (dac != dacRead) {
		errorFlag = true;
		printf ("Loop %d Error in DTMROC %d DAC %s  write: 0x%04X  read: 0x%04X FIRST READ\n", item,
				dtmrocAddress(dtmroc), which, dac, dacRead);	

		fprintf (logFile, "Loop %d Error in DTMROC %d DAC %s  write: 0x%04X  read: 0x%04X FIRST READ\n", item,
				dtmrocAddress(dtmroc), which, dac, dacRead);
		if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmrocAddress(dtmroc), whichDacs, elmbTimeOut, &dacRead)) < 0) {
			printf ("SECOND READ: Read exit with error: %d\n", status);
			printf ("%s\n", zasilacz.GetErrorText ());
			fprintf (logFile, "SECOND READ: Read exit with error: %d  <DTMROC %d DACs %s>\n", 
					status, dtmrocAddress(dtmroc), which);
			fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
		}
		if (dac != dacRead) {
			printf ("Loop %d Error in DTMROC %d DAC %s  write: 0x%04X  read: 0x%04X SECOND READ\n", item, 
					dtmrocAddress(dtmroc), which, dac, dacRead);	
			fprintf (logFile, "Loop %d Error in DTMROC %d DAC %s  write: 0x%04X  read: 0x%04X SECOND READ\n", item,
					dtmrocAddress(dtmroc), which, dac, dacRead);
		}
		else {
			printf ("\tDTMROC %d DAC %s  SECOND READ OK\n",
					dtmrocAddress(dtmroc), which);	
			fprintf (logFile, "\tDTMROC %d DAC %s  SECOND READ OK\n",
					dtmrocAddress(dtmroc), which);
		}

	}

	return (errorFlag);
}

void decodeDtmrocWheelA(int elmbChannel, int *dtmroc, int *regulator, char *kind)
{
	switch (elmbChannel) {
		case 0: *dtmroc = 1; *regulator = 1; sprintf (kind, "I AN_POS"); break;
		case 1: *dtmroc = 1; *regulator = 2; sprintf (kind, "I AN_POS"); break;
		case 2: *dtmroc = 2; *regulator = 1; sprintf (kind, "I AN_POS"); break;
		case 3: *dtmroc = 2; *regulator = 2; sprintf (kind, "I AN_POS"); break;
		case 4: *dtmroc = 3; *regulator = 1; sprintf (kind, "I AN_POS"); break;
		case 5: *dtmroc = 3; *regulator = 2; sprintf (kind, "I AN_POS"); break;
		case 6: *dtmroc = 4; *regulator = 1; sprintf (kind, "I AN_POS"); break;
		case 7: *dtmroc = 4; *regulator = 2; sprintf (kind, "I AN_POS"); break;
		case 8: *dtmroc = 5; *regulator = 1; sprintf (kind, "I AN_POS"); break;
		case 9: *dtmroc = 5; *regulator = 2; sprintf (kind, "I AN_POS"); break;
		case 10: *dtmroc = 6; *regulator = 1; sprintf (kind, "I AN_POS"); break;
		case 11: *dtmroc = 6; *regulator = 2; sprintf (kind, "I AN_POS"); break;

		case 12: *dtmroc = 1; *regulator = 1; sprintf (kind, "I AN_NEG"); break;
		case 13: *dtmroc = 1; *regulator = 2; sprintf (kind, "I AN_NEG"); break;
		case 14: *dtmroc = 2; *regulator = 1; sprintf (kind, "I AN_NEG"); break;
		case 15: *dtmroc = 2; *regulator = 2; sprintf (kind, "I AN_NEG"); break;
		case 16: *dtmroc = 3; *regulator = 1; sprintf (kind, "I AN_NEG"); break;
		case 17: *dtmroc = 3; *regulator = 2; sprintf (kind, "I AN_NEG"); break;
		case 18: *dtmroc = 4; *regulator = 1; sprintf (kind, "I AN_NEG"); break;
		case 19: *dtmroc = 4; *regulator = 2; sprintf (kind, "I AN_NEG"); break;
		case 20: *dtmroc = 5; *regulator = 1; sprintf (kind, "I AN_NEG"); break;
		case 21: *dtmroc = 5; *regulator = 2; sprintf (kind, "I AN_NEG"); break;
		case 22: *dtmroc = 6; *regulator = 1; sprintf (kind, "I AN_NEG"); break;
		case 23: *dtmroc = 6; *regulator = 2; sprintf (kind, "I AN_NEG"); break;

		case 24: *dtmroc = 9; *regulator = 1; sprintf (kind, "I DIGI"); break;
		case 25: *dtmroc = 9; *regulator = 2; sprintf (kind, "I DIGI"); break;
		case 26: *dtmroc = 10; *regulator = 1; sprintf (kind, "I DIGI"); break;
		case 27: *dtmroc = 10; *regulator = 2; sprintf (kind, "I DIGI"); break;
		case 28: *dtmroc = 11; *regulator = 1; sprintf (kind, "I DIGI"); break;
		case 29: *dtmroc = 11; *regulator = 2; sprintf (kind, "I DIGI"); break;
		case 30: *dtmroc = 12; *regulator = 1; sprintf (kind, "I DIGI"); break;
		case 31: *dtmroc = 12; *regulator = 2; sprintf (kind, "I DIGI"); break;
		case 32: *dtmroc = 13; *regulator = 1; sprintf (kind, "I DIGI"); break;
		case 33: *dtmroc = 13; *regulator = 2; sprintf (kind, "I DIGI"); break;
		case 34: *dtmroc = 14; *regulator = 1; sprintf (kind, "I DIGI"); break;
		case 35: *dtmroc = 14; *regulator = 2; sprintf (kind, "I DIGI"); break;

		case 36: *dtmroc = 1; *regulator = 1; sprintf (kind, "V AN_POS"); break;
		case 37: *dtmroc = 2; *regulator = 1; sprintf (kind, "V AN_POS"); break;
		case 38: *dtmroc = 3; *regulator = 1; sprintf (kind, "V AN_POS"); break;
		case 39: *dtmroc = 4; *regulator = 1; sprintf (kind, "V AN_POS"); break;
		case 40: *dtmroc = 5; *regulator = 1; sprintf (kind, "V AN_POS"); break;
		case 41: *dtmroc = 5; *regulator = 2; sprintf (kind, "V AN_POS"); break;
		case 42: *dtmroc = 6; *regulator = 1; sprintf (kind, "V AN_POS"); break;
		case 43: *dtmroc = 6; *regulator = 2; sprintf (kind, "V AN_POS"); break;


		case 44: *dtmroc = 1; *regulator = 1; sprintf (kind, "V AN_NEG"); break;
		case 45: *dtmroc = 2; *regulator = 1; sprintf (kind, "V AN_NEG"); break;
		case 46: *dtmroc = 3; *regulator = 1; sprintf (kind, "V AN_NEG"); break;
		
		case 47: *dtmroc = 4; *regulator = 1; sprintf (kind, "V AN_NEG"); break;
		case 48: *dtmroc = 5; *regulator = 1; sprintf (kind, "V AN_NEG"); break;
		case 49: *dtmroc = 5; *regulator = 2; sprintf (kind, "V AN_NEG"); break;
		case 50: *dtmroc = 6; *regulator = 1; sprintf (kind, "V AN_NEG"); break;
		case 51: *dtmroc = 6; *regulator = 2; sprintf (kind, "V AN_NEG"); break;

		case 52: *dtmroc = 9; *regulator = 1; sprintf (kind, "V DIGI"); break;
		case 53: *dtmroc = 10; *regulator = 1; sprintf (kind, "V DIGI"); break;
		case 54: *dtmroc = 11; *regulator = 1; sprintf (kind, "V DIGI"); break;
		case 55: *dtmroc = 12; *regulator = 1; sprintf (kind, "V DIGI"); break;
		case 56: *dtmroc = 13; *regulator = 1; sprintf (kind, "V DIGI"); break;
		case 57: *dtmroc = 13; *regulator = 2; sprintf (kind, "V DIGI"); break;
		case 58: *dtmroc = 14; *regulator = 1; sprintf (kind, "V DIGI"); break;
		case 59: *dtmroc = 14; *regulator = 2; sprintf (kind, "V DIGI"); break;

	}
}






//   state of regulator:
//   0 - both off, 1 - first on, 2 - second on, 3 - both on
//

bool checkAnalogReadWheelA (int handle, int elmbNb, int state, int portC, int elmbTimeOut, 
							FILE *logFile, double *calibCoeff)
{
bool aTemperatureOK=true;
int status;
//int DACtable[18];
KlasaI zasilacz;
int i;
int dtmroc, regulator;
int adcValues[64];	// array for ADC values (counts or microVolts)
bool adcConvStat[64];	// array for ADC convertion status (OK (0) or error (1))
char kind[10];		// string for description of power channel
double CurrentFactor = CURRENT_FACTOR;		// 
double VoltageFactor = VOLTAGE_FACTOR;  // 
double calib_i, calib_ii;
double minRange, maxRange, offRange;

	if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
		printf ("ReadADC_uVolts exit with error: %d\n", status);
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (logFile, "ReadADC_uVolts exit with error: %d\n", status);
		fprintf (logFile, "%s\n", zasilacz.GetErrorText ());

	}
	else {
		//for (i=0; i<36; i++) printf ("%d. %f\n", i, fabs (adcValues[i] * CurrentFactor));
//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
		printf ("\n Current channels\n");
		fprintf (logFile, "\nCurrent channels\n");
/*
		minRange = CURRENT_COARSE_WHEELA_MIN_5_NOCAL; 
		maxRange = CURRENT_COARSE_WHEELA_MAX_5_NOCAL;
		offRange = CURRENT_COARSE_WHEELA_OFF;
*/
		offRange = CURRENT_COARSE_WHEELA_OFF;

//		for (i=0; i<18; i+=2) {
        for (i=0; i<36; i+=2) {
			decodeDtmrocWheelA(i, &dtmroc, &regulator, kind);
			if (i<12) {
				minRange = CURRENT_WHEELA_ANPOS_MIN_5; 
				maxRange = CURRENT_WHEELA_ANPOS_MAX_5;
			} 
			else if (i<24) {
				minRange = CURRENT_WHEELA_ANNEG_MIN_5; 
				maxRange = CURRENT_WHEELA_ANNEG_MAX_5;

			}
			else {
				minRange = CURRENT_WHEELA_DIGI_MIN_5; 
				maxRange = CURRENT_WHEELA_DIGI_MAX_5;
			}
			if ((i==0 || i==12 || i==24)) 
				printf ("  RANGE: %3.2f  -  %3.2f\n", minRange, maxRange);

			if (adcConvStat[i] | adcConvStat[i+1]) {
				printf ("ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d regulator %d]\n", 
					i, kind, dtmroc, regulator);
				fprintf (logFile, "ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d regulator %d]\n", 
					i, kind, dtmroc, regulator);
			}
			else {
				calib_i = (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000;
				calib_ii = (calibCoeff[i+1]*adcValues[returnVoltageChannelWheelA(i+1)]*11)/10000;
				printf ("***WA***** %d. %f    %d. %f  || %f \n", i, fabs (adcValues[i] * CurrentFactor-calib_i),
					                                    i+1, fabs (adcValues[i+1] * CurrentFactor-calib_ii),
														fabs (adcValues[i] * CurrentFactor-calib_i) +
														fabs (adcValues[i+1] * CurrentFactor-calib_ii));
//***********************zapisz na file
				fprintf (logFile,"******** %d. %f    %d. %f  || %f \n", i, fabs (adcValues[i] * CurrentFactor-calib_i),
					                                    i+1, fabs (adcValues[i+1] * CurrentFactor-calib_ii),
														fabs (adcValues[i] * CurrentFactor-calib_i) +
														fabs (adcValues[i+1] * CurrentFactor-calib_ii));
			}  /// plus nawias
//3333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
                 if (state == 3) {
//(adcValues[i]*CurrentFactor-currentpedestal[i]   ?????  czy to samo ?fabs (adcValues[i] * CurrentFactor - calib_i

					if (!(((fabs (adcValues[i] * CurrentFactor - calib_i)+fabs (adcValues[i+1] * CurrentFactor - calib_ii)) > minRange) &&
					((fabs (adcValues[i] * CurrentFactor - calib_i)+fabs (adcValues[i+1] * CurrentFactor - calib_ii)) < maxRange ))) {
						printf ("ERROR: curr not in range ON(3)  (%3.3f A): ELMB channels %d %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor - calib_i)+fabs (adcValues[i+1] * CurrentFactor - calib_ii),
								i, i+1, kind, dtmroc);
						fprintf (logFile, "ERROR: not in range ON  (%3.3f A): ELMB channels %d %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor - calib_i)+fabs (adcValues[i+1] * CurrentFactor - calib_ii),
								i, i+1, kind, dtmroc);
						aTemperatureOK = false;
					}
 //                   printf ("DACtable %d 0x%X\n", i, DACtable[i]);
				 } // if (state == 3)
//111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
				if (state == 1) {
					if (!((fabs (adcValues[i] * CurrentFactor - calib_i) > minRange) &&
					(fabs (adcValues[i] * CurrentFactor - calib_i) < maxRange ))) {
						printf ("ERROR: curr not in range ON(1)  (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor - calib_i), i, kind, dtmroc);
						fprintf (logFile, "ERROR: curr not in range ON (1)  (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor - calib_i), i, kind, dtmroc);
					}
					if ((fabs (adcValues[i+1] * CurrentFactor - calib_ii)) > offRange) {
						printf ("ERROR: curr too high REG OFF(1) (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i+1] * CurrentFactor - calib_ii), i+1, kind, dtmroc);
						fprintf (logFile, "ERROR: curr too high REG OFF (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i+1] * CurrentFactor - calib_ii), i+1, kind, dtmroc);
                           aTemperatureOK = false;
					}
				} // if (state == 1)
//2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222
				if (state == 2) {
					if ((fabs (adcValues[i] * CurrentFactor - calib_i)) > offRange) {
						printf ("ERROR: curr too high REG OFF2) (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor- calib_i), i, kind, dtmroc);
						fprintf (logFile, "ERROR: curr too high REG OFF (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor - calib_i), i, kind, dtmroc);
					}
					if ((fabs (adcValues[i+1] * CurrentFactor - calib_ii) < minRange) ||
					(fabs (adcValues[i+1] * CurrentFactor - calib_ii) > maxRange )) {
			//if ((fabs (adcValues[i+1] * CurrentFactor - calib_ii) > minRange) ||
			//		(fabs (adcValues[i+1] * CurrentFactor - calib_ii) < maxRange )) {

						printf ("ERROR: curr not in range ON (2)  (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i+1] * CurrentFactor - calib_ii), i+1, kind, dtmroc);
						fprintf (logFile, "ERROR: curr not in range ON (2) (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i+1] * CurrentFactor - calib_ii), i+1, kind, dtmroc);
                        aTemperatureOK = false;
					}
				} //if (state == 2)
//000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
				if (state == 0)  {
					if ((fabs (adcValues[i] * CurrentFactor)+fabs (adcValues[i+1] * CurrentFactor)) > offRange) {
						printf ("ERROR: curr too high REG OFF(0) (%3.3f A): ELMB channels %d %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor)+fabs (adcValues[i+1] * CurrentFactor),
								i, i+1, kind, dtmroc);
						fprintf (logFile, "ERROR: curr too high REG OFF (%3.3f A): ELMB channels %d %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor)+fabs (adcValues[i+1] * CurrentFactor),
								i, i+1, kind, dtmroc);
                                  aTemperatureOK = false;
					}
				}/// nawias..........  if (state == 0) 
				//if (adcValues[i]/adcValues[i+1] < 
		}  // for
//VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV

		printf ("\n Voltage channels\n");
		fprintf (logFile, "\nVoltage channels:\n");

		for (i=36; i<60; i++) {
			// print voltages
			if (i<44) {
				minRange = VOLTAGE_WHEELA_ANPOS_MIN_5; 
				maxRange = VOLTAGE_WHEELA_ANPOS_MAX_5;
			} 
			else if (i<52) {
				minRange = VOLTAGE_WHEELA_ANNEG_MIN_5; 
				maxRange = VOLTAGE_WHEELA_ANNEG_MAX_5;

			}
			else {
				minRange = VOLTAGE_WHEELA_DIGI_MIN_5; 
				maxRange = VOLTAGE_WHEELA_DIGI_MAX_5;
			}
			if ((i==36 || i==44 || i==52)) 
				printf ("  RANGE: %3.2f  -  %3.2f\n", minRange, maxRange);

			decodeDtmrocWheelA(i, &dtmroc, &regulator, kind);
			if (adcConvStat[i]) {
				printf ("ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d]\n", 
					i, kind, dtmroc);
				fprintf (logFile, "ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d]\n", 
					i, kind, dtmroc);
			}
			else {
				printf ("ELMB Channel %d:   %2.3f V   [%s dtmroc %d]\n",
								i, adcValues[i] * VoltageFactor, kind, dtmroc);
				fprintf (logFile, "ELMB Channel %d:   %2.3f V   [%s dtmroc %d]\n",
								i, adcValues[i] * VoltageFactor, kind, dtmroc);
//================================================================================================================
				if (state) {
					if ((fabs (adcValues[i] * VoltageFactor) < VOLTAGE_FINE_WHEELB_MIN_5_AnPos)||
					    (fabs (adcValues[i] * VoltageFactor) > VOLTAGE_FINE_WHEELB_MAX_5_AnPos)){
						printf ("ERROR:...Voltage too LOW  (%2.3f V) in  ELMB channel %d [%s dtmroc %d]\n",
								adcValues[i] * VoltageFactor, i, kind, dtmroc);
						fprintf (logFile, "ERROR: voltage too LOW (%2.3f V) in ELMB channel %d [%s dtmroc %d]\n",
							adcValues[i] * VoltageFactor, i, kind, dtmroc);
						    
                        aTemperatureOK = false;

					                                                                              }
				} // if dlugi

//===================================================================================================================
/*
				if (state) {
					if (fabs (adcValues[i] * VoltageFactor) < VOLTAGE_COARSE_ON) {
						printf ("ERROR: voltage too LOW (%2.3f V) in  ELMB channel %d [%s dtmroc %d]\n",
								adcValues[i] * VoltageFactor, i, kind, dtmroc);
						fprintf (logFile, "ERROR: voltage too LOW (%2.3f V) in ELMB channel %d [%s dtmroc %d]\n",
							adcValues[i] * VoltageFactor, i, kind, dtmroc);
					                                                             }
				          }
				else {
					if (fabs (adcValues[i] * VoltageFactor) > VOLTAGE_COARSE_OFF) {
						printf ("ERROR: voltage too HIGH (%2.3f V) in ELMB channel %d [%s dtmroc %d]\n",
								adcValues[i] * VoltageFactor, i, kind, dtmroc);
						fprintf (logFile, "ERROR: voltage too HIGH (%2.3f V) in ELMB channel %d [%s dtmroc %d]\n",
							adcValues[i] * VoltageFactor, i, kind, dtmroc);
					                                                               }
				}
*/
//========================================================================================================================
			} // if adc (else)
		} // for
//		printf ("DONE\n");
//		fprintf (logFile, "DONE\n");

		printf ("\nTemperature channels:\n");
		fprintf (logFile, "\nTemperature channels:\n");

		for (i=60; i<64; i++) {
			if (adcConvStat[i]) {
				printf ("ERROR: ADC convertion failed for channel %d\n", i);
				fprintf (logFile, "ERROR: ADC convertion failed for channel %d\n", i);
			}	
			else {
				printf ("Channel %d, temperature %2.2f\n", i, temperature(adcValues[i]));
				fprintf (logFile, "Channel %d, temperature %2.2f\n", i, temperature(adcValues[i]));
				if ((temperature(adcValues[i]) > TEMPERATURE_COARSE_MAX)
					|| (temperature (adcValues[i]) < TEMPERATURE_COARSE_MIN)) {
					printf ("ERROR: channel %d - temperature is: %3.2f C (valid range is from %2.2f C to %2.2f C) \n",
								i, temperature (adcValues[i]), TEMPERATURE_COARSE_MIN, TEMPERATURE_COARSE_MAX);
						fprintf (logFile, "ERROR: channel %d - temperature is: %3.2f C (valid range is from %2.2f C to %2.2f C) \n",
							i, temperature (adcValues[i]), TEMPERATURE_COARSE_MIN, TEMPERATURE_COARSE_MAX);
						aTemperatureOK=false;

                        printf ("aTemperatureOK=false \n");

				                                                               }
			}
		}
		

/*
// temperatury !
	  printf ("\n");
	  for (i=60; i<64; i++) {
     //     fprintf (burnFile, "%d\t %d\t %f\n",i,adcConvStat[i], temperature (adcValues[i]));
           printf ("%d\t %d \t %f\n", i, adcConvStat[i],temperature (adcValues[i]));
		   if (temperature (adcValues[i]) > TEMPERATURE_LIMIT) stopFlag = true;
	  }				
// temperatury
*/
		

	}
return aTemperatureOK;
}


bool checkAnalogReadWheelB (int handle, int elmbNb, int state, int portC, int elmbTimeOut, FILE *logFile, double *calibCoeff)
{
bool bTemperatureOK=true;
int status;
KlasaI zasilacz;
int i;
int dtmroc, regulator;
int adcValues[64];	// array for ADC values (counts or microVolts)
bool adcConvStat[64];	// array for ADC convertion status (OK (0) or error (1))
char kind[10];		// string for description of power channel
double CurrentFactor = CURRENT_FACTOR;		// 
double VoltageFactor = VOLTAGE_FACTOR;  // 
double calib_i;

	if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
		printf ("ReadADC_uVolts exit with error: %d\n", status);
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (logFile, "ReadADC_uVolts exit with error: %d\n", status);
		fprintf (logFile, "%s\n", zasilacz.GetErrorText ());

	}
/*
#define CURRENT_FINE_WHEELB_MIN_5_AnPos 0.70
#define CURRENT_FINE_WHEELB_MAX_5_AnPos 0.80

#define VOLTAGE_FINE_WHEELB_MIN_5_AnPos 3.55
#define VOLTAGE_FINE_WHEELB_MAX_5_AnPos 3.72
*/
	else {
		printf ("Current channels %f %f \n",CURRENT_FINE_WHEELB_MIN_5_AnPos,CURRENT_FINE_WHEELB_MAX_5_AnPos);
		fprintf (logFile, "Current channels\n");

		for (i=0; i<36; i++) {
			if ((i==1) || (i==3) || (i==5) || (i==7) ||
				(i==13) || (i==15) || (i==17) || (i==19) ||
				(i==25) || (i==27) || (i==29) || (i==31)) continue;

			decodeDtmrocWheelA(i, &dtmroc, &regulator, kind);
			calib_i = (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000;
			if (adcConvStat[i]) {
				printf ("ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d regulator %d]\n", 
					i, kind, dtmroc, regulator);
				fprintf (logFile, "ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d regulator %d]\n", 
					i, kind, dtmroc, regulator);
			}
			else {
//*******************************zapis na ekran
                if ((i==12) || (i==24)) printf("-------------------------------------\n");
				printf ("******** %d. %f  \n", i, fabs (adcValues[i] * CurrentFactor-calib_i));
//***********************zapisz na file
				fprintf (logFile,"******** %d. %f  \n", i, fabs (adcValues[i] * CurrentFactor-calib_i));
				if (state) {	// voltages on
					if (((fabs (adcValues[i] * CurrentFactor-calib_i)) < CURRENT_COARSE_WHEELB_MIN_5) ||
						((fabs (adcValues[i] * CurrentFactor-calib_i)) > CURRENT_COARSE_WHEELB_MAX_5)){
						printf ("ERROR:...Current not in range (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor-calib_i),
								i, kind, dtmroc);
						fprintf (logFile, "ERROR: current not in range (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor-calib_i),
								i, kind, dtmroc);
						
						        
 //                             bTemperatureOK = false;

						 
					}
					
				}
				else {	// voltages off
					if ((fabs (adcValues[i] * CurrentFactor)) > CURRENT_COARSE_WHEELB_OFF) {
						printf ("ERROR: current too high (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor),
								i, kind, dtmroc);
						fprintf (logFile, "ERROR: current not in range (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor),
								i, kind, dtmroc);
					}
				}
				//if (adcValues[i]/adcValues[i+1] < 
			}
		}
/*
//**********************************************Analog Positive- for testing ************************************
		for (i=0; i<12; i++) {
			if ((i==1) || (i==3) || (i==5) || (i==7) ||
				(i==13) || (i==15) || (i==17) || (i==19) ||
				(i==25) || (i==27) || (i==29) || (i==31)) continue;

			decodeDtmrocWheelA(i, &dtmroc, &regulator, kind);
			calib_i = (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000;
			if (adcConvStat[i]) {
				printf ("ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d regulator %d]\n", 
					i, kind, dtmroc, regulator);
				fprintf (logFile, "ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d regulator %d]\n", 
					i, kind, dtmroc, regulator);
			}
			else {
//*********************** na ekran
//				printf ("******** %d. %f  \n", i, fabs (adcValues[i] * CurrentFactor-calib_i));
//***********************zapisz na file
				fprintf (logFile,"******** %d. %f  \n", i, fabs (adcValues[i] * CurrentFactor-calib_i));
				if (state) {	// voltages on
					if (((fabs (adcValues[i] * CurrentFactor-calib_i)) < CURRENT_FINE_WHEELB_MIN_5_AnPos) ||
						((fabs (adcValues[i] * CurrentFactor-calib_i)) > CURRENT_FINE_WHEELB_MAX_5_AnPos)){
						printf ("ERROR: Current not in range (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor-calib_i),
								i, kind, dtmroc);
						fprintf (logFile, "ERROR: current not in range (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor-calib_i),
						  		i, kind, dtmroc);
 //                            bTemperatureOK = false;

					}
				}
				else {	// voltages off
					if ((fabs (adcValues[i] * CurrentFactor)) > CURRENT_COARSE_WHEELB_OFF) {
						printf ("ERROR: current too high (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor),
								i, kind, dtmroc);
						fprintf (logFile, "ERROR: current not in range (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor),
								i, kind, dtmroc);
					}
				}
				//if (adcValues[i]/adcValues[i+1] < 
			}
		}
		
//************************************************END Analog Positive- for testing*********************
*/
//		printf ("DONE\n");
//		fprintf (logFile, "DONE\n");
/*
#define CURRENT_FINE_WHEELB_MIN_5_AnPos 0.70
#define CURRENT_FINE_WHEELB_MAX_5_AnPos 0.80

#define VOLTAGE_FINE_WHEELB_MIN_5_AnPos 3.55
#define VOLTAGE_FINE_WHEELB_MAX_5_AnPos 3.72
*/



		printf ("Voltage channels %f %f \n",VOLTAGE_FINE_WHEELB_MIN_5_AnPos ,VOLTAGE_FINE_WHEELB_MAX_5_AnPos);
		fprintf (logFile, "Voltage channels %f % f\n",VOLTAGE_FINE_WHEELB_MIN_5_AnPos ,VOLTAGE_FINE_WHEELB_MAX_5_AnPos);

		for (i=36; i<60; i++) {
			decodeDtmrocWheelA(i, &dtmroc, &regulator, kind);
			if (adcConvStat[i]) {
				printf ("ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d]\n", 
					i, kind, dtmroc);
				fprintf (logFile, "ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d]\n", 
					i, kind, dtmroc);
			}
			else {
				printf ("ELMB Channel %d:   %2.3f V   [%s dtmroc %d]\n",
								i, adcValues[i] * VoltageFactor, kind, dtmroc);
				fprintf (logFile, "ELMB Channel %d:   %2.3f V   [%s dtmroc %d]\n",
								i, adcValues[i] * VoltageFactor, kind, dtmroc);
				if (state) {
					if ((fabs (adcValues[i] * VoltageFactor) < VOLTAGE_FINE_WHEELB_MIN_5_AnPos)||
					    (fabs (adcValues[i] * VoltageFactor) > VOLTAGE_FINE_WHEELB_MAX_5_AnPos)){
//						printf ("ERROR:...Voltage too LOW  (%2.3f V) in  ELMB channel %d [%s dtmroc %d]\n",
//								adcValues[i] * VoltageFactor, i, kind, dtmroc);
                        printf ("ERROR: Voltage,current or temperature \n");   
						fprintf (logFile, "ERROR: voltage too LOW (%2.3f V) in ELMB channel %d [%s dtmroc %d]\n",
							adcValues[i] * VoltageFactor, i, kind, dtmroc);
						    
                        bTemperatureOK = false;

					}
				}
				
				else {
					if (fabs (adcValues[i] * VoltageFactor) > VOLTAGE_COARSE_OFF) {
						printf ("ERROR: voltage too HIGH (%2.3f V) in ELMB channel %d [%s dtmroc %d]\n",
								adcValues[i] * VoltageFactor, i, kind, dtmroc);
						fprintf (logFile, "ERROR: voltage too HIGH (%2.3f V) in ELMB channel %d [%s dtmroc %d]\n",
							adcValues[i] * VoltageFactor, i, kind, dtmroc);
					}
				}
			}
		}
//		printf ("DONE\n");
//		fprintf (logFile, "DONE\n");

		printf ("Temperature channels\n");
		for (i=60; i<64; i++) {
			if (adcConvStat[i]) {
				printf ("ERROR: ADC convertion failed for channel %d\n", i);
				fprintf (logFile, "ERROR: ADC convertion failed for channel %d\n", i);
			}	
			else {
				printf ("Channel %d, temperature %2.2f\n", i, temperature(adcValues[i]));
				fprintf (logFile, "Channel %d, temperature %2.2f\n", i, temperature(adcValues[i]));
				if ((temperature(adcValues[i]) > TEMPERATURE_COARSE_MAX)
					|| (temperature (adcValues[i]) < TEMPERATURE_COARSE_MIN)) {
					printf ("ERROR: channel %d - temperature is: %3.2f C (valid range is from %2.2f C to %2.2f C) \n",
								i, temperature (adcValues[i]), TEMPERATURE_COARSE_MIN, TEMPERATURE_COARSE_MAX);
						fprintf (logFile, "ERROR: channel %d - temperature is: %3.2f C (valid range is from %2.2f C to %2.2f C) \n",
								i, temperature (adcValues[i]), TEMPERATURE_COARSE_MIN, TEMPERATURE_COARSE_MAX);

						bTemperatureOK = false;
	
				}
			}
			
		}		
		fprintf (logFile, "Temperature channels\n");


//		for (i=0; i<36; i++) {
//			printf ("%d  %f\n", i, adcValues[i] * CurrentFactor);
//		}
//		printf ("DONE\n");
//		fprintf (logFile, "DONE\n");


	}
	return bTemperatureOK;

}

// end of funtions for COARSE check


//===========================================================================================//

// calibration
//===========================================================================================//


void calibCurrentWheelA (int handle, int elmbNb, int portC, int elmbTimeOut, 
						 FILE *logFile, FILE *calibFile)
{
int status;
KlasaI zasilacz;
int i;
int dtmroc, regulator;
int adcValues[64];	// array for ADC values (counts or microVolts)
bool adcConvStat[64];	// array for ADC convertion status (OK (0) or error (1))
bool errorFlag = false;
char kind[10];		// string for description of power channel
double CurrentFactor = CURRENT_FACTOR;		// 
double VoltageFactor = VOLTAGE_FACTOR;  // 
int step;
double calibDataStep[6][36];
double calibData[36];
double calibCurrentOffset[36];

	for (step = 0; step < 6; step++) {
		// set all dacs to 'step'
		errorFlag = checkWriteBroadcast (handle, elmbNb, 2, step*51, elmbTimeOut, logFile);
		errorFlag = checkWriteBroadcast (handle, elmbNb, 3, step*51, elmbTimeOut, logFile);
		if (errorFlag) {
			printf ("Errors in analog readout. Please check <logfile>, remove errors and restart procedure\n");
			break;
		}
		printf ("  STEP %d  ", step*51);
		if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
			printf ("ReadADC_uVolts exit with error: %d\n", status);
			printf ("%s\n", zasilacz.GetErrorText ());
			fprintf (logFile, "ReadADC_uVolts exit with error: %d\n", status);
			fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			errorFlag = true;
		}
		else {
			for (i=0; i<60; i++) {
				if (adcConvStat[i]) {
					printf ("ERROR: ADC convertion failed for ELMB channel %d \n", i);
					fprintf (logFile, "ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d regulator %d]\n", 
						i, kind, dtmroc, regulator);
					errorFlag = true;
				}
				if (errorFlag) break;
				
			}
			for (i=0; i<36; i++) {
				calibDataStep[step][i] = 
					(double) ((double)adcValues[i]/(VOLTAGE_FACTOR_CALIB*(double)adcValues[returnVoltageChannelWheelA(i)]));
//				printf ("STEP %d  kanal %d prad %d napiecie[%d] %d  calib %f\n",
//					     step, i, adcValues[i], returnVoltageChannelWheelA(i), adcValues[returnVoltageChannelWheelA(i)], calibDataStep[step][i]);
			}
                 
		}
	}
	printf ("\n");
	printf ("I offset for DAC value = 128 \n");
	errorFlag = checkWriteBroadcast (handle, elmbNb, 2, 0x8080, elmbTimeOut, logFile);
	errorFlag = checkWriteBroadcast (handle, elmbNb, 3, 0x8080, elmbTimeOut, logFile);
	if (errorFlag) {
		printf ("Errors in analog readout. Please check <logfile>, remove errors and restart procedure\n");
	}
	if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
		printf ("ReadADC_uVolts exit with error: %d\n", status);
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (logFile, "ReadADC_uVolts exit with error: %d\n", status);
		fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
		errorFlag = true;
	}
	for (i=0; i<36; i++) calibCurrentOffset[i] = adcValues[i] * CurrentFactor;
	// calculate and store to the file
	if (errorFlag) return;

	for (i=0; i< 36; i++) {
		calibData[i] = 0;
		for (step=0; step<6; step++) calibData[i] += calibDataStep[step][i];
		calibData[i] /= 6;
		fprintf (calibFile, "%d  , %f ,  %f\n", i, calibData[i], calibCurrentOffset[i]);
	}

	printf ("Calibration file stored\n");
	fprintf (logFile, "Calibration file stored\n");

	printf ("DAC set to = 0xFFFF \n");
	errorFlag = checkWriteBroadcast (handle, elmbNb, 2, 0xFFFF, elmbTimeOut, logFile);
	errorFlag = checkWriteBroadcast (handle, elmbNb, 3, 0xFFFF, elmbTimeOut, logFile);
	if (errorFlag) {
		printf ("Errors in analog readout. Please check <logfile>, remove errors and restart procedure\n");
	}

	return;
}

void calibCurrentWheelB (int handle, int elmbNb, int state, int portC, int elmbTimeOut, FILE *logFile)
{
int status;
KlasaI zasilacz;
int i;
int dtmroc, regulator;
int adcValues[64];	// array for ADC values (counts or microVolts)
bool adcConvStat[64];	// array for ADC convertion status (OK (0) or error (1))
char kind[10];		// string for description of power channel
double CurrentFactor = CURRENT_FACTOR;		// 
double VoltageFactor = VOLTAGE_FACTOR;  // 


	if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
		printf ("ReadADC_uVolts exit with error: %d\n", status);
		printf ("%s\n", zasilacz.GetErrorText ());
		fprintf (logFile, "ReadADC_uVolts exit with error: %d\n", status);
		fprintf (logFile, "%s\n", zasilacz.GetErrorText ());

	}
	else {
		printf ("Current channels\n");
		fprintf (logFile, "Current channels\n");
		for (i=0; i<36; i++) {
			if ((i==1) || (i==3) || (i==5) || (i==7) ||
				(i==13) || (i==15) || (i==17) || (i==19) ||
				(i==25) || (i==27) || (i==29) || (i==31)) continue;

			decodeDtmrocWheelA(i, &dtmroc, &regulator, kind);
			if (adcConvStat[i]) {
				printf ("ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d regulator %d]\n", 
					i, kind, dtmroc, regulator);
				fprintf (logFile, "ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d regulator %d]\n", 
					i, kind, dtmroc, regulator);
			}
			else {
				if (state) {	// voltages on
					if (((fabs (adcValues[i] * CurrentFactor)) < CURRENT_COARSE_WHEELB_MIN_5) ||
						((fabs (adcValues[i] * CurrentFactor)) > CURRENT_COARSE_WHEELB_MAX_5)){
						printf ("ERROR: current not in range (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor),
								i, kind, dtmroc);
						fprintf (logFile, "ERROR: current not in range (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor),
								i, kind, dtmroc);
					}
				}
				else {	// voltages off
					if ((fabs (adcValues[i] * CurrentFactor)) > CURRENT_COARSE_WHEELB_OFF) {
						printf ("ERROR: current too high (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor),
								i, kind, dtmroc);
						fprintf (logFile, "ERROR: current not in range (%3.3f A): ELMB channel %d [%s dtmroc %d]\n",
								fabs (adcValues[i] * CurrentFactor),
								i, kind, dtmroc);
					}
				}
				//if (adcValues[i]/adcValues[i+1] < 
			}
		}
//		printf ("DONE\n");
//		fprintf (logFile, "DONE\n");

		printf ("Voltage channels\n");
		fprintf (logFile, "Voltage channels\n");

		for (i=36; i<60; i++) {
			decodeDtmrocWheelA(i, &dtmroc, &regulator, kind);
			if (adcConvStat[i]) {
				printf ("ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d]\n", 
					i, kind, dtmroc);
				fprintf (logFile, "ERROR: ADC convertion failed for ELMB channel %d [%s dtmroc %d]\n", 
					i, kind, dtmroc);
			}
			else {
				if (state) {
					if (fabs (adcValues[i] * VoltageFactor) < VOLTAGE_COARSE_ON) {
						printf ("ERROR: voltage too LOW (%2.3f V) in  ELMB channel %d [%s dtmroc %d]\n",
								adcValues[i] * VoltageFactor, i, kind, dtmroc);
						fprintf (logFile, "ERROR: voltage too LOW (%2.3f V) in ELMB channel %d [%s dtmroc %d]\n",
							adcValues[i] * VoltageFactor, i, kind, dtmroc);
					}
				}
				else {
					if (fabs (adcValues[i] * VoltageFactor) > VOLTAGE_COARSE_OFF) {
						printf ("ERROR: voltage too HIGH (%2.3f V) in ELMB channel %d [%s dtmroc %d]\n",
								adcValues[i] * VoltageFactor, i, kind, dtmroc);
						fprintf (logFile, "ERROR: voltage too HIGH (%2.3f V) in ELMB channel %d [%s dtmroc %d]\n",
							adcValues[i] * VoltageFactor, i, kind, dtmroc);
					}
				}
			}
		}
//		printf ("DONE\n");
//		fprintf (logFile, "DONE\n");

		printf ("Temperature channels\n");
		fprintf (logFile, "Temperature channels\n");

		for (i=60; i<64; i++) {
			if (adcConvStat[i]) {
				printf ("ERROR: ADC convertion failed for channel %d\n", i);
				fprintf (logFile, "ERROR: ADC convertion failed for channel %d\n", i);
			}	
			else {
//				printf ("Channel %d, temperature %2.2f\n", i, temperature(adcValues[i]));
//				fprintf (logFile, "Channel %d, temperature %2.2f\n", i, temperature(adcValues[i]));
				if ((temperature(adcValues[i]) > TEMPERATURE_COARSE_MAX)
					|| (temperature (adcValues[i]) < TEMPERATURE_COARSE_MIN)) {
					printf ("ERROR: channel %d - temperature is: %3.2f C (valid range is from %2.2f C to %2.2f C) \n",
								i, temperature (adcValues[i]), TEMPERATURE_COARSE_MIN, TEMPERATURE_COARSE_MAX);
						fprintf (logFile, "ERROR: channel %d - temperature is: %3.2f C (valid range is from %2.2f C to %2.2f C) \n",
								i, temperature (adcValues[i]), TEMPERATURE_COARSE_MIN, TEMPERATURE_COARSE_MAX);

				}
			}
		}
//		for (i=0; i<36; i++) {
//			printf ("%d  %f\n", i, adcValues[i] * CurrentFactor);
//		}
//		printf ("DONE\n");
//		fprintf (logFile, "DONE\n");


	}
}

// read calibration file
void readCalibration(FILE *calibFile, double *calibCoeff, double *calibCurrentOffset, int lines)
{
int i;
int channel;
char line[256];
char comma;
float factor, offset;

	fgets (line, 256, calibFile);
	fgets (line, 256, calibFile);
	fgets (line, 256, calibFile);

	for (i=0; i<lines; i++) {
		fgets (line, 256, calibFile);
		sscanf (line, "%d %c %f %c %f ", &channel, &comma, &factor, &comma, &offset);
		calibCoeff[i] = factor; calibCurrentOffset[i] = offset;
	}


	


	return;




}

// endo of calibration
//==================================================================================================//


// Current division
void currentDivision (int handle, int elmbNb, int elmbTimeOut, double *calibCoeff, int cardNb) 
{
int status;
KlasaI zasilacz;
int whichDac=3, dtmroc=1;
int dacRead, dacWrite;
int adcValues[64];	// array for ADC values (counts or microVolts)
bool adcConvStat[64];	// array for ADC convertion status (OK (0) or error (1))
FILE *fpCurrentDivision, *fpDACs;
int i;
char textBuf[128];

	// create log file name
	sprintf (textBuf, "C:\\LVPP_TESTS\\Test Reports\\Test_90_91_92\\CurrentDiv_%03d.txt", cardNb);
	fpCurrentDivision = fopen (textBuf,"w");
	sprintf (textBuf, "C:\\LVPP_TESTS\\Test Reports\\Test_90_91_92\\DacSettings_%03d.txt", cardNb);
	fpDACs = fopen (textBuf,"w");
	fprintf (fpCurrentDivision, "Card Number: %d\n\n", cardNb);
	fprintf (fpDACs, "Card Number: %d\n\n", cardNb);

	if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
	}
	else {

				printf ("---------------------------------------------\n");
				fprintf (fpCurrentDivision, "----------------------------------------\n");
			}
//-----------------------------------------------------------------------------------------------------------

	for (i=0; i<36; i++) currentpedestal[i] = (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000;

//---------------------------------------Start current division if i(i)>i(i+1)
	for (i=0; i<36; i=i+2) {
		if (i==0)  (dtmroc=1);
		if (i==2)  (dtmroc=2);
		if (i==4)  (dtmroc=3);
		if (i==6)  (dtmroc=4);
		if (i==8)  (dtmroc=5);
		if (i==10) (dtmroc=6);
		if (i==12)  (dtmroc=1);
		if (i==14)  (dtmroc=2);
		if (i==16)  (dtmroc=3);
		if (i==18)  (dtmroc=4);
		if (i==20)  (dtmroc=5);
		if (i==22)  (dtmroc=6);
		if (i==24) (dtmroc=9);
		if (i==26) (dtmroc=10);
		if (i==28) (dtmroc=11);
		if (i==30) (dtmroc=12);
		if (i==32) (dtmroc=13);
		if (i==34) (dtmroc=14);

 	    if (i<24) whichDac = 3;
		else whichDac = 2;

		if ((adcValues[i]*CURRENT_FACTOR-currentpedestal[i]) > (adcValues[i+1]*CURRENT_FACTOR-currentpedestal[i+1])) {         

	        if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, whichDac, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				printf ("DAC %d: 0x%X (i>i+1)\n", whichDac, dacRead);
				fprintf (fpDACs, "ELMB Channel %d DAC %d: 0x%X (%d)(i>i+1)\n", i, whichDac, dacRead, dacRead);
			}

			if (i < 12) dacWrite = dacRead - 0x0001;			// An Pos
			else if (i < 24) dacWrite = dacRead - 0x0100;	// An Neg
			else dacWrite = dacRead - 0x0100;				// Digi

            
			if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmroc, whichDac, dacWrite, elmbTimeOut)) < 0) {
				printf ("Write exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			
		}

		if ((adcValues[i]*CURRENT_FACTOR-currentpedestal[i]) < (adcValues[i+1]*CURRENT_FACTOR-currentpedestal[i+1])) {         

			if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, whichDac, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				fprintf (fpDACs, "ELMB Channel %d DAC %d: 0x%X (%d)(i<i+1)\n", i, whichDac, dacRead);
				printf ("DAC %d: 0x%X (i<i+1)\n", whichDac, dacRead, dacRead);
			}


			if (i < 12) dacWrite = dacRead + 0x0001;
			else if (i < 24) dacWrite = dacRead + 0x0100;
			else dacWrite = dacRead + 0x0100;
            
			if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmroc, whichDac, dacWrite, elmbTimeOut)) < 0) {
				printf ("Write exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}

		}

		fprintf (fpCurrentDivision, " %d\t %f\t %f\t %f\t\n",i,
            (adcValues[i]*CURRENT_FACTOR-currentpedestal[i]),(adcValues[i+1]*CURRENT_FACTOR-currentpedestal[i+1]),
			(adcValues[i]*CURRENT_FACTOR-currentpedestal[i])+(adcValues[i+1]*CURRENT_FACTOR-currentpedestal[i+1]));
        printf ("%d\t %d\t %f\t %f\t %f\n", i, adcConvStat[i],
            (adcValues[i]*CURRENT_FACTOR-currentpedestal[i]),(adcValues[i+1]*CURRENT_FACTOR-currentpedestal[i+1]),
			(adcValues[i]*CURRENT_FACTOR-currentpedestal[i])+(adcValues[i+1]*CURRENT_FACTOR-currentpedestal[i+1]));
		
		} // koniec for

	    // read once again all DACs
	    if ((status = zasilacz.ReadAllDacsWheelA(handle, elmbNb, elmbTimeOut, adcValues)) < 0) {
				printf ("ReadAllDacsWheelA exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
	    }
		fprintf (fpDACs, "\nAnalog Positive \n");
		for (i=0; i<12; i+=2) {
			fprintf (fpDACs, "Voltage channel %d: DAC I1: %d DAC I2: %d \n",
				     (i+1)/2+1, adcValues[i]&0xFF, adcValues[i+1]&0xFF);
		}
		fprintf (fpDACs, "\nAnalog Negative \n");
		for (i=0; i<12; i+=2) {
			fprintf (fpDACs, "Voltage channel %d: DAC I1: %d DAC I2: %d \n",
				     (i+1)/2+1, (adcValues[i]>>8)&0xFF, (adcValues[i+1]>>8)&0xFF);
		}
		fprintf (fpDACs, "\nDigital \n");
		for (i=12; i<18; i++) {
			fprintf (fpDACs, "Voltage channel %d: DAC I1: %d DAC I2: %d \n",
				     (i+1)/2+1, adcValues[i]&0xFF, (adcValues[i]>>8)&0xFF);
		}
		fclose (fpCurrentDivision);
		fclose (fpDACs);

       printf ("\n");
}
                                          


int main(int argc, char* argv[])
{
int handle;	// handle - important - keeps handle number - global wide
int portC; // keeps content of port C - global wide
int cardNb;	// serial card number -IMPORTANT
	
int adcValues[64];	// array for ADC values (counts or microVolts)
bool adcConvStat[64];	// array for ADC convertion status (OK (0) or error (1))
double CurrentFactor = CURRENT_FACTOR;		// 
double VoltageFactor = VOLTAGE_FACTOR;  // 
/*
int DACtable[18];                   //WB/Barrel    ANeg APos    DTMROC
			DACtable[0] = 0x90A0;  //DAC1 DAC0     CH44 CH36         1
			DACtable[1] = 0x90A0;  //DAC3 DAC2
			DACtable[2] = 0x90A0;  //DAC1 DAC0     CH45 CH37         2
			DACtable[3] = 0x90A0;  //DAC3 DAC2
			DACtable[4] = 0x90A0;  //DAC1 DAC0     CH46 CH38         3
			DACtable[5] = 0x90A0;  //DAC3 DAC2
			DACtable[6] = 0x90A0;  //DAC1 DAC0     CH47 CH39         4
			DACtable[7] = 0x90A0;  //DAC3 DAC2

			DACtable[8] = 0x90A0;  //DAC1 DAC0     CH48 CH40         5
			DACtable[9] = 0x90A0;  //DAC3 DAC2     CH49 CH41
		    DACtable[10] = 0x90A0; //DAC1 DAC0     CH50 CH42         6 
		    DACtable[11] = 0x90A0; //DAC3 DAC2     CH51 CH43
//                                                Dig
			DACtable[12] = 0x2020; //DAC1 DAC0          CH52         9
			DACtable[13] = 0x2020; //DAC1 DAC0          CH53        10
			DACtable[14] = 0x2020; //DAC1 DAC0          CH54        11
			DACtable[15] = 0x2020; //DAC1 DAC0          CH55        12
			DACtable[16] = 0x2020; //DAC1 DAC0    CH57  CH56        13
			DACtable[17] = 0x2020; //DAC1 DAC0    CH59  CH58        14
*/

// needed for time read
struct _timeb timebuffer; 
struct tm *newtime; 
char timeline [100];
char timestart[100];
char timestop [100];

int valueRead;			// integer variable for data form LVPP
int dac;
int dacValue;//bk
char dtmrocs[64];	//?
int channel;

int dacRead;
int dtmrocStat;
int configReg;
int end17;
int clockNb;
int pc;
int i;

double CurrMin;
double CurrMax;

double VoltMin;
double VoltMax;

int end86;
FILE *burnFile;
FILE *calibFile;
FILE *fp80;

bool end;
bool bYes;
int status;
unsigned long queue;
canStatus stat;
unsigned long canStatFlags;
char errorMsg[100];

int item;

unsigned char buffer[8];
unsigned int dlen, flags;
unsigned long time;
unsigned char elmbSerialNumber[10];

long sdoID = 0x5Bf;

int dtmroc;
int regulator;
int dacNb;
int value;

int elmbTimeOut = 10000;
int elmbNb;

unsigned short emErr;
char textBuf[64];

int ocmValues[3];
int licznik;

// for GPIB
char gpibBuffer[128];
double keithleyBuffer[24];
double keithleyMeasur;

// for calibration
double calibCoeff[36], calibCurrentOffset[36];

KlasaI zasilacz;

// test program for Dawid LVPP class

portC = 0x0;

//Menu (5 March 2008)

printf ("\nSimple program for LVPP tests\n\n");
printf (" 10 - CAN Bus INIT\n");
printf (" 11 - ELMB INIT\n");
printf (" 12 - DTMROC INIT\n\n");

printf (" 2  - Hard Reset DTMROC\n");
printf (" 22 - set ELMB state \n");
printf (" 23 - read ELMB state \n");
printf (" 24 - Only Clocks\n");
printf (" 25 - PORT C setting\n");
printf (" 26 - PORT C read\n");
printf (" 27 - Read DTMROC Status \n\n");

printf (" 31  - Get Overrun Error \n");
printf (" 32 - Get Emergency Code \n");
printf (" 33 - Read KVASER RX queue \n");
printf (" 34 - Read KVASER TX queue \n");
printf (" 35 - Read CanStatus \n");
printf (" 36 - Flash Transmit Queue \n");
printf (" 37 - SDO Read Error \n\n");

printf (" 41 - Write to any DAC in any dtmroc (single)\n");
printf (" 42 - Write in a loop one value to dtmroc #11, dacs01 \n");
printf (" 43 - Writing Loop with Increment \n");
printf (" 44 - Broadcast writing loop \n");
printf (" 45 - Write a table to all DACs in a board\n\n");

printf (" 51 - Read from any DTMROC dacs 0 1 \n");
printf (" 52 - Read DTMROC #11, dacs 0 1 in a loop \n");
printf (" 53 - Read all dacs in a board to an array\n\n");

printf (" 61 - Set ON 1 regulator\n");
printf (" 62 - Set OFF 1 regulator \n");
printf (" 63 - Set all regulators ON one by one \n");
printf (" 64 - Set all regulators OFF one by one \n");
printf (" 65 - Set ON / OFF with broadcast\n");
printf (" 66 - Read all inhibits one by one - once\n");
printf (" 67 - Read all inhibits one by one in a loop\n");
printf (" 68 - Read all inhibits with one function\n\n");

printf (" 70 - Read all ADCs\n\n");

printf (" 99 - CAN Bus Close \n");
printf (" 0  - quit program\n\n");



end =  true;
//-----------------------------------------------------------------------------
int CanElmb;
printf (" Default CANbus=0  and ELMB ID=1  = 0  \n"); 
printf (" Set by User                      = 1  \n");
printf (" 0 or 1:");
scanf (" %d", &CanElmb);

// initialization of GPIB devices
//int addressMul, int addressSwitch
#ifdef MULTI
initMulSwitch(address, address_sw);   // pomiar napiecia 2000 MULTIMETER
#endif 

switch (CanElmb)  
					{
						case 1:
							if (CanElmb == 1) 
//								printf ("Inhibit bad set: dtmroc %d R %d W %d\n", dtmroc, dacRead, onOff);
                                printf ("Set CAN Port =  "); scanf ("%d", &channel);
                               printf ("Set ELMB Number =  "); scanf ("%d", &elmbNb); 
							break;
						case 0:
							if (CanElmb  == 0) 
								channel=1;
							    elmbNb=1;
                                printf ("CAN Port = 1; ELMB = 1");



							break;
						
						default:
							printf ("Bad:0 or 1 %d\n", CanElmb);
					}








//--------------------------------------------------------------------------------

//printf ("CAN channel (2 - Cracow, 0 - CERN - "); scanf ("%d", &channel);
//printf ("ELMB Nb - "); scanf ("%d", &elmbNb);

while (end) {
//	printf ("\n > Option - "); scanf ("%d", &item);
	printf ("\n > Chose Command / 103 - WA test / 104 - WB test / 13 - Menu /:  ");

	scanf ("%d", &item);

	switch (item) {
		case 0:
			end = false;
			break;
//------------------------------------------------------------------------------------------------
case 1:
			printf ("CANBus,ELMB,DTMROC initialization\n");
			if ((status = zasilacz.Init_CANBus(channel, &handle)) < 0) {
				printf ("Init_CANBus exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Initializing of CANBUS OK\n");

			if ((status = zasilacz.Init_ELMB(handle, elmbNb)) < 0) {
				printf ("Init_ELMB exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Initializing of Init_ELMB OK\n");

			if ((status = zasilacz.Init_DTMROC(handle, elmbNb, elmbTimeOut, dtmrocs)) < 0) {
				printf ("Init_DTMROC exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				printf ("Initializing of DTMROCS OK\n");
				printf ("DTMROC-i: %s\n", dtmrocs);
			}
			break;

//------------------------------------------------------------------------------------------------
		case 10:
			printf ("CAN Bus init\n");
			if ((status = zasilacz.Init_CANBus(channel, &handle)) < 0) {
				printf ("Init_CANBus exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Initializing of CANBUS OK\n");
			break;

		case 11:
			printf ("ELMB Init\n");
			if ((status = zasilacz.Init_ELMB(handle, elmbNb)) < 0) {
				printf ("Init_ELMB exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Initializing of Init_ELMB OK\n");
			break;

		case 12:
			printf ("DTMROC Init\n");
			if ((status = zasilacz.Init_DTMROC(handle, elmbNb, elmbTimeOut, dtmrocs)) < 0) {
				printf ("Init_DTMROC exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				printf ("Initializing of DTMROCS OK\n");
				printf ("DTMROC-i: %s\n", dtmrocs);
			}
			break;

		case 13:
printf ("\nSimple program for LVPP tests\n\n");
printf (" 10 - CAN Bus INIT\n");
printf (" 11 - ELMB INIT\n");
printf (" 12 - DTMROC INIT\n\n");

printf (" 2  - Hard Reset DTMROC\n");
printf (" 22 - set ELMB state \n");
printf (" 23 - read ELMB state \n");
printf (" 24 - Only Clocks\n");
printf (" 25 - PORT C setting\n");
printf (" 26 - PORT C read\n");
printf (" 27 - Read DTMROC Status \n\n");

printf (" 31  - Get Overrun Error \n");
printf (" 32 - Get Emergency Code \n");
printf (" 33 - Read KVASER RX queue \n");
printf (" 34 - Read KVASER TX queue \n");
printf (" 35 - Read CanStatus \n");
printf (" 36 - Flash Transmit Queue \n");
printf (" 37 - SDO Read Error \n\n");

printf (" 41 - Write to any DAC in any dtmroc (single)\n");
printf (" 42 - Write in a loop one value to dtmroc #11, dacs01 \n");
printf (" 43 - Writing Loop with Increment \n");
printf (" 44 - Broadcast writing loop \n");
printf (" 45 - Write a table to all DACs in a board\n\n");

printf (" 51 - Read from any DTMROC dacs 0 1 \n");
printf (" 52 - Read DTMROC #11, dacs 0 1 in a loop \n");
printf (" 53 - Read all dacs in a board to an array\n\n");

printf (" 61 - Set ON 1 regulator\n");
printf (" 62 - Set OFF 1 regulator \n");
printf (" 63 - Set all regulators ON one by one \n");
printf (" 64 - Set all regulators OFF one by one \n");
printf (" 65 - Set ON / OFF with broadcast\n");
printf (" 66 - Read all inhibits one by one - once\n");
printf (" 67 - Read all inhibits one by one in a loop\n");
printf (" 68 - Read all inhibits with one function\n\n");

printf (" 70 - Read all ADCs\n\n");

printf (" 99 - CAN Bus Close \n");
printf (" 0  - quit program\n\n");
			break;
		case 14:
			bYes = true;
			while (bYes) {
				if ((status = zasilacz.sendWriteSDO(handle, elmbNb, 0x6220, 0x12, 1,
						      1, 0x2f, elmbTimeOut)) < 0) {
								  printf ("sendWriteSDO exit with status: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());	
				}
				if ((status = zasilacz.sendWriteSDO(handle, elmbNb, 0x6220, 0x12, 0,
						      1, 0x2f, elmbTimeOut)) < 0) {
								  printf ("sendWriteSDO exit with status: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());	
				}
				if (kbhit ()) bYes = false;
			}
			break;
		case 2:
			printf ("Hard Reset DTMROC\n");
	    for (i=0; i<1; i++){
			status = zasilacz.Hardware_Reset_DTMROC(handle, elmbNb, elmbTimeOut);
			if (status < 0) {
				printf ("Hardware_Reset_DTMROC exit with status: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Hardware_Reset_DTMROC OK\n");

			   Sleep(1000);
		}
			break;

		case 21:
			printf ("Software Reset DTMROC\n");
				status = zasilacz.Software_Reset_DTMROC(handle, elmbNb, 10000);
				if (status < 0) {
					printf ("Software_Reset_DTMROC exit with status: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else 
					printf ("Software_Reset_DTMROC OK\n");

			break;
		case 1921:
			printf ("Software Reset DTMROC\n");			
			status = zasilacz.SPI_write (handle, elmbNb, 0x54, 7, elmbTimeOut);
				if (status < 0) {
					printf ("Software_Reset_DTMROC exit with status: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else 
					printf ("Software_Reset_DTMROC OK\n");

			break;

		case 1957:
			unsigned long sdoData;
			int bitsNumber;
			bYes = 1;
			printf ("Data: 0x"); scanf ("%x", &sdoData);
			printf ("Bits: "); scanf ("%d", &bitsNumber);
	//		while (bYes) {
				zasilacz.SPI_write (handle, elmbNb, sdoData, bitsNumber, elmbTimeOut);
	//			if (kbhit ()) bYes = 0;
	//		}
			break;

		case 22:
			printf ("Send a command to ELMB\n");
			printf ("ELMB command (1, 2, 128, 129, 130) - ");
			scanf ("%d", &value);

			if ((status = zasilacz.Set_ELMB_State (handle, elmbNb, value)) < 0) {
				printf ("Set_ELMB_State exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("ELMB set to: %d\n", value);
			break;

		case 23:
			printf ("Read ELMB state\n");
			if ((status = zasilacz.Read_ELMB_State (handle, elmbNb, &value)) < 0) {
				printf ("Read_ELMB_State exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("ELMB state is: %d\n", value);
			break;

		case 24:
			printf ("Send only clocks\n");
			printf ("Clocks - ");
			scanf ("%d", &clockNb);
			if ((status = zasilacz.BC (handle, clockNb, elmbNb, portC)) < 0) {
				printf ("BC exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("%d clocks sent\n", clockNb);
			break;

		case 1924:
			printf ("Send only clocks\n");
			printf ("Clocks - ");
			scanf ("%d", &clockNb);
			for (i=0; i<clockNb/32; i++) {
				if ((status = zasilacz.SPI_write (handle, elmbNb, 0, 32, elmbTimeOut)) < 0) {
					printf ("BC exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					break;
				}
			}
			if (clockNb%32) 
				if ((status = zasilacz.SPI_write (handle, elmbNb, 0, clockNb%32, elmbTimeOut)) < 0) {
					printf ("BC exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}

			break;

		case 25:
			printf ("Set port C\n");
			printf ("PORT C - ");
			scanf ("%x", &pc);
			portC = pc;
			if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Write_C exit OK - status: %d\n", status);
			break;

		case 26:
			int freqency;
			printf ("Read port C (variable which keeps port C value)\n");
			printf ("PORT C: 0x%02X", portC);
		/*	for(freqency=000; freqency<5000; freqency=freqency+1000)
			{
			Beep(freqency,100);
			} */
			break;

		case 27:
			printf (" Read DTMROC Status \n");
		    printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
			if (((dtmroc > 6) && (dtmroc < 9)) || (dtmroc > 14) || (dtmroc < 1)) {
				printf ("Bad dtmroc: %d. Try again. \n", dtmroc);
				break;
			} //int KlasaI::GetDtmrocState (int handle, int elmbAddress, int dtmrocAddress, int elmbTimeOut, int *state)

			if ((status = zasilacz.GetDtmrocState (handle, elmbNb, dtmroc, elmbTimeOut, &dtmrocStat)) < 0) { // status
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				end17 = 0;
			}
			printf ("DTMROC %d Status: 0x%X\n",dtmroc, dtmrocStat);
			break;

		case 1927:
			printf (" Read DTMROC Status \n");
		    printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
			if (((dtmroc > 6) && (dtmroc < 9)) || (dtmroc > 14) || (dtmroc < 1)) {
				printf ("Bad dtmroc: %d. Try again. \n", dtmroc);
				break;
			}
			if ((status = zasilacz.Read_SPI(handle, elmbNb, dtmroc, StatusRegister, elmbTimeOut, &dtmrocStat)) < 0) { // status
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			printf ("DTMROC %d Status: 0x%X\n",dtmroc, dtmrocStat);
			break;

               case 28:
			


          
	for (i=0; i<1000000; i++){

                if ((status = zasilacz.Write_C(handle, elmbNb, 0x20)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			    }
                if ((status = zasilacz.Write_C(handle, elmbNb, 0x00)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			    }
				/*
                if ((status = zasilacz.Write_C(handle, elmbNb, 0x20)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			    }
                if ((status = zasilacz.Write_C(handle, elmbNb, 0x00)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			    }
				Sleep(100);
				*/
	
	}

			break; 

		case 29:
			printf (" Read status of all DTMROCs \n");
			for (dtmroc = 0; dtmroc < 12; dtmroc++) {
				if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmrocAddress(dtmroc), 4, elmbTimeOut, &dtmrocStat)) < 0) { // status
					printf ("Read exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					end17 = 0;
				}
				if (dtmrocAddress(dtmroc) != (dtmrocStat & 0xF))
					printf ("DTMROC %d BAD Status: %d\n",dtmroc, dtmrocStat & 0xF);
				else
					printf ("DTMROC %d OK Status: %d\n",dtmroc, dtmrocStat & 0xF);
			}
			break;

		case 30:
			printf (" Read DTMROC Config Register \n");
		    printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
			if (((dtmroc > 6) && (dtmroc < 9)) || (dtmroc > 14) || (dtmroc < 1)) {
				printf ("Bad dtmroc: %d. Try again. \n", dtmroc);
				break;
			}
			if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, ConfigRegister, elmbTimeOut, &configReg)) < 0) { // status
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				end17 = 0;
			}
			printf ("DTMROC %d Config Reg: 0x%X\n",dtmroc, configReg);
			break;

		case 1930:
			printf (" Read DTMROC Config Register \n");
		    printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
			if (((dtmroc > 6) && (dtmroc < 9)) || (dtmroc > 14) || (dtmroc < 1)) {
				printf ("Bad dtmroc: %d. Try again. \n", dtmroc);
				break;
			}
			configReg = 0;
			if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, ConfigRegister, elmbTimeOut, &configReg)) < 0) { // status
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				end17 = 0;
			}
			printf ("DTMROC %d Config Reg: 0x%X\n",dtmroc, configReg);
			break;


		case 31: 
			printf ("Get Overrun Error \n");
			if (zasilacz.Get_ELMB_CANBufferOverrun (handle, elmbNb) == true) {
				printf (" Overun");
			}
			else
				printf ("Not Overrun"); 
			break;

		case 32: 
			printf (" 32 - Get Emergency Code \n");
			dac = 0;
			end17 = 1;
			while (end17) {
				if ((emErr = zasilacz.Get_ELMB_EmergencyError (handle, elmbNb)) != 0) {
					printf (" Emergency Error 0x%X\n", emErr);
				}
				Sleep (500);
				if (kbhit ()) end17 = 0;
			}
			break;

		case 33:
			printf (" Read KVASER RX queue \n");
			if ((stat  = canIoCtl(handle, canIOCTL_GET_RX_BUFFER_LEVEL, &queue, 4)) < 0) {
						canGetErrorText(stat, errorMsg, 100);
						printf ("canIoCtl exit with error %d: %s\n", stat, errorMsg);
			}
			else printf ("RX Queue size: 0x%X\n", queue); 
			break;

		case 34:
			printf (" Read KVASER TX queue \n");
			if ((stat  = canIoCtl(handle, canIOCTL_GET_TX_BUFFER_LEVEL, &queue, 4)) < 0) {
						canGetErrorText(stat, errorMsg, 100);
						printf ("canIoCtl exit with error %d: %s\n", stat, errorMsg);
			}
			else printf ("TX Queue size: 0x%X\n", queue); 
			break;

		case 35:
			printf (" Read CanStatus \n");
			if ((stat = canReadStatus(handle, &canStatFlags)) < 0) {
				canGetErrorText(stat, errorMsg, 100);
				printf ("canReadStatus exit with error %d: %s\n", stat, errorMsg);
			}
			else
				printf ("Can Status: 0x%X\n", canStatFlags); 
			break;

		case 36:
			printf (" Flash Transmit Queue \n");
			if ((stat = canFlushTransmitQueue(handle)) < 0) {
				canGetErrorText(stat, errorMsg, 100);
				printf ("canFlushTransmitQueue exit with error %d: %s\n", stat, errorMsg);
			}
			else
				printf ("canFlushTransmitQueue OK\n"); 
			break;

		case 37:
			printf (" 37 - SDO Read Error \n");
			buffer[0] = 0x4f; buffer[1] = 0x01; buffer[2] = 0x10; buffer[3] = 0x00;
			buffer[4] = 0x00; buffer[5] = 0x00; buffer[6] = 0x00; buffer[7] = 0x00;
	
			sdoID = 0x63f;
			if ((stat = canWrite (handle, sdoID, buffer, 8, 0))< 0) {
				canGetErrorText(stat, errorMsg, 100);
				printf ("canWrite exit with error %d: %s\n", stat, errorMsg);
			}

			if ((stat = canReadSyncSpecific (handle, 0x5bf, elmbTimeOut))< 0) {
				canGetErrorText(stat, errorMsg, 100);
				printf ("canReadSyncSpecific exit with error %d: %s\n", stat, errorMsg);
			}

			if ((stat = canReadSpecificSkip (handle, 0x5bf, buffer, &dlen, &flags, &time))< 0) {
				canGetErrorText(stat, errorMsg, 100);
				printf ("canReadSpecificSkip exit with error %d: %s\n", stat, errorMsg);
			}
			else 
				printf ("czytam: dlen - %d, error - 0x%X\n", dlen, buffer[4]);
			break;

		case 38:

			if ((status = zasilacz.Get_ELMB_SerialNumber (handle, elmbNb, elmbSerialNumber)) < 0) {
				printf ("Get_ELMB_SerialNumber exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			printf ("Serial number: %s\n", elmbSerialNumber);
				
/*			printf (" 38 - ELMB Serial Nb \n");
			buffer[0] = 0x43; buffer[1] = 0x00; buffer[2] = 0x31; buffer[3] = 0x00;
			buffer[4] = 0x00; buffer[5] = 0x00; buffer[6] = 0x00; buffer[7] = 0x00;
	
			sdoID = 0x600 | elmbNb; 
			if ((stat = canWrite (handle, sdoID, buffer, 8, 0))< 0) {
				canGetErrorText(stat, errorMsg, 100);
				printf ("canWrite exit with error %d: %s\n", stat, errorMsg);
			}

			sdoID = 0x580 | elmbNb;
			if ((stat = canReadSyncSpecific (handle, sdoID, elmbTimeOut))< 0) {
				canGetErrorText(stat, errorMsg, 100);
				printf ("canReadSyncSpecific exit with error %d: %s\n", stat, errorMsg);
			}

			if ((stat = canReadSpecificSkip (handle, sdoID, buffer, &dlen, &flags, &time))< 0) {
				canGetErrorText(stat, errorMsg, 100);
				printf ("canReadSpecificSkip exit with error %d: %s\n", stat, errorMsg);
			}
			else 
				printf ("%c%c%c%c", buffer[4], buffer[5], buffer[6], buffer[7]); */

			break;

case 40:
	dac=0x0000;
	for (i=0; i<10000000; i++){		 
//int KlasaI::Write_SPI(int handle, int elmbAddress, int dtmrocAddress, int object, int Field6, int portC, int elmbTimeOut)
//		    status = zasilacz.Write_SPI(handle, elmbNb, 63, 2, 0x5555, portC, elmbTimeOut);
            status = zasilacz.Write_SPI(handle, elmbNb, 63, 2, dac, elmbTimeOut);
            if (dac==0xFFFF) {dac=dac+1 & 0xFFFF;}
			dac=(dac+0x3333)& 0xFFFF;
            printf ("dac =: %d %x\n", dac, dac);
			Sleep(1000);
	}
			break;

/*
end17 = 1;
while (end17) {

			
             status = zasilacz.Write_SPI(handle, elmbNb, 63, 2, 0x5555, portC, elmbTimeOut);

            if (kbhit ()) end17 = 0;

			Sleep(1000);
}

           break;
*/

		case 41:
			printf ("Write to any DAC in any dtmroc (single)\n");
			printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
			if (((dtmroc > 6) && (dtmroc < 9)) || (dtmroc > 14) || (dtmroc < 1)) {
				printf ("Bad dtmroc: %d. Try again. \n", dtmroc);
				break;
			}
//        "object" = 1 -> Configuration Register
//		  "object" = 2 -> DAC 0 & DAC 1
//        "object" = 3 -> DAC 2 & DAC 3

			printf ("Dacs 2 - dacs 0,1; 3 dacs - 2,3)  - ");
			scanf ("%d", &dacNb);
			if ((dacNb != 2) && (dacNb != 3)) {
				printf ("Bad dacs: %d. Try again. \n", dacNb);
				break;
			}
			printf ("DAC  (hex) - ");
			scanf ("%x", &dac);
			if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmroc, dacNb, dac, elmbTimeOut)) < 0) {
				printf ("Write exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			break;

		case 1941:
			printf ("Write to any DAC in any dtmroc (SPI)\n");
			printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
			if (((dtmroc > 6) && (dtmroc < 9)) || (dtmroc > 14) || (dtmroc < 1)) {
				printf ("Bad dtmroc: %d. Try again. \n", dtmroc);
				break;
			}
//        "object" = 1 -> Configuration Register
//		  "object" = 2 -> DAC 0 & DAC 1
//        "object" = 3 -> DAC 2 & DAC 3

			printf ("Dacs 2 - dacs 0,1; 3 dacs - 2,3)  - ");
			scanf ("%d", &dacNb);
			if ((dacNb != 2) && (dacNb != 3)) {
				printf ("Bad dacs: %d. Try again. \n", dacNb);
				break;
			}
			printf ("DAC  (hex) - ");
			scanf ("%x", &dac);
			if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmroc, dacNb, dac, elmbTimeOut)) < 0) {
				printf ("Write exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			break;

		case 42:
			int loopnumber;
			FILE *fp;
            fp = fopen ("errorLog_42.txt", "w");
			printf ("Write in a loop one value to selected DTMROCs\n");
            fprintf (fp,"Write in a loop to selected DTMROCs\n");
			end17 = 1;
 //           printf ("dtmroc (hex) - ");
//			scanf ("%x", &dtmroc);

//			printf ("DAC 0&1 (hex) - ");
//			scanf ("%x", &dac);
//			loopnumber=0;
			dac=0;
//			do {
          //  for (dtmroc= 0; dtmroc<=3; dtmroc++)// od 1 do 4
          //    for (dtmroc= 3; dtmroc<=4; dtmroc++) //4 do 5
          // 6loop/min  60/10min 360/h 3600/10h
				for  (loopnumber=0; loopnumber<=4000; loopnumber++){
            for (dtmroc= 0; dtmroc<=11; dtmroc++) //1 do 14 
			  {

				dacRead = 0xabcd;   
                printf ("Loop Number - %d, dtmroc - %d,DAC - %x\n",loopnumber, dtmrocAddress(dtmroc),dac);
//				if ((status = zasilacz.Write(handle, elmbNb, 11, 2, dac, portC, elmbTimeOut)) < 0)
                if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmrocAddress(dtmroc), 2, dac, elmbTimeOut)) < 0)
               { 
//					fprintf (fp, "Write exit with error: %d\n", status);
                    fprintf (fp,"Loop Number - %d ",loopnumber);
					fprintf (fp, "%s", zasilacz.GetErrorText ());
					fprintf (fp, "dtmroc: %d DACs 01\n",dtmrocAddress(dtmroc));
					printf ("Write exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
                    printf ("dtmroc - %d DACs 01\n",dtmrocAddress(dtmroc));

//					end17 = 0;
				}
               if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmrocAddress(dtmroc), 3, dac, elmbTimeOut)) < 0)
               { 
//					fprintf (fp, "Write exit with error: %d\n", status);
                    fprintf (fp,"Loop Number - %d ",loopnumber);
					fprintf (fp, "%s", zasilacz.GetErrorText ());
					fprintf (fp, "dtmroc: %d DACs 23\n",dtmrocAddress(dtmroc));
					printf ("Write exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
                    printf ("dtmroc - %d DACs 23\n",dtmrocAddress(dtmroc));

//					end17 = 0;
				}
  //              printf ("DAC - %x\n",dac);
    //            printf ("Loop Number - %d, dtmroc - %d \n",loopnumber, dtmrocAddress(dtmroc));
			  
 	//			loopnumber=loopnumber + 1;
				dac=dac +1;
                dac = dac & 0xFFFF;
                Sleep (10000);
//				Sleep (371);
//                Sleep (10000);
                
	//			if (kbhit ()) end17 = 0;
	//		  }
			  
			  }
			   } 
	//		while (end17);
            printf ("See:errorLog_42.txt \n");
			fclose (fp);
//            printf ("See:errorLog_42.txt %d\n");
			break;

		case 43:
			printf (" 43 - Writing Loop: writes to dtmroc #10, dac01, with increment\n");
			end17 = 1;
			dac=0;
		//	printf ("DAC 0&1 (hex) - ");
		//	scanf ("%x", &value);
			clockNb = 0;
//			dac = 0;
		
			do {
/*
				if ((status = zasilacz.Write(handle, elmbNb, 10, 2, 0x0404 & 0xFFFF, portC, elmbTimeOut)) < 0) { 
					printf ("Write exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					end17 = 0;
				}
*/
               if ((status = zasilacz.Write_SPI(handle, elmbNb, 3, 2, (dac=dac+0x3333)&0xFFFF, elmbTimeOut)) < 0) { 
					printf ("Write exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					end17 = 0;
				}



				Sleep ((dac%500)*10); printf ("%d ", dac);
				clockNb++;
				if (kbhit ()) end17 = 0;
			} while (end17);
			printf ("Number of 'writes': %d \n", clockNb);
			break;

		case 44:
			printf ("Broadcast writing loop (increment of DAC values in every loop) \n");
			end17 = 1;
			printf ("DACs (01 -> 1, 23 -> 2, all -> 3) - ");
			scanf ("%d", &value);
			dac = 1;
			clockNb = 0;
			do {
				if ((status = zasilacz.WriteDacBrdc(handle, elmbNb, value, (dac)&0xFFFF, elmbTimeOut)) < 0) 
				{ 
					printf ("WriteDacBrdc exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					end17 = 0;
				} 
				Sleep(1000);
//	check, whether all DACs are set to the same value
//  if not -> write error information to a file

				if ((status = zasilacz.DacBrdcCheck(handle, elmbNb, value, (dac++)&0xFFFF, elmbTimeOut)) < 0)
				{ 
					printf ("DacBrdcCheck exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					end17 = 0;				
				}


                Sleep(1000);
				printf ("%d ", dac);
				clockNb++;
		 		if (kbhit ()) end17 = 0;
				Sleep ((1000*dac)%500);
			  }
			while (end17);
			printf ("Number of 'writes': %d\n", clockNb);
			break;

		case 45:
			printf ("Write a table to all dacs in a board\n");
//			printf ("Wheel A -> 1, Wheel B - 2) - ");
//			scanf ("%d", &value);
/*
			int buforek[18];
			buforek[0] = 0x0810; 
			buforek[1] = 0x1820;  
			buforek[2] = 0x2830;  
			buforek[3] = 0x3840; 
			buforek[4] = 0x4850; 
			buforek[5] = 0x5860;  
			buforek[6] = 0x6870;  
			buforek[7] = 0x7880;
			buforek[8] = 0x8890; 
			buforek[9] = 0x98a0;  
			buforek[10] = 0xb8b0;  
			buforek[11] = 0xc8c0;
			buforek[12] = 0xd8d0; 
			buforek[13] = 0xe8e0;  
			buforek[14] = 0xf8ff;  
			buforek[15] = 0x01f1;
			buforek[16] = 0x02f2; 
			buforek[17] = 0x03f3;


int buforek[18];
			buforek[0] = 0x8080; 
			buforek[1] = 0x8080;  
			buforek[2] = 0x8080;  
			buforek[3] = 0x8080; 
			buforek[4] = 0x8080; 
			buforek[5] = 0x8080;  
			buforek[6] = 0x8080;  
			buforek[7] = 0x8080;
			buforek[8] = 0x8080; 
			buforek[9] = 0x8080;  
			buforek[10] = 0x8080;  
			buforek[11] = 0x8080;
			buforek[12] = 0x8080; 
			buforek[13] = 0x8080;  
			buforek[14] = 0x8080;  
			buforek[15] = 0x8080;
			buforek[16] = 0x8080; 
			buforek[17] = 0x8080; 
*/
/*
int buforek[18];
			buforek[0] = 0x0000; 
			buforek[1] = 0x0000;  
			buforek[2] = 0x0000;  
			buforek[3] = 0x0000; 
			buforek[4] = 0x0000; 
			buforek[5] = 0x0000;  
			buforek[6] = 0x0000;  
			buforek[7] = 0x0000;
			buforek[8] = 0x0000; 
			buforek[9] = 0x0000;  
			buforek[10] = 0x0000;  
			buforek[11] = 0x0000;

			buforek[12] = 0x0000; 
			buforek[13] = 0x0000;  
			buforek[14] = 0x0000;  
			buforek[15] = 0x0000;
			buforek[16] = 0x0000; 
			buforek[17] = 0x0000; 
*/
/*
int buforek[18];                   //WB/Barrel    ANeg APos    DTMROC
			buforek[0] = 0x00FF;  //DAC1 DAC0     CH44 CH36         1
			buforek[1] = 0x0000;  //DAC3 DAC2
			buforek[2] = 0x00FF;  //DAC1 DAC0     CH45 CH37         2
			buforek[3] = 0x0000;  //DAC3 DAC2
			buforek[4] = 0x00FF;  //DAC1 DAC0     CH46 CH38         3
			buforek[5] = 0x0000;  //DAC3 DAC2
			buforek[6] = 0x00FF;  //DAC1 DAC0     CH47 CH39         4
			buforek[7] = 0x0000;  //DAC3 DAC2
			buforek[8] = 0x00FF;  //DAC1 DAC0     CH48 CH40         5
			buforek[9] = 0x00FF;  //DAC3 DAC2     CH49 CH41
			buforek[10] = 0x00FF; //DAC1 DAC0     CH50 CH42         6 
			buforek[11] = 0x00FF; //DAC3 DAC2     CH51 CH43
//                                                Dig
			buforek[12] = 0x00FF; //DAC1 DAC0          CH52         9
			buforek[13] = 0x00FF; //DAC1 DAC0          CH53        10
			buforek[14] = 0x00FF; //DAC1 DAC0          CH54        11
			buforek[15] = 0x00FF; //DAC1 DAC0          CH55        12
			buforek[16] = 0xFFFF; //DAC1 DAC0    CH57  CH56        13
			buforek[17] = 0xFFFF; //DAC1 DAC0    CH59  CH58        14
*/
/*
int buforek[18];                   //WA           ANeg APos
			buforek[0] = 0x00FF;  //DAC1 DAC0     CH44 CH36
			buforek[1] = 0x00FF;  //DAC3 DAC2
			buforek[2] = 0x00FF;  //DAC1 DAC0     CH45 CH37
			buforek[3] = 0x00FF;  //DAC3 DAC2
			buforek[4] = 0x00FF;  //DAC1 DAC0     CH46 CH38 
			buforek[5] = 0x00FF;  //DAC3 DAC2
			buforek[6] = 0x00FF;  //DAC1 DAC0     CH47 CH39
			buforek[7] = 0x00FF;  //DAC3 DAC2
			buforek[8] = 0x00FF;  //DAC1 DAC0     CH48 CH40
			buforek[9] = 0x00FF;  //DAC3 DAC2     CH49 CH41
		   buforek[10] = 0x00FF;  //DAC1 DAC0     CH50 CH42
		   buforek[11] = 0x00FF;  //DAC3 DAC2     CH51 CH43
//                                                Dig
			buforek[12] = 0xFFFF; //DAC1 DAC0          CH52
			buforek[13] = 0xFFFF; //DAC3 DAC2          CH53
			buforek[14] = 0xFFFF; //DAC1 DAC0          CH54
			buforek[15] = 0xFFFF; //DAC3 DAC2          CH55  
			buforek[16] = 0xFFFF; //DAC1 DAC0    CH57  CH56
			buforek[17] = 0xFFFF; //DAC3 DAC2    CH59  CH58

*/
 
/*
                int buforek[18];
                buforek[0] = 0x1F1F;
				buforek[1] = 0x0000;
				buforek[2] = 0x3F3F;
				buforek[3] = 0x0000;
				buforek[4] = 0x5F5F;
				buforek[5] = 0x0000;
				buforek[6] = 0x7F7F;
				buforek[7] = 0x0000;
				buforek[8] = 0x9F9F; 
				buforek[9] = 0xBFBF;  
				buforek[10] = 0xDFDF;  
				buforek[11] = 0xFFFF;
				buforek[12] = 0x001F; 
				buforek[13] = 0x003F;  
				buforek[14] = 0x005F;  
				buforek[15] = 0x007F;
				buforek[16] = 0xBF9F; 
				buforek[17] = 0xFFDF; 
*/
/*
                int buforek[18];
                buforek[0] = 0x0000;
				buforek[1] = 0x0000;
				buforek[2] = 0x5555;
				buforek[3] = 0x0000;
				buforek[4] = 0xAAAA;
				buforek[5] = 0x0000;
				buforek[6] = 0xFFFF;
				buforek[7] = 0x0000;
				buforek[8] = 0x0000; 
				buforek[9] = 0x0000;
				buforek[10] = 0xAAAA;  
				buforek[11] = 0xFFFF;
				buforek[12] = 0x0000; 
				buforek[13] = 0x0055;  
				buforek[14] = 0x00AA;  
				buforek[15] = 0x00FF;
				buforek[16] = 0x5500; 
				buforek[17] = 0xFFAA;
*/
/*
                int buforek[18];
                buforek[0] = 0x1110;
				buforek[1] = 0x1312;
				buforek[2] = 0x2120;
				buforek[3] = 0x2322;
				buforek[4] = 0x3130;
				buforek[5] = 0x3332;
				buforek[6] = 0x4140;
				buforek[7] = 0x4342;
				buforek[8] = 0x5150;
				buforek[9] = 0x5352;
				buforek[10] = 0x6160;  
				buforek[11] = 0x6362;
				buforek[12] = 0x9190; 
				buforek[13] = 0xA1A0;  
				buforek[14] = 0xB1B0;  
				buforek[15] = 0xC1C0;
				buforek[16] = 0xD1D0; 
				buforek[17] = 0xE1E0;
*/
/*
int buforek[18];
			buforek[0] = 0xB080; 
			buforek[1] = 0x8080;  
			buforek[2] = 0xB080;  
			buforek[3] = 0x8080; 
			buforek[4] = 0xB080; 
			buforek[5] = 0x8080;  
			buforek[6] = 0xB080;  
			buforek[7] = 0x8080;

			buforek[8] = 0xB080; 
			buforek[9] = 0xB080;  
			buforek[10] = 0xB080; 
			buforek[11] = 0xB080;

			buforek[12] = 0x0000; 
			buforek[13] = 0x0000;  
			buforek[14] = 0x0000;  
			buforek[15] = 0x0000;
			buforek[16] = 0x0000; 
			buforek[17] = 0x0000;
			*/
//*********************************************************************
int buforek[18];                   //WB/Barrel    ANeg APos    DTMROC
			buforek[0] = 0x90A0;  //DAC1 DAC0     CH44 CH36         1
			buforek[1] = 0x90A0;  //DAC3 DAC2
			buforek[2] = 0x90A0;  //DAC1 DAC0     CH45 CH37         2
			buforek[3] = 0x90A0;  //DAC3 DAC2
			buforek[4] = 0x90A0;  //DAC1 DAC0     CH46 CH38         3
			buforek[5] = 0x90A0;  //DAC3 DAC2
			buforek[6] = 0x90A0;  //DAC1 DAC0     CH47 CH39         4
			buforek[7] = 0x90A0;  //DAC3 DAC2

			buforek[8] = 0x90A0;  //DAC1 DAC0     CH48 CH40         5
			buforek[9] = 0x90A0;  //DAC3 DAC2     CH49 CH41
		    buforek[10] = 0x90A0; //DAC1 DAC0     CH50 CH42         6 
		    buforek[11] = 0x90A0; //DAC3 DAC2     CH51 CH43
//                                                Dig
			buforek[12] = 0x0000; //DAC1 DAC0          CH52         9
			buforek[13] = 0x0000; //DAC1 DAC0          CH53        10
			buforek[14] = 0x0000; //DAC1 DAC0          CH54        11
			buforek[15] = 0x0000; //DAC1 DAC0          CH55        12
			buforek[16] = 0x0000; //DAC1 DAC0    CH57  CH56        13
			buforek[17] = 0x0000; //DAC1 DAC0    CH59  CH58        14
//**********************************************************************
// 0xA480    dzielenie pradu
//*************************************************************************
//int KlasaI::WriteAllDacsWheelB(int handle, int elmbAddress, int portC, int elmbTimeOut, int *values)
// open log file
_ftime( &timebuffer ); newtime  = localtime( &(timebuffer.time) );
	printf ("%02d %02d H%02d:%02d:%02d\n",
			newtime->tm_mday, newtime->tm_mon+1,
			newtime->tm_hour, newtime->tm_min, newtime->tm_sec);

	for (i=0; i<100; i++) {
			if ((status = zasilacz.WriteAllDacsWheelA (handle, elmbNb, elmbTimeOut, buforek)) < 0) {
				printf ("WriteAllDacs exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
/*			if ((status = zasilacz.ReadAllDacsWheelA (handle, elmbNb, elmbTimeOut, buforek)) < 0) {
				printf ("WriteAllDacs exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}*/
	}
/*
            if ((status = zasilacz.WriteAllDacsWheelB (handle, elmbNb, portC, elmbTimeOut, buforek)) < 0) {
				printf ("WriteAllDacs exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
*/
	_ftime( &timebuffer ); newtime  = localtime( &(timebuffer.time) );
	printf ("%02d %02d H%02d:%02d:%02d\n",
			newtime->tm_mday, newtime->tm_mon+1,
			newtime->tm_hour, newtime->tm_min, newtime->tm_sec);

			break;

case 46:
			int loopnumber1;
			FILE *fp1;
            fp1 = fopen ("errorLog_46.txt", "w");
			printf ("Write in a loop one value to dtmroc \n");
			end17 = 1;
            printf ("dtmroc (hex) - ");
			scanf ("%x", &dtmroc);

//			printf ("DAC 0&1 (hex) - ");
//			scanf ("%x", &dac);
			loopnumber1=0;
			dac=0xFFFF;
			do {
          
            
			  
				dacRead = 0xabcd;
				if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmroc, 2, dac, elmbTimeOut)) < 0)
//                if ((status = zasilacz.Write_SPI(handle, elmbNb,dtmroc, 2, dac, portC, elmbTimeOut)) < 0)
               { 
//					fprintf (fp1, "Write exit with error: %d\n", status);
                    fprintf (fp1,"Loop Number - %d ",loopnumber1);
					fprintf (fp1, "%s", zasilacz.GetErrorText ());
					fprintf (fp1, "dtmroc: %d\n",dtmroc);
					printf ("Write exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
                    printf ("dtmroc - %d\n",dtmroc);

//					end17 = 0;
				}
                printf ("DAC - %x\n",dac);
                printf ("Loop Number - %d, dtmroc - %d \n",loopnumber1,dtmroc);
            
 				loopnumber1=loopnumber1 + 1;
//				dac=dac + 0x00;
//				dac=dac +0x33;
                dac=dac & 0xFFFF;
//				if (dac==0x00066)  dac=0x0000;
//				if (dac==0x00132)  dac=0x0000;

                 printf ("DAC - %x\n",dac);
  //              dac = dac & 0xFFFF;
 //               Sleep (10);
				Sleep (371);
//                Sleep (1000);
				if (kbhit ()) end17 = 0;
			  }
			    
			while (end17);
			fclose (fp1);
			break;
 
case 47:
            int loopnumber2;
			FILE *fp2;
            fp2 = fopen ("errorLog_47.txt", "w");
	        
			printf ("Broadcast writing loop (increment of DAC values in every loop) \n");
			end17 = 1;
			dac = 5;
			clockNb = 0;
            loopnumber2=0;
			while(end17)
 //           while (end17);
//			do
			{
				if ((status = zasilacz.WriteDacBrdc(handle, elmbNb, 3, (dac)&0xFFFF, elmbTimeOut)) < 0) { 
					printf ("WriteDacBrdc exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
                   
					end17 = 0;
				} 
	//			if ((status = zasilacz.DacBrdcCheck(handle, elmbNb, value, (dac++)&0xFFFF, portC, elmbTimeOut)) < 0) 
				if ((status =  DacBrdcCheckOur (handle, elmbNb, 
								(dac++)&0xFFFF, elmbTimeOut, fp2)) < 0) 
    //bk            (dac)&0xFFFF, portC, elmbTimeOut, fp2)) < 0)
				{ 
					printf ("DacBrdcCheckOur exit with error: %d\n", status);
                    fprintf (fp2,"Loop Number - %d\n ",loopnumber2);
                    fprintf (fp2,"---------------------------------------------------\n ");
//					end17 = 0;
				} 
				// printf ("%d ", dac);
				clockNb++;
				printf ("%d 0x%X\n", loopnumber2,dac);
                loopnumber2=loopnumber2 + 1;
		 		if (kbhit ()) end17 = 0;
				//Sleep ((1000*dac)%500);
			} 
	        fclose (fp2);
			printf ("Number of 'writes': %d\n", clockNb);
            printf ("See:errorLog_47.txt \n");
			break;

case 48:
		FILE *lf;
		short seconds;
		printf ("test broadcast writing loop in a given dtmroc\n");
		_ftime( &timebuffer ); seconds = (short) timebuffer.time; 
		srand (seconds );
		end17 = 1;
		printf ("DTMROC  - ");
		scanf ("%d", &dtmroc);
		end17 = 1;
		lf = fopen ("test48.txt", "w");
		item = 0;
		while (end17) {
			dac = ((rand() & 0xFF) << 8) | (rand() & 0xFF);
			printf ("%d\t All DACs 01 value: 0x%X\n",  item, dac);
			checkWriteBroadcastOneDtmroc (handle, elmbNb, 2, dac, elmbTimeOut, lf, dtmroc, item);
			dac = ((rand() & 0xFF) << 8) | (rand() & 0xFF);
			printf ("\t All DACs 23 value: 0x%X\n",  dac);
			checkWriteBroadcastOneDtmroc (handle, elmbNb, 3, dac, elmbTimeOut, lf, dtmroc, item);
			if (kbhit ()) end17 = 0;
			item++;
			Sleep(1000);
		}
		fclose (lf);
		break;


		case 51:
			printf ("Read from any DTMROC dacs 0 1 \n");
			printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
			if (((dtmroc > 6) && (dtmroc < 9)) || (dtmroc > 14) || (dtmroc < 1)) {
				printf ("Bad dtmroc: %d. Try again. \n", dtmroc);
				break;
			}
			if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, 2, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("DAC 0 1: 0x%X\n", dacRead);
			if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, 3, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("DAC 2 3: 0x%X\n", dacRead); 
			break;

		case 1951:
	/*		if ((status = zasilacz.BC(handle, 2, elmbNb, portC)) < 0) {
				printf ("Write exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			} */
			printf ("Read from any DTMROC dacs 0 1 \n");
			printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
			if (((dtmroc > 6) && (dtmroc < 9)) || (dtmroc > 14) || (dtmroc < 1)) {
				printf ("Bad dtmroc: %d. Try again. \n", dtmroc);
				break;
			}
			printf ("Dacs 2 - dacs 0,1; 3 dacs - 2,3)  - ");
			scanf ("%d", &dacNb);
			if ((dacNb != 2) && (dacNb != 3)) {
				printf ("Bad dacs: %d. Try again. \n", dacNb);
				break;
			}
			if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, dacNb, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				if (dacNb == 2) printf ("DAC 0 1: 0x%X\n", dacRead);
				else printf ("DAC 2 3: 0x%X\n", dacRead);
			}
/*			if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, 3, portC, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("DAC 2 3: 0x%X\n", dacRead); */
			break;

		case 52:
 
			end17 = 1;
			clockNb = 1;
			int dac;
			printf ("Pattern: 0x"); scanf ("%X", &dacRead);
			printf ("\nRead DTMROC all, dacs 0 1 2 3 in a loop \n\n");
//-------------------------------------------------------------------------------------------------
//int KlasaI::Read(int handle, int elmbAddress, int dtmrocAddress, int object, int portC,
//			       int elmbTimeOut, int *value)
//-------------------------------------------------------------------------------------------------

			do {
					i=0; 
					if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmrocAddress(i), 2, 
						        elmbTimeOut, &dac)) < 0) 
					{
						printf ("Read exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
						end17 = 0;
					}
					Sleep(500);
					if (dac != dacRead) 
					{
						printf ("%d. DTMROC %d  DAC 0 1 Value read: 0x%x  should be 0x%X \n", 
							clockNb, dtmrocAddress(i), dac,dacRead/*, dtmrocStat*/);
					//  printf ("DTMROC Number: %d\n",i);
//						end17 = 0;
					}

					if (end17) 
						
					{
					//dacRead = 0x5555;
					if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmrocAddress(i), 3, 
						        elmbTimeOut, &dac)) < 0) 
					{
						printf ("Read exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
//						end17 = 0;
					}
					Sleep (500);
					if (dac != dacRead) 
					{
						printf ("%d. DTMROC %d  DACs 2 3 Value read: 0x%x  should be 0x%X \n", 
							clockNb, dtmrocAddress(i), dac,dacRead/*, dtmrocStat*/);
					//  printf ("DTMROC Number: %d\n",i);
//						end17 = 0;
					}
					}
					
					

#ifdef ALL_DTMROCS
                for (i=0; i<11; i++) {
					//dacRead = 0x5555;
					if ((status = zasilacz.Read (handle, elmbNb, dtmrocAddress(i), 2, portC, elmbTimeOut, &dac)) < 0) {
						printf ("Read exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
						end17 = 0;
					}

					Sleep(500);
					if (dac != dacRead) {
						printf ("DTMROC %d  DAC 0 1 Value read: 0x%x  should be 0x%X \n", dtmrocAddress(i), dac,dacRead/*, dtmrocStat*/);
					//  printf ("DTMROC Number: %d\n",i);
					//	end17 = 0;
					}
					//dacRead = 0x5555;
					if ((status = zasilacz.Read (handle, elmbNb, dtmrocAddress(i), 3, portC, elmbTimeOut, &dac)) < 0) {
						printf ("Read exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
						end17 = 0;
					}

					Sleep(500);
					if (dac != dacRead) {
						printf ("DTMROC %d  DACs 2 3 Value read: 0x%x  should be 0x%X \n", dtmrocAddress(i), dac,dacRead/*, dtmrocStat*/);
					//  printf ("DTMROC Number: %d\n",i);
					//	end17 = 0;
					}
				}
#endif
				 clockNb++;
				if (kbhit ()) end17 = 0;
			} while (end17);
			
//			printf ("Total  %d seconds and %d accesses. %1.2f s for a single access.\n",
//				(int) spanElapsed, (float) spanElapsed/(float) (clockNb));
			printf ("Number of 'reads': %d\n", clockNb);
			break;

		case 53:

			printf ("Read all dacs in a board to an array\n");
			printf ("Type Barrel - 0, Type Wheel A - 1: - "); scanf ("%d", &dacNb);

            
			if ((dacNb != 0) && (dacNb != 1)) {
				printf ("Bad Type Wheel: %d. Try again. \n", dacNb);
				break;

			   }

			if (dacNb) {
				if ((status = zasilacz.ReadAllDacsWheelA (handle, elmbNb, elmbTimeOut, buforek)) < 0) {
					printf ("ReadAllDacsWheelA exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
			}
			else {
				if ((status = zasilacz.ReadAllDacsWheelB (handle, elmbNb, elmbTimeOut, buforek)) < 0) {
					printf ("ReadAllDacsWheelB exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
			}
			for (i=0; i<18; i++)
				printf ("%d: 0x%X\n", i, buforek[i]);
			printf ("All DACs!\n");
            
			break;

		case 54:
 
			end17 = 1;
			clockNb = 1;
			int dac54;
			printf ("Pattern: 0x"); scanf ("%X", &dacRead);
			printf ("\nRead DTMROC all, dacs 0 1 2 3 in a loop \n\n");
//-------------------------------------------------------------------------------------------------
//int KlasaI::Read(int handle, int elmbAddress, int dtmrocAddress, int object, int portC,
//			       int elmbTimeOut, int *value)
//-------------------------------------------------------------------------------------------------

			do {
					i=0; 
					if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmrocAddress(i), 3, 
						        elmbTimeOut, &dac54)) < 0) 
					{
						printf ("Read exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
						end17 = 0;
					}
					Sleep(500);
					if (dac54 != dacRead) 
					{
						printf ("%d. DTMROC %d  DAC 0 1 Value read: 0x%x  should be 0x%X \n", 
							clockNb, dtmrocAddress(i), dac54,dacRead);
					}
#ifdef ASDLR2
					if (end17) 						
					{
					if ((status = zasilacz.Read (handle, elmbNb, dtmrocAddress(i), 3, 
						        portC, elmbTimeOut, &dac54)) < 0) 
					{
						printf ("Read exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					}
					Sleep (500);
					if (dac54 != dacRead) 
					{
						printf ("%d. DTMROC %d  DACs 2 3 Value read: 0x%x  should be 0x%X \n", 
							clockNb, dtmrocAddress(i), dac,dacRead);
					}
					}
#endif
				 clockNb++;
				if (kbhit ()) end17 = 0;
			    } while (end17);
			printf ("Number of 'reads': %d\n", clockNb);
			break;



		case 55:	        
			printf ("Read from any DTMROC DAC0 & DAC1 in the loop !!! \n");
			printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
			if (((dtmroc > 6) && (dtmroc < 9)) || (dtmroc > 14) || (dtmroc < 1)) {
				printf ("Bad dtmroc: %d. Try again. \n", dtmroc);
				break;
			}
            end17 = 1;
			
			while (end17) {

			
			if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, 2, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			 printf("DAC : %x\n", dacRead);

            if (kbhit ()) end17 = 0;
			if(dacRead!=0x5555) {
				printf("Error: %x\n", dacRead);
//				end17 = 0;
			}
			Sleep(1000);
			}
			
			break;

		case 61:
			printf ("Set ON 1 regulator\n");
			printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
//			int KlasaI::Get_INHIBIT(int handle, int elmbAddress, int address, int Level, int *inhibits)
			if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
				printf ("Get_INHIBIT exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else
				printf ("DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);

			printf ("Regulator (1 first, 2 - second, 3 - both) - ");
			scanf ("%d", &regulator);

		//	int KlasaI::Set_INHIBIT(int handle, int elmbAddress, int address, int Level, int inhibitNb, int inhibitValue)
			if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, dtmroc, regulator, 1, elmbTimeOut)) < 0) {
				printf ("Set_INHIBIT exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
				printf ("Setting INHIBIT OK: DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);
			}
			break;

		case 1961:
			printf ("Set ON 1 regulator\n");
			printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
//			int KlasaI::Get_INHIBIT(int handle, int elmbAddress, int address, int Level, int *inhibits)
			if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
				printf ("Get_INHIBIT exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else
				printf ("DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);

			printf ("Regulator (1 first, 2 - second, 3 - both) - ");
			scanf ("%d", &regulator);

		//	int KlasaI::Set_INHIBIT(int handle, int elmbAddress, int address, int Level, int inhibitNb, int inhibitValue)
			if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, dtmroc, regulator, 1, elmbTimeOut)) < 0) {
				printf ("Set_INHIBIT exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
				printf ("Setting INHIBIT OK: DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);
			}
			break;

		case 62:
			printf ("Set OFF 1 regulator\n");
			printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
//			int KlasaI::Get_INHIBIT(int handle, int elmbAddress, int address, int Level, int *inhibits)
			if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
				printf ("Get_INHIBIT exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else
				printf ("DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);

			printf ("Regulator (1 first, 2 - second, 3 - both) - ");
			scanf ("%d", &regulator);

		//	int KlasaI::Set_INHIBIT(int handle, int elmbAddress, int address, int Level, int inhibitNb, int inhibitValue)
			if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, dtmroc, regulator, 0, elmbTimeOut)) < 0) {
				printf ("Set_INHIBIT exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
				printf ("Setting INHIBIT OK: DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);
			}
			break;

		case 1962:
			printf ("Set OFF 1 regulator\n");
			printf ("DTMROC  - ");
			scanf ("%d", &dtmroc);
//			int KlasaI::Get_INHIBIT(int handle, int elmbAddress, int address, int Level, int *inhibits)
			if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
				printf ("Get_INHIBIT exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else
				printf ("DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);

			printf ("Regulator (1 first, 2 - second, 3 - both) - ");
			scanf ("%d", &regulator);

		//	int KlasaI::Set_INHIBIT(int handle, int elmbAddress, int address, int Level, int inhibitNb, int inhibitValue)
			if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, dtmroc, regulator, 0, elmbTimeOut)) < 0) {
				printf ("Set_INHIBIT exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
				printf ("Setting INHIBIT OK: DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);
			}
			break;

		case 63:
			printf ("Set all regulators ON one by one\n");
			regulator = 3;
			for (dtmroc=1; dtmroc<=14; dtmroc++) {
				if ((dtmroc==7) || (dtmroc==8)) continue;
				if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
					printf ("DTMROC %d, inhibit state before: %d\n", dtmroc, dacRead);

				if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, dtmroc, regulator, 1, elmbTimeOut)) < 0) {
					printf ("Set_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else {
					if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
						printf ("Get_INHIBIT exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					}
					else
					printf ("Setting INHIBIT OK: DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);
				}
			}
			break;

		case 1963:
			printf ("Set all regulators ON one by one\n");
			regulator = 3;
			for (dtmroc=1; dtmroc<=14; dtmroc++) {
				if ((dtmroc==7) || (dtmroc==8)) continue;
				if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
					printf ("DTMROC %d, inhibit state before: %d\n", dtmroc, dacRead);

				if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, dtmroc, regulator, 1, elmbTimeOut)) < 0) {
					printf ("Set_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else {
					if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
						printf ("Get_INHIBIT exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					}
					else
					printf ("Setting INHIBIT OK: DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);
				}
			}
			break;

		case 64:
			printf ("Set all regulators OFF one by one\n");
			regulator = 3;
			for (dtmroc=1; dtmroc<=14; dtmroc++) {
				if ((dtmroc==7) || (dtmroc==8)) continue;
				if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
					printf ("DTMROC %d, inhibit state before: %d\n", dtmroc, dacRead);

				if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, dtmroc, regulator, 0, elmbTimeOut)) < 0) {
					printf ("Set_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else {
					if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
						printf ("Get_INHIBIT exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					}
					else
					printf ("Setting INHIBIT OK: DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);
				}
			}
			break;

		case 1964:
			printf ("Set all regulators OFF one by one\n");
			regulator = 3;
			for (dtmroc=1; dtmroc<=14; dtmroc++) {
				if ((dtmroc==7) || (dtmroc==8)) continue;
				if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
					printf ("DTMROC %d, inhibit state before: %d\n", dtmroc, dacRead);

				if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, dtmroc, regulator, 0, elmbTimeOut)) < 0) {
					printf ("Set_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else {
					if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
						printf ("Get_INHIBIT exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					}
					else
					printf ("Setting INHIBIT OK: DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);
				}
			}
			break;

		case 65:
            printf ("PORT C = 0x%x\n",portC );
			printf ("Set ON / OFF with broadcast\n");
//			printf ("ON - 1, OFF - 0: - "); scanf ("%d", &dacNb);
            printf (" 0 - OFF\n 1 - ON even\n 2 - ON odd\n 3 - ON all\n Inhibit ?"); scanf ("%d", &dacNb);
			if ((status = zasilacz.SetInhBrdc (handle, elmbNb, dacNb, elmbTimeOut)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
                if (dacNb==0) {printf ("All regulators should be switched OFF.\n");}
				if (dacNb==1) {printf ("Even regulators should be switched ON.\n");}
				if (dacNb==2) {printf ("Odd regulators should be switched ON.\n");}
				if (dacNb==3) {printf ("All regulators should be switched ON.\n");}
                    
			    }
              printf ("PORT C = 0x%x\n",portC );
			break;

		case 1965:
            printf ("PORT C = 0x%x\n",portC );
			printf ("Set ON / OFF with broadcast\n");
//			printf ("ON - 1, OFF - 0: - "); scanf ("%d", &dacNb);
            printf (" 0 - OFF\n 1 - ON even\n 2 - ON odd\n 3 - ON all\n Inhibit ?"); scanf ("%d", &dacNb);
			if ((status = zasilacz.SetInhBrdc (handle, elmbNb, dacNb, elmbTimeOut)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
                if (dacNb==0) {printf ("All regulators should be switched OFF.\n");}
				if (dacNb==1) {printf ("Even regulators should be switched ON.\n");}
				if (dacNb==2) {printf ("Odd regulators should be switched ON.\n");}
				if (dacNb==3) {printf ("All regulators should be switched ON.\n");}
                    
			    }
              printf ("PORT C = 0x%x\n",portC );
			break;

		case 66:
			printf ("Read all inhibits one by one - once\n");
			for (dtmroc=1; dtmroc<=14; dtmroc++) {
				if ((dtmroc==7) || (dtmroc==8)) continue;
				if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
					printf ("DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);
			}
			break;

		case 1966:
			printf ("Read all inhibits one by one - once\n");
			for (dtmroc=1; dtmroc<=14; dtmroc++) {
				if ((dtmroc==7) || (dtmroc==8)) continue;
				if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) {
					printf ("Get_INHIBIT exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				else
					printf ("DTMROC %d, inhibit state: %d\n", dtmroc, dacRead);
			}
			break;

		case 67:
			int onOff; 
//			printf ("Read all inhibits one by one in a loop\n");
//bk			printf ("Loop: Set ON / OFF with broadcast and then check one by one\n");
//bk			printf ("Both ON - 3, first ON - 1, sec. ON - 2, both OFF - 0: - "); scanf ("%d", &onOff);
			    printf ("Test Inhibits: Inhibit pattern=3\n");
			    onOff=3;
/*			if ((status = zasilacz.SetInhBrdc (handle, elmbNb, portC, onOff, elmbTimeOut)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				if (dacNb)
					printf ("All regulators should be switched on.\n");
				else 
					printf ("All regulators should be switched off.\n");
			}
*/
			regulator = 3;
			end17 = 1;
			dacNb = 0;
			while (end17) {
				if ((status = zasilacz.SetInhBrdc (handle, elmbNb, onOff, elmbTimeOut)) < 0) {
					printf ("%s\n", zasilacz.GetErrorText ());
				}
//				for (dtmroc=1; dtmroc<=14; dtmroc++) 
                for (dtmroc=4; dtmroc<=4; dtmroc++)
				{
					if ((dtmroc==7) || (dtmroc==8)) continue;
					if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) 
					{
						printf ("Get_INHIBIT exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					}
               printf ("dacRead: %x\n", dacRead);   //bk
			   Sleep(1000);
                
					switch (onOff)  
					{
						case 3:
							if (dacRead != 3) 
								printf ("Inhibit bad set: dtmroc %d R %d W %d\n", dtmroc, dacRead, onOff);
							break;
						case 2:
							if (dacRead != 2) 
								printf ("Inhibit bad set: dtmroc %d R %d W %d\n", dtmroc, dacRead, onOff);
							break;
						case 1:
							if (dacRead != 1) 
								printf ("Inhibit bad set: dtmroc %d R %d W %d\n", dtmroc, dacRead, onOff);
							break;
						case 0:
							if (dacRead != 0) 
								printf ("Inhibit bad set: dtmroc %d R %d W %d\n", dtmroc, dacRead, onOff);
							break;
						default:
							printf ("bad inhibit value: %d\n", onOff);
					}
 
				}
				printf ("%d ", dacNb++);
				if (kbhit ()) end17 = 0;
			}
			break;

		case 1967:
			int onOff_1967; 
//			printf ("Read all inhibits one by one in a loop\n");
//bk			printf ("Loop: Set ON / OFF with broadcast and then check one by one\n");
//bk			printf ("Both ON - 3, first ON - 1, sec. ON - 2, both OFF - 0: - "); scanf ("%d", &onOff);
			    printf ("Test Inhibits: Inhibit pattern=3\n");
			    onOff_1967=3;
/*			if ((status = zasilacz.SetInhBrdc (handle, elmbNb, portC, onOff, elmbTimeOut)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				if (dacNb)
					printf ("All regulators should be switched on.\n");
				else 
					printf ("All regulators should be switched off.\n");
			}
*/
			regulator = 3;
			end17 = 1;
			dacNb = 0;
			while (end17) {
				if ((status = zasilacz.SetInhBrdc (handle, elmbNb, onOff_1967, elmbTimeOut)) < 0) {
					printf ("%s\n", zasilacz.GetErrorText ());
				}
//				for (dtmroc=1; dtmroc<=14; dtmroc++) 
                for (dtmroc=4; dtmroc<=4; dtmroc++)
				{
					if ((dtmroc==7) || (dtmroc==8)) continue;
					if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) 
					{
						printf ("Get_INHIBIT exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					}
               printf ("dacRead: %x\n", dacRead);   //bk
			   Sleep(1000);
                
					switch (onOff)  
					{
						case 3:
							if (dacRead != 3) 
								printf ("Inhibit bad set: dtmroc %d R %d W %d\n", dtmroc, dacRead, onOff);
							break;
						case 2:
							if (dacRead != 2) 
								printf ("Inhibit bad set: dtmroc %d R %d W %d\n", dtmroc, dacRead, onOff);
							break;
						case 1:
							if (dacRead != 1) 
								printf ("Inhibit bad set: dtmroc %d R %d W %d\n", dtmroc, dacRead, onOff);
							break;
						case 0:
							if (dacRead != 0) 
								printf ("Inhibit bad set: dtmroc %d R %d W %d\n", dtmroc, dacRead, onOff);
							break;
						default:
							printf ("bad inhibit value: %d\n", onOff);
					}
 
				}
				printf ("%d ", dacNb++);
				if (kbhit ()) end17 = 0;
			}
			break;


		case 68:
			printf ("Read all inhibits with one function\n");

		//	int KlasaI::GetAllInhibitsWheelB(int handle, int elmbAddress, int portC, int elmbTimeOut, int *values)
			printf ("Type Barrel - 0, Type Wheel A - 1: - "); scanf ("%d", &dacNb);
			if (dacNb) {
				if ((status = zasilacz.GetAllInhibitsWheelA (handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
					printf ("%s\n", zasilacz.GetErrorText ());
				}
			}
			else {
				if ((status = zasilacz.GetAllInhibitsWheelB (handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
					printf ("%s\n", zasilacz.GetErrorText ());
				}
			}
			printf ("Inhibit word: 0x%X\n", dacRead);
			break;

		case 1968:
			printf ("Read all inhibits with one function\n");

		//	int KlasaI::GetAllInhibitsWheelB(int handle, int elmbAddress, int portC, int elmbTimeOut, int *values)
			printf ("Type Barrel - 0, Type Wheel A - 1: - "); scanf ("%d", &dacNb);
			if (dacNb) {
				if ((status = zasilacz.GetAllInhibitsWheelA (handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
					printf ("%s\n", zasilacz.GetErrorText ());
				}
			}
			else {
				if ((status = zasilacz.GetAllInhibitsWheelB (handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
					printf ("%s\n", zasilacz.GetErrorText ());
				}
			}
			printf ("Inhibit word: 0x%X\n", dacRead);
			break;

		case 69:

			end17 = 1;
			dacNb = 0;
			while (end17) 
			    {
				if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 3, elmbTimeOut)) < 0) {
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				if ((status = zasilacz.WriteDacBrdc(handle, elmbNb, 3, 0x5555, elmbTimeOut)) < 0) { 
						printf ("WriteDacBrdc exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
				}
				//int KlasaI::DacBrdcCheck (int handle, int elmbAddress, int dacs, int Field6, int portC, int elmbTimeOut)

				if ((status =  zasilacz.DacBrdcCheck (handle, elmbNb, 3, 0x5555, elmbTimeOut)) < 0) 
				{ 
					printf ("DacBrdcCheck exit with error: %d\n", status);
				} 
				for (i=0; i<=17; i++) buforek[i] = 20 + i;
				if ((status = zasilacz.WriteAllDacsWheelB (handle, elmbNb, elmbTimeOut, buforek)) < 0) {
					printf ("WriteAllDacs exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}

				for (dtmroc = 0; dtmroc < 12; dtmroc++) {
					if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmrocAddress(dtmroc), 4, elmbTimeOut, &dtmrocStat)) < 0) { // status
						printf ("Read exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
						end17 = 0;
					}
					if (dtmrocAddress(dtmroc) != (dtmrocStat & 0xF))
						printf ("DTMROC %d BAD Status: %d\n",dtmroc, dtmrocStat & 0xF);
				}

				if (checkTemperatures (handle, elmbNb)) end17 = 0;
                 
				if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 0, elmbTimeOut)) < 0) {
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				
				printf ("%d ", dacNb++);

				if (kbhit ()) end17 = 0;
				}
			break;

		case 1969:

			end17 = 1;
			dacNb = 0;
			while (end17) 
			    {
				if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 3, elmbTimeOut)) < 0) {
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				if ((status = zasilacz.WriteDacBrdc(handle, elmbNb, 3, 0x5555, elmbTimeOut)) < 0) { 
						printf ("WriteDacBrdc exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
				}
				//int KlasaI::DacBrdcCheck (int handle, int elmbAddress, int dacs, int Field6, int portC, int elmbTimeOut)

				if ((status =  zasilacz.DacBrdcCheck (handle, elmbNb, 3, 0x5555, elmbTimeOut)) < 0) 
				{ 
					printf ("DacBrdcCheck exit with error: %d\n", status);
				} 
				for (i=0; i<=17; i++) buforek[i] = 20 + i;
				if ((status = zasilacz.WriteAllDacsWheelB (handle, elmbNb, elmbTimeOut, buforek)) < 0) {
					printf ("WriteAllDacs exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}

				for (dtmroc = 0; dtmroc < 12; dtmroc++) {
					if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmrocAddress(dtmroc), 4, elmbTimeOut, &dtmrocStat)) < 0) { // status
						printf ("Read exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
						end17 = 0;
					}
					if (dtmrocAddress(dtmroc) != (dtmrocStat & 0xF))
						printf ("DTMROC %d BAD Status: %d\n",dtmroc, dtmrocStat & 0xF);
				}

				if (checkTemperatures (handle, elmbNb)) end17 = 0;
                 
				if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 0, elmbTimeOut)) < 0) {
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				
				printf ("%d ", dacNb++);

				if (kbhit ()) end17 = 0;
				}
			break;




		case 70:
			printf ("czytamy ADC\n");

			if ((status = zasilacz.ReadADC_Counts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				for (i=0; i<64; i++) {
				
						
					printf ("%d. Conversion Status: %d \t %d\n", i, adcConvStat[i], adcValues[i]);
					}
				
				}
			break;

		case 71:
			printf ("czytamy ADC (mikrowolty) \n");

			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				for (i=0; i<64; i++) {
					printf ("%d Conversion Status: %d \t %d\n", i, adcConvStat[i], adcValues[i]);
				}
			}
			break;


		case 80:
			
			
 			fp80 = fopen ("IoutVout_80.txt","w");
			/*
            printf ("Voltage Regulators 0utputs [uV] for  DAC = 0xFFFF (255) \n");
            fprintf (fp80,"Voltage Regulators 0utputs [uV] for  DAC = 0xFFFF (255) \n");

            if ((status = zasilacz.WriteDacBrdc(handle, elmbNb,3, 0xFFFF, portC, elmbTimeOut)) < 0) { 
					printf ("WriteDacBrdc exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());		
				} 

           */
			printf ("Card Serial Number: ");  // read calibration file
			scanf ("%d", &cardNb);

			// create calibfilename
			sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\LVPP_CALIB_%03d.csv", cardNb, cardNb);
			
			if ((calibFile = fopen (textBuf, "r")) == NULL) {
				printf ("Calibration file does not exist for card %d - please make calibration\n", cardNb); 
				break;
			}

			readCalibration(calibFile, calibCoeff, calibCurrentOffset, 36);
			
			fclose (calibFile);

			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {

				printf ("---------------------------------------------\n");
				fprintf (fp80, "----------------------------------------\n");
//----------------------------------------------------------------------------------------------------
				fprintf (fp80, "Iout without calibration\n");
				printf ("Iout without calibration\n");


				for (i=0; i<36; i++) {
					fprintf (fp80, " %d\t %f\t \n",i, 
						adcValues[i]*CurrentFactor);
					printf ("%d\t %d\t %f\n", i, adcConvStat[i], 
					adcValues[i]*CurrentFactor);
				}

				printf ("---------------------------------------------\n");
				fprintf (fp80, "----------------------------------------\n");
				fprintf (fp80, "Iout calibrated with calibCoeff=adcValue/Vout\n");
				printf ("Iout calibrated with calibCoeff=adcValue/Vout\n");

for (i=0; i<36; i++) {				
		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*CurrentFactor);
//			fprintf (fp, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*CurrentFactor);
//            fprintf (fp80, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*CurrentFactor);
              fprintf (fp80, " %d\t %f\t \n",i, 
			  adcValues[i]*CurrentFactor - (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000);

              printf ("%d\t %d\t %f\n", i, adcConvStat[i], 
			  adcValues[i]*CurrentFactor - (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000);

    // sumowanie pradow
  //              printf (" %d\t %d\t %f\t %f\t %f\n",i,i+1,adcValues[i]*CurrentFactor,adcValues[i+1]*CurrentFactor,
	//		     (adcValues[i]*CurrentFactor)+ (adcValues[i+1]*CurrentFactor));
//			if(i % 2==0){printf ("i+(i+1)= %d\t %d\t %f\n", i, adcConvStat[i],(adcValues[i]*CurrentFactor+adcValues[i+1]*CurrentFactor));}

}
         printf ("---------------------------------------------\n");
		 fprintf (fp80, "----------------------------------------\n");  //Current offset for DAC = 128 counts
		fprintf (fp80, "Current offset for DAC = 128 counts\n");
		printf ("Current offset for DAC = 128 counts\n");

		for (i=0; i<36; i++) {
            fprintf (fp80, " %d\t %f\t \n",i, 
				  adcValues[i]*CurrentFactor - calibCurrentOffset[i]);
			printf ("%d\t %d\t %f\n", i, adcConvStat[i], 
			   adcValues[i]*CurrentFactor - calibCurrentOffset[i]);
		}

         printf ("---------------------------------------------\n");
		 fprintf (fp80, "----------------------------------------\n");

for (i=36; i<60; i++) {
		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*VoltageFactor);
//	fprintf (fp80, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*VoltageFactor);
	
    fprintf (fp80, " %d \t %f\n",i, adcValues[i]*VoltageFactor);
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
 //    fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor);       

		}
         printf ("---------------------------------------------\n");
		for (i=60; i<64; i++) {
		//	printf ("%d. Conversion Status: %d \t %d\n", 
		//		i, adcConvStat[i], adcValues[i]);
//			fprintf (fp, "%d. Conversion Status: %d \t %d\n",i, adcConvStat[i], adcValues[i]);
          fprintf (fp80, "%d\t %d\t %f\n",i,adcConvStat[i], temperature (adcValues[i]));
           printf ("%d\t %d \t %f\n", i, adcConvStat[i],temperature (adcValues[i]));
		}
			}
			
			fclose (fp80);

             for(freqency=0; freqency<5000; freqency=freqency+1000)
			      {
			      Beep(freqency,100);
				  }

			break;
//------------------------------------------------------------------------------------------------
		case 81:
			printf ("Voltage Regulators 0utputs [counts] () \n");

           	if ((status = zasilacz.ReadADC_Counts (handle, elmbNb, adcValues, adcConvStat)) < 0) { 
//			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				
for (i=0; i<36; i++) {
		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*CurrentFactor);
//			fprintf (fp, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*CurrentFactor);
//            fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*CurrentFactor);

//-------------------------------------------------------------------------------
       if (adcValues[i] >= 0x8000)

		{
 
        adcValues[i] = ((0xFFFF - adcValues[i])+1) *(-1);
        printf ("%d\t %d \t %d\n", i, adcConvStat[i], adcValues[i]);
		}
	   else
            
	   

          printf ("%d\t %d \t %d\n", i, adcConvStat[i], adcValues[i]);

//-----------------------------------------------------------------------------------

}
         printf ("---------------------------------------------\n");
		for (i=36; i<60; i++) {
		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*VoltageFactor);
//	fprintf (fp, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*VoltageFactor);
      printf ("%d\t %d \t %d\n", i, adcConvStat[i],adcValues[i]);
 //    fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor);       

		}
         printf ("---------------------------------------------\n");
		for (i=60; i<64; i++) {
		//	printf ("%d. Conversion Status: %d \t %d\n", 
		//		i, adcConvStat[i], adcValues[i]);
//			fprintf (fp, "%d. Conversion Status: %d \t %d\n",i, adcConvStat[i], adcValues[i]);
   //       fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]);
           printf ("%d\t %d \t %d\n", i, adcConvStat[i],adcValues[i]);


				}
			}
			break;
//--------------------------------------------------------------------------------------------------------
case 84:
			int j;
			
            FILE *fp84;
			fp84 = fopen ("IoutVout_84.txt","w");
            printf ("Voltage Regulators 0utputs [uV] for  DAC = 0xFFFF (255) \n");
            fprintf (fp84,"Voltage Regulators 0utputs [uV] for  DAC = 0xFFFF (255) \n");

            if ((status = zasilacz.WriteDacBrdc(handle, elmbNb,3, 0xFFFF, elmbTimeOut)) < 0) { 
					printf ("WriteDacBrdc exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());		
				} 

            for (j=0; j<2; j++) {
       
			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				
for (i=0; i<36; i++) {
		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*CurrentFactor);
//			fprintf (fp, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*CurrentFactor);
            fprintf (fp84, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*CurrentFactor);
            printf ("%d\t %d \t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
}
         printf ("---------------------------------------------\n");
		for (i=36; i<60; i++) {
		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*VoltageFactor);
	fprintf (fp84, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*VoltageFactor);
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
 //    fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor);       

		}
         printf ("---------------------------------------------\n");
		for (i=60; i<64; i++) {
		//	printf ("%d. Conversion Status: %d \t %d\n", 
		//		i, adcConvStat[i], adcValues[i]);
//			fprintf (fp, "%d. Conversion Status: %d \t %d\n",i, adcConvStat[i], adcValues[i]);
          fprintf (fp84, "%d\t %d\t %f\n",i,adcConvStat[i], temperature (adcValues[i]));
           printf ("%d\t %d \t %f\n", i, adcConvStat[i],temperature (adcValues[i]));


		}
			}
			if ((status = zasilacz.WriteDacBrdc(handle, elmbNb,3, 0x0000, elmbTimeOut)) < 0) { 
					printf ("WriteDacBrdc exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());		
				}
			if (j==0){
            printf ("Voltage Regulators 0utputs [uV] for  DAC = 0x0000 (0000) \n");
            fprintf (fp84,"Voltage Regulators 0utputs [uV] for  DAC = 0x0000 (0000) \n");
			}
			}



			fclose (fp84);
			break;

//---------------------------------------------------------------------------------------------------------        
		case 85:
			int end85;
			end85=1;

			printf ("All regulators ON\n");
			if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 3, elmbTimeOut)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}

//-------------------------------------------------------------------------------------------------
			printf ("Test Voltage Regulators Outputs () \n");
while(end85)
{

			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {

	 printf ("------------------------Current-------------------------\n");
            for (i=0; i<36; i++) {
//-------------------Analog Positive
          if (i==00) { printf ("Wheel A1:");}
	      if (i==01) { printf ("Wheel A1:");}

		  if (i==02) { printf ("Wheel A2:");}
		  if (i==03) { printf ("Wheel A2:");}

		  if (i==04) { printf ("Wheel A3:");}
		  if (i==05) { printf ("Wheel A3:");}

		  if (i==06) { printf ("Wheel A4:");}
		  if (i==07) { printf ("Wheel A4:");}

          if (i==8) { printf ("Wheel A5:");}
	      if (i==9) { printf ("Wheel A5:");}

		  if (i==10) { printf ("Wheel A6:");}
		  if (i==11) { printf ("Wheel A6:");}
//------------------Analog Negative

          if (i==12) { printf ("Wheel A1:");}
	      if (i==13) { printf ("Wheel A1:");}

		  if (i==14) { printf ("Wheel A2:");}
		  if (i==15) { printf ("Wheel A2:");}

		  if (i==16) { printf ("Wheel A3:");}
		  if (i==17) { printf ("Wheel A3:");}

		  if (i==18) { printf ("Wheel A4:");}
		  if (i==19) { printf ("Wheel A4:");}

          if (i==20) { printf ("Wheel A5:");}
	      if (i==21) { printf ("Wheel A5:");}

		  if (i==22) { printf ("Wheel A6:");}
		  if (i==23) { printf ("Wheel A6:");}	
//-----------------Digital Positive

          if (i==24) { printf ("Wheel A1:");}
	      if (i==25) { printf ("Wheel A1:");}

		  if (i==26) { printf ("Wheel A2:");}
		  if (i==27) { printf ("Wheel A2:");}

		  if (i==28) { printf ("Wheel A3:");}
		  if (i==29) { printf ("Wheel A3:");}

		  if (i==30) { printf ("Wheel A4:");}
		  if (i==31) { printf ("Wheel A4:");}

          if (i==32) { printf ("Wheel A5:");}
	      if (i==33) { printf ("Wheel A5:");}

		  if (i==34) { printf ("Wheel A6:");}
		  if (i==35) { printf ("Wheel A6:");}		  



		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*CurrentFactor);
//			fprintf (fp, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*CurrentFactor);
//            fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*CurrentFactor);
            printf ("%d\t %d \t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
		}


		 printf ("-------------------------------------------------------------------\n");		

	  for (i=36; i<44; i++) {
		  if (i==36) { printf ("Wheel A1:");}
	      if (i==37) { printf ("Wheel A2:");}
		  if (i==38) { printf ("Wheel A3:");}
		  if (i==39) { printf ("Wheel A4:");}
		  if (i==40) { printf ("Wheel A5:");}
		  if (i==41) { printf ("Wheel A5:");}
		  if (i==42) { printf ("Wheel A6:");}
		  if (i==43) { printf ("Wheel A6:");}
		  
		  

         
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
	  if (2.9>(adcValues[i]*VoltageFactor)|| (adcValues[i]*VoltageFactor)>3.1) {
	  printf ("Voltage Error\n");
			}
      }
 
	  for (i=44; i<52; i++) {
          if (i==44) { printf ("Wheel A1:");}
          if (i==45) { printf ("Wheel A2:");}
		  if (i==46) { printf ("Wheel A3:");}
		  if (i==47) { printf ("Wheel A4:");}
		  if (i==48) { printf ("Wheel A5:");}
		  if (i==49) { printf ("Wheel A5:");}
		  if (i==50) { printf ("Wheel A6:");}
		  if (i==51) { printf ("Wheel A6:");}
	if (-3.7>(adcValues[i]*VoltageFactor)|| (adcValues[i]*VoltageFactor)>-3.5) {
				printf ("Voltage Error_44-51");
      
      }
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
	  /*
      if (-3.7>(adcValues[i]*VoltageFactor)|| (adcValues[i]*VoltageFactor)>-3.5) {
				printf ("Voltage Error\n");
      
      }*/
	    }
	  for (i=52; i<60; i++) {
          if (i==52) { printf ("Wheel A1:");}
          if (i==53) { printf ("Wheel A2:");}
		  if (i==54) { printf ("Wheel A3:");}
		  if (i==55) { printf ("Wheel A4:");}
		  if (i==56) { printf ("Wheel A5:");}
		  if (i==57) { printf ("Wheel A5:");}
		  if (i==58) { printf ("Wheel A6:");}
		  if (i==59) { printf ("Wheel A6:");}
	
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
      if (2.3>(adcValues[i]*VoltageFactor)|| (adcValues[i]*VoltageFactor)>2.5) {
				printf ("Voltage Error\n");
      }
    		
			}
			}
			if(kbhit()) end85=0;
}
	  
			break;

//-----------------------------------------------------------------------------------------
	
	case 86:
		
		int howManyCards;
		int serialCards[8];
		int elmbs[8];
		FILE *burnFiles[8];
		bool initCardOK[8];
		int loops;

		howManyCards = 1;

		serialCards[0] = 1;	   elmbs[0] = 07;
		serialCards[1] = 2;    elmbs[1] = 11;
		serialCards[2] = 999;  elmbs[2] = 11;
		serialCards[3] = 999;  elmbs[3] = 11;
		serialCards[4] = 999;  elmbs[4] = 11;
		serialCards[5] = 999;  elmbs[5] = 11;
		serialCards[6] = 999;  elmbs[6] = 11;
		serialCards[7] = 999;  elmbs[7] = 11;

		   
		

		// ile jest kart
		// jakie adresy ELMB
		// jakie numery seryjne kart

		end86=1;
//----------------------------------Get Time----------------------------------------------------------

	_ftime( &timebuffer );
        newtime  = localtime( &(timebuffer.time) ); 

		printf ("Date and time: %02d.%02d.%4d %02d:%02d:%02d\n",
			newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900,
			newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
		sprintf (timeline, "Date and time: %02d.%02d.%4d %02d:%02d:%02d\n",
			newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900,
			newtime->tm_hour, newtime->tm_min, newtime->tm_sec);

		// Canbus initialization
	if ((handle = canbusOnlyInit (channel)) == -1) { //error in canbus
		printf ("Test end -> INIT with errors\n");
		break;
	}

	for (i = 0; i<howManyCards; i++) {
		sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\burnreport5ohm_%02d%02dH%02d%02d.txt",
				serialCards[i], newtime->tm_mday, newtime->tm_mon+1, newtime->tm_hour, newtime->tm_min);

		if (!(burnFiles[i]=fopen(textBuf,"w"))) {
			printf ("File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
			end86=0;
			break;
		}

//-------------------------------------------------------------------------------------------------
		printf ("Test Voltage Regulators Outputs () \n");
		fprintf(burnFiles[i],"Burn-in Test started:  %s\n",
				timeline);

		// inicjalizacja
		if ((elmbDtmrocInit (handle, burnFiles[i], elmbs[i], elmbTimeOut, portC)) == -1) { //error
			fprintf (burnFiles[i], "Test end -> INIT with errors ELMB nb %d\n", elmbs[i]);
			fclose (burnFiles[i]);
			initCardOK[i] = 0;
           
		}
		else
          initCardOK[i] = 1;
	}
	item = 0;
//--------------------------------------------------------------------------------------------------
    printf ("How many loops  ?(one = 1min)  - ");
	scanf ("%d",&loops );
    
	while(loops-- > 0)	
	{  
		_ftime( &timebuffer );
         newtime  = localtime( &(timebuffer.time) ); 
		 sprintf (timeline, "Date and time: %02d.%02d.%4d %02d:%02d:%02d\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900,
				newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
		for (i = 0; i < howManyCards; i++) {
			if (initCardOK[i]) {
				fprintf (burnFiles[i], "\n--------------------------------------------------------\n");
				fprintf (burnFiles[i], "Loop nb: %d  %s",item++, timeline); 
				if (burnInCheck (handle,loops, elmbs[i], portC, elmbTimeOut, burnFiles[i]))
				{ // over temperature stop program 
					loops = 0;
					// send hardreset
					if (zasilacz.Hardware_Reset_DTMROC(handle, elmbNb, elmbTimeOut) < 0) {
						printf ("Hardware_Reset_DTMROC exit with error\n");
						printf ("%s\n", zasilacz.GetErrorText ());
					}
					printf ("Board temperature exceeded limit: %f.\n", TEMPERATURE_LIMIT);
					printf ("Voltage regulators has been switched off.\n");
					break;
				}
 //               printf ("[i]in burnInCheck %d\n",initCardOK[i]);
			}
            printf ("initCardOK[i] %d\n",initCardOK[i]);//bk
			//Sleep(1000*60);//60s + 27s readout
		}
	}  // while 

	for (i=0; i<howManyCards; i++) {
		if (initCardOK[i]) {
			fprintf (burnFiles[i], "Test end\n");
			fclose (burnFiles[i]);
		}
        printf ("initCardOK[%d] %d\n",i, initCardOK[i]);//bk

	}
	printf ("Test 86 finished.\n");
	
	break;
//----------------------------------------------------------------------------------------
case 87:
		
		int howManyCardsB;
		int serialCardsB[8];
		int elmbsB[8];
		FILE *burnFilesB[8];
		bool testStateB[8];

		howManyCardsB = 1;

		serialCardsB[0] = 1;	elmbsB[0] = 01;
		serialCardsB[1] = 2;    elmbsB[1] = 11;
		serialCardsB[2] = 999;  elmbsB[2] = 11;
		serialCardsB[3] = 999;  elmbsB[3] = 11;
		serialCardsB[4] = 999;  elmbsB[4] = 11;
		serialCardsB[5] = 999;  elmbsB[5] = 11;
		serialCardsB[6] = 999;  elmbsB[6] = 11;
		serialCardsB[7] = 999;  elmbsB[7] = 11;

		   
		

		// ile jest kart
		// jakie adresy ELMB
		// jakie numery seryjne kart

		end86=1;
//----------------------------------Get Time----------------------------------------------------------

	_ftime( &timebuffer );
        newtime  = localtime( &(timebuffer.time) ); 

		printf ("Date and time: %02d.%02d.%4d %02d:%02d:%02d\n",
			newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900,
			newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
        
		sprintf (timeline, "Date and time: %02d.%02d.%4d %02d:%02d:%02d\n",
			newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900,
			newtime->tm_hour, newtime->tm_min, newtime->tm_sec);

// ------------------------------Canbus initialization--------------------------------------------------
	if ((handle = canbusOnlyInit (channel)) == -1) { //error in canbus
		printf ("Test end -> INIT with errors\n");
		break;
	}

	for (i = 0; i<howManyCardsB; i++) {
		sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\burnreport5ohm_%02d%02dH%02d%02d.txt",
				serialCardsB[i], newtime->tm_mday, newtime->tm_mon+1, newtime->tm_hour, newtime->tm_min);

		if (!(burnFilesB[i]=fopen(textBuf,"w"))) {
			printf ("File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
			
			break;
		}

//-------------------------------------------------------------------------------------------------
		printf ("Test Voltage Regulators Outputs () \n");
		fprintf(burnFilesB[i],"Burn-in Test started:  %s\n",
				timeline);

		// inicjalizacja
		if ((elmbDtmrocInit (handle, burnFilesB[i], elmbsB[i], elmbTimeOut, portC)) == -1) { //error
			fprintf (burnFilesB[i], "Test end -> INIT with errors ELMB nb %d\n", elmbsB[i]);
			fclose (burnFilesB[i]);
			testStateB[i] = 0;
           
		}
		else
          testStateB[i] = 1;
	}
	item = 0;
//--------------------------------------------------------------------------------------------------

            printf ("How many loops  ?(one = 1/2 min)  - ");
			scanf ("%d",&loops );


	while(loops--)
	{  
            
			_ftime( &timebuffer );
         newtime  = localtime( &(timebuffer.time) ); 
		 sprintf (timeline, "Date and time: %02d.%02d.%4d %02d:%02d:%02d\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900,
				newtime->tm_hour, newtime->tm_min, newtime->tm_sec);

		for (i = 0; i < howManyCardsB; i++) 
		{
			if (testStateB[i]) 
			{
//				fprintf (burnFilesB[i], "\n--------------------------------------------------------\n");
//				fprintf (burnFilesB[i], "Loop nb: %d  %s",item++, timeline); 
				if (burnInCheckB (handle, elmbsB[i], portC, elmbTimeOut, burnFilesB[i])) 
				{ 
					// stop program
					loops = 0;
					// send hardreset
					if (zasilacz.Hardware_Reset_DTMROC(handle, elmbNb, elmbTimeOut) < 0) 
					{
						printf ("Hardware_Reset_DTMROC exit with error\n");
						printf ("%s\n", zasilacz.GetErrorText ());
					}
					printf ("Board temperature exceeded limit: %f.\n", TEMPERATURE_LIMIT);
					printf ("Voltage regulators has been switched off.\n");
				}
                printf ("testStateB[i]in burnInCheck %d\n",testStateB[i]);
				break;
			}
            printf ("testStateB[i] %d\n",testStateB[i]);//bk
	/*
            printf ("Start Sleep\n");//bk
			Sleep(1000*60);//60s + 27s readout
            printf ("Stop Sleep\n");//bk
	*/
		}
		

	}

	for (i=0; i<howManyCardsB; i++) {
		if (testStateB[i]) {
			fprintf (burnFilesB[i], "Test end\n");
//----------------------------------Get Time----------------------------------------------------------

	_ftime( &timebuffer );
        newtime  = localtime( &(timebuffer.time) ); 

		printf ("Date and time: %02d.%02d.%4d %02d:%02d:%02d\n",
			newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900,
			newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
     
		sprintf (timeline, "Date and time: %02d.%02d.%4d %02d:%02d:%02d\n",
			newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900,
			newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
//--------------------------------------------------------------------------------------------------
		fprintf(burnFilesB[i],"Burn-in Test finished:  %s\n",
				timeline);
			fclose (burnFilesB[i]);
		}
        printf ("testStateB[i] %d\n",testStateB[i]);//bk

	}
	       
	
	break;
//-----------------------------------------------------------------------------------------
case 88:
		
//		int howManyCards;
//		int serialCards[8];
//		int elmbs[8];
//		FILE *burnFiles[8];
		bool testState[8];

		howManyCards = 1;

		serialCards[0] = 1;	   elmbs[0] = 63;
		serialCards[1] = 2;    elmbs[1] = 11;
		serialCards[2] = 999;  elmbs[2] = 11;
		serialCards[3] = 999;  elmbs[3] = 11;
		serialCards[4] = 999;  elmbs[4] = 11;
		serialCards[5] = 999;  elmbs[5] = 11;
		serialCards[6] = 999;  elmbs[6] = 11;
		serialCards[7] = 999;  elmbs[7] = 11;

		   
		

		// ile jest kart
		// jakie adresy ELMB
		// jakie numery seryjne kart

		end86=1;
//----------------------------------Get Time----------------------------------------------------------

	_ftime( &timebuffer );
        newtime  = localtime( &(timebuffer.time) ); 

		printf ("Date and time: %02d.%02d.%4d %02d:%02d:%02d\n",
			newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900,
			newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
		sprintf (timeline, "Date and time: %02d.%02d.%4d %02d:%02d:%02d\n",
			newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900,
			newtime->tm_hour, newtime->tm_min, newtime->tm_sec);

		// Canbus initialization
	if ((handle = canbusOnlyInit (channel)) == -1) { //error in canbus
		printf ("Test end -> INIT with errors\n");
		break;
	}

	for (i = 0; i<howManyCards; i++) {
		sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\burnreport5ohm_%02d%02dH%02d%02d.txt",
				serialCards[i], newtime->tm_mday, newtime->tm_mon+1, newtime->tm_hour, newtime->tm_min);

		if (!(burnFiles[i]=fopen(textBuf,"w"))) {
			printf ("File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
			end86=0;
			break;
		}

//-------------------------------------------------------------------------------------------------
		printf ("Test Voltage Regulators Outputs () \n");
		fprintf(burnFiles[i],"Burn-in Test started:  %s\n",
				timeline);

		// inicjalizacja
		if ((elmbDtmrocInit (handle, burnFiles[i], elmbs[i], elmbTimeOut, portC)) == -1) { //error
			fprintf (burnFiles[i], "Test end -> INIT with errors ELMB nb %d\n", elmbs[i]);
			fclose (burnFiles[i]);
			testState[i] = 0;
           
		}
		else
          testState[i] = 1;
	}
	item = 0;
//--------------------------------------------------------------------------------------------------
//	while(end86)
//	{  
			_ftime( &timebuffer );
         newtime  = localtime( &(timebuffer.time) ); 
		 sprintf (timeline, "Date and time: %02d.%02d.%4d %02d:%02d:%02d\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900,
				newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
		for (i = 0; i < howManyCards; i++) {
			if (testState[i]) {
				fprintf (burnFiles[i], "\n--------------------------------------------------------\n");
				fprintf (burnFiles[i], "Loop nb: %d  %s",item++, timeline); 
				if (burnInCheck (handle,loops, elmbs[i], portC, elmbTimeOut, burnFiles[i])) { // stop program
					end86 = 0;
					// send hardreset
					if (zasilacz.Hardware_Reset_DTMROC(handle, elmbNb, elmbTimeOut) < 0) {
						printf ("Hardware_Reset_DTMROC exit with error\n");
						printf ("%s\n", zasilacz.GetErrorText ());
					}
					printf ("Board temperature exceeded limit: %f.\n", TEMPERATURE_LIMIT);
					printf ("Voltage regulators has been switched off.\n");
				}
                printf ("testState[i]in burnInCheck %d\n",testState[i]);
				break;
			}
            printf ("testState[i] %d\n",testState[i]);//bk
			Sleep(1000*60);//60s + 27s readout
	//		Sleep (1000*60*20);
     //      if(kbhit()) end86=0;

		}
//		if(kbhit()) {
//			end86=0;
  //          fclose (burnFiles[i]);}
fclose (burnFiles[i]);
	

	for (i=0; i<howManyCards; i++) {
		if (testState[i]) {
			fprintf (burnFiles[i], "Test end\n");
			fclose (burnFiles[i]);
		}
        printf ("testState[i] %d\n",testState[i]);//bk

	}	
	break;


//-----------------------------------------------------------------------------------------
		case 90:

            #define VolAnalPos80Max 3.8
			#define VolAnalPos80Min 3.7

            #define VolAnalNeg80Max 3.7
			#define VolAnalNeg80Min 3.5

            #define VolDigPos80Max 3.4
			#define VolDigPos80Min 3.2

			double AnalPosTot; 
			double AnalNegTot;
			double DigTot;
//--------------for 5 Ohm load-----------------
/* moved to lvppCompleteTest.h
			#define AnalPosTotMax  4.3 
            #define AnalPosTotMin  4.1
			#define AnalNegTotMax  5.1
			#define AnalNegTotMin  4.8
			#define DigTotMax  3.2
			#define DigTotMin  3.0
*/

//--------------for 2 Ohm load-----------------
/*			#define AnalPosTotMax  10.8 
            #define AnalPosTotMin  10.2
			#define AnalNegTotMax  12.8
			#define AnalNegTotMin  12.2
			#define DigTotMax  7.8
			#define DigTotMin  7.2
*/
//            double currentpedestal[36];
            FILE *fp90;
//			fp90 = fopen ("IoutVout_90.txt","w");

			/*
            printf ("Voltage Regulators 0utputs [uV] for  DAC = 0xFFFF (255) \n");
            fprintf (fp80,"Voltage Regulators 0utputs [uV] for  DAC = 0xFFFF (255) \n");

            if ((status = zasilacz.WriteDacBrdc(handle, elmbNb,3, 0xFFFF, portC, elmbTimeOut)) < 0) { 
					printf ("WriteDacBrdc exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());		
				} 

           */
//-----------------------------------------------------------------------------------------------------
			printf ("Card Serial Number: ");  // read calibration file
			scanf ("%d", &cardNb);

			// create log file name
			sprintf (textBuf, "C:\\LVPP_TESTS\\Test Reports\\Test_90_91\\IoutVout_90_%03d.txt", cardNb);
			fp90 = fopen (textBuf,"w");

			// create calibfilename
			sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\LVPP_CALIB_%03d.csv", cardNb, cardNb);
			
			if ((calibFile = fopen (textBuf, "r")) == NULL) {
				printf ("Calibration file does not exist for card %d - please make calibration\n", cardNb); 
				break;
			}

			readCalibration(calibFile, calibCoeff, calibCurrentOffset, 36);
			
			fclose (calibFile);
//********Write to DAC 0x8080 *************************************************************
/*				
                 if (checkWriteBroadcast (handle, elmbNb, 2, 0x8080, portC, elmbTimeOut, NULL)) 
					printf ("Error when setting DACS 2\n");
				if (checkWriteBroadcast (handle, elmbNb, 3, 0x8080, portC, elmbTimeOut, NULL)) 
                 printf ("Error when setting DACS 3\n");
*/
//*****************read ADC *****************************************************

			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {

				printf ("---------------------------------------------\n");
				fprintf (fp90, "----------------------------------------\n");
//                            printf ("Set ELMB Number =  "); scanf ("%d", &elmbNb);
                fprintf (fp90, "Subprogram 90 \n");
               fprintf (fp90, "LVPP WA = %d\t \n",cardNb);
			}
//-----------------------------------------------------------------------------------------------------------


          for (i=0; i<36; i++) {				
	
			  currentpedestal[i] = (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000;

		                       }

//------------------------------------------------------------------------------------------------------------       
			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());

			}
			else {
              AnalPosTot=
				             (adcValues[0]*CurrentFactor-currentpedestal[0])+
                             (adcValues[1]*CurrentFactor-currentpedestal[1])+
				             (adcValues[2]*CurrentFactor-currentpedestal[2])+
                             (adcValues[3]*CurrentFactor-currentpedestal[3])+
                             (adcValues[4]*CurrentFactor-currentpedestal[4])+ 
                             (adcValues[5]*CurrentFactor-currentpedestal[5])+
                             (adcValues[6]*CurrentFactor-currentpedestal[6])+
                             (adcValues[7]*CurrentFactor-currentpedestal[7])+
                             (adcValues[8]*CurrentFactor-currentpedestal[8])+
                             (adcValues[9]*CurrentFactor-currentpedestal[9])+
                             (adcValues[10]*CurrentFactor-currentpedestal[10])+
                             (adcValues[11]*CurrentFactor-currentpedestal[11]);
/*
                if ((fabs(AnalPosTot) > 4.3) || (fabs(AnalPosTot) < 4.1)) 
	
				{printf ("Error:  Analog Positive Total [A]: %f\n",AnalPosTot);
*/

                if ((fabs(AnalPosTot) > AnalPosTotMax) || (fabs(AnalPosTot) < AnalPosTotMin)) 
	
				{printf ("Error:  Analog Positive Total(41.-4.3) [A]: %f\n",AnalPosTot);

                  for(freqency=0; freqency<2500; freqency=freqency+1000)
			      {
			      Beep(freqency,100);
				  }


				}


 	/*			
                if (((fabs(AnalPosTot)) < 4.4) ||((fabs(AnalPosTot)) > 4.0))
	
				{ printf ("Current Error in channel !!!!!!!!!!! %f \n, AnalPosTot");}

	*/			  

							 
            printf ("Analog Positive Total(41.-4.3)[A]: %f\n",AnalPosTot);
            fprintf (fp90, "Analog Positive Total(41.-4.3) [A] = %f\t \n",AnalPosTot);
/*
			printf ("Analog Positive Total [A]: %f\n",( 
				             adcValues[0]*CurrentFactor+
				             adcValues[2]*CurrentFactor+
                             adcValues[4]*CurrentFactor+ 
                             adcValues[6]*CurrentFactor+
                             adcValues[8]*CurrentFactor+
                             adcValues[9]*CurrentFactor+
                             adcValues[10]*CurrentFactor+
                             adcValues[11]*CurrentFactor)
				   );
*/
             printf ("---------------------------------------------\n");


          for (i=0; i<12; i=i+2) {
             fprintf (fp90, " %d\t %f\t %f\t %f\t \n",i,
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]), 
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
            printf ("%d\t %d\t %f\t %f\t %f\t \n", i, adcConvStat[i],
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]),
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
           }
// for WB/Barrel
		  /*
            for (i=8; i<12; i=i+1) {
            fprintf (fp90, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);}
*/
           printf ("\n");
              AnalNegTot=
	/*			             adcValues[12]*CurrentFactor+
				             adcValues[14]*CurrentFactor+
                             adcValues[16]*CurrentFactor+ 
                             adcValues[18]*CurrentFactor+
                             adcValues[20]*CurrentFactor+
                             adcValues[21]*CurrentFactor+
                             adcValues[22]*CurrentFactor+
                             adcValues[23]*CurrentFactor; */
                               
			                 (adcValues[12]*CurrentFactor-currentpedestal[12])+
                             (adcValues[13]*CurrentFactor-currentpedestal[13])+
				             (adcValues[14]*CurrentFactor-currentpedestal[14])+
                             (adcValues[15]*CurrentFactor-currentpedestal[15])+
                             (adcValues[16]*CurrentFactor-currentpedestal[16])+ 
                             (adcValues[17]*CurrentFactor-currentpedestal[17])+
                             (adcValues[18]*CurrentFactor-currentpedestal[18])+
                             (adcValues[19]*CurrentFactor-currentpedestal[19])+
                             (adcValues[20]*CurrentFactor-currentpedestal[20])+
                             (adcValues[21]*CurrentFactor-currentpedestal[21])+
                             (adcValues[22]*CurrentFactor-currentpedestal[22])+
                             (adcValues[23]*CurrentFactor-currentpedestal[23]);

                if ((fabs(AnalNegTot) > AnalNegTotMax) || (fabs(AnalNegTot) < AnalNegTotMin)) 
	
				{printf ("Error:  Analog Negative Total(4.8-5.1) [A]: %f\n",AnalNegTot);
				}



            printf ("Analog Negative Total (4.8-5.1)[A]:  %f\n",AnalNegTot);
            fprintf (fp90, "Analog Negative Total (4.8-5.1)[A] = %f\t \n",AnalNegTot);
/*
           printf ("Analog Negative Total [A]: %f\n",(
			                 adcValues[12]*CurrentFactor+
				             adcValues[14]*CurrentFactor+
                             adcValues[16]*CurrentFactor+ 
                             adcValues[18]*CurrentFactor+
                             adcValues[20]*CurrentFactor+
                             adcValues[21]*CurrentFactor+
                             adcValues[22]*CurrentFactor+
                             adcValues[23]*CurrentFactor)
				   );
*/
          printf ("---------------------------------------------\n");

          for (i=12; i<24; i=i+2) {
             fprintf (fp90, " %d\t %f\t %f\t %f\t \n",i,
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]), 
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
            printf ("%d\t %d\t %f\t %f\t %f\t \n", i, adcConvStat[i],
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]),
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
           }
/*
            for (i=12; i<24; i=i+2) {
            fprintf (fp90, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
           }

            for (i=20; i<24; i=i+1) {
            fprintf (fp90, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f \n ", i, adcConvStat[i], adcValues[i]*CurrentFactor); }
*/
            printf ("\n");
           DigTot=
	/*		                 adcValues[24]*CurrentFactor+
				             adcValues[26]*CurrentFactor+
                             adcValues[28]*CurrentFactor+ 
                             adcValues[30]*CurrentFactor+
                             adcValues[32]*CurrentFactor+
                             adcValues[33]*CurrentFactor+
                             adcValues[34]*CurrentFactor+
                             adcValues[35]*CurrentFactor;
*/
			                 (adcValues[24]*CurrentFactor-currentpedestal[24])+
                             (adcValues[25]*CurrentFactor-currentpedestal[25])+
				             (adcValues[26]*CurrentFactor-currentpedestal[26])+
                             (adcValues[27]*CurrentFactor-currentpedestal[27])+
                             (adcValues[28]*CurrentFactor-currentpedestal[28])+ 
                             (adcValues[29]*CurrentFactor-currentpedestal[29])+
                             (adcValues[30]*CurrentFactor-currentpedestal[30])+
                             (adcValues[31]*CurrentFactor-currentpedestal[31])+
                             (adcValues[32]*CurrentFactor-currentpedestal[32])+
                             (adcValues[33]*CurrentFactor-currentpedestal[33])+
                             (adcValues[34]*CurrentFactor-currentpedestal[34])+
                             (adcValues[35]*CurrentFactor-currentpedestal[35]);

                if ((fabs(DigTot) > DigTotMax) || (fabs(DigTot) < DigTotMin)) 
	
				{printf ("Error:  Digital Total(3.0-3.2) [A]: %f\n",DigTot);
				}



            printf ("Digital Total (3.0-3.2)[A]: %f\n",DigTot);
            fprintf (fp90, " Digital Total (3.0-3.2)[A]= %f\t \n",DigTot);
/*

           printf ("Digital Positive Total [A]: %f\n",(
			                 adcValues[24]*CurrentFactor+
				             adcValues[26]*CurrentFactor+
                             adcValues[28]*CurrentFactor+ 
                             adcValues[30]*CurrentFactor+
                             adcValues[32]*CurrentFactor+
                             adcValues[33]*CurrentFactor+
                             adcValues[34]*CurrentFactor+
                             adcValues[35]*CurrentFactor)
				   );
*/
         printf ("---------------------------------------------\n");
/*
  		   for (i=24; i<32; i=i+2) {
            fprintf (fp90, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
           }
            for (i=32; i<36; i=i+1) {
            fprintf (fp90, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
           }
*/
//---------------------------------" %d\t %f\t \n",i,
            for (i=24; i<36; i=i+2) {
            fprintf (fp90, " %d\t %f\t %f\t %f\t \n",i,
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]), 
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
            printf ("%d\t %d\t %f\t %f\t %f\t \n", i, adcConvStat[i],
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]),
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
           }

           printf ("\n");
//----------------------------------------------------------------------------------------------------------
         printf ("Outputs [V]\n");
         printf ("---------------------------------------------\n");
         fprintf (fp90, "Vout Analog Positive,Negative,Digital [V] = \n");
//------------------------------------------------------------------------------------------
for (i=36; i<44; i++) {
	
      fprintf (fp90, " %d \t %f\n",i, adcValues[i]*VoltageFactor);
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);

                if ((fabs(adcValues[i]*VoltageFactor) > VolAnalPos80Max) || 
				    (fabs(adcValues[i]*VoltageFactor) < VolAnalPos80Min)) 
	
				{printf ("Error: Output Voltage !! \n");
				}
}
//------------------------------------------------------------------------------------------------
for (i=44; i<52; i++) {
	
      fprintf (fp90, " %d \t %f\n",i, adcValues[i]*VoltageFactor);
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);

                if ((fabs(adcValues[i]*VoltageFactor) > VolAnalNeg80Max) || 
				    (fabs(adcValues[i]*VoltageFactor) < VolAnalNeg80Min)) 
	
				{printf ("Error: Output Voltage !! \n");
				}
}
//------------------------------------------------------------------------------------------------
for (i=52; i<60; i++) {
	
      fprintf (fp90, " %d \t %f\n",i, adcValues[i]*VoltageFactor);
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);

                if ((fabs(adcValues[i]*VoltageFactor) > VolDigPos80Max) || 
				    (fabs(adcValues[i]*VoltageFactor) < VolDigPos80Min)) 
	
				{printf ("Error: Output Voltage !! \n");
				}
}
//------------------------------------------------------------------------------------------------
/*
for (i=36; i<60; i++) {
		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*VoltageFactor);
//	fprintf (fp80, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*VoltageFactor);
	
      fprintf (fp90, " %d \t %f\n",i, adcValues[i]*VoltageFactor);
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
 //    fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor);       
}
*/
//----------------------------------------------------------------------------------------------------
         printf ("---------------------------------------------\n");
		for (i=60; i<64; i++) {
		//	printf ("%d. Conversion Status: %d \t %d\n", 
		//		i, adcConvStat[i], adcValues[i]);
//			fprintf (fp, "%d. Conversion Status: %d \t %d\n",i, adcConvStat[i], adcValues[i]);
          fprintf (fp90, "%d\t %d\t %f\n",i,adcConvStat[i], temperature (adcValues[i]));
           printf ("%d\t %d \t %f\n", i, adcConvStat[i],temperature (adcValues[i]));
		}
			}
			
			fclose (fp90);
			break;
//------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
		case 91:
			
			double AnalPosTot_B; 
			double AnalNegTot_B;
			double DigTot_B;

//--------------for 5 Ohm load-----------------
/* moved to lvppTest.h
			#define AnalPosTotMax_B  5.8 
            #define AnalPosTotMin_B  5.4
			#define AnalNegTotMax_B  6.6
			#define AnalNegTotMin_B  6.3
			#define DigTotMax_B  4.4
			#define DigTotMin_B  4.0
*/

//--------------for 2 Ohm load-----------------
/*			#define AnalPosTotMax  10.8 
            #define AnalPosTotMin  10.2
			#define AnalNegTotMax  12.8
			#define AnalNegTotMin  12.2
			#define DigTotMax  7.8
			#define DigTotMin  7.2
*/
//            double currentpedestal[36];
            FILE *fp91;
			
			/*
            printf ("Voltage Regulators 0utputs [uV] for  DAC = 0xFFFF (255) \n");
            fprintf (fp80,"Voltage Regulators 0utputs [uV] for  DAC = 0xFFFF (255) \n");

            if ((status = zasilacz.WriteDacBrdc(handle, elmbNb,3, 0xFFFF, portC, elmbTimeOut)) < 0) { 
					printf ("WriteDacBrdc exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());		
				} 

           */
//-----------------------------------------------------------------------------------------------------
			printf ("Card Serial Number: ");  // read calibration file
			scanf ("%d", &cardNb);


			printf ("Outputs of WB/Barrel :\n ");  // !!!!!!!!!!!!!!!!!!!!!!

			// create log file name
			sprintf (textBuf, "C:\\LVPP_TESTS\\Test Reports\\Test_90_91\\IoutVout_91_%03d.txt", cardNb);
			fp91 = fopen (textBuf,"w");

			// create calibfilename
			sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\LVPP_CALIB_%03d.csv", cardNb, cardNb);
			
			if ((calibFile = fopen (textBuf, "r")) == NULL) {
				printf ("Calibration file does not exist for card %d - please make calibration\n", cardNb); 
				break;
			}

			readCalibration(calibFile, calibCoeff, calibCurrentOffset, 36);
			
			fclose (calibFile);
/*
//****************************************get DAC****************************
			printf ("DAC  (hex)0xXXYY : ");
			scanf ("%x", &dac);


//********Write to DAC 0x8080 *************************************************************
				if (checkWriteBroadcast (handle, elmbNb, 2, dac, portC, elmbTimeOut, NULL)) 
					printf ("Error when setting DACS 2\n");
				if (checkWriteBroadcast (handle, elmbNb, 3, dac, portC, elmbTimeOut, NULL)) 
					printf ("Error when setting DACS 3\n");
//*****************read ADC *****************************************************
*/
			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {

				printf ("---------------------------------------------\n");
				fprintf (fp91, "----------------------------------------\n");
			}
//-----------------------------------------------------------------------------------------------------------


          for (i=0; i<36; i++) {				
	
			  currentpedestal[i] = (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000;

		                       }

//------------------------------------------------------------------------------------------------------------       
			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());

			}
			else {
              AnalPosTot_B=
				             (adcValues[0]*CurrentFactor-currentpedestal[0])+
 //                            (adcValues[1]*CurrentFactor-currentpedestal[1])+
				             (adcValues[2]*CurrentFactor-currentpedestal[2])+
   //                          (adcValues[3]*CurrentFactor-currentpedestal[3])+
                             (adcValues[4]*CurrentFactor-currentpedestal[4])+ 
     //                        (adcValues[5]*CurrentFactor-currentpedestal[5])+
                             (adcValues[6]*CurrentFactor-currentpedestal[6])+
       //                      (adcValues[7]*CurrentFactor-currentpedestal[7])+
                             (adcValues[8]*CurrentFactor-currentpedestal[8])+
                             (adcValues[9]*CurrentFactor-currentpedestal[9])+
                             (adcValues[10]*CurrentFactor-currentpedestal[10])+
                             (adcValues[11]*CurrentFactor-currentpedestal[11]);
/*
                if ((fabs(AnalPosTot) > 4.3) || (fabs(AnalPosTot) < 4.1)) 
	
				{printf ("Error:  Analog Positive Total [A]: %f\n",AnalPosTot);
*/

                if ((fabs(AnalPosTot_B) > AnalPosTotMax_B) || (fabs(AnalPosTot_B) < AnalPosTotMin_B)) 
	
				{printf ("Error:  Analog Positive Total_B(5.4.-5.8)[A]:%f\t %f\t %f\n",AnalPosTotMin_B, AnalPosTotMax_B,AnalPosTot_B);

                  for(freqency=0; freqency<2500; freqency=freqency+1000)
			      {
			      Beep(freqency,100);
				  }


				}


 	/*			
                if (((fabs(AnalPosTot)) < 4.4) ||((fabs(AnalPosTot)) > 4.0))
	
				{ printf ("Current Error in channel !!!!!!!!!!! %f \n, AnalPosTot");}

	*/			  

							 
            printf ("Analog Positive Total_B[A]:%f\t %f\t %f\n",AnalPosTotMin_B, AnalPosTotMax_B,AnalPosTot_B);
            fprintf (fp91, "Analog Positive Total_B(5.4 - 5.8)[A] = %f\t \n",AnalPosTot_B);
/*
			printf ("Analog Positive Total [A]: %f\n",( 
				             adcValues[0]*CurrentFactor+
				             adcValues[2]*CurrentFactor+
                             adcValues[4]*CurrentFactor+ 
                             adcValues[6]*CurrentFactor+
                             adcValues[8]*CurrentFactor+
                             adcValues[9]*CurrentFactor+
                             adcValues[10]*CurrentFactor+
                             adcValues[11]*CurrentFactor)
				   );
*/
             printf ("---------------------------------------------\n");


          for (i=0; i<8; i=i+2) {
            fprintf (fp91, " %d\t %f\t \n",i, (adcValues[i]*CurrentFactor-currentpedestal[i]));
            printf ("%d\t %d\t %f\n", i, adcConvStat[i],
			adcValues[i]*CurrentFactor-currentpedestal[i]);
           }
// for WB/Barrel
		  
            for (i=8; i<12; i=i+1) {
            fprintf (fp91, " %d\t %f\t \n",i, (adcValues[i]*CurrentFactor-currentpedestal[i]));
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], (adcValues[i]*CurrentFactor-currentpedestal[i]));
			                        }

           printf ("\n");
              AnalNegTot_B=
	/*			             adcValues[12]*CurrentFactor+
				             adcValues[14]*CurrentFactor+
                             adcValues[16]*CurrentFactor+ 
                             adcValues[18]*CurrentFactor+
                             adcValues[20]*CurrentFactor+
                             adcValues[21]*CurrentFactor+
                             adcValues[22]*CurrentFactor+
                             adcValues[23]*CurrentFactor; */
                               
			                 (adcValues[12]*CurrentFactor-currentpedestal[12])+
 //                            (adcValues[13]*CurrentFactor-currentpedestal[13])+
				             (adcValues[14]*CurrentFactor-currentpedestal[14])+
   //                          (adcValues[15]*CurrentFactor-currentpedestal[15])+
                             (adcValues[16]*CurrentFactor-currentpedestal[16])+ 
     //                        (adcValues[17]*CurrentFactor-currentpedestal[17])+
                             (adcValues[18]*CurrentFactor-currentpedestal[18])+
       //                      (adcValues[19]*CurrentFactor-currentpedestal[19])+
                             (adcValues[20]*CurrentFactor-currentpedestal[20])+
                             (adcValues[21]*CurrentFactor-currentpedestal[21])+
                             (adcValues[22]*CurrentFactor-currentpedestal[22])+
                             (adcValues[23]*CurrentFactor-currentpedestal[23]);

                if ((fabs(AnalNegTot_B) > AnalNegTotMax_B) || (fabs(AnalNegTot_B) < AnalNegTotMin_B)) 
	
				{printf ("Error:  Analog Negative Total_B [A]:%f\t %f\t %f\n", AnalNegTotMin_B,AnalNegTotMax_B,AnalNegTot_B);
				}



            printf ("Analog Negative Total_B [A]: %f\t %f\t %f\n", AnalNegTotMin_B,AnalNegTotMax_B,AnalNegTot_B);
			fprintf (fp91, "Analog Negative Total_B [A]: %f\t %f\t %f\n", AnalNegTotMin_B,AnalNegTotMax_B,AnalNegTot_B);
/*
           printf ("Analog Negative Total [A]: %f\n",(
			                 adcValues[12]*CurrentFactor+
				             adcValues[14]*CurrentFactor+
                             adcValues[16]*CurrentFactor+ 
                             adcValues[18]*CurrentFactor+
                             adcValues[20]*CurrentFactor+
                             adcValues[21]*CurrentFactor+
                             adcValues[22]*CurrentFactor+
                             adcValues[23]*CurrentFactor)
				   );
*/
          printf ("---------------------------------------------\n");

          for (i=12; i<20; i=i+2) {
            fprintf (fp91, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor-currentpedestal[i]);
                  printf("%d\t %d\t %f\n", i, adcConvStat[i],adcValues[i]*CurrentFactor-currentpedestal[i]);
           }

            for (i=20; i<24; i=i+1) {
            fprintf (fp91, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor-currentpedestal[i]);
            printf ("%d\t %d\t %f \n ", i, adcConvStat[i], adcValues[i]*CurrentFactor-currentpedestal[i]); }

            printf ("\n");
           DigTot_B=
	/*		                 adcValues[24]*CurrentFactor+
				             adcValues[26]*CurrentFactor+
                             adcValues[28]*CurrentFactor+ 
                             adcValues[30]*CurrentFactor+
                             adcValues[32]*CurrentFactor+
                             adcValues[33]*CurrentFactor+
                             adcValues[34]*CurrentFactor+
                             adcValues[35]*CurrentFactor;
*/
			                 (adcValues[24]*CurrentFactor-currentpedestal[24])+
//                             (adcValues[25]*CurrentFactor-currentpedestal[25])+
				             (adcValues[26]*CurrentFactor-currentpedestal[26])+
  //                           (adcValues[27]*CurrentFactor-currentpedestal[27])+
                             (adcValues[28]*CurrentFactor-currentpedestal[28])+ 
    //                         (adcValues[29]*CurrentFactor-currentpedestal[29])+
                             (adcValues[30]*CurrentFactor-currentpedestal[30])+
      //                       (adcValues[31]*CurrentFactor-currentpedestal[31])+
                             (adcValues[32]*CurrentFactor-currentpedestal[32])+
                             (adcValues[33]*CurrentFactor-currentpedestal[33])+
                             (adcValues[34]*CurrentFactor-currentpedestal[34])+
                             (adcValues[35]*CurrentFactor-currentpedestal[35]);

                if ((fabs(DigTot_B) > DigTotMax_B) || (fabs(DigTot_B) < DigTotMin_B)) 
	
				{printf ("Error:  Digital Total_B [A]:%f\t %f\t %f\n",DigTotMin_B,DigTotMax_B,DigTot_B);
				}



            printf ("Digital Total_B [A]: %f\t %f\t %f\n",DigTotMin_B,DigTotMax_B,DigTot_B);
            fprintf (fp91, " Digital Total_B [A]=%f\t %f\t %f\n",DigTotMin_B,DigTotMax_B,DigTot_B);
/*

           printf ("Digital Positive Total [A]: %f\n",(
			                 adcValues[24]*CurrentFactor+
				             adcValues[26]*CurrentFactor+
                             adcValues[28]*CurrentFactor+ 
                             adcValues[30]*CurrentFactor+
                             adcValues[32]*CurrentFactor+
                             adcValues[33]*CurrentFactor+
                             adcValues[34]*CurrentFactor+
                             adcValues[35]*CurrentFactor)
				   );
*/
         printf ("---------------------------------------------\n");

  		   for (i=24; i<32; i=i+2) {
            fprintf (fp91, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor-currentpedestal[i]);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor-currentpedestal[i]);
           }
            for (i=32; i<36; i=i+1) {
            fprintf (fp91, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor-currentpedestal[i]);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor-currentpedestal[i]);
           }

/*
            for (i=24; i<36; i=i+2) {
            fprintf (fp91, " %d\t %f\t \n",i, (adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
            printf ("%d\t %d\t %f\t %f\t %f\n", i, adcConvStat[i],
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]),
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
           }
*/
           printf ("\n");
//----------------------------------------------------------------------------------------------------------
         printf ("Outputs [V]\n");
         printf ("---------------------------------------------\n");
         fprintf (fp91, "Analog Positive Vout [V] = \n");
for (i=36; i<60; i++) {
		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*VoltageFactor);
//	fprintf (fp80, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*VoltageFactor);
	
    fprintf (fp91, " %d \t %f\n",i, adcValues[i]*VoltageFactor);
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
 //    fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor);       

		}
         printf ("---------------------------------------------\n");
		for (i=60; i<64; i++) {
		//	printf ("%d. Conversion Status: %d \t %d\n", 
		//		i, adcConvStat[i], adcValues[i]);
//			fprintf (fp, "%d. Conversion Status: %d \t %d\n",i, adcConvStat[i], adcValues[i]);
          fprintf (fp91, "%d\t %d\t %f\n",i,adcConvStat[i], temperature (adcValues[i]));
           printf ("%d\t %d \t %f\n", i, adcConvStat[i],temperature (adcValues[i]));
		}
			}
			
			fclose (fp91);
			break;
//------------------------------------------------------------------------------------------------

//----------------------------------------------------------
// for current division

   case 92:

int loop92;


//			double AnalPosTot; 
//			double AnalNegTot;
//			double DigTot;
//--------------for 5 Ohm load-----------------
/* moved to lvppCompleteTest.h 
 
			#define AnalPosTotMax  4.3 
            #define AnalPosTotMin  4.1
			#define AnalNegTotMax  5.1
			#define AnalNegTotMin  4.8
			#define DigTotMax  3.2
			#define DigTotMin  3.0
*/
//--------------for 2 Ohm load-----------------
/*			#define AnalPosTotMax  10.8 
            #define AnalPosTotMin  10.2
			#define AnalNegTotMax  12.8
			#define AnalNegTotMin  12.2
			#define DigTotMax  7.8
			#define DigTotMin  7.2
*/
//            double currentpedestal[36];
            FILE *fp92;
//			fp92 = fopen ("IoutVout_92.txt","w");

			/*
            printf ("Voltage Regulators 0utputs [uV] for  DAC = 0xFFFF (255) \n");
            fprintf (fp80,"Voltage Regulators 0utputs [uV] for  DAC = 0xFFFF (255) \n");

            if ((status = zasilacz.WriteDacBrdc(handle, elmbNb,3, 0xFFFF, portC, elmbTimeOut)) < 0) { 
					printf ("WriteDacBrdc exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());		
				} 

           */



//-----------------------------------------------------------------------------------------------------
			printf ("Card Serial Number: ");  // read calibration file
			scanf ("%d", &cardNb);

for  (loop92=0; loop92<=20; loop92++){

			// create log file name
			sprintf (textBuf, "C:\\LVPP_TESTS\\Test Reports\\Test_90_91_92\\IoutVout_92_%03d.txt", cardNb);
			fp92 = fopen (textBuf,"w");

			// create calibfilename
			sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\LVPP_CALIB_%03d.csv", cardNb, cardNb);
			
			if ((calibFile = fopen (textBuf, "r")) == NULL) {
				printf ("Calibration file does not exist for card %d - please make calibration\n", cardNb); 
				break;
			}

			readCalibration(calibFile, calibCoeff, calibCurrentOffset, 36);
			
			fclose (calibFile);
//********Write to DAC 0x8080 *************************************************************
/*				
                 if (checkWriteBroadcast (handle, elmbNb, 2, 0x8080, portC, elmbTimeOut, NULL)) 
					printf ("Error when setting DACS 2\n");
				if (checkWriteBroadcast (handle, elmbNb, 3, 0x8080, portC, elmbTimeOut, NULL)) 
                 printf ("Error when setting DACS 3\n");
*/

//*****************read ADC *****************************************************


			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {

				printf ("---------------------------------------------\n");
				fprintf (fp92, "----------------------------------------\n");
			}
//-----------------------------------------------------------------------------------------------------------


          for (i=0; i<36; i++) {				
	
			  currentpedestal[i] = (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000;

		                       }

//------------------------------------------------------------------------------------------------------------       
			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());

			}
			else {
              AnalPosTot=
				             (adcValues[0]*CurrentFactor-currentpedestal[0])+
                             (adcValues[1]*CurrentFactor-currentpedestal[1])+
				             (adcValues[2]*CurrentFactor-currentpedestal[2])+
                             (adcValues[3]*CurrentFactor-currentpedestal[3])+
                             (adcValues[4]*CurrentFactor-currentpedestal[4])+ 
                             (adcValues[5]*CurrentFactor-currentpedestal[5])+
                             (adcValues[6]*CurrentFactor-currentpedestal[6])+
                             (adcValues[7]*CurrentFactor-currentpedestal[7])+
                             (adcValues[8]*CurrentFactor-currentpedestal[8])+
                             (adcValues[9]*CurrentFactor-currentpedestal[9])+
                             (adcValues[10]*CurrentFactor-currentpedestal[10])+
                             (adcValues[11]*CurrentFactor-currentpedestal[11]);
/*
                if ((fabs(AnalPosTot) > 4.3) || (fabs(AnalPosTot) < 4.1)) 
	
				{printf ("Error:  Analog Positive Total [A]: %f\n",AnalPosTot);
*/

                if ((fabs(AnalPosTot) > AnalPosTotMax) || (fabs(AnalPosTot) < AnalPosTotMin)) 
	
				{printf ("Error:  Analog Positive Total[A] : %f\n",AnalPosTot);

                  for(freqency=0; freqency<2500; freqency=freqency+1000)
			      {
			      Beep(freqency,100);
				  }


				}


 	/*			
                if (((fabs(AnalPosTot)) < 4.4) ||((fabs(AnalPosTot)) > 4.0))
	
				{ printf ("Current Error in channel !!!!!!!!!!! %f \n, AnalPosTot");}

	*/			  

							 
            printf ("Analog Positive Total: %f\n",AnalPosTot);
			fprintf (fp92, "Analog Positive Total [A]: %f\t \n",AnalPosTot);
/*
			printf ("Analog Positive Total [A]: %f\n",( 
				             adcValues[0]*CurrentFactor+
				             adcValues[2]*CurrentFactor+
                             adcValues[4]*CurrentFactor+ 
                             adcValues[6]*CurrentFactor+
                             adcValues[8]*CurrentFactor+
                             adcValues[9]*CurrentFactor+
                             adcValues[10]*CurrentFactor+
                             adcValues[11]*CurrentFactor)
				   );
*/
           printf ("---------------------------------------------\n");
//------------------------------------------------------------------------------------------------------------
/*
          for (i=0; i<12; i=i+2) {
            fprintf (fp92, " %d\t %f\t %f\t %f\t\n",i,
            adcValues[i]*CurrentFactor-currentpedestal[i],adcValues[i+1]*CurrentFactor-currentpedestal[i+1],
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
            printf ("%d\t %d\t %f\t %f\t %f\n", i, adcConvStat[i],
			adcValues[i]*CurrentFactor-currentpedestal[i],adcValues[i+1]*CurrentFactor-currentpedestal[i+1], 
			(adcValues[i]*CurrentFactor-currentpedestal[i])	+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
		  }
*/
// for WB/Barrel
		  /*
            for (i=8; i<12; i=i+1) {
            fprintf (fp92, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);}
*/
//--------------------------------------------------------------------------------------------------
//---------------------------------------Start current division if i(i)>i(i+1)
            for (i=0; i<11; i=i+2) {

if ((adcValues[i]*CurrentFactor-currentpedestal[i]) > (adcValues[i+1]*CurrentFactor-currentpedestal[i+1]))
{
 //printf (" Start current division\n");
             if (i==0)  (dtmroc=1);
             if (i==2)  (dtmroc=2);
             if (i==4)  (dtmroc=3);
             if (i==6)  (dtmroc=4);
	         if (i==8)  (dtmroc=5);
             if (i==10) (dtmroc=6);
         

	            if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, 3, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("DAC 0 1: 0x%X\n", dacRead);
			dac = dacRead - 0x0001;

            
			if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmroc, 3, dac, elmbTimeOut)) < 0) {
				printf ("Write exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}

			
}
// printf (" Start current division\n");


//Start current division if i(i)<i(i+1)

if ((adcValues[i]*CurrentFactor-currentpedestal[i]) < (adcValues[i+1]*CurrentFactor-currentpedestal[i+1]))
{
// printf (" Start current division\n");
             if (i==0)  (dtmroc=1);
             if (i==2)  (dtmroc=2);
             if (i==4)  (dtmroc=3);
             if (i==6)  (dtmroc=4);
	         if (i==8)  (dtmroc=5);
             if (i==10) (dtmroc=6);
         

	            if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, 3, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("DAC 2 3: 0x%X\n", dacRead);
			    dac = dacRead + 0x0001;

            
			if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmroc, 3, dac, elmbTimeOut)) < 0) {
				printf ("Write exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}

			
                                           }

//-----------------------------------------------------Stop current division

            fprintf (fp92, " %d\t %f\t %f\t %f\t\n",i,
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]),
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
            printf ("%d\t %d\t %f\t %f\t %f\n", i, adcConvStat[i],
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]),
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
			}

           printf ("\n");
//----------------------------------------------------------------------------------------------------------
           printf ("\n");
              AnalNegTot=
	/*			             adcValues[12]*CurrentFactor+
				             adcValues[14]*CurrentFactor+
                             adcValues[16]*CurrentFactor+ 
                             adcValues[18]*CurrentFactor+
                             adcValues[20]*CurrentFactor+
                             adcValues[21]*CurrentFactor+
                             adcValues[22]*CurrentFactor+
                             adcValues[23]*CurrentFactor; */
                               
			                 (adcValues[12]*CurrentFactor-currentpedestal[12])+
                             (adcValues[13]*CurrentFactor-currentpedestal[13])+
				             (adcValues[14]*CurrentFactor-currentpedestal[14])+
                             (adcValues[15]*CurrentFactor-currentpedestal[15])+
                             (adcValues[16]*CurrentFactor-currentpedestal[16])+ 
                             (adcValues[17]*CurrentFactor-currentpedestal[17])+
                             (adcValues[18]*CurrentFactor-currentpedestal[18])+
                             (adcValues[19]*CurrentFactor-currentpedestal[19])+
                             (adcValues[20]*CurrentFactor-currentpedestal[20])+
                             (adcValues[21]*CurrentFactor-currentpedestal[21])+
                             (adcValues[22]*CurrentFactor-currentpedestal[22])+
                             (adcValues[23]*CurrentFactor-currentpedestal[23]);

                if ((fabs(AnalNegTot) > AnalNegTotMax) || (fabs(AnalNegTot) < AnalNegTotMin)) 
	
				{printf ("Error:  Analog Negative Total [A]: %f\n",AnalNegTot);
				}



            printf ("Analog Negative Total [A]:  %f\n",AnalNegTot);
            fprintf (fp92, "Analog Negative Total [A] = %f\t \n",AnalNegTot);
/*
           printf ("Analog Negative Total [A]: %f\n",(
			                 adcValues[12]*CurrentFactor+
				             adcValues[14]*CurrentFactor+
                             adcValues[16]*CurrentFactor+ 
                             adcValues[18]*CurrentFactor+
                             adcValues[20]*CurrentFactor+
                             adcValues[21]*CurrentFactor+
                             adcValues[22]*CurrentFactor+
                             adcValues[23]*CurrentFactor)
				   );
*/
          printf ("---------------------------------------------\n");
/*
          for (i=12; i<24; i=i+2) {
            fprintf (fp92, " %d\t %f\t %f\t %f\t\n",i, 
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]),
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));

            printf ("%d\t %d\t %f\t %f\t %f\n", i, adcConvStat[i],
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]),
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
           }
*/
/*
            for (i=12; i<24; i=i+2) {
            fprintf (fp92, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
           }

            for (i=20; i<24; i=i+1) {
            fprintf (fp92, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f \n ", i, adcConvStat[i], adcValues[i]*CurrentFactor); }
*/
//---------------------------------------Start current division if i(i)>i(i+1)
            for (i=12; i<23; i=i+2) {

if ((adcValues[i]*CurrentFactor-currentpedestal[i]) > (adcValues[i+1]*CurrentFactor-currentpedestal[i+1]))
{
 //printf (" Start current division\n");
             if (i==12)  (dtmroc=1);
             if (i==14)  (dtmroc=2);
             if (i==16)  (dtmroc=3);
             if (i==18)  (dtmroc=4);
	         if (i==20)  (dtmroc=5);
             if (i==22)  (dtmroc=6);
         

	            if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, 3, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("DAC 0 1: 0x%X\n", dacRead);
			dac = dacRead - 0x0100;

            
			if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmroc, 3, dac, elmbTimeOut)) < 0) {
				printf ("Write exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}

			
}
// printf (" Start current division\n");


//--------------------------------------------------Start current division if i(i)<i(i+1)

if ((adcValues[i]*CurrentFactor-currentpedestal[i]) < (adcValues[i+1]*CurrentFactor-currentpedestal[i+1]))
{
// printf (" Start current division\n");
             if (i==12)  (dtmroc=1);
             if (i==14)  (dtmroc=2);
             if (i==16)  (dtmroc=3);
             if (i==18)  (dtmroc=4);
	         if (i==20)  (dtmroc=5);
             if (i==22) (dtmroc=6);
         

	            if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, 3, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("DAC 0 1: 0x%X\n", dacRead);
			dac = dacRead + 0x0100;

            
			if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmroc, 3, dac, elmbTimeOut)) < 0) {
				printf ("Write exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}

			
                                           }

//-----------------------------------------------------Stop current division

            fprintf (fp92, " %d\t %f\t %f\t %f\t\n",i,
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]),
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
            printf ("%d\t %d\t %f\t %f\t %f\n", i, adcConvStat[i],
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]),
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
           }

           printf ("\n");
//----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------Stop current division

//----------------------------------------Digital-------------------------------------------------
            printf ("\n");
           DigTot=
	/*		                 adcValues[24]*CurrentFactor+
				             adcValues[26]*CurrentFactor+
                             adcValues[28]*CurrentFactor+ 
                             adcValues[30]*CurrentFactor+
                             adcValues[32]*CurrentFactor+
                             adcValues[33]*CurrentFactor+
                             adcValues[34]*CurrentFactor+
                             adcValues[35]*CurrentFactor;
*/
			                 (adcValues[24]*CurrentFactor-currentpedestal[24])+
                             (adcValues[25]*CurrentFactor-currentpedestal[25])+
				             (adcValues[26]*CurrentFactor-currentpedestal[26])+
                             (adcValues[27]*CurrentFactor-currentpedestal[27])+
                             (adcValues[28]*CurrentFactor-currentpedestal[28])+ 
                             (adcValues[29]*CurrentFactor-currentpedestal[29])+
                             (adcValues[30]*CurrentFactor-currentpedestal[30])+
                             (adcValues[31]*CurrentFactor-currentpedestal[31])+
                             (adcValues[32]*CurrentFactor-currentpedestal[32])+
                             (adcValues[33]*CurrentFactor-currentpedestal[33])+
                             (adcValues[34]*CurrentFactor-currentpedestal[34])+
                             (adcValues[35]*CurrentFactor-currentpedestal[35]);

                if ((fabs(DigTot) > DigTotMax) || (fabs(DigTot) < DigTotMin)) 
	
				{printf ("Error:  Digital Total [A]: %f\n",DigTot);
				}



            printf ("Digital Total [A]: %f\n",DigTot);
            fprintf (fp92, " Digital Total [A]= %f\t \n",DigTot);
//-------------------------------------------------------------------------------------------------------
/*

           printf ("Digital Positive Total [A]: %f\n",(
			                 adcValues[24]*CurrentFactor+
				             adcValues[26]*CurrentFactor+
                             adcValues[28]*CurrentFactor+ 
                             adcValues[30]*CurrentFactor+
                             adcValues[32]*CurrentFactor+
                             adcValues[33]*CurrentFactor+
                             adcValues[34]*CurrentFactor+
                             adcValues[35]*CurrentFactor)
				   );
*/
         printf ("---------------------------------------------\n");
/*
  		   for (i=24; i<32; i=i+2) {
            fprintf (fp92, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
           }
            for (i=32; i<36; i=i+1) {
            fprintf (fp92, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
           }
*/




//---------------------------------------Start current division if i(i)>i(i+1)
            for (i=24; i<36; i=i+2) {

if ((adcValues[i]*CurrentFactor-currentpedestal[i]) > (adcValues[i+1]*CurrentFactor-currentpedestal[i+1]))
{
 //printf (" Start current division\n");
             if (i==24) (dtmroc=9);
             if (i==26) (dtmroc=10);
             if (i==28) (dtmroc=11);
             if (i==30) (dtmroc=12);
	         if (i==32) (dtmroc=13);
             if (i==34) (dtmroc=14);
         

	            if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, 2, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("DAC 0 1: 0x%X\n", dacRead);
			dac = dacRead - 0x0100;

            
			if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmroc, 2, dac, elmbTimeOut)) < 0) {
				printf ("Write exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}

			
}
// printf (" Start current division\n");


//--------------------------------------------------Start current division if i(i)<i(i+1)

if ((adcValues[i]*CurrentFactor-currentpedestal[i]) < (adcValues[i+1]*CurrentFactor-currentpedestal[i+1]))
{
// printf (" Start current division\n");
             if (i==24) (dtmroc=9);
             if (i==26) (dtmroc=10);
             if (i==28) (dtmroc=11);
             if (i==30) (dtmroc=12);
	         if (i==32) (dtmroc=13);
             if (i==34) (dtmroc=14);
         

	            if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmroc, 2, elmbTimeOut, &dacRead)) < 0) {
				printf ("Read exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("DAC 0 1: 0x%X\n", dacRead);
			dac = dacRead + 0x0100;

            
			if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmroc, 2, dac, elmbTimeOut)) < 0) {
				printf ("Write exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}

			
                                           }

//-----------------------------------------------------Stop current division

            fprintf (fp92, " %d\t %f\t %f\t %f\t\n",i,
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]),
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
            printf ("%d\t %d\t %f\t %f\t %f\n", i, adcConvStat[i],
            (adcValues[i]*CurrentFactor-currentpedestal[i]),(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]),
			(adcValues[i]*CurrentFactor-currentpedestal[i])+(adcValues[i+1]*CurrentFactor-currentpedestal[i+1]));
           }

           printf ("\n");
//----------------------------------------------------------------------------------------------------------
         printf ("Outputs [V]\n");
         printf ("---------------------------------------------\n");
         fprintf (fp92, "Analog Positive Vout [V] = \n");
for (i=36; i<60; i++) {
		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*VoltageFactor);
//	fprintf (fp80, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*VoltageFactor);
	
    fprintf (fp92, " %d \t %f\n",i, adcValues[i]*VoltageFactor);
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
 //    fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor);       

		}
         printf ("---------------------------------------------\n");
		for (i=60; i<64; i++) {
		//	printf ("%d. Conversion Status: %d \t %d\n", 
		//		i, adcConvStat[i], adcValues[i]);
//			fprintf (fp, "%d. Conversion Status: %d \t %d\n",i, adcConvStat[i], adcValues[i]);
          fprintf (fp92, "%d\t %d\t %f\n",i,adcConvStat[i], temperature (adcValues[i]));
           printf ("%d\t %d \t %f\n", i, adcConvStat[i],temperature (adcValues[i]));
		}
			}			
			fclose (fp92);
			
			 }

			break;
//------------------------------------------------------------------------------------------------

	
	case 96:// Burn-in test. 8 LVPP in the box under 5 ohm load.
//		    FILE *burnFile;
//			int end86;
			end86=1;
//            int  adcValues[64];
//			double CurrMin;
  //          double CurrMax;
            CurrMin=0.1;
		    CurrMax=0.9;
            burnFile=fopen("burnreport5ohm.txt","w");
			printf ("All regulators ON\n");
			if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 3, elmbTimeOut)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}

//-------------------------------------------------------------------------------------------------
			printf ("Test Voltage Regulators Outputs () \n");
while(end86)
{
            for (elmbNb=0x3f; elmbNb<=0x3f;   elmbNb++) {

			if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 3, elmbTimeOut)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				if (1)
					printf ("All regulators should be switched on.\n");
				else 
					printf ("All regulators should be switched off.\n");
			}
			Sleep (1000);
//-----------------------------------------------------------------------------------------------

				if ((status = zasilacz.GetAllInhibitsWheelA (handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
					printf ("%s\n", zasilacz.GetErrorText ());
				}
			    printf ("Inhibit word: 0x%X\n", dacRead);
			
//-----------------------------------------------------------------------------------------------

			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {

	 printf ("------------------------Current-------------------------\n");
            for (i=0; i<36; i++) {

		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*CurrentFactor);
//			fprintf (fp, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*CurrentFactor);
//            fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*CurrentFactor);
            printf ("%d\t %d \t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
           
		}

//------------------------------------------------------------------------------------------------------

			for (i=0; i<11;i+=2){		      
                if (((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)) < CurrMin) ||
				 ((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)) > CurrMax))
				  { printf ("Voltage Error in channel:%d\t %d\t %f\n",i,i+1,((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor))));
                  fprintf(burnFile,"Current Reading Error %d\t %d\n",i,elmbNb);
				  }
				 else
                printf (" ch(n)+ch(n+1)=%d\t %d\t %f\n",i,i+1,
			    (fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)));
                 fprintf (burnFile," ch(n)+ch(n+1)=%d\t %d\t %f\n",i,i+1,
			    (fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)));
				}

//------------------------------------------------------------------------------------------------------
for (i=12; i<23;i+=2){		      
                if (((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)) < CurrMin) ||
				 ((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)) > CurrMax))
				  { printf ("Voltage Error in channel:%d\t %d\t %f\n",i,i+1,((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor))));
                  fprintf(burnFile,"Current Reading Error %d\t %d\n",i,elmbNb);
				  }
				 else
                printf (" ch(n)+ch(n+1)=%d\t %d\t %f\n",i,i+1,
			    (fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)));
				}

//------------------------------------------------------------------------------------------------------
for (i=24; i<35;i+=2){		      
                if (((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)) < CurrMin) ||
				 ((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)) > CurrMax))
				  { printf ("Voltage Error in channel:%d\t %d\t %f\n",i,i+1,((fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor))));
                  fprintf(burnFile,"Current Reading Error %d\t %d\n",i,elmbNb);
				  }
				 else
                printf (" ch(n)+ch(n+1)=%d\t %d\t %f\n",i,i+1,
			    (fabs (adcValues[i]*CurrentFactor)+ fabs (adcValues[i+1]*CurrentFactor)));
				}

//------------------------------------------------------------------------------------------------------
	  for (i=36; i<44; i++) {     
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
	  if (2.9>(adcValues[i]*VoltageFactor)|| (adcValues[i]*VoltageFactor)>3.1) {
	  printf ("Voltage Error in channel:%d\n",i);
			}
      }
 
	  for (i=44; i<52; i++) {
     printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);     
	if (-3.7>(adcValues[i]*VoltageFactor)|| (adcValues[i]*VoltageFactor)>-3.5) {
		printf ("Voltage Error in channel:%d\n",i); 
      }
   	    }
	  for (i=52; i<60; i++) {
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
      if (2.3>(adcValues[i]*VoltageFactor)|| (adcValues[i]*VoltageFactor)>2.5) {
				printf ("Voltage Error in channel:%d\n",i);
      }
    		
			}
	        
//-----------------------------------------------------------------------------------------------

			if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 0, elmbTimeOut)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				if (0)
					printf ("All regulators should be switched on.\n");
				else 
					printf ("All regulators should be switched off.\n");
			}
//			Sleep (60000);//10 sec
            Sleep (1000);
//---------------------------------------------------------------------------------------------------------
				if ((status = zasilacz.GetAllInhibitsWheelA (handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
					printf ("%s\n", zasilacz.GetErrorText ());
				}
			    printf ("Inhibit word: 0x%X\n", dacRead);
			
//-----------------------------------------------------------------------------------------------
            printf ("--------------------------------------------------\n");
//-----------------------------------------------------------------------------------------------
			}
			}
			if(kbhit()) end86=0;
}
	        fclose (burnFile);
			break;
//----------------------------------------------------------------------------------------

		case 99:
			if ((status = zasilacz.CloseCan (handle)) < 0) {
				printf ("CloseCan exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else
				printf ("Closing CANBus OK\n");
			break;

// ==============================================================================================
//  'excel' test

		case 100:
			int first, last, step;
//			int next;
//			char fname[40];
//			int dacValue;
			FILE *pFile, *logFile;
/*
			printf ("First value  - ");
			scanf ("%d", &first);

			printf ("Last value  - ");
			scanf ("%d", &last);

			printf ("step  - ");
			scanf ("%d", &step);


			printf ("File name  - ");
			scanf ("%s", fname);
		
			if (!(pFile=fopen(fname,"w"))) {
				printf ("File %s was not created.\n", fname);
                break;
				}
*/
           first=0;
			last=255;
			step=51;

//            first=160;
//			last=170;
//			step=1;


			// create directory (if does not exist)

		printf ("Card Serial Number: ");
		scanf ("%d", &cardNb);

		// create directory
		sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d", cardNb);

		if ((_mkdir (textBuf))) { 
			if (errno != 17) { 
				printf ("directory not created : %s\n", strerror (errno )); 
			} 
		} 
			// time and date
		_ftime( &timebuffer );
		newtime  = localtime( &(timebuffer.time) ); 
		// data file
		sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\LVPP_WA_%02d%02dH%02d%02d.txt",
			cardNb, newtime->tm_mday, newtime->tm_mon+1,
			newtime->tm_hour, newtime->tm_min);

		if (!(pFile =fopen(textBuf,"w"))) {
			printf ("File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
			break;
		}

		// log file
		sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\LVPP_WA_LOG_%02d%02dH%02d%02d.txt",
			cardNb, newtime->tm_mday, newtime->tm_mon+1, 
			newtime->tm_hour, newtime->tm_min);

		if (!(logFile =fopen(textBuf,"w"))) {
			printf ("File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
			break;
		}

				
		fprintf (logFile, "Basic test (100) started: %02d%02d%4d %02d:%02d:%02d\n",
			newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900,
			newtime->tm_hour, newtime->tm_min, newtime->tm_sec);

//			fprintf(pFile,"ChannelNumber \t ConvStatus \t  RealValue \n");
            fprintf(pFile,"ChaNum \t ConvSt \t  RVal[V] \n");
//            fprintf(pFile,"ChannelNumber   RealValue\n");

			// initialisation

			printf ("CAN Bus init:\n");
			if ((status = zasilacz.Init_CANBus(channel, &handle)) < 0) {
				printf ("Init_CANBus exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "Init_CANBus exit with error: %d\n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
				fclose (pFile); fclose (logFile);
				break;
			}
			else {
				printf ("Initializing of CANBUS OK\n");
				fprintf (logFile, "Initializing of CANBUS OK\n");
			}

			printf ("ELMB Init:\n");
			if ((status = zasilacz.Init_ELMB(handle, elmbNb)) < 0) {
				printf ("Init_ELMB exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "Init_ELMB exit with error: %d\n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
				fclose (pFile); fclose (logFile);
				break;
			}
			else {
				printf ("Initializing of Init_ELMB OK\n");
				fprintf (logFile, "Initializing of Init_ELMB OK, elmb address: %d\n", elmbNb);
			}

			printf ("DTMROC Init:\n");
			if ((status = zasilacz.Init_DTMROC(handle, elmbNb, elmbTimeOut, dtmrocs)) < 0) {
				printf ("Init_DTMROC exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "Init_DTMROC exit with error: %d\n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
				fclose (pFile); fclose (logFile);
				break;
			}
			else {
				printf ("Initializing of DTMROCS OK\n");
				printf ("Initializing of DTMROCS OK\n");
				fprintf (logFile, "DTMROC-s: %s\n", dtmrocs);
				fprintf (logFile, "DTMROC-s: %s\n", dtmrocs);
			}
//*************Read all ADCs when regulators are OFF****************************************************
			
			printf ("Read all ADCs when regulators are OFF\n");
            fprintf(pFile,"Read all ADCs when regulators are OFF \n");
            fprintf(logFile,"Read all ADCs when regulators are OFF \n");
			if (readElmbADC (handle, elmbNb, adcValues, adcConvStat, 
				             CurrentFactor, VoltageFactor, pFile, logFile) == -1) {
				fclose (pFile);
				break;
			}
//****************Read all ADCs when regulators are ON****************************************************
			printf ("Switch all regulators ON\n");
            
			if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 3, elmbTimeOut)) < 0)
			{
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
				fclose (pFile); fclose (logFile);
				break;
			}
			
			// check
			if ((status = zasilacz.GetAllInhibitsWheelA (handle, elmbNb, elmbTimeOut, &valueRead)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
				fclose (pFile);fclose(logFile);
				break;
			}

			if (valueRead != 0xFFFFFF) {
				printf ("Not all regulators have been switched ON!: 0x%X\n", valueRead);
				fprintf (logFile, "Not all regulators have been switched ON!: 0x%X\n", valueRead);
				fclose (pFile); fclose (logFile);
				break;
			}

			printf ("All regulators are switched ON\n");	
            fprintf(pFile,"Read all ADCs when regulators are ON \n");
            fprintf(logFile,"Read all ADCs when regulators are ON \n");
			

//**************************************** loop**********************************
			for (dac = first; dac<=last; dac+=step) 
			{
				printf ("Dac set to: %d \n", dac);
				fprintf (pFile, "Dac set to: %d \n", dac);
				dacValue = (dac<<8) | dac; 
 //               dacValue = (dac<<8) | 0x00;
//				dacValue = dac;
				printf ("0x%X 0x%X\n", dac, dacValue);
				if ((status = zasilacz.WriteDacBrdc(handle, elmbNb, 3, dacValue, elmbTimeOut)) < 0) { 
//                if ((status = zasilacz.WriteDacBrdc(handle, elmbNb, 1, dacValue, portC, elmbTimeOut)) < 0) {
					printf ("WriteDacBrdc exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					fprintf (logFile, "WriteDacBrdc exit with error: %d\n", status);
					fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
					fclose (pFile); fclose (logFile);
					break;
			}

				// check DacBrdcCheck
				if ((status = zasilacz.DacBrdcCheck(handle, elmbNb, 1, dacValue, elmbTimeOut)) < 0) { 
					printf ("Error in setting DAC values:  %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					fprintf (logFile, "Error in setting DAC values:  %d\n", status);
					fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
				}

				if (readElmbADC (handle, elmbNb, adcValues, adcConvStat, 
					CurrentFactor, VoltageFactor, pFile, logFile) == -1) {
				 fclose (pFile); fclose (logFile);
            	 break;
				}
 
//			printf ("Next  - ");
//			scanf ("%d", &next);

			}
//*****************************************OFF Regulators********************************
printf ("Switch all regulators OFF\n");
            
			if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 0, elmbTimeOut)) < 0)
			{
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
				fclose (pFile); fclose (logFile);
				break;
			}
			
			// check
			if ((status = zasilacz.GetAllInhibitsWheelA (handle, elmbNb, elmbTimeOut, &valueRead)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
				fclose (pFile);fclose(logFile);
				break;
			}

			if (valueRead != 0x000000) {
				printf ("Not all regulators have been switched OFF!: 0x%X\n", valueRead);
				fprintf (logFile, "Not all regulators have been switched ON!: 0x%X\n", valueRead);
				fclose (pFile); fclose (logFile);
				break;
			}

			printf ("All regulators are switched OFF\n");	
            fprintf(pFile,"Read all ADCs when regulators are ON \n");
            fprintf(logFile,"Read all ADCs when regulators are ON \n");

//***************************************************************************************
		 fclose (pFile);
 

            if ((status = zasilacz.CloseCan (handle)) < 0) {
				printf ("CloseCan exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "CloseCan exit with error: %d\n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			}
			else {
				printf ("Closing CANBus OK\n");
				fprintf (logFile, "Closing CANBus OK\n");
			}
 
		 printf (" KONIEC!!!\n");
		 fprintf (logFile, " KONIEC!!!\n");
		fclose (logFile);

		break;

//==========================================================================================
//		COARSE test for wheel A

		case 103:
			printf ("============================================\n");
			printf ("Check basic functions of a card TYPE A\n");
			printf ("============================================\n");

// Canbus initialization
//			if ((handle = canbusOnlyInit (channel)) == -1) { //error in canbus
//				printf ("Test end -> INIT with errors\n");
//				break;
//			}
			printf ("Card Serial Number: ");
			scanf ("%d", &cardNb);
//------------------------------------------------------------------------------------------------
			int ilePetli;
			
			printf ("How many loops:  ");
			scanf ("%d", &ilePetli);

			printf ("CANBus,ELMB,DTMROC initialization\n");
			if ((status = zasilacz.Init_CANBus(channel, &handle)) < 0) {
				printf ("Init_CANBus exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Initializing of CANBUS OK\n");

			if ((status = zasilacz.Init_ELMB(handle, elmbNb)) < 0) {
				printf ("Init_ELMB exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Initializing of Init_ELMB OK\n");

			if ((status = zasilacz.Init_DTMROC(handle, elmbNb, elmbTimeOut, dtmrocs)) < 0) {
				printf ("Init_DTMROC exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				printf ("Initializing of DTMROCS OK\n");
				printf ("DTMROC-i: %s\n", dtmrocs);
			}
			

//------------------------------------------------------------------------------------------------


// create directory

			sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d", cardNb);

			if ((_mkdir (textBuf))) { 
				if (errno != 17) { 
					printf ("directory not created : %s\n", strerror (errno )); 
				}	 
			} 

// open log file
			_ftime( &timebuffer ); newtime  = localtime( &(timebuffer.time) );
			sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\LVPP_COARSE_%02d%02dH%02d%02d.txt",
			cardNb, newtime->tm_mday, newtime->tm_mon+1,
			newtime->tm_hour, newtime->tm_min);

			if (!(logFile =fopen(textBuf,"w"))) {
				printf ("File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
				break;
			}

			fprintf (logFile, "============================================\n");
			fprintf (logFile, "Check basic functions of a card TYPE A\n");
			fprintf (logFile, "============================================\n");
				fprintf (logFile, "Card Serial Number: %d\n", cardNb);
/*
			printf (" with calibration: 0   without calibration: 1  -> "); 
			scanf ("%d", &value);
*/
            value=0;
			if (!value) {
				sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\LVPP_CALIB_%03d.csv", cardNb, cardNb);
			
				if ((calibFile = fopen (textBuf, "r")) == NULL) {
					printf ("Calibration file does not exist for card %d - please make calibration\n", cardNb); 
					break;
				}

				readCalibration(calibFile, calibCoeff, calibCurrentOffset, 36);
			
				fclose (calibFile);
				printf ("CALIBRATION FILE: %s\n", textBuf);
				fprintf (logFile, "CALIBRATION FILE: %s\n", textBuf);
				printf ("Load: 5 ohms, valid current range sum: <%2.3f A - %2.3f A> \n",
				     CURRENT_COARSE_WHEELA_MIN_5_CALIB, CURRENT_COARSE_WHEELA_MAX_5_CALIB);
				fprintf (logFile, "Load: 5 ohms, valid current range: <%2.3f A - %2.3f A> \n",					
				     CURRENT_COARSE_WHEELA_MIN_5_CALIB, CURRENT_COARSE_WHEELA_MAX_5_CALIB);
			}
			else {
				for (i=0; i<36; i++) calibCoeff[i] = 0;
				printf ("NO CALIBRATION \n");
				fprintf (logFile, "NO CALIBRATION \n");
				printf ("Load: 5 ohms, valid current range summed: <%2.3f A - %2.3f A> \n",
				     CURRENT_COARSE_WHEELA_MAX_5_NOCAL, CURRENT_COARSE_WHEELA_MIN_5_NOCAL);
				fprintf (logFile, "Load: 5 ohms, valid current range: <%2.3f A - %2.3f A> \n",
				     CURRENT_COARSE_WHEELA_MAX_5_NOCAL, CURRENT_COARSE_WHEELA_MIN_5_NOCAL);
			}
			printf ("                temperature range: <%2.2f C - %2.2f C> \n\n",
				                    TEMPERATURE_COARSE_MIN, TEMPERATURE_COARSE_MAX);
			fprintf (logFile, "                temperature range: <%2.2f C - %2.2f C> \n\n",
				                    TEMPERATURE_COARSE_MIN, TEMPERATURE_COARSE_MAX);


			_ftime( &timebuffer ); seconds = (short) timebuffer.time; newtime  = localtime( &(timebuffer.time) );
			srand (seconds );
			fprintf (logFile, "TEST is started: %02d.%02d.%4d %02d:%02d:%02d\n\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900, 
				newtime->tm_hour, newtime->tm_min,
				newtime->tm_sec); 
			printf ("TEST is started: %02d.%02d.%4d %02d:%02d:%02d\n\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900, 
				newtime->tm_hour, newtime->tm_min,
				newtime->tm_sec); 

			if ((elmbDtmrocInit (handle, logFile, elmbNb, elmbTimeOut, portC)) == -1) { //error
				printf ("Test end -> INIT with errors ELMB nb %d\n", elmbNb);
				fprintf (logFile, "Test end -> INIT with errors ELMB nb %d\n", elmbNb);
				fclose (logFile);
				break;
			}
			checkInhibitsBroadcast (handle, elmbNb, portC,  3, elmbTimeOut, logFile);

			printf ("Single Write/Read DACs 01 23 in all DTMROCS\n\n");
			fprintf (logFile, "Single Write/Read DACs 01 23 in all DTMROCS\n\n");

			for (dtmroc = 0; dtmroc <12; dtmroc++) {
				for (item = 0; item < 3; item ++) {
					dac = ((rand() & 0xFF) << 8) | (rand() & 0xFF);
					checkWrite_SPI (handle, elmbNb, dtmroc, 2, dac, elmbTimeOut, logFile); 
				}
				for (item = 0; item < 3; item ++) {
					dac = ((rand() & 0xFF) << 8) | (rand() & 0xFF);
					checkWrite_SPI (handle, elmbNb, dtmroc, 3, dac, elmbTimeOut, logFile); 

				}
			}

			printf ("Write to all DACs - broadcast \n");
			fprintf (logFile, "Write to all DACs  - broadcast\n");

			for (item = 0; item < 3; item ++) {
				dac = ((rand() & 0xFF) << 8) | (rand() & 0xFF);

				checkWriteBroadcast (handle, elmbNb, 2, dac, elmbTimeOut, logFile);

				dac = ((rand() & 0xFF) << 8) | (rand() & 0xFF);

				checkWriteBroadcast (handle, elmbNb, 3, dac, elmbTimeOut, logFile);
				
			}
			
			printf ("\nCheck set INHIBITS\n");
			fprintf (logFile, "Check set INHIBITS");

			printf ("Set all regulators ON one by one\n");
			fprintf (logFile, "\nSet all regulators ON one by one\n"); 
			for (dtmroc = 0; dtmroc <12; dtmroc++) {
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 1, 1, elmbTimeOut, logFile); 
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 2, 1, elmbTimeOut, logFile); 
			}
// read and check all once more
			if ((status = zasilacz.GetAllInhibitsWheelA(handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
						printf ("GetAllInhibitsWheelA exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
						fprintf (logFile, "GetAllInhibitsWheelA exit with error: %d \n", status);
						fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			}
			if (dacRead != 0xFFFFFF) {
				printf ("ERROR: not all regulators are ON: 0x%X\n", dacRead);
				fprintf (logFile, "ERROR: not all regulators are ON: 0x%X\n", dacRead);
			}
//-----Set all regulators OFF one by one
			printf ("Set all regulators OFF one by one\n");
			fprintf (logFile, "Set all regulators OFF one by one\n"); 
			for (dtmroc = 0; dtmroc <12; dtmroc++) {
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 1, 0, elmbTimeOut, logFile);
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 2, 0, elmbTimeOut, logFile);
			}
// read and check all once more
			if ((status = zasilacz.GetAllInhibitsWheelA(handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
						printf ("GetAllInhibitsWheelA exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
						fprintf (logFile, "GetAllInhibitsWheelA exit with error: %d \n", status);
						fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			}
			if (dacRead != 0) {
				printf ("ERROR: not all regulators are OFF: 0x%X\n", dacRead);
				fprintf (logFile, "ERROR: not all regulators are OFF: 0x%X\n", dacRead);

			}
//-------Set all regulators ON - BROADCAST
			printf ("Set all regulators ON - BROADCAST\n");
			fprintf (logFile, "Set all regulators ON - BROADCAST\n"); 

			checkInhibitsBroadcast (handle, elmbNb, portC,  3, elmbTimeOut, logFile);
//-------Set all regulators ON - BROADCAST
			printf ("Set all regulators OFF - BROADCAST\n");
			fprintf (logFile, "Set all regulators OFF - BROADCAST\n"); 
			checkInhibitsBroadcast (handle, elmbNb, portC, 0, elmbTimeOut, logFile);
//-------Check ANALOG readout
			printf ("\nCheck ANALOG readout\n");
			fprintf (logFile, "\nCheck ANALOG readout\n");
			
			printf ("\nHard reset: all regulators are off, all DACs are set to 0xFF\n");
			fprintf (logFile, "\nHard reset: all regulators are off, all DACs are set to 0xFF.\n");
			if ((status = zasilacz.Hardware_Reset_DTMROC(handle, elmbNb, elmbTimeOut)) <0) {
				printf ("Hardware_Reset_DTMROC exit with status: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "Hardware_Reset_DTMROC exit with status: %d\n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			}

			checkAnalogReadWheelA (handle, elmbNb, 0, portC, elmbTimeOut, logFile, calibCoeff);
//*********************************************************************
int DACtableout[18];
int DACtable[18];  
/*
                 //WB/Barrel    ANeg APos    DTMROC
			DACtable[0] = 0x90A0;  //DAC1 DAC0     CH44 CH36         1
			DACtable[1] = 0x90A0;  //DAC3 DAC2
			DACtable[2] = 0x90A0;  //DAC1 DAC0     CH45 CH37         2
			DACtable[3] = 0x90A0;  //DAC3 DAC2
			DACtable[4] = 0x90A0;  //DAC1 DAC0     CH46 CH38         3
			DACtable[5] = 0x90A0;  //DAC3 DAC2
			DACtable[6] = 0x90A0;  //DAC1 DAC0     CH47 CH39         4
			DACtable[7] = 0x90A0;  //DAC3 DAC2

			DACtable[8] = 0x90A0;  //DAC1 DAC0     CH48 CH40         5
			DACtable[9] = 0x90A0;  //DAC3 DAC2     CH49 CH41
		    DACtable[10] = 0x90A0; //DAC1 DAC0     CH50 CH42         6 
		    DACtable[11] = 0x90A0; //DAC3 DAC2     CH51 CH43
//                                                Dig
			DACtable[12] = 0x0000; //DAC1 DAC0          CH52         9
			DACtable[13] = 0x0000; //DAC1 DAC0          CH53        10
			DACtable[14] = 0x0000; //DAC1 DAC0          CH54        11
			DACtable[15] = 0x0000; //DAC1 DAC0          CH55        12
			DACtable[16] = 0x0000; //DAC1 DAC0    CH57  CH56        13
			DACtable[17] = 0x0000; //DAC1 DAC0    CH59  CH58        14
*/
                  //WB/Barrel    ANeg APos    DTMROC
			DACtable[0]  = 0x8080;  //DAC1 DAC0     CH44 CH36         1
			DACtable[1]  = 0x8080;  //DAC3 DAC2
			DACtable[2]  = 0x8080;  //DAC1 DAC0     CH45 CH37         2
			DACtable[3]  = 0x8080;  //DAC3 DAC2
			DACtable[4]  = 0x8080;  //DAC1 DAC0     CH46 CH38         3
			DACtable[5]  = 0x8080;  //DAC3 DAC2
			DACtable[6]  = 0x8080;  //DAC1 DAC0     CH47 CH39         4
			DACtable[7]  = 0x8080;  //DAC3 DAC2

			DACtable[8]  = 0x8080;  //DAC1 DAC0     CH48 CH40         5
			DACtable[9]  = 0x8080;  //DAC3 DAC2     CH49 CH41
		    DACtable[10] = 0x8080; //DAC1 DAC0     CH50 CH42         6 
		    DACtable[11] = 0x8080; //DAC3 DAC2     CH51 CH43
//                                                Dig
			DACtable[12] = 0x8080; //DAC1 DAC0          CH52         9
			DACtable[13] = 0x8080; //DAC1 DAC0          CH53        10
			DACtable[14] = 0x8080; //DAC1 DAC0          CH54        11
			DACtable[15] = 0x8080; //DAC1 DAC0          CH55        12
			DACtable[16] = 0x8080; //DAC1 DAC0    CH57  CH56        13
			DACtable[17] = 0x8080; //DAC1 DAC0    CH59  CH58        14

//*************************************************************************************
			if ((status = zasilacz.WriteAllDacsWheelA (handle, elmbNb, elmbTimeOut, DACtable)) < 0) {
				printf ("\nWriteAllDacs exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
        
//**************************************************************************************
//*************************************Regulator 1 ON ***********************
//------- Write 0x80 (128) to all DACs
/*
			checkWriteBroadcast (handle, elmbNb, 2, 0x8080, portC, elmbTimeOut, logFile);
			checkWriteBroadcast (handle, elmbNb, 3, 0x8080, portC, elmbTimeOut, logFile);
*/

//*************************************Regulator 1 ON ***********************
			printf ("\nSet regulator number 1 ON\n");
			fprintf (logFile, "\nSet all regulators ON\n");
			checkInhibitsBroadcast (handle, elmbNb, portC,  1, elmbTimeOut, logFile);

			checkAnalogReadWheelA (handle, elmbNb, 1, portC, elmbTimeOut, logFile, calibCoeff);			
//********************************* Regulator 2 ON **************************
			printf ("\nSet regulator number 2 ON\n");
			fprintf (logFile, "\nSet all regulators ON\n");
			checkInhibitsBroadcast (handle, elmbNb, portC,  2, elmbTimeOut, logFile);

			checkAnalogReadWheelA (handle, elmbNb, 2, portC, elmbTimeOut, logFile, calibCoeff);			
//************************************ Both ON **************************
			printf ("\nSet both regulators ON\n");
			fprintf (logFile, "\nSet all regulators ON\n");
			checkInhibitsBroadcast (handle, elmbNb, portC,  3, elmbTimeOut, logFile);


			int iPetlaA; iPetlaA = 1;
//			int petlaA;
//			petlaA=0;

//************************************portC**********************************
/*
            printf ("PORT C: 0x%02X\n", portC);
			printf ("4.Set port C\n");
			printf ("PORT C - ");
			scanf ("%x", &pc);
			portC = pc;

			if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Write_C exit OK - status: %d\n", status);
*/
			portC=0xF0;
			if ((status = zasilacz.Write_C(handle, elmbNb,portC)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Write_C exit OK - status: %d\n", status);
//**********************************end portC********************************

			printf ("Starting test loop: all regulators are on\n");
       //     checkInhibitsBroadcast (handle, elmbNb, portC,  3, elmbTimeOut, logFile);

//***********************loop***********************************************

			while ((iPetlaA ==! 0) || (iPetlaA < ilePetli+1 )) {

            printf ("\n Loop: %d\n", iPetlaA);
			iPetlaA=iPetlaA+1;

            printf ("PORT C: 0x%02X\n", portC);
            printf ("Element DACtable %d 0x%X\n", 1, DACtable[1]);

            printf ("\n Write DACtable \n");
//-----------------------------------------------------------------------------
                  printf ("Beep\n");
                  printf ("Starting with tune is time to shortened load resistors by means of cable\n");
                  printf ("Shortened channels should appears as 'Errors' and testprogram is interruted\n");

//Tune
Beep(329,100); Beep(493,300); Beep(698,300); Beep(659,600);Beep(392,250); Beep(440,200); Beep(587,300); Beep(349,250);
Beep(587,500); Beep(329,300); Beep(493,300); Beep(698,300); Beep(659,600); Beep(783,300); Beep(698,300); Beep(659,600); 
Beep(329,100); Beep(493,300); Beep(698,300); Beep(659,600);Beep(392,250); Beep(440,200); Beep(587,300); Beep(349,250);
Beep(587,400);
				 Sleep (1000*20);

				  for(freqency=0; freqency<2500; freqency=freqency+1000)
			      {
			      Beep(freqency,100);
				  }
//--------------------------------------------------------------------
//*********************************************************************
/*
                  //WB/Barrel    ANeg APos    DTMROC
                  //WB/Barrel    ANeg APos    DTMROC
			DACtable[0] = 0x90A0;  //DAC1 DAC0     CH44 CH36         1
			DACtable[1] = 0x90A0;  //DAC3 DAC2
			DACtable[2] = 0x90A5;  //DAC1 DAC0     CH45 CH37         2
			DACtable[3] = 0x80A0;  //DAC3 DAC2
			DACtable[4] = 0x90A0;  //DAC1 DAC0     CH46 CH38         3
			DACtable[5] = 0x90A5;  //DAC3 DAC2
			DACtable[6] = 0x90A5;  //DAC1 DAC0     CH47 CH39         4
			DACtable[7] = 0x80A0;  //DAC3 DAC2

			DACtable[8] = 0x90A5;  //DAC1 DAC0     CH48 CH40         5
			DACtable[9] = 0x8090;  //DAC3 DAC2     CH49 CH41
		    DACtable[10] = 0x80A5; //DAC1 DAC0     CH50 CH42         6 
		    DACtable[11] = 0x60A0; //DAC3 DAC2     CH51 CH43
//                                                Dig
			DACtable[12] = 0x0a01; //DAC1 DAC0          CH52         9
			DACtable[13] = 0x010a; //DAC1 DAC0          CH53        10
			DACtable[14] = 0x0101; //DAC1 DAC0          CH54        11
			DACtable[15] = 0x0101; //DAC1 DAC0          CH55        12
			DACtable[16] = 0x0101; //DAC1 DAC0    CH57  CH56        13
			DACtable[17] = 0x0105; //DAC1 DAC0    CH59  CH58        14
*/

//*************************************************************************************
//*********************************************************************
                  //WB/Barrel    ANeg APos    DTMROC
                  //WB/Barrel    ANeg APos    DTMROC
/*			DACtable[0]  = 0x8080;  //DAC1 DAC0     CH44 CH36         1
			DACtable[1]  = 0x8080;  //DAC3 DAC2
			DACtable[2]  = 0x8080;  //DAC1 DAC0     CH45 CH37         2
			DACtable[3]  = 0x8080;  //DAC3 DAC2
			DACtable[4]  = 0x8080;  //DAC1 DAC0     CH46 CH38         3
			DACtable[5]  = 0x8080;  //DAC3 DAC2
			DACtable[6]  = 0x8080;  //DAC1 DAC0     CH47 CH39         4
			DACtable[7]  = 0x8080;  //DAC3 DAC2

			DACtable[8]  = 0x8080;  //DAC1 DAC0     CH48 CH40         5
			DACtable[9]  = 0x8080;  //DAC3 DAC2     CH49 CH41
		    DACtable[10] = 0x8080; //DAC1 DAC0     CH50 CH42         6 
		    DACtable[11] = 0x8080; //DAC3 DAC2     CH51 CH43
//                                                Dig
			DACtable[12] = 0x8080; //DAC1 DAC0          CH52         9
			DACtable[13] = 0x8080; //DAC1 DAC0          CH53        10
			DACtable[14] = 0x8080; //DAC1 DAC0          CH54        11
			DACtable[15] = 0x8080; //DAC1 DAC0          CH55        12
			DACtable[16] = 0x8080; //DAC1 DAC0    CH57  CH56        13
			DACtable[17] = 0x8080; //DAC1 DAC0    CH59  CH58        14
*/
//*************************************************************************************

/*			if ((status = zasilacz.WriteAllDacsWheelA (handle, elmbNb, portC, elmbTimeOut, DACtable)) < 0) {
				printf ("\nWriteAllDacs exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				iPetlaA = 0;
			} */
//**************************************************************************************
	  printf ("Start current division\n");
       currentDivision (handle, elmbNb, elmbTimeOut, calibCoeff, cardNb);

			printf ("\nDACs are set OK   and   ");
			Sleep (1000*10);
		 if ((status = zasilacz.ReadAllDacsWheelA (handle, elmbNb, elmbTimeOut, DACtableout)) < 0) {
						printf ("ReadAllDacsWheelA exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
						iPetlaA = 0;
			}
			printf ("DAC read back OK\n");
			Sleep (1000*5); 
			printf ("\n Read out voltages, currents and temperatures\n");
		// EB	    printf ("6.checkAnalogReadWheelA\n");
         //EB       checkInhibitsBroadcast (handle, elmbNb, portC,  1, elmbTimeOut, logFile);
         //EB       if (checkAnalogReadWheelA (handle, elmbNb, 1, portC, elmbTimeOut, logFile, calibCoeff) == false) iPetlaA = 0;
        //  EB      checkInhibitsBroadcast (handle, elmbNb, portC,  2, elmbTimeOut, logFile);
         //  EB     if (checkAnalogReadWheelA (handle, elmbNb, 2, portC, elmbTimeOut, logFile, calibCoeff) == false) iPetlaA = 0;
        // EB        checkInhibitsBroadcast (handle, elmbNb, portC,  3, elmbTimeOut, logFile);
				if (checkAnalogReadWheelA (handle, elmbNb, 3, portC, elmbTimeOut, logFile, calibCoeff) == false) iPetlaA = 0;



								   }  // glowna petla while


/*
			if ((status = zasilacz.Hardware_Reset_DTMROC(handle, elmbNb, portC)) <0) {
				printf ("Hardware_Reset_DTMROC exit with status: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "Hardware_Reset_DTMROC exit with status: %d\n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			}

*/
			portC=0x00;
			if ((status = zasilacz.Write_C(handle, elmbNb,portC)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Write_C exit OK - status: %d\n", status);

            printf ("PORT C: 0x%02X\n", portC);
/*
			if ((status = zasilacz.CloseCan (handle)) < 0) {
				printf ("CloseCan exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "CloseCan exit with error: %d\n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			}
*/
	
			_ftime( &timebuffer ); newtime  = localtime( &(timebuffer.time) );

			fprintf (logFile, "\n TEST is finished: temperature,current or voltage error. %02d.%02d.%4d %02d:%02d:%02d\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900, 
				newtime->tm_hour, newtime->tm_min,
				newtime->tm_sec); 
			printf ("\n TEST is finished: %02d.%02d.%4d %02d:%02d:%02d\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900, 
				newtime->tm_hour, newtime->tm_min,
				newtime->tm_sec); 

			fclose (logFile);

			break;

//     END OF COARSE test Wheel A
// =======================================================================================================






		case 104:

//==========================================================================================
//		COARSE test for wheel B / Barrel

//			short seconds;
int n_beep;
            printf ("=====================================================\n");			
			printf ("Check basic functions of a card TYPE Wheel B/Barrel\n");
            printf ("=====================================================\n");	


//------------------------------------------------------------------------------------------------

			printf ("Card Serial Number: ");
			scanf ("%d", &cardNb);


//-------------------- Canbus initialization
//			if ((handle = canbusOnlyInit (channel)) == -1) { //error in canbus
//				printf ("Test end -> INIT with errors\n");
//				break;
//			}
//-----------------Initialization of CANbus,ELMB and DTMROC-----------------------------------

			printf ("CANBus,ELMB,DTMROC initialization\n");
			if ((status = zasilacz.Init_CANBus(channel, &handle)) < 0) {
				printf ("Init_CANBus exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Initializing of CANBUS OK\n");

			if ((status = zasilacz.Init_ELMB(handle, elmbNb)) < 0) {
				printf ("Init_ELMB exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Initializing of Init_ELMB OK\n");

			if ((status = zasilacz.Init_DTMROC(handle, elmbNb, elmbTimeOut, dtmrocs)) < 0) {
				printf ("Init_DTMROC exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				printf ("Initializing of DTMROCS OK\n");
				printf ("DTMROC-i: %s\n", dtmrocs);
			}
			



//---------- create directory
			sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d", cardNb);

			if ((_mkdir (textBuf))) { 
				if (errno != 17) { 
					printf ("directory not created : %s\n", strerror (errno )); 
				}	 
			} 
	
//-------------- open log file
			_ftime( &timebuffer ); newtime  = localtime( &(timebuffer.time) );
			sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\LVPP_COARSE_%02d%02dH%02d%02d.txt",
			cardNb, newtime->tm_mday, newtime->tm_mon+1,
			newtime->tm_hour, newtime->tm_min);

			if (!(logFile =fopen(textBuf,"w"))) {
				printf ("File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
				break;
			}

			_ftime( &timebuffer ); seconds = (short) timebuffer.time; newtime  = localtime( &(timebuffer.time) );
			srand (seconds );
            fprintf (logFile, "=====================================================\n");			
			fprintf (logFile, "Check basic functions of a card TYPE Wheel B/Barrel\n");
            fprintf (logFile, "=====================================================\n");			
/*			printf ("Load: 5 ohms, valid current range: <%2.3f A - %2.3f A> \n",
				     CURRENT_COARSE_WHEELB_MIN_5, CURRENT_COARSE_WHEELB_MAX_5);
			printf ("                temperature range: <%2.2f C - %2.2f C> \n\n",
				                    TEMPERATURE_COARSE_MIN, TEMPERATURE_COARSE_MAX);
			fprintf (logFile, "Load: 5 ohms, valid current range: <%2.3f A - %2.3f A> \n",
				     CURRENT_COARSE_WHEELB_MIN_5, CURRENT_COARSE_WHEELB_MAX_5);
			fprintf (logFile, "                temperature range: <%2.2f C - %2.2f C> \n\n",
				                    TEMPERATURE_COARSE_MIN, TEMPERATURE_COARSE_MAX); */
//			printf (" with calibration: 0   without calibration: 1  -> "); scanf ("%d", &value);
			value=0;
			if (!value) {
				sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\LVPP_CALIB_%03d.csv", cardNb, cardNb);
			
				if ((calibFile = fopen (textBuf, "r")) == NULL) {
					printf ("Calibration file does not exist for card %d - please make calibration\n", cardNb); 
					break;
				}

				readCalibration(calibFile, calibCoeff, calibCurrentOffset, 36);
			
				fclose (calibFile);
				printf ("CALIBRATION FILE: %s\n", textBuf);
				fprintf (logFile, "CALIBRATION FILE: %s\n", textBuf);
				printf ("Load: 5 ohms, valid current range sum: <%2.3f A - %2.3f A> \n",
				     CURRENT_COARSE_WHEELA_MIN_5_CALIB, CURRENT_COARSE_WHEELA_MAX_5_CALIB);
				fprintf (logFile, "Load: 5 ohms, valid current range: <%2.3f A - %2.3f A> \n",					
				     CURRENT_COARSE_WHEELA_MIN_5_CALIB, CURRENT_COARSE_WHEELA_MAX_5_CALIB);
			}
			else {
				for (i=0; i<36; i++) calibCoeff[i] = 0;
				printf ("NO CALIBRATION \n");
				fprintf (logFile, "NO CALIBRATION \n");
				printf ("Load: 5 ohms, valid current range summed: <%2.3f A - %2.3f A> \n",
				     CURRENT_COARSE_WHEELA_MAX_5_NOCAL, CURRENT_COARSE_WHEELA_MIN_5_NOCAL);
				fprintf (logFile, "Load: 5 ohms, valid current range: <%2.3f A - %2.3f A> \n",
				     CURRENT_COARSE_WHEELA_MAX_5_NOCAL, CURRENT_COARSE_WHEELA_MIN_5_NOCAL);
			}
			printf ("TEST is started: %02d.%02d.%4d %02d:%02d:%02d\n\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900, 
				newtime->tm_hour, newtime->tm_min,
				newtime->tm_sec); 

			fprintf (logFile, "TEST is started: %02d.%02d.%4d %02d:%02d:%02d\n\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900, 
				newtime->tm_hour, newtime->tm_min,
				newtime->tm_sec); 

			if ((elmbDtmrocInit (handle, logFile, elmbNb, elmbTimeOut, portC)) == -1) { //error
				printf ("Test end -> INIT with errors ELMB nb %d\n", elmbNb);
				fprintf (logFile, "Test end -> INIT with errors ELMB nb %d\n", elmbNb);
				fclose (logFile);
				break;
			}


		//	for (i=0; i<17; i++) {

			fprintf (logFile, "\nSingle Write/Read DACs 01 23 in all DTMROCS\n");
			printf ("Single Write/Read DACs 01 23 in all DTMROCS\n");

			for (dtmroc = 0; dtmroc <12; dtmroc++) {
				for (item = 0; item < 3; item ++) {
				//	if (!(item%5))
				//		dac = (item*8 << 8) | item*8;
				//	else
						dac = ((rand() & 0xFF) << 8) | (rand() & 0xFF);
					//printf ("\tDTMROC %d dac 01 value: 0x%X\n", dtmrocAddress(dtmroc), dac);
					checkWrite_SPI (handle, elmbNb, dtmroc, 2, dac, elmbTimeOut, logFile); 
				}
				for (item = 0; item < 3; item ++) {
				//	if (!(item%5))
				//		dac = (item*8 << 8) | item*8;
				//	else
						dac = ((rand() & 0xFF) << 8) | (rand() & 0xFF);
				//	printf ("\tDTMROC %d dac 23 value: 0x%X\n", dtmrocAddress(dtmroc), dac);
					checkWrite_SPI (handle, elmbNb, dtmroc, 3, dac, elmbTimeOut, logFile); 

				}
			//	printf ("DTMROC %d single READ-WRITE DONE\n\n", dtmrocAddress(dtmroc)); 
			//	fprintf (logFile, "DTMROC %d single READ-WRITE DONE\n\n", dtmrocAddress(dtmroc)); 
			}

			printf ("Write to all DACs - broadcast \n");
			fprintf (logFile, "Write/Read DACs BROADCAST 01 23 in all DTMROCS\n");

			for (item = 0; item < 3; item ++) {
			//	if (!(item%5))
			//		dac = (item*8 << 8) | item*8;
			//	else
					dac = ((rand() & 0xFF) << 8) | (rand() & 0xFF);
					//printf ("\t All DACs 01 value: 0x%X\n",  dac);

				checkWriteBroadcast (handle, elmbNb, 2, dac, elmbTimeOut, logFile);

			//	if (!(item%5))
			//		dac = (item*8 << 8) | item*8;
			//	else
					dac = ((rand() & 0xFF) << 8) | (rand() & 0xFF);

				//	printf ("\t All DACs 23 value: 0x%X\n", dac);
				checkWriteBroadcast (handle, elmbNb, 3, dac, elmbTimeOut, logFile);
				
			//	printf ("DTMROC BROADCAST step %d READ-WRITE DONE\n\n", item); 
			//	fprintf (logFile, "DTMROC BROADCAST step %d READ-WRITE DONE\n\n", item); 
			}

			printf ("\nCheck set INHIBITS \n");
			fprintf (logFile, "\nCheck set INHIBITS\n");

			printf ("Set all regulators ON one by one\n");
			fprintf (logFile, "\nSet all regulators ON one by one\n"); 
			for (dtmroc = 0; dtmroc <4; dtmroc++) 
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 1, 1, elmbTimeOut, logFile); 
			for (dtmroc = 4; dtmroc <6; dtmroc++) {
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 1, 1, elmbTimeOut, logFile); 
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 2, 1, elmbTimeOut, logFile); 
			}
			for (dtmroc = 6; dtmroc <10; dtmroc++) 
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 1, 1, elmbTimeOut, logFile); 
			for (dtmroc = 10; dtmroc <12; dtmroc++) {
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 1, 1, elmbTimeOut, logFile); 
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 2, 1, elmbTimeOut, logFile); 
			}
			// read and check all once more
			if ((status = zasilacz.GetAllInhibitsWheelB(handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
						printf ("GetAllInhibitsWheelB exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
						fprintf (logFile, "GetAllInhibitsWheelB exit with error: %d \n", status);
						fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			}
			if (dacRead != 0xFFFF) {
				printf ("ERROR: not all regulators are ON: 0x%X\n", dacRead);
				fprintf (logFile, "ERROR: not all regulators are ON: 0x%X\n", dacRead);
			}

		//	printf ("DONE\n");
		//	fprintf (logFile, "DONE\n");

			printf ("Set all regulators OFF one by one\n");
			fprintf (logFile, "Set all regulators OFF one by one\n"); 
			for (dtmroc = 0; dtmroc <4; dtmroc++) 
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 1, 0, elmbTimeOut, logFile); 
			for (dtmroc = 4; dtmroc <6; dtmroc++) {
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 1, 0, elmbTimeOut, logFile); 
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 2, 0, elmbTimeOut, logFile); 
			}
			for (dtmroc = 6; dtmroc <10; dtmroc++) 
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 1, 0, elmbTimeOut, logFile); 
			for (dtmroc = 10; dtmroc <12; dtmroc++) {
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 1, 0, elmbTimeOut, logFile); 
				checkInhibitsSingle (handle, elmbNb, dtmroc, portC, 2, 0, elmbTimeOut, logFile); 
			}
			// read and check all once more
			if ((status = zasilacz.GetAllInhibitsWheelB(handle, elmbNb, elmbTimeOut, &dacRead)) < 0) {
						printf ("GetAllInhibitsWheelB exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
						fprintf (logFile, "GetAllInhibitsWheelB exit with error: %d \n", status);
						fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			}
			if (dacRead != 0) {
				printf ("ERROR: not all regulators are OFF: 0x%X\n", dacRead);
				fprintf (logFile, "ERROR: not all regulators are OFF: 0x%X\n", dacRead);

			}
		//	printf ("DONE\n");
		//	fprintf (logFile, "DONE\n");

			printf ("Set all regulators ON - BROADCAST\n");
			fprintf (logFile, "Set all regulators ON - BROADCAST\n"); 

			checkInhibitsBroadcastWheelB (handle, elmbNb, portC,  3, elmbTimeOut, logFile);

		//	printf ("DONE\n");
		//	fprintf (logFile, "DONE\n");
	
			printf ("Set all regulators OFF - BROADCAST\n");
			fprintf (logFile, "Set all regulators OFF - BROADCAST\n"); 
			checkInhibitsBroadcastWheelB (handle, elmbNb, portC, 0, elmbTimeOut, logFile);

		//	printf ("DONE\n");
		//	fprintf (logFile, "DONE\n");
//***************************Check ANALOG readout************************************

//   #ifdef ANALOG
			printf ("\n 1.Check ANALOG readout\n");
			fprintf (logFile, "\nCheck ANALOG readout\n");
			
			printf ("\n 2.Hard reset: all regulators are off, all DACs are set to 0xFF\n");
			fprintf (logFile, "\nHard reset: all regulators are off, all DACs are set to 0xFF.\n");
			if ((status = zasilacz.Hardware_Reset_DTMROC(handle, elmbNb, elmbTimeOut)) <0) {
				printf ("Hardware_Reset_DTMROC exit with status: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "Hardware_Reset_DTMROC exit with status: %d\n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			}

			checkAnalogReadWheelB (handle, elmbNb, 0, portC, elmbTimeOut, logFile, calibCoeff);
//***********************************************************************************
/*
			// write 0x80 (128) to all DACs
			checkWriteBroadcast (handle, elmbNb, 2, 0x8080, portC, elmbTimeOut, logFile);
			checkWriteBroadcast (handle, elmbNb, 3, 0x8080, portC, elmbTimeOut, logFile);
*/
//******************************************************************************
			int iPetla; iPetla = 1;
			int petla;
			petla=0;


			printf ("3.Set all regulators ON\n");
			fprintf (logFile, "\nSet all regulators ON\n");
			checkInhibitsBroadcastWheelB (handle, elmbNb, portC,  3, elmbTimeOut, logFile);
/*
            printf ("4.Set portC=0xF0 \n");
			if ((status = zasilacz.Write_C(handle, elmbNb,portC)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
*/
//************************************portC**********************************
            printf ("PORT C: 0x%02X\n", portC);
/*
			printf ("4.Set port C\n");
			printf ("PORT C - ");
			scanf ("%x", &pc);
			portC = pc; 
*/			
            portC =0xF0;

			if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Write_C exit OK - status: %d\n", status);

            printf ("PORT C: 0x%02X\n", portC);
//**********************************end portC********************************
//*** here portC=0xFO; OCM circuit is enable********************************

			while (iPetla) {
/*
             printf ("Hardware Reset\n");
			if ((status = zasilacz.Hardware_Reset_DTMROC(handle, elmbNb, portC)) <0) {
				printf ("Hardware_Reset_DTMROC exit with status: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "Hardware_Reset_DTMROC exit with status: %d\n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			}
*/
            printf ("\n Petla: %d\n", petla);
			petla=petla+1;
            printf ("PORT C: 0x%02X\n", portC);
			printf ("5.Write a DACtable to all dacs in a board\n");

//------------------------------------------------------------------------------------------------------
                  printf ("Beep\n");
                  printf ("Starting with tune is time to shortened load resistors by means of cable\n");
                  printf ("Shortened channels should appears as 'Errors' and testprogram is interruted\n");

//Tune
Beep(329,100); Beep(493,300); Beep(698,300); Beep(659,600);Beep(392,250); Beep(440,200); Beep(587,300); Beep(349,250);
Beep(587,500); Beep(329,300); Beep(493,300); Beep(698,300); Beep(659,600); Beep(783,300); Beep(698,300); Beep(659,600); 
Beep(329,100); Beep(493,300); Beep(698,300); Beep(659,600);Beep(392,250); Beep(440,200); Beep(587,300); Beep(349,250);
Beep(587,400);
//				Sleep (1000*20);
                  for (n_beep = 0; n_beep <25; n_beep++){
				  Sleep (500);
				  for(freqency=0; freqency<2500; freqency=freqency+1000)
			      {
			      Beep(freqency,100);
				  }
				  }
//------------------------------------------------------------------------------------------------------------
/*
int DACtable[18];
			DACtable[0] = 0xB090; 
			DACtable[1] = 0xFFFF;  
			DACtable[2] = 0xB090;  
			DACtable[3] = 0xFFFF; 
			DACtable[4] = 0xB090; 
			DACtable[5] = 0xFFFF;  
			DACtable[6] = 0xB090;  
			DACtable[7] = 0xFFFF;

			DACtable[8] = 0xB090; 
			DACtable[9] = 0xB090;  
			DACtable[10] = 0xB090; 
			DACtable[11] = 0xB090;

			DACtable[12] = 0xFF00; 
			DACtable[13] = 0xFF00;  
			DACtable[14] = 0xFF00;  
			DACtable[15] = 0xFF00;
			DACtable[16] = 0x0000; 
			DACtable[17] = 0x0000;
*/
/*
int DACtable[18];                   //WB/Barrel    ANeg APos    DTMROC
			DACtable[0] = 0xB090;  //DAC1 DAC0     CH44 CH36         1
			DACtable[1] = 0x0000;  //DAC3 DAC2
			DACtable[2] = 0xB090;  //DAC1 DAC0     CH45 CH37         2
			DACtable[3] = 0x0000;  //DAC3 DAC2
			DACtable[4] = 0xB090;  //DAC1 DAC0     CH46 CH38         3
			DACtable[5] = 0x0000;  //DAC3 DAC2
			DACtable[6] = 0xB090;  //DAC1 DAC0     CH47 CH39         4
			DACtable[7] = 0x0000;  //DAC3 DAC2

			DACtable[8] = 0xB090;  //DAC1 DAC0     CH48 CH40         5
			DACtable[9] = 0xB090;  //DAC3 DAC2     CH49 CH41
		    DACtable[10] = 0xB090; //DAC1 DAC0     CH50 CH42         6 
		    DACtable[11] = 0xB090; //DAC3 DAC2     CH51 CH43
//                                                Dig
			DACtable[12] = 0xFF00; //DAC1 DAC0          CH52         9
			DACtable[13] = 0xFF00; //DAC1 DAC0          CH53        10
			DACtable[14] = 0xFF00; //DAC1 DAC0          CH54        11
			DACtable[15] = 0xFF00; //DAC1 DAC0          CH55        12
			DACtable[16] = 0x0000; //DAC1 DAC0    CH57  CH56        13
			DACtable[17] = 0x0000; //DAC1 DAC0    CH59  CH58        14
*/
//*********************************************************************
int DACtable1[18];                   //WB/Barrel    ANeg APos    DTMROC
			DACtable1[0] = 0x0000;  //DAC1 DAC0     CH44 CH36         1
			DACtable1[1] = 0x0000;  //DAC3 DAC2
			DACtable1[2] = 0x0000;  //DAC1 DAC0     CH45 CH37         2
			DACtable1[3] = 0x0000;  //DAC3 DAC2
			DACtable1[4] = 0x0000;  //DAC1 DAC0     CH46 CH38         3
			DACtable1[5] = 0x0000;  //DAC3 DAC2
			DACtable1[6] = 0x0000;  //DAC1 DAC0     CH47 CH39         4
			DACtable1[7] = 0x0000;  //DAC3 DAC2

			DACtable1[8] = 0x0000;  //DAC1 DAC0     CH48 CH40         5
			DACtable1[9] = 0x0000;  //DAC3 DAC2     CH49 CH41
		    DACtable1[10] = 0x0000; //DAC1 DAC0     CH50 CH42         6 
		    DACtable1[11] = 0x0000; //DAC3 DAC2     CH51 CH43
//                                                Dig
			DACtable1[12] = 0x0000; //DAC1 DAC0          CH52         9
			DACtable1[13] = 0x0000; //DAC1 DAC0          CH53        10
			DACtable1[14] = 0x0000; //DAC1 DAC0          CH54        11
			DACtable1[15] = 0x0000; //DAC1 DAC0          CH55        12
			DACtable1[16] = 0x0000; //DAC1 DAC0    CH57  CH56        13
			DACtable1[17] = 0x0000; //DAC1 DAC0    CH59  CH58        14
//*************************************************************************************
			if ((status = zasilacz.WriteAllDacsWheelA (handle, elmbNb, elmbTimeOut, DACtable1)) < 0) {
				printf ("\nWriteAllDacs exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
//**************************************************************************************
//*********************************************************************
int DACtable[18];                   //WB/Barrel    ANeg APos    DTMROC
			DACtable[0] = 0x90A0;  //DAC1 DAC0     CH44 CH36         1
			DACtable[1] = 0x0000;  //DAC3 DAC2
			DACtable[2] = 0x90A0;  //DAC1 DAC0     CH45 CH37         2
			DACtable[3] = 0x0000;  //DAC3 DAC2
			DACtable[4] = 0x90A0;  //DAC1 DAC0     CH46 CH38         3
			DACtable[5] = 0x0000;  //DAC3 DAC2
			DACtable[6] = 0x90A0;  //DAC1 DAC0     CH47 CH39         4
			DACtable[7] = 0x0000;  //DAC3 DAC2

			DACtable[8] = 0x90A0;  //DAC1 DAC0     CH48 CH40         5
			DACtable[9] = 0x90A0;  //DAC3 DAC2     CH49 CH41
		    DACtable[10] = 0x90A0; //DAC1 DAC0     CH50 CH42         6 
		    DACtable[11] = 0x90A0; //DAC3 DAC2     CH51 CH43
//                                                Dig
			DACtable[12] = 0xFF20; //DAC1 DAC0          CH52         9
			DACtable[13] = 0xFF20; //DAC1 DAC0          CH53        10
			DACtable[14] = 0xFF20; //DAC1 DAC0          CH54        11
			DACtable[15] = 0xFF20; //DAC1 DAC0          CH55        12
			DACtable[16] = 0x2020; //DAC1 DAC0    CH57  CH56        13
			DACtable[17] = 0x2020; //DAC1 DAC0    CH59  CH58        14
//*************************************************************************************
			if ((status = zasilacz.WriteAllDacsWheelA (handle, elmbNb, elmbTimeOut, DACtable)) < 0) {
				printf ("\nWriteAllDacs exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
//**************************************************************************************

//			int iPetla; iPetla = 1;
//			while (iPetla) {
			    printf ("6.checkAnalogReadWheelB\n");
				if (checkAnalogReadWheelB (handle, elmbNb, 1, portC, elmbTimeOut, logFile, calibCoeff) == false) iPetla = 0;


				  
			}
		//	}  // petla na 17 przebiegow



			if ((status = zasilacz.Hardware_Reset_DTMROC(handle, elmbNb, elmbTimeOut)) <0) {
				printf ("Hardware_Reset_DTMROC exit with status: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "Hardware_Reset_DTMROC exit with status: %d\n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			}

/*

			if ((status = zasilacz.CloseCan (handle)) < 0) {
				printf ("CloseCan exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "CloseCan exit with error: %d\n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			}
*/
//  #endif
//************************************portC**********************************
            printf ("PORT C: 0x%02X\n", portC);				
            portC =0x00;

			if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else 
				printf ("Write_C exit OK - status: %d\n", status);

            printf ("PORT C: 0x%02X\n", portC);
//**********************************end portC********************************
			_ftime( &timebuffer ); newtime  = localtime( &(timebuffer.time) );

			fprintf (logFile, "\n TEST is finished: %02d.%02d.%4d %02d:%02d:%02d\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900, 
				newtime->tm_hour, newtime->tm_min,
				newtime->tm_sec); 
			printf ("\n TEST is finished: %02d.%02d.%4d %02d:%02d:%02d\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900, 
				newtime->tm_hour, newtime->tm_min,
				newtime->tm_sec); 

			fclose (logFile);

			break;

//     END OF COARSE test Wheel B / Barrel
// =======================================================================================================

			case 140:
				printf ("Value to be written to all DACs (16 bits!!!!): 0x"); scanf ("%x", &value);
				if (checkWriteBroadcast (handle, elmbNb, 2, value, elmbTimeOut, NULL)) 
					printf ("Error when setting DACS 2\n");
				if (checkWriteBroadcast (handle, elmbNb, 3, value, elmbTimeOut, NULL)) 
					printf ("Error when setting DACS 3\n");
				break;

           case 200:
			if ((status = zasilacz.CloseCan (handle)) < 0) {
				printf ("CloseCan exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else
				printf ("Closing CANBus OK\n");
			break;


//**********************************Hardware Test*****************************************
case 300:
	int loop;
	dac=0x1111;
	dtmroc=1;
				for  (loop=0; loop<=10000; loop++){
     
                if ((status = zasilacz.Write_SPI(handle, elmbNb, dtmrocAddress(dtmroc), 2, dac, elmbTimeOut)) < 0)

                { printf ("Write exit with error: %d\n", status);
				}
				Sleep(1000);

				}
               

			break;
//**********************************Read OCM*****************************************

case 400:
			// ocmValues[0] - digital
			// ocmValues[1] - analog positive
			// acmValues[2] - analog negative
            
			int section;
//            portC = 0x00;
           
			int z;

			int end400;
			int OCMclear;
            int portC;
			OCMclear = 0x0;
		    end400=1;
             FILE *fp400;
			 fp400 = fopen ("OCM report_400.txt","w");
			 printf ("PortC=0xF0 OCM enable \n");
			 printf ("PortC=0xA0 OCM disable :\n");
			 scanf ("%x", &portC);


			 printf ("Licznik :\n");
			 scanf ("%d", &licznik);

// Clear OCM

			 		// F-F CLR=0,Q=0; i AN_OCM=1, OCM disable
             if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
 //              if ((status = zasilacz.Write_C(handle, elmbNb, OCMclear)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			 }



//             portC = 0xA0;   // CLEAR - poziom wysoki, FLIP=FLOP aktywny
		                       // AN_OCM_D - 0 -> bramka na inhibit zablokowana
             if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			printf("Test of Over Current Monitor \n");
            printf("4 = Digital, 2 = Analog Negative, 1 = Analog Positive  \n");
            printf("Regulator\t OCM_MUX\t Response\n");

            fprintf(fp400,"Test of Over Current Monitor \n");
            fprintf(fp400,"4 = Digital, 2 = Analog Negative, 1 = Analog Positive  \n");
            fprintf(fp400,"Regulator\t OCM_MUX\t Response\n");
//			licznik = 36; // WA
//			licznik = 24; // WB/B
			printf ("Licznik przed wejsciem do while %d\n", licznik);

            while(licznik>0){
                  for(freqency=0; freqency<5000; freqency=freqency+1000)
			      {
			      Beep(freqency,100);
				  }
				Sleep (2000);
	        for  (z=0; z<12; z++){
               if (z==0){ (section=0);}
               if (z==1){ (section=8);}
               if (z==2){ (section=4);}
               if (z==3){ (section=0xC);}
               if (z==4){ (section=2);}
               if (z==5){ (section=0xA);}
               if (z==6){ (section=6);}
               if (z==7){ (section=0xE);}
               if (z==8) { (section=1);}
               if (z==9){ (section=9);}
               if (z==10){ (section=5);}
               if (z==11){ (section=0xD);}
				if ((status = zasilacz.Read_OCM(handle,elmbNb,section,portC, ocmValues))<0){
				printf ("Read exit with error: %d\n", status);
				}
				if (ocmValues[2] == 2){
               licznik--;
               if (z==0){ printf("CH12 \t");fprintf(fp400,"CH12 \t");}
               if (z==1){ printf("CH13 \t");fprintf(fp400,"CH13 \t");}
               if (z==2){ printf("CH14 \t");fprintf(fp400,"CH14 \t");}
               if (z==3){ printf("CH15 \t");fprintf(fp400,"CH15 \t");}
               if (z==4){ printf("CH16 \t");fprintf(fp400,"CH16 \t");}
               if (z==5){ printf("CH17 \t");fprintf(fp400,"CH17 \t");}
               if (z==6){ printf("CH18 \t");fprintf(fp400,"CH18 \t");}
               if (z==7){ printf("CH19 \t");fprintf(fp400,"CH19 \t");}
               if (z==8){ printf("CH20 \t");fprintf(fp400,"CH20 \t");}
               if (z==9){ printf("CH21 \t");fprintf(fp400,"CH21 \t");}
               if (z==10){ printf("CH22 \t");fprintf(fp400,"CH22 \t");}
               if (z==11){printf("CH23 \t");fprintf(fp400,"CH23 \t");}
               
					printf ("%10d\t  %10d\n",z,ocmValues[2]);
                 
                    fprintf (fp400,"%10d\t %10d\n",z,ocmValues[2]);
                 
					Sleep(500); 
                   for(freqency=0; freqency<5000; freqency=freqency+1000)
			      {
			      Beep(freqency,100);
				  }
				if (kbhit ()) {
					end400 = 0;
					fclose (fp400);
				
					printf ("kbit!\n");
				}
				}             
			} // for 1

          for  (z=0; z<12; z++){
               if (z==0){ (section=0);}
               if (z==1){ (section=8);}
               if (z==2){ (section=4);}
               if (z==3){ (section=0xC);}
               if (z==4){ (section=2);}
               if (z==5){ (section=0xA);}
               if (z==6){ (section=6);}
               if (z==7){ (section=0xE);}
               if (z==8) { (section=1);}
               if (z==9){ (section=9);}
               if (z==10){ (section=5);}
               if (z==11){ (section=0xD);}
				if ((status = zasilacz.Read_OCM(handle,elmbNb,section,portC, ocmValues))<0){
				printf ("Read exit with error: %d\n", status);
				}

				if (ocmValues[1] == 1){
               
               licznik--;
               if (z==0){ printf("CH0  \t");fprintf(fp400,"CH0 \t");}
               if (z==1){ printf("CH1  \t");fprintf(fp400,"CH1 \t");}
               if (z==2){ printf("CH2  \t");fprintf(fp400,"CH2 \t");}
               if (z==3){ printf("CH3  \t");fprintf(fp400,"CH3 \t");}
               if (z==4){ printf("CH4  \t");fprintf(fp400,"CH4 \t");}
               if (z==5){ printf("CH5  \t");fprintf(fp400,"CH5 \t");}
               if (z==6){ printf("CH6  \t");fprintf(fp400,"CH6 \t");}
               if (z==7){ printf("CH7  \t");fprintf(fp400,"CH7 \t");}
               if (z==8){ printf("CH8  \t");fprintf(fp400,"CH8 \t");}
               if (z==9){ printf("CH9  \t");fprintf(fp400,"CH9 \t");}
               if (z==10){ printf("CH10 \t");fprintf(fp400,"CH10 \t");}
               if (z==11){printf("CH11 \t");fprintf(fp400,"CH11 \t");}

					printf ("%10d\t  %10d\n",z,ocmValues[1]);
                   fprintf (fp400,"%10d\t  %10d\n",z,ocmValues[1]);
                 
					Sleep(500); 
                   for(freqency=0; freqency<5000; freqency=freqency+1000)
			      {
			      Beep(freqency,100);
				  }
				if (kbhit ()) {
					end400 = 0;
					fclose (fp400);
				
					printf ("kbit!\n");
				}
				}   // if  ocmValues[1]         
			} // for 2



	         for  (section=0; section<12; section++){

   				if ((status = zasilacz.Read_OCM(handle,elmbNb,section,portC, ocmValues))<0){
				printf ("Read exit with error: %d\n", status);
				}
				if (ocmValues[0] == 4){
					licznik--;
               if (section==0){ printf("CH24  \t");fprintf(fp400,"CH24 \t");}
               if (section==1){ printf("CH25  \t");fprintf(fp400,"CH25 \t");}
               if (section==2){ printf("CH26  \t");fprintf(fp400,"CH26 \t");}
               if (section==3){ printf("CH27  \t");fprintf(fp400,"CH27 \t");}
               if (section==4){ printf("CH28  \t");fprintf(fp400,"CH28 \t");}
               if (section==5){ printf("CH29  \t");fprintf(fp400,"CH29 \t");}
               if (section==6){ printf("CH30  \t");fprintf(fp400,"CH30 \t");}
               if (section==7){ printf("CH31  \t");fprintf(fp400,"CH31 \t");}
               if (section==8){ printf("CH32  \t");fprintf(fp400,"CH32 \t");}
               if (section==9){ printf("CH33  \t");fprintf(fp400,"CH33 \t");}
               if (section==10){ printf("CH34 \t");fprintf(fp400,"CH34 \t");}
               if (section==11){printf("CH35 \t");fprintf(fp400,"CH35 \t");}
					printf ("%10d\t  %10d\n",section,ocmValues[0]);
                    fprintf (fp400,"%10d\t  %10d\n",section,ocmValues[0]);
					Sleep(500);
                    for(freqency=0; freqency<5000; freqency=freqency+1000)
			        {
			        Beep(freqency,100);
			        }

				if (kbhit ()) {
					end400 = 0;
					fclose (fp400);
				
					printf ("kbit!\n");
				}
				}	
			 } // for (3)
//---------------------------------------------------------------------------------------------------------------------

  //              if ((status = zasilacz.Write_C(handle, elmbNb, 0xF0)) < 0) { // // F-F nie jes zerowany, INH odblokowany
                if ((status = zasilacz.Write_C(handle, elmbNb, 0x00)) < 0) {  // OCMclear and OCM enable
			 
   //            if ((status = zasilacz.Write_C(handle, elmbNb, 0x00)) < 0) {  // zerujemy FLIP-FLOP
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				}
				


//				Sleep (500);
//-------------------------------------------------------------------------------------------------------------------------
//				portC = 0xF0;

				if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
//	            if ((status = zasilacz.Write_C(handle, elmbNb, 0xA0)) < 0) {
					printf ("Write_C exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}

			
				if (kbhit ()) {
					end400 = 0;
					fclose (fp400);
				
					printf ("kbit!\n");
				}

//				end400--;
			}  // koniec While  !!!

/*
				portC = 0x0;
				if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
					printf ("Write_C exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
*/
			printf ("oto licznik: %d\n", licznik);
                 for(freqency=0; freqency<20000; freqency=freqency+1000)
			      {
			      Beep(freqency,100);
			      }
			fclose (fp400);
            if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 0, elmbTimeOut)) < 0)
			{
				printf ("%s\n", zasilacz.GetErrorText ());
					
					
			} 
   

			break;

	case 449:	// eb
		printf ("Switch all regulators ON - Wheel B");
		if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 1, elmbTimeOut)) < 0) {
			printf ("%s\n", zasilacz.GetErrorText ());
		}
		if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, 5, 3, 1, elmbTimeOut)) < 0) {
			printf ("Set_INHIBIT exit with error: %d\n", status);
			printf ("%s\n", zasilacz.GetErrorText ());
		}
		if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, 6, 3, 1, elmbTimeOut)) < 0) {
			printf ("Set_INHIBIT exit with error: %d\n", status);
			printf ("%s\n", zasilacz.GetErrorText ());
		}
		if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, 13, 3, 1, elmbTimeOut)) < 0) {
			printf ("Set_INHIBIT exit with error: %d\n", status);
			printf ("%s\n", zasilacz.GetErrorText ());
		}
		if ((status = zasilacz.Set_INHIBIT (handle, elmbNb, 14, 3, 1, elmbTimeOut)) < 0) {
			printf ("Set_INHIBIT exit with error: %d\n", status);
			printf ("%s\n", zasilacz.GetErrorText ());
		}
		break;
		
	case 450:	//EB - test int KlasaI::Read_OCM_All_WheelB(int handle, int elmbAddress, int portC, int *ocmValues)

//			 portC = 0xA0;
             if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			printf("Test of Over Current Monitor \n");

			licznik = 36;
			printf ("Licznik przed wejsciem do while %d\n", licznik);
            while(licznik>0){
				Sleep (100);
				if ((status = zasilacz.Read_OCM_All_WheelB(handle, elmbNb, portC, ocmValues)) < 0) {
					printf ("Read_OCM_All_WheelB exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				if (ocmValues[0] || ocmValues[1] || ocmValues[2]) {
					for(freqency=000; freqency<5000; freqency=freqency+1000) {
						Beep(freqency,100);
					} 
					printf ("DIGITAL 0x%X \t POSITIVE 0x%X \t NEGATIVE 0x%X \n",
					      ocmValues[0], ocmValues[1], ocmValues[2]);

	/*				if ((status = zasilacz.Write_C(handle, elmbNb, 0xA0)) < 0) {
						printf ("Write_C exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					}

					if ((status = zasilacz.Write_C(handle, elmbNb, 0xA0)) < 0) {
						printf ("Write_C exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					} */
				}
				//licznik--;
				if (kbhit()) licznik = -1;
			}
			printf ("halo hej!\n");

	/*		portC = 0x0;
			if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}

            if ((status = zasilacz.SetInhBrdc (handle, elmbNb, portC, 0, elmbTimeOut)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());				
			} 
*/
			break;
			
//*********************************Reading pedestals***************************************




		case 500:
			int first_ped, last_ped, step_ped;
//			char fname[40];
//			int dacValue;
			FILE *pedFile, *logpedFile;
/*
			printf ("First value  - ");
			scanf ("%d", &first);

			printf ("Last value  - ");
			scanf ("%d", &last);

			printf ("step  - ");
			scanf ("%d", &step);


			printf ("File name  - ");
			scanf ("%s", fname);
		
			if (!(pFile=fopen(fname,"w"))) {
				printf ("File %s was not created.\n", fname);
                break;
				}
*/
            first_ped=0;
			last_ped=250;
			step_ped=50;

			// create directory (if does not exist)

		printf ("Card Serial Number: ");
		scanf ("%d", &cardNb);

		// create directory
		sprintf (textBuf, "C:\\LVPP_TESTS\\pedestal\\card_%03d", cardNb);

		if ((_mkdir (textBuf))) { 
			if (errno != 17) { 
				printf ("directory not created : %s\n", strerror (errno )); 
			} 
		} 
			// time and date
		_ftime( &timebuffer );
		newtime  = localtime( &(timebuffer.time) ); 
		// data file
		sprintf (textBuf, "C:\\LVPP_TESTS\\pedestal\\card_%03d\\LVPP_WA_%02d%02dH%02d%02d.txt",
			cardNb, newtime->tm_mday, newtime->tm_mon+1,
			newtime->tm_hour, newtime->tm_min);
		if (!(pedFile =fopen(textBuf,"w"))) {
			printf ("File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
			break;
		}

		// log file
		sprintf (textBuf, "C:\\LVPP_TESTS\\pedestal\\card_%03d\\LVPP_WA_LOG_%02d%02dH%02d%02d.txt",
			cardNb, newtime->tm_mday, newtime->tm_mon+1, 
			newtime->tm_hour, newtime->tm_min);

		if (!(logpedFile =fopen(textBuf,"w"))) {
			printf ("File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
			break;
		}

				
		fprintf (logpedFile, "Basic test (100) started: %02d%02d%4d %02d:%02d:%02d\n",
			newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900,
			newtime->tm_hour, newtime->tm_min, newtime->tm_sec);

//			fprintf(pFile,"ChannelNumber \t ConvStatus \t  RealValue \n");
            fprintf(pedFile,"ChaNum \t ConvSt \t  RVal[V] \n");
//            fprintf(pFile,"ChannelNumber   RealValue\n");

			// initialisation

			printf ("CAN Bus init:\n");
			if ((status = zasilacz.Init_CANBus(channel, &handle)) < 0) {
				printf ("Init_CANBus exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logpedFile, "Init_CANBus exit with error: %d\n", status);
				fprintf (logpedFile, "%s\n", zasilacz.GetErrorText ());
				fclose (pedFile); fclose (logpedFile);
				break;
			}
			else {
				printf ("Initializing of CANBUS OK\n");
				fprintf (logpedFile, "Initializing of CANBUS OK\n");
			}

			printf ("ELMB Init:\n");
			if ((status = zasilacz.Init_ELMB(handle, elmbNb)) < 0) {
				printf ("Init_ELMB exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logpedFile, "Init_ELMB exit with error: %d\n", status);
				fprintf (logpedFile, "%s\n", zasilacz.GetErrorText ());
				fclose (pedFile); fclose (logpedFile);
				break;
			}
			else {
				printf ("Initializing of Init_ELMB OK\n");
				fprintf (logpedFile, "Initializing of Init_ELMB OK, elmb address: %d\n", elmbNb);
			}

			printf ("DTMROC Init:\n");
			if ((status = zasilacz.Init_DTMROC(handle, elmbNb, elmbTimeOut, dtmrocs)) < 0) {
				printf ("Init_DTMROC exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logpedFile, "Init_DTMROC exit with error: %d\n", status);
				fprintf (logpedFile, "%s\n", zasilacz.GetErrorText ());
				fclose (pedFile); fclose (logpedFile);
				break;
			}
			else {
				printf ("Initializing of DTMROCS OK\n");
				printf ("Initializing of DTMROCS OK\n");
				fprintf (logpedFile, "DTMROC-s: %s\n", dtmrocs);
				fprintf (logpedFile, "DTMROC-s: %s\n", dtmrocs);
			}
//*************Read all ADCs when regulators are OFF****************************************************
			printf ("Read all ADCs when regulators are OFF\n");
            fprintf(pedFile,"Read all ADCs when regulators are OFF \n");
            fprintf(logpedFile,"Read all ADCs when regulators are OFF \n");
			if (readElmbADC (handle, elmbNb, adcValues, adcConvStat, 
				             CurrentFactor, VoltageFactor, pFile, logFile) == -1) {
				fclose (pedFile);
				break;
			}
//****************Read all ADCs when regulators are ON****************************************************
			printf ("Switch all regulators ON\n");
            
			if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 3, elmbTimeOut)) < 0)
			{
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logpedFile, "%s\n", zasilacz.GetErrorText ());
				fclose (pedFile); fclose (logpedFile);
				break;
			}
			
			// check
			if ((status = zasilacz.GetAllInhibitsWheelA (handle, elmbNb, elmbTimeOut, &valueRead)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logpedFile, "%s\n", zasilacz.GetErrorText ());
				fclose (pedFile);fclose(logpedFile);
				break;
			}

			if (valueRead != 0xFFFFFF) {
				printf ("Not all regulators have been switched ON!: 0x%X\n", valueRead);
				fprintf (logpedFile, "Not all regulators have been switched ON!: 0x%X\n", valueRead);
				fclose (pedFile); fclose (logpedFile);
				break;
			}

			printf ("All regulators are switched ON\n");	
            fprintf(pedFile,"Read all ADCs when regulators are ON \n");
            fprintf(logpedFile,"Read all ADCs when regulators are ON \n");

			// loop
			for (dac = first_ped; dac<=last_ped; dac+=step_ped) 
			{
				printf ("Dac set to: %d \n", dac);
				fprintf (pedFile, "Dac set to: %d \n", dac);
				dacValue = (dac<<8) | dac; 
				printf ("0x%X 0x%X\n", dac, dacValue);
				if ((status = zasilacz.WriteDacBrdc(handle, elmbNb, 3, dacValue, elmbTimeOut)) < 0) { 
					printf ("WriteDacBrdc exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					fprintf (logpedFile, "WriteDacBrdc exit with error: %d\n", status);
					fprintf (logpedFile, "%s\n", zasilacz.GetErrorText ());
					fclose (pedFile); fclose (logpedFile);
					break;
			}

				// check DacBrdcCheck
				if ((status = zasilacz.DacBrdcCheck(handle, elmbNb, 3, dacValue, elmbTimeOut)) < 0) { 
					printf ("Error in setting DAC values:  %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());
					fprintf (logpedFile, "Error in setting DAC values:  %d\n", status);
					fprintf (logpedFile, "%s\n", zasilacz.GetErrorText ());
				}

				if (readElmbADC (handle, elmbNb, adcValues, adcConvStat, 
					CurrentFactor, VoltageFactor, pFile, logFile) == -1) {
				 fclose (pedFile); fclose (logpedFile);
				 break;
				}
			}

		 fclose (pedFile);

            if ((status = zasilacz.CloseCan (handle)) < 0) {
				printf ("CloseCan exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logpedFile, "CloseCan exit with error: %d\n", status);
				fprintf (logpedFile, "%s\n", zasilacz.GetErrorText ());
			}
			else {
				printf ("Closing CANBus OK\n");
				fprintf (logpedFile, "Closing CANBus OK\n");
			}
        
		 printf (" KONIEC!!!\n");
		 fprintf (logpedFile, " KONIEC!!!\n");
		fclose (logpedFile);

		break;

// ========================================================================================= //
//   To jest 80 z sumowaniem pradow
// ======================================================================================== //
		case 800:
			printf ("80-tka z sumowaniem pradow \n");
			
//            FILE *fp80;
			fp80 = fopen ("IoutVout_80.txt","w");
			/*
            printf ("Voltage Regulators 0utputs [uV] for  DAC = 0xFFFF (255) \n");
            fprintf (fp80,"Voltage Regulators 0utputs [uV] for  DAC = 0xFFFF (255) \n");

            if ((status = zasilacz.WriteDacBrdc(handle, elmbNb,3, 0xFFFF, portC, elmbTimeOut)) < 0) { 
					printf ("WriteDacBrdc exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());		
				} 

           */
			printf ("Card Serial Number: ");
			scanf ("%d", &cardNb);

			// create calibfilename
			sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\LVPP_CALIB_%03d.csv", cardNb, cardNb);
			
			if ((calibFile = fopen (textBuf, "r")) == NULL) {
				printf ("Calibration file does not exist for card %d - please make calibration\n", cardNb); 
				break;
			}

			readCalibration(calibFile, calibCoeff, calibCurrentOffset, 36);
			
			fclose (calibFile);

			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {

				printf ("---------------------------------------------\n");
				fprintf (fp80, "----------------------------------------\n");
				fprintf (fp80, "Iout without calibration\n");
				printf ("Iout without calibration\n");

				for (i=0; i<36; i++) {
					fprintf (fp80, " %d\t %f\t \n",i, 
						adcValues[i]*CurrentFactor);
					printf ("%d\t %d\t %f\n", i, adcConvStat[i], 
					adcValues[i]*CurrentFactor);
				}

				printf ("---------------------------------------------\n");
				fprintf (fp80, "----------------------------------------\n");
				fprintf (fp80, "Iout calibrated with calibCoeff=adcValue/Vout\n");
				printf ("Iout calibrated with calibCoeff=adcValue/Vout\n");

//for (i=0; i<36; i++) {				
for (i=0; i<36; i=i+2) {     //sumowanie pradow
		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*CurrentFactor);
//			fprintf (fp, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*CurrentFactor);
//            fprintf (fp80, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*CurrentFactor);
              fprintf (fp80, " %d\t %f\t \n",i, 
			  (adcValues[i]*CurrentFactor - (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000)
              +(adcValues[i+1]*CurrentFactor - (calibCoeff[i+1]*adcValues[returnVoltageChannelWheelA(i+1)]*11)/10000));

              printf ("%d\t %d\t %f\n", i, adcConvStat[i], 
			  (adcValues[i]*CurrentFactor - (calibCoeff[i]*adcValues[returnVoltageChannelWheelA(i)]*11)/10000)
			  +(adcValues[i+1]*CurrentFactor - (calibCoeff[i+1]*adcValues[returnVoltageChannelWheelA(i+1)]*11)/10000));

    // sumowanie pradow
  //              printf (" %d\t %d\t %f\t %f\t %f\n",i,i+1,adcValues[i]*CurrentFactor,adcValues[i+1]*CurrentFactor,
	//		     (adcValues[i]*CurrentFactor)+ (adcValues[i+1]*CurrentFactor));
//			if(i % 2==0){printf ("i+(i+1)= %d\t %d\t %f\n", i, adcConvStat[i],(adcValues[i]*CurrentFactor+adcValues[i+1]*CurrentFactor));}

}
         printf ("---------------------------------------------\n");
		 fprintf (fp80, "----------------------------------------\n");  //Current offset for DAC = 128 counts
		fprintf (fp80, "Current offset for DAC = 128 counts\n");
		printf ("Current offset for DAC = 128 counts\n");
/*
		    for (i=0; i<36; i++) {
            fprintf (fp80, " %d\t %f\t \n",i, 
				     adcValues[i]*CurrentFactor - calibCurrentOffset[i]);
			printf ("%d\t %d\t %f\n", i, adcConvStat[i], 
				    adcValues[i]*CurrentFactor - calibCurrentOffset[i]);}
*/
		
		for (i=0; i<36; i=i+2){
		    fprintf (fp80, " %d\t %f\t \n",i, 
			(adcValues[i]*CurrentFactor - calibCurrentOffset[i])+(adcValues[i+1]*CurrentFactor - calibCurrentOffset[i+1]));
			printf ("%d\t %d\t %f\n", i, adcConvStat[i], 
				(adcValues[i]*CurrentFactor - calibCurrentOffset[i])+(adcValues[i+1]*CurrentFactor - calibCurrentOffset[i+1]));}

			

         printf ("---------------------------------------------\n");
		 fprintf (fp80, "----------------------------------------\n");

for (i=36; i<60; i++) {
		//	printf ("%d. Conversion Status: %d \t %f\n", 
		//		i, adcConvStat[i], adcValues[i]*VoltageFactor);
//	fprintf (fp80, "%d. Conversion Status: %d \t %f\n",i, adcConvStat[i], adcValues[i]*VoltageFactor);
	
    fprintf (fp80, " %d \t %f\n",i, adcValues[i]*VoltageFactor);
      printf ("%d\t %d \t %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor);
 //    fprintf (fp, "%d\t %d\t %f\n",i,adcConvStat[i], adcValues[i]*VoltageFactor);       

		}
         printf ("---------------------------------------------\n");
		for (i=60; i<64; i++) {
		//	printf ("%d. Conversion Status: %d \t %d\n", 
		//		i, adcConvStat[i], adcValues[i]);
//			fprintf (fp, "%d. Conversion Status: %d \t %d\n",i, adcConvStat[i], adcValues[i]);
          fprintf (fp80, "%d\t %d\t %f\n",i,adcConvStat[i], temperature (adcValues[i]));
           printf ("%d\t %d \t %f\n", i, adcConvStat[i],temperature (adcValues[i]));
		}
			}
			
			fclose (fp80);

             for(freqency=0; freqency<5000; freqency=freqency+1000)
			      {
			      Beep(freqency,100);
				  }

			break;
//***********************Main Test********************************************************************
case 1000:
//			int first, last, step;
//			char reportname[40];
//			int dacValue;
			FILE *reportFile;
/*
			printf ("First value  - ");
			scanf ("%d", &first);

			printf ("Last value  - ");
			scanf ("%d", &last);

			printf ("step  - ");
			scanf ("%d", &step);


			printf ("File name  - ");
			scanf ("%s", fname);
		
			if (!(pFile=fopen(fname,"w"))) {
				printf ("File %s was not created.\n", fname);
                break;
				}
*/
             first=0;
			 last=250;
			 step=50;
             reportFile=fopen("LVPP_Report.txt","w");
			 
				
			
//			fprintf(pFile,"ChannelNumber \t ConvStatus \t  RealValue \n");
//            fprintf(reportFile,"ChaNum \t ConvSt \t  RVal[V] \n");
//            fprintf(pFile,"ChannelNumber   RealValue\n");

			// initialisation
//-------------------------------------------------------------------------------------------
			printf ("CAN Bus init:\n");
			if ((status = zasilacz.Init_CANBus(channel, &handle)) < 0) {
				printf ("Init_CANBus exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fclose (reportFile);
				break;
			}
			else 
				printf ("Initializing of CANBUS OK\n");
                fprintf(reportFile, "Initializing of CANBUS OK\n");
//-----------------------------------------------------------------------------------------
			printf ("ELMB Init:\n");
			if ((status = zasilacz.Init_ELMB(handle, elmbNb)) < 0) {
				printf ("Init_ELMB exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fclose (pFile);
				break;
			}
			else 
				printf ("Initializing of Init_ELMB OK\n");
                fprintf( reportFile,"Initializing of Init_ELMB OK\n");
//----------------------------------------------------------------------------------------------
			printf ("DTMROC Init:\n");
			if ((status = zasilacz.Init_DTMROC(handle, elmbNb, elmbTimeOut, dtmrocs)) < 0) {
				printf ("Init_DTMROC exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fclose (pFile);
				break;
			}
			else {
				printf ("Initializing of DTMROCS OK\n");
                fprintf( reportFile,"Initializing of DTMROCS OK\n");
				printf ("DTMROC-s: %s\n", dtmrocs);
			}
//-----------------------------Inhibit Test--------------------------------------

//bk			int onOff; 
//			printf ("Read all inhibits one by one in a loop\n");
//bk			printf ("Loop: Set ON / OFF with broadcast and then check one by one\n");
//bk			printf ("Both ON - 3, first ON - 1, sec. ON - 2, both OFF - 0: - "); scanf ("%d", &onOff);
			    printf ("Inhibits Test: Inhibit pattern=3");
			    onOff=3;
/*			if ((status = zasilacz.SetInhBrdc (handle, elmbNb, portC, onOff, elmbTimeOut)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			else {
				if (dacNb)
					printf ("All regulators should be switched on.\n");
				else 
					printf ("All regulators should be switched off.\n");
			}
*/
//			regulator = 3;
			int endtest;
			end17 = 1;
			dacNb = 0;
//bk			while (end17) {
			    for (endtest=1; endtest<=10; endtest++){
				if ((status = zasilacz.SetInhBrdc (handle, elmbNb, onOff, elmbTimeOut)) < 0) {
					printf ("%s\n", zasilacz.GetErrorText ());
				}
				for (dtmroc=1; dtmroc<=14; dtmroc++) 
				{
					if ((dtmroc==7) || (dtmroc==8)) continue;
					if ((status = zasilacz.Get_INHIBIT (handle, elmbNb, dtmroc, elmbTimeOut, &dacRead)) < 0) 
					{
						printf ("Get_INHIBIT exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					}
 //               printf ("dacRead: %x\n", dacRead);   //bk
                
					switch (onOff)  
					{
						case 3:
							if (dacRead != 3) 
								printf ("Inhibit bad set: dtmroc %d R %d W %d\n", dtmroc, dacRead, onOff);
							break;
						case 2:
							if (dacRead != 2) 
								printf ("Inhibit bad set: dtmroc %d R %d W %d\n", dtmroc, dacRead, onOff);
							break;
						case 1:
							if (dacRead != 1) 
								printf ("Inhibit bad set: dtmroc %d R %d W %d\n", dtmroc, dacRead, onOff);
							break;
						case 0:
							if (dacRead != 0) 
								printf ("Inhibit bad set: dtmroc %d R %d W %d\n", dtmroc, dacRead, onOff);
							break;
						default:
							printf ("bad inhibit value: %d\n", onOff);
					}
//                 printf ("dtmroc: %d\n", dtmroc);
                   printf (".");
				}
  //             printf ("Loop Test: %d\n", endtest);
                 printf ("*\n");
			}
//------------------------------------------------------------------------------
            printf ("Inhibits Test Done\n");
            fprintf( reportFile,"Test done!!!\n");
		    fclose (reportFile);
            if ((status = zasilacz.CloseCan (handle)) < 0) {
				printf ("CloseCan exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			    }
			    else
				printf ("Closing CANBus OK\n");

			break;
		case 1002:
		   while (!kbhit()) {
		   portC = 0xF0;
			if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			
			if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 3, elmbTimeOut)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			for(freqency=000; freqency<5000; freqency=freqency+1000)
			{
			Beep(freqency,100);
			} 

			Sleep (100);
			portC = 0;
			if ((status = zasilacz.Write_C(handle, elmbNb, portC)) < 0) {
				printf ("Write_C exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			Sleep (5000);

			if ((status = zasilacz.SetInhBrdc (handle, elmbNb, 0, elmbTimeOut)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			Sleep (5000);
		   }

		  break;	

//*************************************************************************
		case 2000:
#ifdef MULTI
			 for (i=0; i<24; i++) {
				if   (      
					(My_Send(address_sw, "INIT") & ERR) //send a clear
					||   (My_Send(address_sw, "TRIG:IMM") & ERR)  //7001 SWITCH
			  	  
			//	    || (My_Send(address_sw, "*CLS") & ERR) //send a clear
					)
					gpib_error(5, "Problem sending TRIGGER");
				Sleep(500);
				keithleyMeasur = read2000MUL (gpibBuffer, 80, address, address_sw);
				printf ("Nasz pomiar: %f\n", keithleyMeasur);
				//fprintf (fp90, "%f\n",atof(tmp));
				Sleep (500);
			}
#endif
			break;
//***************************************************************************
 		case 2090:
#ifdef MULTI
			printf ("Read ELMB + KEITHLEY\n");
			
            FILE *fp2090;
			printf ("Card Serial Number: ");
			scanf ("%d", &cardNb);

			// create directory
			sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d", cardNb);

			if ((_mkdir (textBuf))) { 
				if (errno != 17) { 
					printf ("directory not created : %s\n", strerror (errno )); 
				}	 
			} 
			sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\IoutVout_keithley.txt", cardNb);

			fp2090 = fopen (textBuf,"w");
			/*
            printf ("Voltage Regulators 0utputs [uV] for  DAC = 0xFFFF (255) \n");
            fprintf (fp80,"Voltage Regulators 0utputs [uV] for  DAC = 0xFFFF (255) \n");

            if ((status = zasilacz.WriteDacBrdc(handle, elmbNb,3, 0xFFFF, portC, elmbTimeOut)) < 0) { 
					printf ("WriteDacBrdc exit with error: %d\n", status);
					printf ("%s\n", zasilacz.GetErrorText ());		
				} 

           */
			// read ELMB

			if ((status = zasilacz.ReadADC_uVolts (handle, elmbNb, adcValues, adcConvStat)) < 0) {
				printf ("%s\n", zasilacz.GetErrorText ());
				break;
			}
			// read KEITHLEY
			 for (i=0; i<24; i++) {
				if   (      
					(My_Send(address_sw, "INIT") & ERR) //send a clear
					||   (My_Send(address_sw, "TRIG:IMM") & ERR)  //7001 SWITCH
			  	  
			//	    || (My_Send(address_sw, "*CLS") & ERR) //send a clear
					)
					gpib_error(5, "Problem sending TRIGGER");
				Sleep(100);
				keithleyBuffer[i] = read2000MUL (gpibBuffer, 80, address, address_sw);
				Sleep (100);
			}

			for (i=0; i<24; i++)
				printf ("%d. %f\n", i, keithleyBuffer[i]);

			printf ("Analog Positive Total [A]: %f\n",( 
				             adcValues[0]*CurrentFactor+
				             adcValues[2]*CurrentFactor+
                             adcValues[4]*CurrentFactor+ 
                             adcValues[6]*CurrentFactor+
                             adcValues[8]*CurrentFactor+
                             adcValues[9]*CurrentFactor+
                             adcValues[10]*CurrentFactor+
                             adcValues[11]*CurrentFactor)
				   );
             printf ("---------------------------------------------\n");

          for (i=0; i<8; i=i+2) {
            fprintf (fp2090, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
           }
          for (i=8; i<12; i=i+1) {
            fprintf (fp2090, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
           }
           printf ("\n");
           printf ("Analog Negative Total [A]: %f\n",(
			                 adcValues[12]*CurrentFactor+
				             adcValues[14]*CurrentFactor+
                             adcValues[16]*CurrentFactor+ 
                             adcValues[18]*CurrentFactor+
                             adcValues[20]*CurrentFactor+
                             adcValues[21]*CurrentFactor+
                             adcValues[22]*CurrentFactor+
                             adcValues[23]*CurrentFactor)
				   );
          printf ("---------------------------------------------\n");

            for (i=12; i<20; i=i+2) {
            fprintf (fp2090, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
           }

            for (i=20; i<24; i=i+1) {
            fprintf (fp2090, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f \n ", i, adcConvStat[i], adcValues[i]*CurrentFactor);
           }
            printf ("\n");
           printf ("Digital Positive Total [A]: %f\n",(
			                 adcValues[24]*CurrentFactor+
				             adcValues[26]*CurrentFactor+
                             adcValues[28]*CurrentFactor+ 
                             adcValues[30]*CurrentFactor+
                             adcValues[32]*CurrentFactor+
                             adcValues[33]*CurrentFactor+
                             adcValues[34]*CurrentFactor+
                             adcValues[35]*CurrentFactor)
				   );
         printf ("---------------------------------------------\n");

  		   for (i=24; i<32; i=i+2) {
            fprintf (fp2090, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
           }
            for (i=32; i<36; i=i+1) {
            fprintf (fp2090, " %d\t %f\t \n",i, adcValues[i]*CurrentFactor);
            printf ("%d\t %d\t %f\n", i, adcConvStat[i], adcValues[i]*CurrentFactor);
           }
           printf ("\n");
//----------------------------------------------------------------------------------------------------------
         printf ("Outputs [V]    ELMB    2000 Multimeter\n");
         printf ("---------------------------------------------\n");
 //     for (i=36; i<60; i++){
 //   fprintf (fp2090, " %d \t %f\n",i, adcValues[i]*VoltageFactor);
 //   printf ("%d\t %d \t %f %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor, keithleyBuffer[i-36]);
 //   		               }
    for (i=36; i<52; i++){
    fprintf (fp2090, " %d \t %f %f\n",i, adcValues[i]*VoltageFactor, keithleyBuffer[i-28]);
    printf ("%d\t %d \t %f %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor, keithleyBuffer[i-28]);
    		               }
    for (i=52; i<60; i++){
    fprintf (fp2090, " %d \t %f %f\n",i, adcValues[i]*VoltageFactor,keithleyBuffer[i-52]);
    printf ("%d\t %d \t %f %f\n", i, adcConvStat[i],adcValues[i]*VoltageFactor, keithleyBuffer[i-52]);
    		               }

         printf ("---------------------------------------------\n");

		for (i=60; i<64; i++) {
		//	printf ("%d. Conversion Status: %d \t %d\n", 
		//		i, adcConvStat[i], adcValues[i]);
//			fprintf (fp, "%d. Conversion Status: %d \t %d\n",i, adcConvStat[i], adcValues[i]);
          fprintf (fp2090, "%d\t %d\t %f\n",i,adcConvStat[i], temperature (adcValues[i]));
           printf ("%d\t %d \t %f\n", i, adcConvStat[i],temperature (adcValues[i]));
		}
			
			
			fclose (fp2090);
#endif
			break;
//------------------------------------------------------------------------------------------------

// calibration 
// ========================================================================//

			case 5000:
            printf ("=====================================================\n");			
			printf ("Calibration: card TYPE Wheel A\n");
            printf ("=====================================================\n");	
			portC=0;

				// Canbus initialization
/*			if ((handle = canbusOnlyInit (channel)) == -1) { //error in canbus
				printf ("Test end -> INIT with errors\n");
				break;
			} 
*/
			printf ("Card Serial Number: ");
			scanf ("%d", &cardNb);

			// create directory
			sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d", cardNb);

			if ((_mkdir (textBuf))) { 
				if (errno != 17) { 
					printf ("directory not created : %s\n", strerror (errno )); 
				}	 
			} 
	
			// open log file
			_ftime( &timebuffer ); newtime  = localtime( &(timebuffer.time) );
			sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\LVPP_CALIB_LOG_%02d%02dH%02d%02d.txt",
			cardNb, newtime->tm_mday, newtime->tm_mon+1,
			newtime->tm_hour, newtime->tm_min);

			if (!(logFile =fopen(textBuf,"w"))) {
				printf ("File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
				break;
			}
			printf ("Calibration log file name: '%s'\n", textBuf);
			// open calib file
			sprintf (textBuf, "C:\\LVPP_TESTS\\data\\card_%03d\\LVPP_CALIB_%03d.csv", cardNb, cardNb);

			if (!(calibFile =fopen(textBuf,"w"))) {
				printf ("File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
				fprintf (logFile, "File <%s> cannot be opened: %s\n", textBuf, strerror (errno));
				fclose (logFile);
				break;
			}
			_ftime( &timebuffer ); seconds = (short) timebuffer.time; newtime  = localtime( &(timebuffer.time) );
			srand (seconds );
            fprintf (logFile, "=====================================================\n");			
			fprintf (logFile, "Calibration: card TYPE Wheel A\n");
            fprintf (logFile, "=====================================================\n");			
			printf ("Calibration File Name: '%s'\n\n", textBuf); 
			fprintf (logFile, "Calibration File Name: %s\n\n", textBuf); 
			printf ("Calibration started: %02d.%02d.%4d %02d:%02d:%02d\n\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900, 
				newtime->tm_hour, newtime->tm_min,
				newtime->tm_sec); 
			fprintf (logFile, "Calibration started: %02d.%02d.%4d %02d:%02d:%02d\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900, 
				newtime->tm_hour, newtime->tm_min,
				newtime->tm_sec); 
			
			fprintf (calibFile, "Calibration done: %02d.%02d.%4d %02d:%02d:%02d\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900, 
				newtime->tm_hour, newtime->tm_min,
				newtime->tm_sec);

			// read ELMB serial number
			if ((status = zasilacz.Get_ELMB_SerialNumber (handle, elmbNb, elmbSerialNumber)) < 0) {
				printf ("Get_ELMB_SerialNumber exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
			}
			fprintf (logFile, "ELMB serial number: %s\n", elmbSerialNumber);
			fprintf (calibFile, "%s\n", elmbSerialNumber);

			fprintf (calibFile, "Current channel, calibration factor\n");
			
			// set all regulators ON

			if (checkInhibitsBroadcast (handle, elmbNb, portC,  3, elmbTimeOut, logFile) == false) {
// make calibration
				calibCurrentWheelA (handle, elmbNb, portC, elmbTimeOut, logFile, calibFile);
			}


			else {
				printf ("Errors when switching voltages on. Please check log file. Calibration not done\n");
			}
			if (checkInhibitsBroadcast (handle, elmbNb, portC,  0, elmbTimeOut, logFile) == true) 
				printf ("Problems with switching regulators OFF\n");

/*			if ((status = zasilacz.CloseCan (handle)) < 0) {
				printf ("CloseCan exit with error: %d\n", status);
				printf ("%s\n", zasilacz.GetErrorText ());
				fprintf (logFile, "CloseCan exit with error: %d\n", status);
				fprintf (logFile, "%s\n", zasilacz.GetErrorText ());
			} */

			_ftime( &timebuffer ); seconds = (short) timebuffer.time; newtime  = localtime( &(timebuffer.time) );
			printf ("Calibration finished: %02d.%02d.%4d %02d:%02d:%02d\n\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900, 
				newtime->tm_hour, newtime->tm_min,
				newtime->tm_sec); 
			fprintf (logFile, "Calibration started: %02d.%02d.%4d %02d:%02d:%02d\n\n",
				newtime->tm_mday, newtime->tm_mon+1, newtime->tm_year+1900, 
				newtime->tm_hour, newtime->tm_min,
				newtime->tm_sec); 

			fclose (calibFile);
			fclose (logFile);

			break;

			case 1086:
				int  buforekOut[18];
				int iStatus;
				printf ("Checks in the pit\n");
				// Canbus initialization
/*
				buforek[0] = 0x32C1;
				buforek[1] = 0x32C1;
				buforek[2] = 0x32C2;
				buforek[3] = 0x32C2;
				buforek[4] = 0x32C3;
				buforek[5] = 0x32C3;
				buforek[6] = 0x32C4;
				buforek[7] = 0x32C4;
				buforek[8] = 0x32C5; 
				buforek[9] = 0x32C5;  
				buforek[10] = 0x32C6;  
				buforek[11] = 0x32C6;
				buforek[12] = 0xc8b1; 
				buforek[13] = 0xc8b2;  
				buforek[14] = 0xc8b3;  
				buforek[15] = 0xc8b4;
				buforek[16] = 0xc8b5; 
				buforek[17] = 0xc8b6; 
*/
			    buforek[0] = 0x001F;
				buforek[1] = 0x0000;
				buforek[2] = 0x003F;
				buforek[3] = 0x0000;
				buforek[4] = 0x005F;
				buforek[5] = 0x0000;
				buforek[6] = 0x007F;
				buforek[7] = 0x0000;
				buforek[8] = 0x009F; 
				buforek[9] = 0x00BF;  
				buforek[10] = 0x00DF;  
				buforek[11] = 0x00FF;
				buforek[12] = 0xc8b1; 
				buforek[13] = 0xc8b2;  
				buforek[14] = 0xc8b3;  
				buforek[15] = 0xc8b4;
				buforek[16] = 0xc8b5; 
				buforek[17] = 0xc8b6; 
				//for (i=0; i<18; i++) buforek[i] = 0x9090;

	/*			if ((handle = canbusOnlyInit (channel)) == -1) { //error in canbus
						printf ("Test end -> INIT with errors\n");
						break;
					}
*/					// set data

				do {
					iStatus = 0;
					printf ("Write to DACs\n");
					if ((status = zasilacz.WriteAllDacsWheelA (handle, elmbNb, elmbTimeOut, buforek)) < 0) {
						printf ("WriteAllDacs exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					}

					// read back data
					if ((status = zasilacz.ReadAllDacsWheelA (handle, elmbNb, elmbTimeOut, buforekOut)) < 0) {
						printf ("ReadAllDacsWheelB exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					}

					for (i=0; i<18; i++) {
						if (buforek[i] != buforekOut[i]) {
								printf ("Written: 0x%4X  read 0x%4X\n",
										buforek[i], buforekOut[i]);
								iStatus = 1;
						}
					}
					printf ("Write to DACs iStatus %d\n", iStatus);
				} while (iStatus);

				// barrel on
				checkInhibitsBroadcast (handle, elmbNb, portC,  3, elmbTimeOut, NULL);

				printf ("All regulators are ON\n");

					// loop - read dacs, check dtmrocs, set dacs, read dacs, read temperature
				j = 1;
				while (j) {
					if ((status = zasilacz.ReadAllDacsWheelA (handle, elmbNb, elmbTimeOut, buforekOut)) < 0) {
						printf ("ReadAllDacsWheelB exit with error: %d\n", status);
						printf ("%s\n", zasilacz.GetErrorText ());
					}
					
					for (i=0; i<18; i++) {
						if (buforek[i] != buforekOut[i]) printf ("Written: 0x%4X  read 0x%4X\n",
							buforek[i], buforekOut[i]);
					}
					for (i=0; i<12; i++) {
						if ((status = zasilacz.Read_SPI (handle, elmbNb, dtmrocAddress(i), 4, 
								elmbTimeOut, &dtmrocStat)) < 0) { // status
							printf ("Read exit with error: %d\n", status);
							printf ("%s\n", zasilacz.GetErrorText ());
						}
						if ((dtmrocStat&0xF) != dtmrocAddress(i)) {
							printf ("ERROR: DTMROC status is: %d should be %d\n", dtmrocStat&0x7, 
								dtmrocAddress(i));

						}
					}
                    printf (" %d ", j++);
					
				}

			break;

			default:
			printf ("Bad option: %d\n", item);
			break;

		} // end of switch

	} // end of while

	// complete GPIB access
	//We're done with the program, so take the board off-line

#ifdef MULTI
    ibonl(0,0); 
#endif

	printf ("Bye bye  \n");

	return 0;
}

