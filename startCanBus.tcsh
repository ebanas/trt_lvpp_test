echo "Starting CAN bus ports for SYSTEC"
sudo ip link set can0 type can bitrate 50000
sudo ifconfig can0 up
echo "Port can0 speed 50k -> started"
sudo ip link set can1 type can bitrate 50000
sudo ifconfig can1 up
echo "Port can1 speed 50k -> started"
